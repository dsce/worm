function getClickTasks(para, callback) {
    var API = new Api();
    API.getClickTasks(para, function (data) {
        if (data.success == 1) {
            setHostStatus(1202000);
            var tasks = {};
            var _account_id = '';
            var _task_id = '';
            data.data.task_list.forEach(function (p1, p2, p3) {
                _account_id = p1.detail.account_id;
                _task_id = p1.task_id;
                tasks = TC.checkTaskType(p1.task_type, p1.task_id, p1.detail, tasks)
            })
            setSharedTaskWorks({hostMessageId:_task_id})
            send_trackerInfo_to_tracker({
                "id":_task_id
            });
            setLocal({
                tasks: tasks,
                accountId: _account_id
            }, callback)
            send_trackerInfo_to_tracker({
                "account_id":_account_id
            });
        } else {
            console.log('no click task', data.message);
            notify('暂无点点任务 ', 10);
            setHostStatus(1202004);
            callback && callback();
        }
    }, function (err) {
        console.error('获取点点任务失败');
        console.error(err);
        callback && callback();
    })
}

function getReceiptTask(callback) {
    var API = new Api();
    API.getReceiptTasks(function (data) {
        console.log('收评任务数据',data);
        if (data.success == 1) {
            var tasks = {};
            var _account_id = '';
            var _task_id='';
            data.data.task_list.forEach(function (p1, p2, p3) {
                _account_id = p1.detail.account_id;
                _task_id = p1.detail.order_id;
                tasks = TC.checkTaskType(p1.task_type, p1.task_id, p1.detail, tasks)
                if(p1.task_type == 'share'){
                    setSharedTaskWorks({shareExist:true,shareTaskDetail:p1.detail})
                }
            })
            setSharedTaskWorks({hostMessageId:_task_id})
            send_trackerInfo_to_tracker({
                "id":_task_id
            });
            setLocal({
                tasks: tasks,
                accountId: _account_id
            }, callback)
            send_trackerInfo_to_tracker({
                "account_id":_account_id
            });
        } else {
            console.log('no receipt task', data.message);
            notify('暂无收评任务 ', 10);
            statusReport('1202003', 60, function () {
                notify('准备重新获取任务');
                setTimeout(callback,50e3);
            });
            //callback && callback();
        }
    }, function (err) {
        console.error('获取收评任务失败');
        console.error(err);
        callback && callback();
    })
}

/**
 * 获取xss任务开始
 * @param callback
 */
function getXssTask(callback) {
    //console.log(callback);
    var xss = {};
    var tasks = {};
    var API = new Api();

    //领单计时
    var get_task_start_time = new Date().getTime();

    //领单
    //setHostStatus(1201001);
    API.getXssTaskOrder(function (taskOrderData) {
        //领单计时
        var get_task_end_time = new Date().getTime();
        var get_task_use_time = get_task_end_time - get_task_start_time;
        notify("自动领单耗时：" + get_task_use_time + "毫秒");

        if (taskOrderData.success == 1) {
            //订单是否异常
            if (taskOrderData.data.is_exception == 1) {
                notify('这是异常订单：' + taskOrderData.data.oid);
                setLocal({
                    host_status: 1202000
                }, function () {
                    callback();
                });

            } else {
                setSharedTaskWorks({hostMessageId:taskOrderData.data.id})
                send_trackerInfo_to_tracker({
                    "id":taskOrderData.data.id
                });
                notify('领单成功：' + taskOrderData.data.oid);
                console.log('领单成功', taskOrderData.data);
                xss = taskOrderData.data;

                //分配账号
                //getXssTaskAssignment(xss, callback);

                //准备分配账号 计时
                var get_task_account_start_time = new Date().getTime();
                //setHostStatus(1201002);//降低频率，带宽超出
                API.getXssTaskAssignment({
                        order_id: xss.id
                    },
                    function (taskAccountData) {
                        var get_task_account_end_time = new Date().getTime();
                        var get_task_account_use_time = get_task_account_end_time - get_task_account_start_time;
                        notify("自动分配账号耗时：" + get_task_account_use_time + "毫秒");

                        if (taskAccountData.success == 1) {

                            if (taskAccountData.data.is_exception == 1) {
                                notify('分配账号发现是异常订单：' + taskOrderData.data.oid);
                                setLocal({
                                    host_status: 1202001
                                }, function () {
                                    callback();
                                });
                            } else {
                                notify('分配账号成功：订单' + taskAccountData.data.task_order_oid + '，账号' + taskAccountData.data.business_account_id);
                                xss = $.extend(xss, taskAccountData.data);

                                send_trackerInfo_to_tracker({
                                    "account_id":taskAccountData.data.business_account_id
                                });

                                //getXssTaskPromotionUrl(xss, callback);

                                //准备获取分配的推广链接
                                var get_task_promotion_url_start_time = new Date().getTime(); //计时
                                //setHostStatus(1201003);//降低频率，带宽超出
                                API.getXssTaskPromotionUrl({
                                        order_id: xss.id
                                    },
                                    function (taskPromotionUrlData) {

                                        var get_task_promotion_url_end_time = new Date().getTime();
                                        var get_task_promotion_url_use_time = get_task_promotion_url_end_time - get_task_promotion_url_start_time;
                                        notify("分配推广链接耗时：" + get_task_promotion_url_use_time + "毫秒");

                                        //分配推广链接成功
                                        if (taskPromotionUrlData.success == 1) {
                                            send_notes_to_tracker("分配推广链接成功");

                                            xss.promotion_url = taskPromotionUrlData.data.url;
                                            if (xss.promotion_url == '' || xss.promotion_url == "https://www.jd.com" || xss.promotion_url == "https://www.jd.com/") {
                                                //不使用推广链接
                                                xss.unpromotion = true;
                                                //默认推广链接
                                                xss.promotion_url = defaultPromotionUrls[xss.is_mobile][xss.worldwide];

                                            } else {
                                                //使用推广链接， 如果是全球购，替换跳转地址
                                                if (xss.worldwide == 1) { //全球购
                                                    if (xss.is_mobile == 1) { //手机端
                                                        //xss.promotion_url = 'http://m.jd.com/?cu=true&utm_source=p.egou.com&utm_medium=tuiguang&utm_campaign=t_36378_792893_disise201504';
                                                        xss.promotion_url = xss.promotion_url.replace('m.jd.com', 'm.jd.hk');
                                                    } else {
                                                        //xss.promotion_url = 'http://www.jd.hk/??cu=true&utm_source=p.egou.com&utm_medium=tuiguang&utm_campaign=t_36378_792893_disise201504';
                                                        xss.promotion_url = xss.promotion_url.replace('www.jd.com', 'www.jd.hk');
                                                    }
                                                }
                                            }

                                            //推广链接标志替换
                                            if (xss.promotion_url.indexOf('disise201504') != -1) {
                                                var promotionUrlCode = randomSerialNumber();
                                                xss.promotion_url = xss.promotion_url.replace('disise201504', promotionUrlCode);
                                                xss.promotionUrlCode = promotionUrlCode;
                                            }
                                            xss.start_at = xss.took_at;


                                            //获取任务列表 开始
                                            API.getXssTaskLists(xss.id, function (taskLists) {
                                                if (taskLists.success == 1) {

                                                    taskLists.data.task_list.forEach(function (detail) {
                                                        if (detail.task_type == 'submit_order') {
                                                            xss = $.extend(detail.detail, xss);
                                                            tasks = TC.checkTaskType(detail.task_type, detail.task_id, xss, tasks);
                                                            //兼容原xss任务
                                                            //tasks = TC.checkTaskType('xss', xss.id, xss, tasks);
                                                            setLocal({
                                                                task: xss,
                                                                accountId: xss.business_account_id
                                                            }); //兼容xss使用
                                                        } else {
                                                            tasks = TC.checkTaskType(detail.task_type, detail.task_id, detail.detail, tasks);
                                                        }
                                                    })

                                                    setLocal({
                                                        tasks: tasks
                                                    }, callback);
                                                    //兼容原xss任务
                                                    //tasks = TC.checkTaskType('xss', xss.id, xss, tasks);
                                                    //setLocal({task:xss},callback); //兼容xss使用

                                                } else {
                                                    console.log('xss taskLists fail', taskClankdata.message);
                                                    callback();
                                                }
                                            }, function (e) {
                                                console.error('获取xss任务列表失败', e);
                                                callback();
                                            })
                                            //获取任务列表 结束

                                            /**
                                             //保存xss任务
                                             tasks = TC.checkTaskType('xss', xss.id, xss, tasks);
                                             setLocal({task:xss}); //兼容xss使用

                                             tasks = TC.checkTaskType('payment', xss.id, xss, tasks);

                                             setLocal({tasks:tasks,accountId: xss.business_account_id}, function(){
                                                //获取点点任务
                                                API.getXssTaskClank(xss.id, function (taskClankdata) {
                                                    if(taskClankdata.success == 1){

                                                        //保存点点任务
                                                        taskClankdata.data.task_details.forEach(function (detail) {
                                                            tasks = TC.checkTaskType(detail.type, detail.id, detail, tasks);
                                                        })

                                                        setLocal({tasks:tasks}, callback);

                                                    }else{
                                                        console.log('no clank task', taskClankdata.message);
                                                        callback();
                                                    }
                                                },function (e) {
                                                    console.error('获取点点任务失败', e);
                                                    callback();
                                                })
                                            });
                                             **/



                                        } else {
                                            notify("分配推广链接：" + taskPromotionUrlData.message);
                                            callback();
                                        }
                                    },
                                    function (e) {
                                        console.error('分配推广链接失败', e);
                                        notify('分配推广链接失败');
                                        callback();
                                    }
                                );

                            }
                        } else {
                            //获取账号失败
                            notify("分配账号：" + taskAccountData.message);
                            callback();
                        }
                    },

                    function (e) {
                        console.error('分配账号失败', e);
                        notify('分配账号失败');
                        callback();
                    }
                );
            }


        } else {
            //获取领单失败
            console.log(taskOrderData.message);
            notify("领单： " + taskOrderData.message);
            if (taskOrderData.message == '插件版本号低,无法领单') {
                //版本过低，重新开始
                notify('插件版本号低,无法领单,10S重新开始');
                setTimeout(function () {
                    getTasksStaus = false;
                    useLocal(select_task_by_usage);
                }, 10000);
            } else {
                statusReport('1202002', 60, callback)
            }
        }

    }, function (e) {
        console.error('获取xss任务失败', e);
        notify('分配xss任务失败');
        callback();
    });

    //获取任务
    //获取帐号
    //获取推广链接
    //获取xss的点点任务
}

function getGroupXssTask(callback) {
    //console.log(callback);
    var xss = {};
    var tasks = {};
    var API = new Api();

    //领单计时
    var get_task_start_time = new Date().getTime();

    //领单
    setHostStatus(1201001);
    API.getXssGroupTaskOrder(function (taskOrderData) {
        //领单计时
        var get_task_end_time = new Date().getTime();
        var get_task_use_time = get_task_end_time - get_task_start_time;
        notes("自动领单耗时：" + get_task_use_time + "毫秒");

        if (taskOrderData.success == 1) {
            //订单是否异常
            if (taskOrderData.data.is_exception == 1) {
                notify('这是异常订单：' + taskOrderData.data.oid);
                setLocal({
                    host_status: 1202000
                }, function () {
                    callback();
                });

            } else {
                setSharedTaskWorks({hostMessageId:taskOrderData.data.id});
                send_trackerInfo_to_tracker({
                    "id":taskOrderData.data.id
                });
                notify('领单成功：' + taskOrderData.data.id);
                console.log('领单成功', taskOrderData.data);
                xss = taskOrderData.data;


                //分配账号
                //getXssTaskAssignment(xss, callback);

                //准备分配账号 计时
                var get_task_account_start_time = new Date().getTime();
                setHostStatus(1201002);
                API.getXssGroupTaskAssignment({
                        // order_id: xss.id
                        order_group_id : xss.id
                    },
                    function (taskAccountData) {
                        var get_task_account_end_time = new Date().getTime();
                        var get_task_account_use_time = get_task_account_end_time - get_task_account_start_time;
                        notes("自动分配账号耗时：" + get_task_account_use_time + "毫秒");

                        if (taskAccountData.success == 1) {

                            if (taskAccountData.data.is_exception == 1) {
                                notify('分配账号发现是异常订单：' + taskOrderData.data.oid);
                                setLocal({
                                    host_status: 1202001
                                }, function () {
                                    callback();
                                });
                            } else{
                                    notes('分配账号成功：订单包' + taskAccountData.data.id + '，账号' + taskAccountData.data.account_id);
                                    xss = $.extend(xss, taskAccountData.data);

                                    send_trackerInfo_to_tracker({
                                        "account_id":taskAccountData.data.account_id
                                    });

                                    setSharedTaskWorks({account_info:taskAccountData.data},function(){
                                        
                                    })

                                    //getXssTaskPromotionUrl(xss, callback);

                                    //准备获取分配的推广链接
                                    var get_task_promotion_url_start_time = new Date().getTime(); //计时
                                    setHostStatus(1201003);
                           
                               

                                    var get_task_promotion_url_end_time = new Date().getTime();
                                    var get_task_promotion_url_use_time = get_task_promotion_url_end_time - get_task_promotion_url_start_time;
                                    notes("分配推广链接耗时：" + get_task_promotion_url_use_time + "毫秒");

                                    //分配推广链接成功
                                    send_notes_to_tracker("分配推广链接成功");

                                    xss.promotion_url = taskAccountData.data.orders[0].promotion_url;
                                    if (!xss.promotion_url || xss.promotion_url == "https://www.jd.com" || xss.promotion_url == "https://www.jd.com/") {
                                        //不使用推广链接
                                        xss.unpromotion = true;
                                        //默认推广链接
                                        xss.promotion_url = defaultPromotionUrls[xss.is_mobile][xss.worldwide ? xss.worldwide : 0];

                                    } else {
                                        //使用推广链接， 如果是全球购，替换跳转地址
                                        if (xss.worldwide == 1) { //全球购

                                            if (xss.is_mobile == 1) { //手机端
                                                //xss.promotion_url = 'http://m.jd.com/?cu=true&utm_source=p.egou.com&utm_medium=tuiguang&utm_campaign=t_36378_792893_disise201504';
                                                xss.promotion_url = xss.promotion_url.replace('m.jd.com', 'm.jd.hk');
                                            } else {
                                                //xss.promotion_url = 'http://www.jd.hk/??cu=true&utm_source=p.egou.com&utm_medium=tuiguang&utm_campaign=t_36378_792893_disise201504';
                                                xss.promotion_url = xss.promotion_url.replace('www.jd.com', 'www.jd.hk');
                                            }
                                        }
                                    }

                                    //推广链接标志替换
                                    if (xss.promotion_url.indexOf('disise201504') != -1) {
                                        var promotionUrlCode = randomSerialNumber();
                                        xss.promotion_url = xss.promotion_url.replace('disise201504', promotionUrlCode);
                                        xss.promotionUrlCode = promotionUrlCode;
                                    }
                                    xss.start_at = xss.took_at;


                                    //获取任务列表 开始
                                    API.getXssGroupTaskLists(xss.id, function (taskLists) {
                                        if (taskLists.success == 1) {

                                            taskLists.data.task_list.forEach(function (detail) {
                                                if (detail.task_type.indexOf('submit_order') != -1) {
                                                    xss = $.extend(detail.detail, xss);
                                                    tasks = TC.checkTaskType(detail.task_type, detail.task_id, xss, tasks);
                                                    //兼容原xss任务
                                                    //tasks = TC.checkTaskType('xss', xss.id, xss, tasks);
                                                    if(xss.worldwide == 1){
                                                        xss.consignee.name = xss.identity_name;
                                                    }
                                                    setLocal({
                                                        task: xss,
                                                        accountId: xss.account_id
                                                    }); //兼容xss使用
                                                } else {
                                                    tasks = TC.checkTaskType(detail.task_type, detail.task_id, detail.detail, tasks);
                                                }
                                            })
                                            
                                            setSharedTaskWorks({is_group:true},function(){
                                                setLocal({
                                                    tasks: tasks
                                                }, callback);
                                            })
                                            
                                            //兼容原xss任务
                                            //tasks = TC.checkTaskType('xss', xss.id, xss, tasks);
                                            //setLocal({task:xss},callback); //兼容xss使用

                                        } else {
                                            console.log('xss taskLists fail', taskClankdata.message);
                                            callback();
                                        }
                                    }, function (e) {
                                        console.error('获取xss任务列表失败', e);
                                        callback();
                                    })
                            }

                            
                        } else {
                            //获取账号失败
                            notify("分配账号：" + taskAccountData.message);
                            callback();
                        }
                    },

                    function (e) {
                        console.error('分配账号失败', e);
                        notify('分配账号失败');
                        callback();
                    }
                );
            }
            
        }else{
            //获取领单失败
            console.log(taskOrderData.message);
            notify("领单： " + taskOrderData.message);
            if (taskOrderData.message == '插件版本号低,无法领单') {
                //版本过低，重新开始
                notify('插件版本号低,无法领单,10S重新开始');
                setTimeout(function () {
                    getTasksStaus = false;
                    useLocal(select_task_by_usage);
                }, 10000);
            } else {
                statusReport('1202002', 60, callback)
            }
        }

    }, function (e) {
        console.error('获取xss任务失败', e);
        notify('分配xss任务失败');
        callback();
    });

    //获取任务
    //获取帐号
    //获取推广链接
    //获取xss的点点任务
}

/**
 * 获取app任务
 */
function get_app_task() {
    var tasks = {};
    last_watchdog_time = new Date().getTime();
    watchdog_timeout_count = 0;
    console.log('get APP task');
    if (GetTaskStatus === true) {
        console.log('已经去过');
        return false;
    }
    GetTaskStatus = true;
    //app请求服务器任务报告去掉,影响当前app状态显示
    //setHostStatus(1201000);
    useLocal(function (local) {
        //领单计时
        var get_task_start_time = new Date().getTime();
        API = new Api();
        var params = {
            // "action": "get_app_pay_task_order",
            "host_id": local.host_id
            // "username": local.username,
            // "password": local.password
        };
        // API.setParams(params);
        // API.ajaxPOST(function (webTask) {
        API.getAppPayTask(params, function (webTask) {

            var t = new Date().getTime();
            if (app_checked_tracker == false || parseInt(t - app_task_check_tracker_start) > 60 * 10 * 1000) {
                update_check_to_tracker(); //检测app主机赤兔更新
                app_checked_tracker = true;
            }

            var get_task_end_time = new Date().getTime();
            var get_task_use_time = get_task_end_time - get_task_start_time;
            notify("自动领单（APP付款）耗时：" + get_task_use_time + "毫秒");
            if (webTask.success == 1 && webTask.data && webTask.data.business_account_id && webTask.data.task_id) {

                app_checked_tracker = false; //领单成功重置检测状态

                setHostStep(_host_step.payment); //主机步骤 直接到支付
                setHostStatus(1202000); //得到服务器任务
                //var task = webTask.data;
                var xss = webTask.data;

                setSharedTaskWorks({hostMessageType:'worm',hostMessageId:xss.id});
                send_trackerInfo_to_tracker({
                    "type":'worm',
                    "id":xss.id,
                    "account_id":xss.business_account_id
                });



                tasks = TC.checkTaskType('order_payment', 'app_' + xss.id, xss, tasks);
                setLocal({
                    task: xss
                }); //兼容xss使用

                setLocal({
                    tasks: tasks,
                    accountId: xss.business_account_id
                }, function () {
                    useLocal(function (local) {
                        //begin_app_task(local.task);
                        checkTasks();
                    });
                });

            } else {
                //请求服务器任务失败
                console.log(webTask.message);
                notify(webTask.message + "，10秒后重新领任务");
                if (webTask.message == '插件版本号低,无法领单') {
                    notify('插件版本号低,无法领单,10S重新开始');
                    setTimeout(startHost, 10000);
                } else {
                    setLocal({
                        host_status: 1202001
                    }, function () {
                        GetTaskStatus = false;
                        setTimeout(taskStart, 10000);
                    });
                }

            }
        }, function () {
            //错误，10秒后重新请求
            GetTaskStatus = false;
            setTimeout(taskStart, 10000);

        });

    });

}

/**
 * 获取app包付款任务
 */
function get_app_group_task() {
    var tasks = {};
    last_watchdog_time = new Date().getTime();
    watchdog_timeout_count = 0;
    console.log('get APP group task');
    if (GetTaskStatus === true) {
        console.log('已经去过');
        return false;
    }
    GetTaskStatus = true;
    //app请求服务器任务报告去掉,影响当前app状态显示
    //setHostStatus(1201000);
    useLocal(function (local) {
        //领单计时
        var get_task_start_time = new Date().getTime();
        API = new Api();
        var params = {
            // "action": "get_app_pay_task_order",
            "host_id": local.host_id
            // "username": local.username,
            // "password": local.password
        };
        // API.setParams(params);
        // API.ajaxPOST(function (webTask) {
        API.getAppGroupPayTask(params, function (webTask) {

            var t = new Date().getTime();
            if (app_checked_tracker == false || parseInt(t - app_task_check_tracker_start) > 60 * 10 * 1000) {
                update_check_to_tracker(); //检测app主机赤兔更新
                app_checked_tracker = true;
            }

            var get_task_end_time = new Date().getTime();
            var get_task_use_time = get_task_end_time - get_task_start_time;
            notify("自动领包（APP付款）耗时：" + get_task_use_time + "毫秒");
            if (webTask.success == 1 && webTask.data && webTask.data.business_account_id && webTask.data.id) {

                app_checked_tracker = false; //领单成功重置检测状态

                setHostStep(_host_step.payment); //主机步骤 直接到支付
                setHostStatus(1202000); //得到服务器任务
                //var task = webTask.data;
                var xss = webTask.data;

                setSharedTaskWorks({hostMessageType:'worm',hostMessageId:xss.id});
                send_trackerInfo_to_tracker({
                    "type":'worm',
                    "id":xss.id,
                    "account_id":xss.business_account_id
                });

                xss.promotion_url = null;

                setLocal({
                    task: xss
                }); //兼容xss使用
                
                tasks = TC.checkTaskType('group_order_payment', 'app_' + xss.id, xss, tasks);
                setLocal({
                    tasks: tasks,
                    accountId: xss.business_account_id
                }, function () {
                    useLocal(function (local) {
                        //begin_app_task(local.task);
                        setSharedTaskWorks({is_group:true},function(){
                            checkTasks();
                        })
                        
                    });
                });

            } else {
                //请求服务器任务失败
                console.log(webTask.message);
                notify(webTask.message + "，10秒后重新领任务");
                if (webTask.message == '插件版本号低,无法领单') {
                    notify('插件版本号低,无法领单,10S重新开始');
                    setTimeout(startHost, 10000);
                } else {
                    setLocal({
                        host_status: 1202001
                    }, function () {
                        GetTaskStatus = false;
                        setTimeout(taskStart, 10000);
                    });
                }

            }
        }, function () {
            //错误，10秒后重新请求
            GetTaskStatus = false;
            setTimeout(taskStart, 10000);

        });

    });

}