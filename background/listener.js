/*background.listener*/
/**
 * 扩展外部消息
 */
function messageExternalListener(){
	console.log("XSS messageExternalListener");
	chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse){
		console.log('onMessageExternal', request,sender);
		var response = {success: 0, message: ''};
		switch (request.act) {
			case "ping":
				response.success = true;
				response.message = 'tong';
				break;
			case "check_extension_active_finish":
				// checkActiveExtensionFinish();
				response.success = true;
				break;
			default:

				break;
		}
		sendResponse(response);
	});
}

/**
 * 监控特殊的重定向操作
 * 订单详情重定向到首页，重新进入订单详情
 * 提交订单重定向到频繁页面，重新进入订单列表
 */
function chromeRedirectListener(){
	chrome.webRequest.onBeforeRedirect.addListener(function(details){
		//console.log(details);

		if(details.redirectUrl == 'http://www.jd.com/'){
			if(details.url.indexOf('order.jd.com/normal/item.action?orderid=')!=-1){
				//订单详情重定向到首页，重新打开订单详情
				console.log('订单详情重定向到首页，重新打开订单详情');
				chrome.tabs.update(details.tabId,{url: details.url});
			}else{
				console.log('页面 重定向到首页，重新打开 ', details);

				if(details.redirectUrl.indexOf('?l=1&err=3') != -1){
					chrome.tabs.update(details.tabId,{url: details.url});
				}
				//chrome.tabs.update(details.tabId,{url: details.url});
			}
		}else if(details.redirectUrl == 'http://trade.jd.com/orderBack.html' || details.redirectUrl == 'http://trade.jd.com/shopping/orderBack.html'){
			if(details.url.indexOf('trade.jd.com/shopping/order/getOrderInfo.action')!=-1 || details.url.indexOf('http://trade.jd.com/orderBack.html') != -1){
				//提交订单重定向到刷新频繁，跳转到订单列表
				console.log('提交订单重定向到刷新频繁，跳转到订单列表');
				getTaskWorks(function(tw){
					var redirect_nums = tw.redirect_nums || 0;
					redirect_nums++;
					setTaskWorks({redirect_nums:redirect_nums},function(){
						console.log('当前因302跳转订单列表次数：' + redirect_nums);
						chrome.tabs.update(details.tabId,{url: "https://order.jd.com/center/list.action"});
					})
				})
				
			}

		}

	},{urls: ["*://*.jd.com/*"]})
}

/**
 * 针对部分地址进行重定向操作
 * 目前已经不在需要
 */
function received_redirect_listener(){
	chrome.webRequest.onHeadersReceived.addListener(function(details){
		if(details.url == "https://order.jd.com/center/lists.action" ){
			console.log("lists.action https redirect http");
			return {redirectUrl: "https://order.jd.com/center/lists.action"}
		}else if(details.url.indexOf("http://search.jd.com/Search?keyword")!=-1){
			//console.log("search.jd.com http redirect https");
			//return {redirectUrl: details.url.replace('http','https')}
		}else{
			console.log('received', details);
		}
	}, {urls: [
		"http://search.jd.com/Search?keyword=*",
		"https://order.jd.com/center/lists.action"
	], types: ['main_frame']}, ["blocking", "responseHeaders"]);
}


/**
 * bg接收tab的message
 */
function backgroundMessageListener() {
	console.log('XSS background message listener');
	chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
		last_watchdog_time = new Date().getTime();
		//console.log('message receive:', msg, sender);
        sender.bgTaskVars = msg.bgTaskVars;
        console.warn('message: ',msg.act, msg.val, sender);
        try{
			var request = msg;
			//global.saveLoginType, submit_order.saveOrderInfo
			new Function('request','sender', 'M.'+request.act+"(request,sender)")(request.val,sender);
		}catch(e){
			
			var request = msg;

			console.log('global方法:' + request.act +' 未定义',e);
			//兼容sm方法的.
            msg.act = msg.act.replace('global.','',msg.act);

			switch (msg.act) {

				case 'order_operate':
					//订单操作 重置，异常
					client_task_order_operate(msg.val);
					break;
				case 'drop_order':
					orderGroupException('XSS 手动踢单',sender,msg.val);
					break;
				// case 'curr_task_reset':
				// 	M.global.currentTaskReset(request,sender);
				// 	break;
				case 'https_tabs_verify_code': //https打码，传入img地址
					httpsTabsVerifyCode(msg, sender);
					break;
				case 'https_tabs_verify_code_from_uuyun': //https打码，传入img地址
					httpsTabsVerifyCodeFromUUYun(msg, sender);
					break;
				// case 'https_tabs_verify_code_for_chat': //聊天打码，传入img地址
				// 	httpsTabsVerifyCodeForChat(msg, sender);
				// 	break;
				case 'get_chat_msg': //获取聊天信息
					getChatMsg(sender);
					break;
				case 'https_tabs_verify_code_by_base': //https打码，传入base64编码
					httpsTabsVerifyCodeByase64(msg, sender);
					break;
				case 'https_tabs_verify_code_by_base_from_youyouyun': //https打码，传入base64编码
					httpsTabsVerifyCodeByase64FromYouyouYun(msg, sender);
					break;
				case 'https_tabs_verify_fail': //https打码错误，报错
					httpsTabsVerifyFail(msg);
					break;
				case 'https_tabs_verify_fail_to_youyouyun': //优优云打码结果错误，报错
					httpsTabsVerifyFailToYouyouYun(msg);
					break;
				case 'save_host_task_order_detail': //保存订单详情数据
					useLocal(function(local){
						local.task.business_slug
						if(local.task.business_slug == 'jd'){
							// saveHostTaskOrderDetail();
							// saveUserCookiesToRemote('jd.com', saveHostTaskOrderDetail);
							saveUserCookiesToRemote('jd.com', function(){
								getSharedTaskWorks(function(sTask){
									if(sTask.is_group){
										saveGroupHostTaskOrderDetail(sender,msg.val);
									}else{
										saveHostTaskOrderDetail(sender);
									}
								})
								
							});
						}else if(local.task.business_slug == 'yhd'){
							saveUserCookiesToRemote('yhd.com', saveHostTaskOrderDetail);
						}else{
							// saveHostTaskOrderDetail(sender);
							getSharedTaskWorks(function(sTask){
								if(sTask.is_group){
									saveGroupHostTaskOrderDetail(sender,msg.val);
								}else{
									saveHostTaskOrderDetail(sender);
								}
							})
						}
					})
					break;
				case 'order_exception': //订单标记异常 + 备注信息
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							var order_ids = msg.order_ids ? msg.order_ids : '';
							var task_order_id = msg.task_order_id ? msg.task_order_id : '';
							orderGroupException(msg.val,sender,task_order_id,order_ids);
						}else{
							orderException(msg.val,sender);
						}
					})
					
					break;
				case 'set_cookies_to_windows':
					setUserCookiesToWindows(msg.val);sendResponse();
					break;
				case 'remove_domain_cookies_but_str':
					removeDomainCookies(msg.val.domain, msg.val.but_str, function(){
						//chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: 123, text: 'text'});
						setHostStep(3, function(){
							useLocal(function(local){
								var task = local.task;
								if(task.is_mobile == '1'){
									task.promotion_url = 'http://m.jd.com/?utm_source=';
								}else{
									task.promotion_url = 'http://www.jd.com/?utm_source=';
								}
								setLocal({task: task}, function(){
									watchDogTimeOut();
								});
							});

						});

					});
					break;
				case 'change_id_card':
					var reason = msg.val;
					changeIdCard(reason,function(identity){
						chrome.tabs.sendMessage(sender.tab.id, {act: 'id_card_changed', identity: identity});
					});
					break;
				case 'storage_to_tracker': //发送storage给赤兔
					send_storage_to_tracker();
					break;
				case 'search_keyword_exception'://搜索关键词异常
					keywordsException();
					break;
				case 'add_360hk_cookie_thor'://补上hk登录状态
					add360hkCookieThor(function(){
						chrome.tabs.sendMessage(sender.tab.id, {act: 'created_360hk_cookie_thor'});
					});
					break;
				case 'check_active_tab':
					checkActiveTab(sender.tab.id);
					break;
				case 'save_order_info'://保存订单信息
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							saveGroupOrderInfo(msg.val,sender.tab.id);
						}else{
							saveOrderInfo(msg.val,sender.tab.id);
						}
					})
					
					break;
				case 'get_payment_info'://保存订单信息
					getPaymentInfo(sender);
					break;
				//save_bank_form_to_remote
				case 'save_bank_form_to_remote'://保存银行表单信息
					console.log(msg);
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							saveGroupBankFormToRemote(msg.data.bank,msg.data.bank_order_id,msg.data.body,msg.amount);
						}else{
							saveBankFormToRemote(msg.data.bank,msg.data.bank_order_id,msg.data.body,msg.amount);
						}
					})
					
				case 'change_proxy'://再次拨号更换代理
				                    //openAdslForChangeProxy(sender);
				                    // changeProxy(sender);
					break;
				case 'get_pay_state':
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							getGroupPayState(sender);
						}else{
							getPayState(sender);
						}
					})
					
					break;
				case 'set_exception':
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							reportGroupProductStockout(msg.val,sender,msg.task_order_id ? msg.task_order_id : '');
						}else{
							reportProductStockout(msg.val,sender);
						}
					})
					// reportProductStockout(msg.val,sender);
					break;
				case 'add_remark':
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							addGroupTaskOrderRemark(msg.val,sender);
						}else{
							addTaskOrderRemark(msg.val,sender);
						}
					})
					// addTaskOrderRemark(msg.val,sender);
					break;
				case 'get_order_payment_info':
					getOrderPaymentInfo(sender);
					break;
				case 'save_business_check_order':
					saveBusinessCheckOrder(msg.val);
					break;
				case 'save_comment_state':
					// saveCommentState(msg.val);
					break;
				case 'next_app_task':
					TC.done(sender,msg.val)
					break;
				case 'disable_account':
					//禁用账号
					//config/background.js taskTypes配置每个任务执行的禁用账号操作
					// taskConfig[TC.currentTaskType].disableAccount(msg.val, sender);

					M.global.disableAccount(msg.val,sender);
					break;

					console.log(M)

					M[TC.currentTaskType].accountFail(msg, sender);

					//先执行帐号禁用 之后执行各自的accountFail
                    appDisableAccount(msg.val,sender,function (msg, sender) {
                        M[TC.currentTaskType].accountFail(msg, sender)
                    });

					var task_type = TC.currentTaskType;

					if(task_type == 'xss' || task_type == 'payment'){
						appDisableAccountOrderReset(msg.val, sender);
					}else{
						appDisableAccount(msg.val, sender);
					}
					break;
				case 'save_no_found_order':
					saveNoFoundOrder(msg.val);
					break;

				default:
					break;


			}
		};
		//消息完成
		sendResponse();
	});
}


/**
 * 取消所有下载任务
 */
function chromeDownloadsCancel(){
	console.log('XSS background download cancel listener');
	chrome.downloads.onCreated.addListener(function(data){
		console.log(data);
		chrome.downloads.cancel(data.id);

		//if(data.url.indexOf('jd.com') != -1){
		//    chrome.downloads.cancel(data.id);
		//}
	})
}


/**
 * 阻止部分请求
 * 涉及到相关请求(已知固定) 直接取消
 */
function impede_request_listener(){
	chrome.webRequest.onBeforeRequest.addListener(function(details){

		//console.log('阻止google相关地址&JD广告');
		//console.log(details);
		return {cancel: true};

	}, {urls: [
		"*://www.googleadservices.com/*", //google
		"*://www.googletagmanager.com/*", //google
		"*://*.360buyimg.com/n1/*",
		"*://*.360buyimg.com/n2/*",
		"*://*.360buyimg.com/n3/*",
		"*://*.360buyimg.com/n4/*",
		"*://*.360buyimg.com/n5/*",
		"*://*.360buyimg.com/n6/*",
		"*://*.360buyimg.com/n7/*",
		"*://*.360buyimg.com/n8/*",
		"*://*.360buyimg.com/n9/*",
		"*://*.360buyimg.com/n0/*",
		"*://*.360buyimg.com/n11/*",
		"*://*.360buyimg.com/n12/*",
		//"*://*.360buyimg.com/m/*",
		"*://*.360buyimg.com/cms/*",
		"*://*.360buyimg.com/mobilecms/*",
		"*://*.360buyimg.com/imgzone/*",
		"*://*.360buyimg.com/da/*",
		"*://*.360buyimg.com/vclist/*",
		"*://*.360buyimg.com/popWaterMark/*",
		"*://payrisk.jd.com/*"

	], types: ["main_frame", "sub_frame", "stylesheet", "script", "image", "object", "xmlhttprequest", "other"]
	}, ["blocking", "requestBody"]);
}

/**
 * 防劫持
 * 阻止打开部分链接地址
 */
function intercept_ad_request_listener(request_cnts){

	request_cnts ? request_cnts : 0;
	chrome.webRequest.onBeforeRequest.removeListener(adRequestListener);
	var API = new Api();
	API.getRequestBlockUrl(function(ret){
		// if(ret.success == 1 && ret.data.length>0){
		//     requestBlockUrls = ret.data;
		// }
		requestBlockUrls = ret;
		chrome.webRequest.onBeforeRequest.addListener(adRequestListener, {urls: requestBlockUrls, types: ["main_frame"]}, ["blocking", "requestBody"]);
	}, function(){
		setTimeout(function(){
			request_cnts++;
			if(request_cnts >1){
				//请求失败超过2次
				console.log('请求cnapi的阻止链接接口失败超过2次，换api');
				API.getRequestBlockUrlAPI(function(ret){
					if(ret.success == 1 && ret.data.length>0){
					    requestBlockUrls = ret.data;
					}
					chrome.webRequest.onBeforeRequest.addListener(adRequestListener, {urls: requestBlockUrls, types: ["main_frame"]}, ["blocking", "requestBody"]);
				},
				function(){
					//请求失败
					intercept_ad_request_listener(request_cnts);
				})
			}else{
				setTimeout(function(){
					intercept_ad_request_listener(request_cnts);
				},3*1000)
				
			}
			
		}, 1000);
	});
}

function adRequestListener(details){
	console.log('阻止JD广告');
	console.log(details);
	if(details.tabId != jd_ad_tab_id){
		setTimeout(function(){
			watchDogTimeOut();
		}, 2000);
	}
	return {cancel: true};
}

/**
 * 监控付款页跳转失败的情况
 */
function listenErrRequest(){
	chrome.webRequest.onErrorOccurred.addListener(function(details){
		console.log(details);
		//"net::ERR_CONNECTION_TIMED_OUT"  "net::ERR_ABORTED"
		if(details.error == 'net::ERR_CONNECTION_TIMED_OUT'){
			useLocal(function(local){
				if(local.usage == 1){
					//app主机
					changeIPForApp(local.host_id);
				}else{
					openAdsl(true);//重新拨号
				}
			})

		}

	}, {urls: [
		"*://cashier.jd.com/payment/pay.action?*"
	]
	});
}


/**
 * 拦截ccb form data
 */
function ccbFormDataIntercepted(){
	console.log('ccb form data intercepted');
	chrome.webRequest.onBeforeRequest.addListener(function(details){
		console.log(details);
		if(details.requestBody.formData){
			if(details.requestBody.formData.ORDERID.length >0 && details.requestBody.formData.BRANCHID.length >0){
				var form_data = details.requestBody.formData;
				var form_action = details.url;

				var bank_order_id = form_data.ORDERID[0];//流水号
				var branchid = form_data.BRANCHID[0];//付款商户标识码，只使用网银在线

				var amount = form_data.PAYMENT[0];//付款金额

				var body = {form_action: form_action, form_data: form_data};
				if(branchid == '110000000' || branchid == '330000000'){//建行，只允许网银在线(jd),支付宝(yhd)
					getSharedTaskWorks(function(sTask){
						if(sTask.is_group){
							saveGroupBankFormToRemote('CCB', bank_order_id, body,amount);
						}else{
							saveBankFormToRemote('CCB', bank_order_id, body,amount);
						}
					})
					
				}else{
					//1502002
					setHostStatus(1502002);

				}
			}
		}

	}, {urls: [
		// "https://ibsbjstar.ccb.com.cn/app/ccbMainAliPayPlatV5",//yhd建行付款
		// "https://ibsbjstar.ccb.com.cn/app/ccbMain",//jd建行付款
		"*://ibsbjstar.ccb.com.cn/*"
	], types: ["main_frame"]
	}, ["blocking", "requestBody"]);
}

/**
 * 拦截bocom form data
 */
function bcomFormDataIntercepted(){
	console.log('bcom form data intercepted');
	chrome.webRequest.onBeforeRequest.addListener(function(details){
		console.log(details);
		if(details.requestBody.formData){
			var form_data = details.requestBody.formData;
			var form_action = details.url;

			var bank_order_id = form_data.orderid[0];//订单号
			var body = {form_action: form_action, form_data: form_data};

			var amount = form_data.amount[0];

			// saveBankFormToRemote('COMM', bank_order_id, body,amount);
			getSharedTaskWorks(function(sTask){
				if(sTask.is_group){
					saveGroupBankFormToRemote('COMM', bank_order_id, body,amount);
				}else{
					saveBankFormToRemote('COMM', bank_order_id, body,amount);
				}
			})

		}

	}, {urls: [
		// "<all_urls>"
		"*://pay.95559.com.cn/*"//交行付款
	], types: ["main_frame"]
	}, ["blocking", "requestBody"]);
}

/**
 * 拦截icbc form data
 */
function icbcFormDataIntercepted(){
	console.log('icbc form data intercepted');
	chrome.webRequest.onBeforeRequest.addListener(function(details){
		console.log(details);
		if(details.requestBody.formData){
			var form_data = details.requestBody.formData;
			var form_action = details.url;
			var body = {form_action: form_action, form_data: form_data};

			// setLocal({body:body},function(){});
			setTaskWorks({body:body},function(){
				
			})

		}

	}, {urls: [
		// "<all_urls>"
		"*://b2c.icbc.com.cn/*"//工行付款
	], types: ["main_frame"]
	}, ["blocking", "requestBody"]);
}

/**
 * 拦截 boc form data
 */
function bocFormDataIntercepted(){
	console.log('boc form data intercepted');
	chrome.webRequest.onBeforeRequest.addListener(function(details){
		console.log(details);

		var form_data = details.requestBody.formData;
		var form_action = details.url;

		var bank_order_id = form_data.orderNo[0];//订单号
		var body = {form_action: form_action, form_data: form_data};

		var amount = form_data.orderAmount[0];

		// saveBankFormToRemote('BOC', bank_order_id, body,amount);
		getSharedTaskWorks(function(sTask){
			if(sTask.is_group){
				saveGroupBankFormToRemote('BOC', bank_order_id, body,amount);
			}else{
				saveBankFormToRemote('BOC', bank_order_id, body,amount);
			}
		})

	}, {urls: [
		//"https://ebspay.boc.cn/PGWPortal/RecvOrder.do",//yhd中行付款
		"https://ebspay.boc.cn/PGWPortal/RecvOrder.do"//jd中行付款
	], types: ["main_frame"]
	}, ["blocking", "requestBody"]);
}


/**
 * 工商银行替换useragent
 * @param details
 * @returns {{requestHeaders: *}}
 */
function icbcOnBeforeSendHeadersListener(details){
	// console.log(details);
	//console.log("icbc user agent");
	if(details.url.indexOf('icbc.com.cn') >= 0){
		for (var i = 0; i < details.requestHeaders.length; ++i) {
			if (details.requestHeaders[i].name === 'User-Agent') {
				// console.log(details.requestHeaders[i]);
				// console.log(details.requestHeaders[i].value);
				var icbc_useragent = details.requestHeaders[i].value.replace(/Chrome\S+/,'Chrome/24.0');
				details.requestHeaders[i].value = icbc_useragent;
				break;
			}
		}
	}
	return {requestHeaders: details.requestHeaders};
}
//工商银行替换useragent
function icbc_change_useragent_addlistener(){
	//console.log('工商银行切换UserAgent');
	console.log('icbc change user agent listener');
	chrome.webRequest.onBeforeSendHeaders.addListener(icbcOnBeforeSendHeadersListener,
		{urls: ["<all_urls>"]},
		["blocking", "requestHeaders"]);
}
function icbc_change_useragent_removelistener(){
	//console.log('删除工商银行UserAgent替换');
	console.log('clear icbc change user agent listener');
	chrome.webRequest.onBeforeSendHeaders.removeListener(onBeforeSendHeadersListener);
}


/**
 * 获取jd登陆验证码跨域问题
 * @constructor
 */
function JdLoginAuthCodeCrossOriginListener(){
	chrome.webRequest.onHeadersReceived.addListener(
		JdLoginAuthCodeCrossOriginDetails , {
			urls: ["*://authcode.jd.com/*","*://captcha.jd.com/*"]
		}, ['blocking', 'responseHeaders']);
}
function JdLoginAuthCodeCrossOriginDetails(details) {
	console.log("JdLoginAuthCodeCrossOriginDetails");
	details.responseHeaders.push({
		'name': 'Access-Control-Allow-Origin',
		'value': '*'
	});

	return {
		responseHeaders: details.responseHeaders
	}
}
/**
 * 京东登陆验证码listen 去掉
 * @constructor
 */
function JdLoginAuthCodeCrossOriginRemoveListener(){
	chrome.webRequest.onHeadersReceived.removeListener(JdLoginAuthCodeCrossOriginDetails);
}

var TaskUserAgent = '';
function setUserAgent(ua) {
    TaskUserAgent = ua;
    clearUserAgent(function () {
        chrome.webRequest.onBeforeSendHeaders.addListener(TaskUserAgentListener,
            {urls: ["<all_urls>"]},
            ["blocking", "requestHeaders"]);
    });
}

function clearUserAgent(callback) {
    chrome.webRequest.onBeforeSendHeaders.removeListener(TaskUserAgentListener);
    callback && callback();
}

function TaskUserAgentListener(details) {
    for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === 'User-Agent') {
            if(TaskUserAgent){
                details.requestHeaders[i].value = TaskUserAgent;
            }
            break;
        }
    }
    return {requestHeaders: details.requestHeaders};
}

//替换手机useragent
/**
 * M端替换useragent
 */
function change_useragent_addlistener(){
	console.log('change mobile user agent');

	var ua_l = _MobileUserAgent.length;
	var i = parseInt(Math.random()*ua_l);
	UserAgent = _MobileUserAgent[i];
	//console.log(UserAgent);
	//console.log(i);
	//UserAgent = 'Mozilla/5.0 (Linux; Android 5.0.1; Nexus 5 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.1.0.56_r1021013.540 NetType/WIFI';
	chrome.webRequest.onBeforeSendHeaders.addListener(onBeforeSendHeadersListener,
		{urls: ["<all_urls>"]},
		["blocking", "requestHeaders"]);
}
//替换请求 函数
function onBeforeSendHeadersListener(details){
	for (var i = 0; i < details.requestHeaders.length; ++i) {
		if (details.requestHeaders[i].name === 'User-Agent') {
			//console.log(details.requestHeaders[i]);
			//details.requestHeaders.splice(i, 1);
			if(UserAgent){
				// console.log(UserAgent);
				details.requestHeaders[i].value = UserAgent;
			}
			break;
		}
	}
	return {requestHeaders: details.requestHeaders};
}
//取消替换手机useragent
function change_useragent_removelistener(callback){
	console.log('删除useragent  onBeforeSendHeaders.removelistener');
	chrome.webRequest.onBeforeSendHeaders.removeListener(onBeforeSendHeadersListener);
	callback && callback();
}