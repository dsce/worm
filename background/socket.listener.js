//是否打开socket开关
var open_socket = true;
var socket_info = {};

if(open_socket){
	checkHostId(index);
}


function index(local){

	console.log('listen socket');
	// const code = 'man:'+new Date().getMilliseconds();
	// var code = 'hn_001:pacgjeifepfilgalcbpkcghmebbmgafc';
	var code = local.host_id + ':' + chrome.runtime.id;
	console.log(code);
	var sock = new socketio(code);

	console.log('sock' , sock);

	socket_info = sock.io;

	setTimeout(checkSocketStatus,5*1000);

	sock.addAction('restart', function(){
		console.log('重新开始任务了');
		bgAction.startHost();
	});

	//openAdsl
	sock.addAction('openAdsl', function(){
		console.log('重新拨号');
		bgAction.changeProxy(local.host_id);
		
	});


}

//检查当前主机编号
function checkHostId(callback){
	useLocal(function(local){
		if(local.host_id){
			callback && callback(local);
		}else{
			console.log('准备socket连接，获取不到主机编号，3S后重新检测');
			setTimeout(function(){
				checkHostId(callback);
			},3*1000);
		}
	})
}

//检查socket当前状态
function checkSocketStatus(){
	console.log('connected',socket_info.connected);
	if(!socket_info.connected){
		setLocal({sock_connected:false},function(){
			console.log('socket 连接失败',socket_info);
		})	
	}else{
		console.log('socket 连接状态正常：',socket_info);
	}

	setTimeout(checkSocketStatus,10*1000);
}

var bgAction = {
	openAdsl:function(){
		// openAdsl(true);
		console.log('open adsl');
			chrome.windows.create({
				url: 'adsl:adsl'
			}, function (win) {
				if(win.id >0){
					notify('创建ADSL拨号');

					setTimeout(function(){
						closeTabByUrl("adsl:adsl");
					},3*1000);
				}
			})
	},

	//app付款更换代理
	changeProxy:function(host_id){
		changeIPForApp(host_id,function(){
			bgAction.openAdsl();
		});
	},

	startHost:function(){
		startHost();
	}

	//
}
