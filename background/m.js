//message 公用 action
M.global = {
	setHostStatus: function (request, sender) {
		setLocal({host_status: request.hostStatus}, function () {
			statusReport(request.hostStatus, request.timeout);
		});
	},
	startHost: startHost,
	hostStep: function(request){
		setHostStep(request);
	},
	resetHostByStep: watchDogTimeOut,
	notify: function (request, sender) {
		notify(request.message);
	},
	label: function(request, sender){
		hostRunningMessage(request.message, request.timeout);

		console.warn('label消息',request,sender);
        labelWatch.message = request.message;
        labelWatch.time = new Date().getTime();
        labelWatch.timeout = request.timeout||60;
        labelWatch.tab = sender.tab;

	},
	wangwang: function(){
		last_watchdog_time = new Date().getTime();
		labelWatch.time = new Date().getTime();
	},
	next_app_task: function (request, sender) {
		TC.done(sender, request);
	},
	taskDone: function (request, sender) {
		M.global.next_app_task(request, sender)
	},
	taskReset: function (request, sender) {
		TC.reset(request.taskName, sender);
	},
    taskRedo:function (request,sender) {
        TC.redo(request.taskName,request.condition,sender);
    },
    currentTaskReset: function (request, sender) {
        TC.resetCurrentTask(sender);
    },
    disable_account: function (request, sender) {
		//兼容下面的函数写法
        eval(TC.taskConfig[TC.currentTaskType]['disableAccount'] + '(request,sender)');
    },
	disableAccount: function (request, sender) {
		eval(TC.taskConfig[TC.currentTaskType]['disableAccount'] + '(request,sender)');
	},
    screenShot:function (request, sender){
        console.log('request screen capture');
        chrome.windows.getCurrent(function(window){
            if(window.id > 0){
                setTimeout(function(){
                    chrome.tabs.captureVisibleTab(window.id,{format:'png'},function(dataUrl){//截屏
                        // console.log('截屏地址',dataUrl);
                        smToTab(sender.tab.id,'screenShotResult',{img:dataUrl})
                    })
                },3000);
            }
        })
    },
    backgroundScreenShot:function (request, sender){
        console.log('request screen capture',request);
        chrome.windows.getCurrent(function(window){
            if(window.id > 0){
                setTimeout(function(){
                    chrome.tabs.captureVisibleTab(window.id,{format:'png'},function(dataUrl){//截屏
                        // console.log('截屏地址',dataUrl);
                        smToTab(sender.tab.id,'global.backgroundScreenShotResult',{img:dataUrl,order_id:request.order_id,task_type:request.task_type})
                    })
                },100);
            }
        })
    },
    imageUploadResult:function (request, sender) {
        console.log('saveImage', request);
        if (request.url) {
            var api = new Api();
            api.retryTimes = 3;
            api.saveScreenCaptureUrl(request.order_id, request.task_type, request.url)
        }
    },
	report: {
		clank: {
			success: function (result, sender) {
				var api = new Api();
				useLocal(function (local) {
					result.taskType = local.taskType;
					var _reportData = {
						clank_task_id: result.clank_task_id || local.tasks[local.taskId].uuid,
						status: 3,
						finished_result: ''
					}
					// api.reportClankTask(_reportData, function () {
					// 	M.global.taskDone(result, sender);
					// }, function () {
					// 	M.global.taskDone(result, sender);
					// })
					if(local.task && local.task.orders){
	                    	//有打包单不用保存点点状态
	                    	M.global.taskDone(result,sender);
	                    }else{
	                    	api.reportClankTask(_reportData, function(){
	                    		M.global.taskDone(result,sender);
		                    }, function(){
		                    	M.global.taskDone(result,sender);
		                    })
	                    }
					});
			},
			error: function (result, sender) {
				var api = new Api();
				useLocal(function (local) {
					result.taskType = local.taskType;
					var _reportData = {
						clank_task_id: result.clank_task_id || local.tasks[local.taskId].uuid,
						status: result.status,
						finished_result: result.finished_result
					}
					// api.reportClankTask(_reportData, function () {
					// 	M.global.taskDone(result, sender);
					// }, function () {
					// 	M.global.taskDone(result, sender);
					// })
					if(local.task && local.task.orders){
	                    //有打包单不用保存点点状态
                    	M.global.taskDone(result,sender);
                    }else{
                    	api.reportClankTask(_reportData, function(){
                    		M.global.taskDone(result,sender);
	                    }, function(){
	                    	// M.global.taskDone(result,sender);
	                    	notify('汇报点点任务失败，10S后再次汇报');
	                    	setTimeout(function(){
	                    		M.global.report.clank.error(result, sender)
	                    	},10e3);
	                    	
	                    })
                    }

				});
			}
		},
        rear: {
            success: function (result, sender) {
            	
                var api = new Api();
                useLocal(function (local) {
                	result.taskType = local.taskType;
                    //type, order_id, delay, message
                    var _reportData = {
                        type: result.cmd,
                        order_id: result.order_id,
                        delay: result.delay,
                        message:result.message
                    }
                    api.reportRearTask(_reportData, function () {
                    	if(local.applicationName == 'clank' && local.tasks[local.taskId].clank_task_id){
                			//同时汇报点点收评晒的状态
	            			M.global.report.clank.success({clank_task_id:local.tasks[local.taskId].clank_task_id}, sender);
	                	}else{
	                		M.global.taskDone(result, sender);
	                	}
                        
                    }, function () {

                        M.global.taskDone(result, sender);
                    })
                });
            },
            error: function (result, sender) {
            	
                var api = new Api();
                useLocal(function (local) {
                	result.taskType = local.taskType;
                	//type, order_id, delay, message
                    var _reportData = {
                        type: result.cmd,
                        order_id: result.order_id,
                        delay: result.delay,
                        message:result.message
                    }

                    //任务多次尝试，达到次数报告失败
                    if(local.tasks[local.taskId].client_try_times > 5){
                    	_reportData.delay = 0;
                    	_reportData.message = '任务已经尝试执行多次，依然失败，原因：' + _reportData.message; 
                    }
                    var _taskType = null;
                    if(result.cmd.indexOf('receipt_error')>-1) _taskType=1;
                    if(result.cmd.indexOf('comment_error')>-1) _taskType=2;
                    if(result.cmd.indexOf('share_fail')>-1) _taskType=3;
                    if(result.cmd.indexOf('append_fail')>-1) _taskType=4;
                    if(_taskType) M.global.backgroundScreenShot({order_id: result.order_id,task_type:_taskType},sender);
                    api.reportRearTask(_reportData, function () {
                        setTimeout(function () {
                        	if(local.applicationName == 'clank' && local.tasks[local.taskId].clank_task_id){
                        		//同时汇报点点任务状态
                        		M.global.report.clank.error({status:4,finished_result:result.message,clank_task_id:local.tasks[local.taskId].clank_task_id},sender)
                        	}else{
                        		M.global.taskDone(result, sender);
                        	}
                            
                        },_taskType?10e3:100);
                    }, function () {
                        setTimeout(function () {
                            M.global.taskDone(result, sender);
                        },_taskType?10e3:100);
                    })
                });
            }
        }
	}, closeThisTab: function (request, sender) {
		chrome.tabs.remove(sender.tab.id);
	}, createTabByUrl: function (request, sender) {
		chrome.tabs.create({url: request.url});
	}, createIncognitoWindowByUrl: function (request, sender) {
		create_incognito_window_by_url(request.url);
	}, setTabPinned: function (request, sender) {
		chrome.tabs.update(sender.tabs.id, {pinned: true});
	}, icbcChangeUseragentStart: icbc_change_useragent_addlistener
	, icbcChangeUseragentEnd: icbc_change_useragent_removelistener
	, removeUseragentListener: change_useragent_removelistener
	, create_tab_by_rand_click_url: function (request, sender) {
		create_tab_by_rand_click_url(request.url, sender);
	}, tabs_remove_cookies: function (request, sender) {
		removeCookies(request.details);
	}, saveUserCookiesToRemote: function (request, sender) {
		saveUserCookiesToRemote(request.domain);
	}, tabs_get_cookies: function (request, sender) {
		getCookies(request.details, function (cookies) {
			//smToTab(sender.tab.id, 'tab_get_cookies_response', {cookies: cookies});
			chrome.tabs.sendMessage(sender.tab.id, {act: 'tab_get_cookies_response', cookies: cookies});
		});
	}, getBusinessAccount: function (request, sender) {
		getBusinessAccountPassword(sender);
	}, tab_update_selected_by_url: function (request) {
		tabsUpdateSelectedByUrl(request.url);
	}, MChangedToPc: function (request) {
		MChangedToPc(request.url);
	}, wxRemoveCookieLoginStart: wxRemoveCookieLoginStart,

	//京东登陆获取验证码 跨域 增加监听获取
	JdLoginAuthCodeCrossOriginListener: JdLoginAuthCodeCrossOriginListener,
	//京东登陆验证码 跨域监听获取,移除
	JdLoginAuthCodeCrossOriginRemoveListener: JdLoginAuthCodeCrossOriginRemoveListener,
	// 删除指定域下的cookie
	removeDomainCookieByName: function(request){
		removeDomainCookieByName(request.domain, request.names);
	},
	//更新昵称
	updateNickname: function(request, sender){
		updateAccountNikename();
	},
	//更新生日
	updateBirthday: function(request, sender){
		updateAccountBirthday();
	},
	//检查活动窗口
	checkActiveTab: function(request, sender){
		checkActiveTab(sender.tab.id);
	},

	sendNotesToTracker: function(request){
		send_notes_to_tracker(request.message);
	},

	//保存收获地址给远程
	saveAddressToRemote: function(request, sender){
		getSharedTaskWorks(function(sTask){
			if(sTask.is_group){
				saveGroupAddressToRemote(sender);
			}else{
				saveAddressToRemote(sender);
			}
		})
		
	},

	//获取订单的付款状态
	getOrderPayState: function(request, sender){
		getSharedTaskWorks(function(sTask){
			if(sTask.is_group){
				getGroupPayState(sender);
			}else{
				getPayState(sender);
			}
		})
		
	},

	getGroupOrdersInfo: function(request, sender){
		getGroupOrdersInfo(sender);
	},
	//保存账号登录日志
	saveAccountLoginLog: function (request, sender) {
		saveAccountLoginLog(request.loginType, request.clientCode);
	},

	sendMessageToTab:function(request, sender){
		sendMessageToTab(request, sender);
	},

	closeOtherTabs:function(request, sender){
		closeOtherTabs(sender);
	},
	delCookies:function(request, sender){
		delCookies(sender);
	}


};