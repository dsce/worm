//background.func.js

//message 公用 action
M.global = {
	setHostStatus: function(request, sender){
		setLocal({host_status: request.hostStatus}, function () {
			statusReport(request.hostStatus, request.timeout);
		});
	},
	startHost: startHost,
	hostStep: setHostStep,
	resetHostByStep: watchDogTimeOut,
	notify: function(request, sender){
		notify(request.message);
	},
	next_app_task: function(request, sender){
		TC.done(sender, request);
	},
    taskDone:function(request,sender){
		M.global.next_app_task(request,sender)
	},
	taskReset:function (request,sender) {
        TC.reset(request.taskName, sender);
    },
	disableAccount:function (request,sender) {
        eval(TC.taskConfig[TC.currentTaskType]['disableAccount'] + '(request,sender)');
    },
	report:{
        clank:{
    		success:function (result,sender) {
                var api = new Api();
                useLocal(function (local) {
                    var _reportData = {
                        clank_task_id: local.tasks[local.taskId].uuid,
                        status: 3,
                        finished_result: ''
                    }

                    if(local.task.orders){
                    	//有打包单不用保存点点状态
                    	M.global.taskDone(result,sender);
                    }else{
                    	api.reportClankTask(_reportData, function(){
                    		M.global.taskDone(result,sender);
	                    }, function(){
	                    	M.global.taskDone(result,sender);
	                    })
                    }
                    
                });
            },
			error:function (result,sender) {
                var api = new Api();
                useLocal(function (local) {
                    var _reportData = {
                        clank_task_id: local.tasks[local.taskId].uuid,
                        status: result.status,
                        finished_result: result.finished_result
                    }
                    api.reportClankTask(_reportData, function(){
                    	M.global.taskDone(result,sender);
                    }, function(){
                    	M.global.taskDone(result,sender);
                    })
                });
            }
		}
	}, closeThisTab : function(request, sender){
		chrome.tabs.remove(sender.tab.id);
	}, createTabByUrl: function(request, sender){
		chrome.tabs.create({url: request.url});
	}, createIncognitoWindowByUrl: function(request, sender){
		create_incognito_window_by_url(request.url);
	}, setTabPinned: function(request, sender){
		chrome.tabs.update(sender.tabs.id, {pinned: true});
	}, icbcChangeUseragentStart: icbc_change_useragent_addlistener
	, icbcChangeUseragentEnd: icbc_change_useragent_removelistener
	, removeUseragentListener: change_useragent_removelistener
	, create_tab_by_rand_click_url: function(request, sender){
		create_tab_by_rand_click_url(request.url, sender);
	}, tabs_remove_cookies: function(request, sender){
		removeCookies(request.details);
	}, saveUserCookiesToRemote: function(request, sender){
		saveUserCookiesToRemote(request.domain);
	}, tabs_get_cookies: function(request, sender){
		getCookies(request.details, function (cookies) {
			//smToTab(sender.tab.id, 'tab_get_cookies_response', {cookies: cookies});
			chrome.tabs.sendMessage(sender.tab.id, {act: 'tab_get_cookies_response', cookies: cookies});
		});
	}, getBusinessAccount: function(request, sender){
		getBusinessAccountPassword(sender);
	}, tab_update_selected_by_url: function(request){
		tabsUpdateSelectedByUrl(request.url);
	}, MChangedToPc: function(request){
		MChangedToPc(request.url);
	}


};


function background_default(){
	console.log("XSS background default func");

	useLocal(function(local){

		if(local.extension_reload){
			console.log('升级完成 准备刷新');
			setLocal({extension_reload: false}, function(){
				chrome.runtime.reload();
			})
			return false;
		}

		console.log('准备获取主机信息');

		var API = new Api();
		API.background_get_file_client_id(function(){
			console.log('检测是否自动升级');
			if(local.update_check){
				//插件更新，5s直接开始任务，状态执行中
				setLocal({update_check: false, isRunning: true}, function(){
                    console.log('准备startHost');
					setTimeout(function(){
						startHost();
					}, 5000);
				})
			}


		});

	});


}


//global.saveLoginType, submit_order.saveOrderInfo



//监听更新事件
function update_check_available_listener(){
	chrome.runtime.onUpdateAvailable.addListener(function(details){
		console.log("update check available");
		chrome.runtime.reload();
	});
}

function extensionsAutoUpdateCheck(callback){
	chrome.runtime.requestUpdateCheck(function(status, details){
		//"throttled", "no_update", or "update_available"
		console.log('update status:',status);
		if(status == 'update_available'){

            update_check_available_listener();
			//防止更新失败导致插件停止
			setTimeout(function () {
				notify('插件没有自动更新 ,手动刷新');
				chrome.runtime.reload();
            },60e3)

			setLocal({host_step:0},function(){
				watchdog_timeout_count = 0;//超时次数清空
			})
			notify('XSS版本自动升级' + details.version);
		}else{
			setLocal({update_check: true, extension_reload: true}, function(){
				console.log('XSS NO UPDATE');
				notify('XSS 没有进行更新');
				callback && callback();
			});

		}

	});
}

/**
 * chrome content setting
 */
function chromeContentSetting(){
	console.log('XSS Content Settings');
	//图片设置允许所有, 一号店例外
	chrome.contentSettings.images.set({primaryPattern: '<all_urls>', setting: 'allow'});
	chrome.contentSettings.images.set({primaryPattern: '*://*.yhd.com/*', setting: 'block'});
	chrome.contentSettings.images.set({primaryPattern: '*://*.yihaodianimg.com/*', setting: 'block'});
	chrome.contentSettings.images.set({primaryPattern: '*://passport.yhd.com/*', setting: 'allow'});//yhd登陆验证码需要显示
	chrome.contentSettings.images.set({primaryPattern: '*://buy.m.yhd.com/*', setting: 'allow'});//yhd手机端提交订单验证码需要显示

	//jd聊天页面弹窗允许popups [*.]item.jd.com
	chrome.contentSettings.popups.set({primaryPattern: '*://item.jd.com/*', setting: 'allow'});

	//允许跳转银行页面弹出窗口
	chrome.contentSettings.popups.set({primaryPattern: '*://cashier.jd.com/*', setting: 'allow'});
	//全球购选银行
	chrome.contentSettings.popups.set({primaryPattern: '*://pay.jd.hk/*', setting: 'allow'});
	chrome.contentSettings.popups.set({primaryPattern: '*://pcashier.jd.hk/*', setting: 'allow'});

	//pcashier.jd.com
	chrome.contentSettings.popups.set({primaryPattern: '*://pcashier.jd.com/*', setting: 'allow'});

	chrome.contentSettings.popups.set({primaryPattern: '*://home.jd.com/*', setting: 'allow'});


}


function smToTab(tabId, action, value, callback){
	chrome.tabs.sendMessage(tabId, {'act': action, 'val': value || {} }, function(){
		callback && callback();
	});
}

/**
 * 保存银行付款数据到远程
 * @param bank_order_id
 * @param body
 */

function saveBankFormToRemote(bank, bank_order_id, body,amount){
	chrome.storage.local.get(null, function(local){

		getSharedTaskWorks(function(shareTask){

			var card_no = local.bank_info ? local.bank_info.card_no : '';

			if(card_no == ''){//1414001
				notify('错误，没有设置 ' + bank + ' 网银银行卡', false);
				setHostStatus(1414001);
				setLocal({isRunning: false}, function(){  });
				return false;
			}

			var API = new Api();
			var data = {
				username: local.username,
				password: local.password,
				task_order_id: local.task.task_order_id,
				bank: bank,
				// bank_user: bank_username,
				card_no:card_no,
				bank_order_number: bank_order_id,
				business_oid: shareTask.jd_order_id,
				business_account_id: local.accountId,
				body: body,
				money : amount,
				towh_id:local.task.towh_id ? local.task.towh_id : 0
			};

			if(!data.money){
				notify('付款金额 =' + data.money + ' 保存的付款金额不正确');
				return false;
			}

			API.saveCardPay(data,function (ret) {
				if(ret.success == 1){
					setSharedTaskWorks({saved_card_pay:true},function(){
						console.log('保存付款数据成功');
					})
					
				}else{
					notify('错误: '+ret.message, false);
					setHostStatus(1502001);//标记付款失败
					// setTimeout(function(){
					//     saveBankFormToRemote(bank, bank_order_id, body);
					// },10000)
				}
			}, function(ret){
				setTimeout(function(){
					saveBankFormToRemote(bank, bank_order_id, body,amount);
				},10000)
			});
		});

	})
}

function saveGroupBankFormToRemote(bank, bank_order_id, body,amount){
	chrome.storage.local.get(null, function(local){

		getSharedTaskWorks(function(shareTask){

			var card_no = local.bank_info ? local.bank_info.card_no : '';

			if(card_no == ''){//1414001
				notify('错误，没有设置 ' + bank + ' 网银银行卡', false);
				setHostStatus(1414001);
				setLocal({isRunning: false}, function(){  });
				return false;
			}

			var API = new Api();
			var data = {
				// username: local.username,
				// password: local.password,
				// task_order_id: local.task.task_order_id,
				order_group_id:local.task.id,
				order_group_work_id:local.task.order_group_work_id,
				bank: bank,
				// bank_user: bank_username,
				card_no:card_no,
				bank_order_number: bank_order_id,
				business_oid: shareTask.jd_order_id,
				business_account_id: local.accountId,
				body: body,
				money : amount
				// towh_id:local.task.towh_id ? local.task.towh_id : 0
			};

			if(!data.money){
				notify('付款金额 =' + data.money + ' 保存的付款金额不正确');
				return false;
			}

			API.saveGroupCardPay(data,function (ret) {
				if(ret.success == 1){
					setSharedTaskWorks({saved_card_pay:true},function(){
						console.log('保存付款数据成功');
					})
					
				}else{
					notify('错误: '+ret.message, false);
					setHostStatus(1502001);//标记付款失败
					// setTimeout(function(){
					//     saveBankFormToRemote(bank, bank_order_id, body);
					// },10000)
				}
			}, function(ret){
				setTimeout(function(){
					saveGroupBankFormToRemote(bank, bank_order_id, body,amount);
				},10000)
			});
		});

	})
}

/**
 * 插件 自动关闭
 */
function extension_self_disabled(){
	chrome.management.getSelf(function(selfInfo){
		chrome.management.setEnabled(selfInfo.id, false, null);
	});
}

/**
 * 获取对应域cookie
 * @param details
 * @param callback
 */
function getCookies(details,callback){
	//details = {domain: "taobao.com"}
	chrome.cookies.getAll(details, function(cookies) {
		console.log(cookies);
		callback && callback(cookies);
	});
}

/**
 * 设置对应域cookie
 * @param details
 * @param callback
 */
function setCookies(details,callback){
	chrome.cookies.set(details, function (cookie){
		console.log('set cookies result', cookie);
		callback && callback();
	});
}

/**
 * 删除对应域cookie
 * @param details
 * @param callback
 */
function removeCookies(details, callback){
	chrome.cookies.remove(details, function (cookie){
		console.log('remove cookies result', cookie);
		callback && callback();
	});
}


/**
 * https页面打码
 * @param request object 包含图片的地址
 * @param sender object 请求验证码的sender
 */
function httpsTabsVerifyCode(request,sender){
	console.log('https_tabs_verify_code_result');
	//chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: 123, text: 'text'});

	var dama = new DaMa();
	dama.submit(request.imgsrc, function (cid, text) {
		chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: cid, text: text});
	}, function (errno) {
		console.log('error callback no:' + errno);
		chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_errno', errno:errno,type:1});
	});

}

/**
 * https页面打码
 * @param request object 包含图片的地址
 * @param sender object 请求验证码的sender
 */
function httpsTabsVerifyCodeFromUUYun(request,sender){
	console.log('https_tabs_verify_code_result_from_uuyun');

	var dama = new youyouYun();
	// console.log(request.base);
	dama.init_for_src(request.imgsrc,
		function(result,udata){
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', udata:udata, text: result,type:2});
		},
		function (errno) {
			console.log('error callback no-yyu:' + errno);
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_errno', errno:errno,type:2});
		}
	);

}

// function httpsTabsVerifyCodeForChat(request,sender){
// 	console.log('https_tabs_verify_code_result_for_chat');

// 	var dama = new DaMa();
// 	dama.submit(request.imgsrc, function (cid, text) {
// 		chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: cid, text: text,error:0});
// 	}, function () {
// 		chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', error:1});
// 		//chrome.tabs.reload(sender.tab.id);
// 	}, request.cookie);

// }

// function getChatMsg(sender){
// 	console.log('get_chat_msg_result');
// 	useLocal(function(local){
// 		var task = local.task;
// 		var request_nums = task.getChatMsgNums ? task.getChatMsgNums : 1;

// 		var API = new Api();
// 		API.getChatMsg(
// 			function(ret){
// 				if(ret.success == 1){
// 					console.log('获取聊天信息成功');
// 					chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:ret.data});
// 				}else{
// 					console.log('获取信息失败 ',ret.message);
// 					chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:null});
// 				}
// 			},
// 			function(){
// 				console.log('请求失败，再次尝试');
// 				if(request_nums < 5){
// 					setTimeout(function(){
// 						task.getChatMsgNums = parseInt(request_nums + 1);
// 						setLocal({task:task},function(){
// 							getChatMsg(sender);//再次请求
// 						})

// 					},5000);
// 				}else{
// 					console.log('获取信息失败');
// 					chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:null});
// 				}

// 			}
// 		);

// 	})

// }

/**
 * https页面打码
 * @param request object 包含图片的地址
 * @param sender object 请求验证码的sender
 */
function httpsTabsVerifyCodeByase64(request,sender){
	console.log('https_tabs_verify_code_by_base_result');
	//chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: 123, text: 'text'});

	var dama = new DaMa();
	dama.submitByBase64(request.base, function (cid, text) {
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: cid, text: text,type:1});
            chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_tabCommon', val:{cid: cid, text: text,type:1}});
		},
		function (errno) {
			console.log('error callback no:' + errno);
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_errno', errno:errno,type:1});
            chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_errno_tabCommon', val:{errno:errno,type:1}});
        });
}

//youyouYun 平台打码
function httpsTabsVerifyCodeByase64FromYouyouYun(request,sender){

	console.log('https_tabs_verify_code_by_base_result_from_youyouyun');
	var dama = new youyouYun();
	dama.init(request.base,
		function(result,udata){
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', udata:udata, text: result,type:2});
		},
		function (errno) {
			console.log('error callback no-yyu:' + errno);
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result_errno', errno:errno,type:2});
		}
	);
}

/**
 * https打码错误，报错
 * @param request object 包含验证码报错使用的cid
 */
function httpsTabsVerifyFail(request){
	console.log('https_tabs_verify_fail');
	var dama = new DaMa();
	dama.report(request.cid, function () {});
}

/**
 * https打码错误，报错
 * @param codeID 接口返回的id
 */
function httpsTabsVerifyFailToYouyouYun(request){
	console.log('https_tabs_verify_fail_to_youyouyun');
	if(request.udata.ID > 0){
		var dama = new youyouYun();
		dama.reportError(request.udata, function () {});
	}
}



/**
 * 设置任务运行状态
 * @param status
 */
function setHostStatus(status, callback){
	setLocal({host_status: status}, function () {
		statusReport(status,60, callback);
	});
}
/**
 * 设置主机运行步骤
 * @param step
 */
function setHostStep(step, callback){
	setLocal({host_step: step}, function(){
		callback && callback();
	});
}

/**
 * 插件状态报告，发送
 * @param callback
 */
function statusReport(status, timeout, callback) {

	var API = new Api();

	last_watchdog_time = new Date().getTime();

	useLocal(function(local){

		var host_status_result = _host_status[status].text;
		var host_status_timeout = _host_status[status].timeout;
		host_status_timeout = timeout===undefined?(host_status_timeout===undefined?60:host_status_timeout):timeout;//没有设置超时时间默认60

		console.log(status,host_status_result,local);

		// if(local.task != undefined && local.task.task_order_id > 0 && status < 2000000) {
		//     var params = {
		//         action: 'task_order_report_status',
		//         host_id: local.host_id,
		//         username: local.username,
		//         password: local.password,
		//         task_order_id: local.task.task_order_id,
		//         host_finish_result: host_status_result
		//     };

		//     API.setParams(params);
		//     API.ajaxPOST(function (ret) {
		//         callback && callback();
		//     }, null);
		// } else {
		//     callback && callback();
		// }

		callback && callback();

		hostRunningMessage(host_status_result, host_status_timeout);
	});
}

/**
 * 汇报主机及时状态
 * @param message
 * @param timeout
 */
var oldMessage,oldTimeout,oldReportHostMessageTime;
function hostRunningMessage(message, timeout){

	//发送提示信息到tracker
	send_notes_to_tracker("["+timeout+"]"+message);

	var nowTime = $.now();
	if(oldMessage==message&&timeout==oldTimeout&&(oldReportHostMessageTime-nowTime)<oldTimeout){
		console.log("消息重复不报告给服务器");
		return null;
	}
	oldMessage=message,oldTimeout=timeout;
	oldReportHostMessageTime= $.now();

	reportHostMessage(message, timeout);

	 //reportHostRunningMessage(message, timeout);

}
/**
 * 状态设置为0的时候需要执行的，开始或重新开始或下一单
 */
function startHost(){
	useLocal(function(local){

		//var extensionId = tracker_env[local.env];
		//message_export(extensionId, {act: "extension_active_check"});

		//最后一单否
		if (local.last_order) {
			//最后一单，暂停 ，不继续领单
			notify("最后一单，暂停，不继续领单");
			setLocal({isRunning: false , host_status: 0}, function () {
				//setLocal({last_order: false}, function () {  });
				chrome.windows.getAll(function(wins){
					if(wins.length >0){
						for(var i=0;i<wins.length;i++){
							chrome.windows.remove(wins[i].id);
						}
					}
				})

                create_incognito_window({url:'about:blank'});
			});

		} else {
			//不是最后一单继续领单，
			GetTaskStatus = false;
			StartTaskOpenUrl = false;

			//清空 app任务
			taskRequest.lastTime=(new Date).getTime();//防止任务超时
			TC.tasks = [];

			//停止label超时
			labelWatch.time='';

			//初始化客户端  关闭所有窗口 准备开始任务
			setHostStatus(1001000, CloseWindowsAll);
		}
	});

}

//关闭所有窗口
function CloseWindowsAll() {
	setHostStatus(1101000);
	console.log('close window all');
	chrome.windows.getAll(function (wins) {
		var winsLength = wins.length;
		var index = 0;
		if(winsLength > 0){
			for (var i = 0; i < winsLength; i++) {
				chrome.windows.remove(wins[i].id, function () {
					index++;
					if (winsLength == index) {
						//清除所有历史信息 10秒之后打开adsl拨号
						setTimeout(function () {
							clearCookies(function () {
								send_storage_to_tracker(function () {
                                    intercept_ad_request_listener(request_block_cnts);//更新一下需要阻止的链接列表
                                    setTimeout(openAdsl, 10000);
                                });
							});
						}, 10000);

					}

				});

			};
		}else{
			//清除所有历史信息 10秒之后打开adsl拨号
			setTimeout(function () {
				clearCookies(function () {
					setTimeout(openAdsl, 10000);
				});
			}, 10000);
		}
	});
}

/**
 * ADSL拨号
 */
var last_adsl_time = new Date().getTime()-30e3;
function openAdsl(adsl) {
	if(new Date().getTime() - last_adsl_time < 30e3) {
		notify('拨号频繁,已禁止本次拨号')
		return;
	}
	last_adsl_time = new Date().getTime();
	last_watchdog_time = new Date().getTime();
	useLocal(function(local){
		if(local.usage == 1){//是app任务,跳过ADSL
			select_task_by_usage(local);
		}else{
			setHostStatus(1102000);
			console.log('chage ip');
			changeIp(function(ret){
				if(ret){
					setTimeout(function(){
						verifyIP();
					},15*1000);
				}else{
					console.log('open adsl');
					chrome.windows.create({
						url: 'adsl:adsl'
					}, function (win) {
						if (win.id) {
							chrome.windows.update(win.id, {state: "maximized"}, function () {
								//ip重复判断 开始任务
								// setTimeout(verifyIP, 10000);
								setTimeout(function(){
									verifyIP(adsl);
								},10*1000);

							});
						}

					});
				}
				
			})
			
			
		}
	});



}

function changeIPForApp(host_id,callback){
	var url = 'http://127.0.0.1:6086/change';
	// var url = 'http://127.0.0.1:8080';
	var data = {host_id:host_id};
	$.ajax({url: url,type:'POST',data:data, timeout: 3000}).done(function (data) {
		console.log(data);
		if(data == 'ok'){
			notify('更换代理成功');
			callback && callback();
			//chrome.tabs.sendMessage(sender.tab.id,{act:'change_proxy_result'})
		}else{
			notify('更换代理失败');
			setTimeout(function(){
				changeIPForApp(host_id);
			},3000)
		}
	}).fail(function (Request, textStatus, errorThrown) {
		notify("请求更换代理失败，3S后再次请求");
		setTimeout(function(){

			if(callback){
				callback && callback()
			}else{
				changeIPForApp(host_id);
			}
			
		},3000)

	});
}


//app主机请求更换代理
function changeProxy(sender){
	var url = 'http://127.0.0.1:6086';
	// var url = 'http://127.0.0.1:8080';
	$.ajax({url: url, timeout: 3000}).done(function (data) {
		console.log(data);
		if(data == 'ok'){
			notify('更换代理成功');
			chrome.tabs.sendMessage(sender.tab.id,{act:'change_proxy_result'})
		}else{
			notify('更换代理失败');
			setTimeout(function(){
				changeProxy(sender);
			},3000)
		}
	}).fail(function (Request, textStatus, errorThrown) {
		notify("请求更换代理失败，3S后再次请求");
		setTimeout(function(){
			changeProxy(sender);
		},3000)

	});
}

/*
 @重新拨号来更换代理
 */
function openAdslForChangeProxy(sender) {
	last_watchdog_time = new Date().getTime();
	console.log('open ADSL for change proxy');
	chrome.windows.create({
		url: 'adsl:adsl'
	}, function (win) {
		if (win.id) {
			chrome.windows.update(win.id, {state: "minimized"}, function () {
				//ip重复判断 开始任务
				setTimeout(function(){
					verifyIPForChangeProxy(sender,'',function(){
						chrome.tabs.sendMessage(sender.tab.id,{act:'change_proxy_result'});
					})
				}, 10000);
			});
		}

	});
}
/*
 @重新拨号后验证IP
 */
function verifyIPForChangeProxy(sender,url,callback) {
	var url_used = url ? true : false;
	var url = url ? url : 'http://106.75.37.26/remote_addr';
	last_watchdog_time = new Date().getTime();
	console.log('verify ip');
	useLocal( function (local) {
		var lastIp = local.last_ip;
		$.ajax({url: url, timeout: 3000}).done(function (data) {
			//console.log(data);
			if (validate_ip_address(data)) {
				ADSLAfterConnectNubmber = 0;//获取到ip,连接成功,清空
				if (data == lastIp && local.env != 2&& local.env != 1) {
					//ip相同重新拨号
					notify('IP重复，准备重新拨号');
					setTimeout(function(){
						openAdslForChangeProxy(sender);
					}, 10000);
				} else {
					closeTabByUrl("adsl:adsl");
					//ip与上一个不重复，检查是否可用，
					setLocal({'last_ip': data}, function(){
						if(local.env != 2 && local.env != 1){
							var API = new Api();
							API.setParams({action: 'order_start_ip_no_repeat', host_id: local.host_id});
							API.get(function(ret){
								notify(ret.message);
								if(ret.success == 1){
									callback && callback();

								}else{
									openAdslForChangeProxy(sender);
								}
							});
						}else{
							notes('测试和本地环境跳过ip重复验证，按用途选择任务，直接获取任务');
							callback && callback();

						}

					});
				}
			} else {
				notify('服务器返回IP地址格式不正确，重新执行更换IP');
				setTimeout(function(){
					openAdslForChangeProxy(sender);
				}, 10000);

			}
		}).fail(function (Request, textStatus, errorThrown) {
			//请求IP失败
			console.log('get ip fail', Request);

			ADSLAfterConnectNubmber += 1;

			if(Request.status == 0 && ADSLAfterConnectNubmber > 3){
				notify("ADSL重新拨号");
				openAdslForChangeProxy(sender);
			}else{
				notify("IP不能成功获取,重新获取IP");
				var url =  url_used ? 'http://ipaddr.poptop.cc/remote_addr' : 'https://ipaddr.disi.se/remote_addr';
				setTimeout(function(){
					verifyIPForChangeProxy(sender,url,callback);
				},10000);
				//setTimeout(verifyIP, 10000);
			}

		});
	});
}

//验证ip
function confirmationIp(callback, index, num){
	last_watchdog_time = new Date().getTime();
	index = index>0 ? index : 0;
	num = num>0 ? num : 0;
	var url_arr = ['https://ipaddr.disi.se/remote_addr','http://106.75.37.26/remote_addr'];

	var getIp = function(url, success, error){
		console.log('get ip ', url);
        taskRequest.lastTime = (new Date).getTime();//防止任务超时
		$.ajax({url: url, timeout: 5000}).done(function(res){
			success(res);
		}).fail(function(Request, textStatus, errorThrown){
			error();
		});
	}

	//同URL尝试3次
	if(num > 9){
		index++;
		num = 0;
	}

	if(index > url_arr.length-1){
		//没有可用的url了；
		callback&&callback(false);
	}else{
		var url = url_arr[index];
		console.log('adsl', url, num);
		getIp(url, function(res){
			if(validate_ip_address(res)){
				//ip正常
				callback&&callback(res);
			}else{
				//不对，
				num++;
				setTimeout(function(){
					confirmationIp(callback, index, num);
				}, 5000);
			}
		}, function(){
			//fail，
			num++;
			setTimeout(function(){
				confirmationIp(callback, index, num);
			}, 5000);
		})
	}
}


/**
 * adsl后验证ip
 */
function verifyIP(adsl) {
	last_watchdog_time = new Date().getTime();
	console.log('verify ip');
	confirmationIp(function(ip){
		if(ip){
			useLocal(function(local){
				//ip是否和上一次的一样， 开发环境不验证
				//local.env=0;
				if (ip == local.last_ip && local.env != 2&& local.env != 1) {
					//ip相同重新拨号
					notify('IP重复，准备重新拨号');
					setHostStatus(1103001, function () {
						setTimeout(CloseWindowsAll, 10000);
						// last_adsl_time = 0;
						// setTimeout(openAdsl, 10000);//ip重复，10后重新拨号
					});

				} else {
					//notify('IP正常，准备 开始下单！！');
					closeTabByUrl("adsl:adsl");
					//ip与上一个不重复，检查是否可用，
					setLocal({'last_ip': ip}, function(){
						if(local.env != 2&& local.env != 1){
							// if(1){
							order_ip_no_repeat(local);

						}else{
							notes('测试和本地环境跳过ip重复验证，按用途选择任务，直接获取任务');
							//setHostStatus(1104000, getTask);
							//ip可用，选择任务类型 ，获取任务
							select_task_by_usage(local);

						}

					});
				}
			});
		}else{
			notify('不能获取ip， 重新拨号');
			setHostStatus(1103002, function () {
				setTimeout(CloseWindowsAll, 10000);
			});
		}
	});

	//var url_arr = ['http://106.75.37.26/remote_addr', 'http://ipaddr.poptop.cc/remote_addr','https://ipaddr.disi.se/remote_addr'];
	//var url = url_arr[url_used_nums] ? url_arr[url_used_nums] : url_arr[0] ;
	//last_watchdog_time = new Date().getTime();
	//console.log('verify ip');
	//useLocal( function (local) {
	//    setHostStatus(1103000);
	//    var lastIp = local.last_ip;
	//    //http://ipaddr.poptop.cc/remote_addr
	//    //http://b1.poptop.cc/remote_addr
	//    $.ajax({url: url, timeout: 3000}).done(function (data) {
	//        //console.log(data);
	//        if (validate_ip_address(data)) {
	//            url_used_nums = 0;
	//
	//            if(adsl == true){
	//                console.log('拨号成功');
	//                closeTabByUrl("adsl:adsl");
	//                return false;
	//            }
	//            // ADSLAfterConnectNubmber = 0;//获取到ip,连接成功,清空
	//            if (data == lastIp && local.env != 2&& local.env != 1) {
	//                //ip相同重新拨号
	//                notify('IP重复，准备重新拨号');
	//                setHostStatus(1103001, function () {
	//                    setTimeout(CloseWindowsAll, 10000);
	//                });
	//
	//            } else {
	//                //notify('IP正常，准备 开始下单！！');
	//                closeTabByUrl("adsl:adsl");
	//                //ip与上一个不重复，检查是否可用，
	//                setLocal({'last_ip': data}, function(){
	//                    if(local.env != 2&& local.env != 1){
	//                    // if(1){
	//                        order_ip_no_repeat(local);
	//
	//                    }else{
	//                        notify('测试和本地环境跳过ip重复验证，按用途选择任务，直接获取任务');
	//                        //setHostStatus(1104000, getTask);
	//                        //ip可用，选择任务类型 ，获取任务
	//                        select_task_by_usage(local);
	//
	//                    }
	//
	//                });
	//            }
	//        } else {
	//            notify('服务器返回IP地址格式不正确，重新执行更换IP');
	//            setHostStatus(1103002, function () {
	//                setTimeout(CloseWindowsAll, 10000);
	//            });
	//        }
	//    }).fail(function (Request, textStatus, errorThrown) {
	//        //请求IP失败
	//        console.log('get ip fail', Request);
	//
	//        // ADSLAfterConnectNubmber += 1;
	//
	//        // if(Request.status == 0 && ADSLAfterConnectNubmber > 3){
	//        //     notify("ADSL重新拨号");
	//        //     CloseWindowsAll();
	//        // }else{ }
	//
	//            notify("IP不能成功获取,重新获取IP");
	//
	//            if(Request.status == 0){
	//                //网络失败
	//                url_repeat_nums++;
	//               // var url = url_arr[parseInt(url_used_nums-1)];
	//
	//            }else{
	//
	//                url_used_nums++;
	//                // var url = url_arr[parseInt(url_used_nums-1)];
	//            }
	//
	//            notify("ADSL重新拨号");
	//
	//            if(url_used_nums > 2){
	//
	//                url_used_nums = 0;
	//                url_repeat_nums= 0;
	//                CloseWindowsAll();
	//            }else if(url_repeat_nums > 5){
	//
	//                if(url_used_nums < 2){
	//                    url_used_nums++;
	//                }else{
	//                    url_used_nums = 0;
	//                }
	//
	//                url_repeat_nums= 0;
	//                CloseWindowsAll();
	//
	//            }else{
	//                setTimeout(function(){
	//                    verifyIP(adsl);
	//                },10000);
	//            }
	//
	//            //setTimeout(verifyIP, 10000);
	//
	//
	//    });
	//});
}




//验证IP重复
function order_ip_no_repeat(local){
	var  API = new Api();
	API.orderIpNoRepeat({host_id:local.host_id},
		function(ret){
			notify(ret.message);
			if(ret.success == 1){
				//getTask();
				//ip可用，选择任务类型 ，获取任务
				select_task_by_usage(local);

			}else{
				//setHostStatus(1103003, CloseWindowsAll);

				//ip 不可用，10s后重新拨号
				setHostStatus(1103003, function(){
					last_adsl_time = 0;
					setTimeout(openAdsl, 10000);
				});

			}
		},
		function(){
			setTimeout(function(){
				console.log('请求验证ip重复接口失败');
				order_ip_no_repeat(local);
			},3*1000)
		})
}

function select_task_by_usage(local){

	//排除app主机赤兔检查更新
	if(local.usage != '1'){
		update_check_to_tracker();
	}
	//记录app请求检查赤兔更新时间开始
	app_task_check_tracker_start = new Date().getTime();

	//获取任务前 先检查插件是否需要更新
	var API = new Api();
	// API.get_extension_update_status(local.host_id, function(ret){
	// 	if(ret.version){
	// 		var usage = local.usage;
	// 		if(!compare_versions(local.version_env, chrome.runtime.getManifest().version, ret.version)){
	// 			setLocal({update_check: true, extension_reload: true}, function(){
	// 				extensionsAutoUpdateCheck(function(){
	// 					taskStart();//任务开始,
	// 				});
	// 			});
	API.get_extension_update_status_api(local.host_id,function(ret){
		if(ret.data){
			var usage = local.usage;
			if(!compare_versions(local.version_env, chrome.runtime.getManifest().version, ret.data)){
				setLocal({update_check: true, extension_reload: true}, function(){
					extensionsAutoUpdateCheck(function(){
						taskStart();//任务开始,
					});
				});
			}else{

				notes("已经是最新版本! bangbang 哒！");

				taskStart();//任务开始,
			}
		}else{
			notify("插件未知");
		}

	}, function(){
		setTimeout(function(){
			select_task_by_usage(local);
			// request_check_update_nums++;
			// if(request_check_update_nums >1){
			// 	console.log('请求cnapi获取更新版本接口失败超过2次，更换api');
			// 	API.get_extension_update_status_api(local.host_id,function(ret){
			// 		if(ret.data){
			// 			var usage = local.usage;
			// 			if(!compare_versions(local.version_env, chrome.runtime.getManifest().version, ret.data)){
			// 				setLocal({update_check: true, extension_reload: true}, function(){
			// 					extensionsAutoUpdateCheck(function(){
			// 						taskStart();//任务开始,
			// 					});
			// 				});
			// 			}else{

			// 				notes("已经是最新版本! bangbang 哒！");

			// 				taskStart();//任务开始,
			// 			}
			// 		}else{
			// 			notify("插件未知");
			// 		}
			// 	},function(){
			// 		select_task_by_usage(local);
			// 	})
			// }else{
			// 	setTimeout(function(){
			// 		select_task_by_usage(local);
			// 	},3*1000)
				
			// }
			
		}, 5000);
	});

}


/**
 * 看门狗计时器
 */
function watchDogTimer(){
	chrome.storage.local.get(null, function (local) {
		var step = local.host_step;
		if(last_watchdog_time && local.isRunning === true){
			console.log('step-' + step, 'dog-' + last_watchdog_time);
			var time = new Date().getTime();
			if((time - last_watchdog_time) > watchdog_timeout_time){
				//喂狗超时
				console.log('step-' + step, 'dog timeout - ' + watchdog_timeout_count);

				if (last_watchdog_timeout_step == step) {
					watchdog_timeout_count += 1;//没人喂
				} else {
					watchdog_timeout_count = 0;//有人喂
				}

				//  超过3次没人喂
				if (watchdog_timeout_count > 3) {
					//超时3次，不处理，，，，
					//watchdog_timeout_count = 0;
					last_watchdog_time = new Date().getTime();
					console.log("当前超时已多于三次不再处理");
					notify("当前超时已多于三次不再处理");

					//点点遇到情况，超时 有任务数据，taskType不存在，TC.currentTaskType 为空，重新开始
					if(!local.taskType && Object.keys(local.tasks).length>0 && TC.currentTaskType==''&&TC.currentTask==''&&TC.tasks.length>0){
						//这种类型的任务需要重新启动
						last_watchdog_time = new Date().getTime();
						notify('有任务数据，taskType不存在，TC.currentTaskType 为空，重新开始');
						console.warn('label超时重新开始');
						startHost();
					}

					//非洗刷刷任务，超过3次超时不处理，重新开始
					if(local.taskType && appTimeOutRestartTaskLists.indexOf(local.taskType)!=-1){
						//这种类型的任务需要重新启动
						last_watchdog_time = new Date().getTime();
                        console.warn('label超时3次+重新开始');
						startHost();
					}

				} else {

					//超时自动步骤重置

					watchDogTimeOut();

					last_watchdog_timeout_step = step;
					last_watchdog_time = time;
				}

			}

		}else{
			//插件暂停，自动喂狗
			last_watchdog_time = new Date().getTime();
		}

		setTimeout(watchDogTimer, 3000);
	})
}

/**
 * 关闭所有窗口
 * 当前任务重新执行
 */
function watchDogTimeOutByLabel(){
	useLocal(function(local){
		//当前任务重新开始
        var startPage = getStartPage(TC.currentTaskType);

		if(!startPage){ console.log("startPage 错误", startPage); return false;}

		//步骤重置,从原来的关闭tab转到关闭windows,解决异常关闭没有重新打开的问题
        create_incognito_window({url: startPage})
	});
}
/**
 * 插件重新按照当前步骤自动处理（看门狗计时器超出时执行）
 */
function watchDogTimeOut(){
	TC.tasks.length && TC.resetCurrentTask();
	console.log('watchDogTimeOut 不处理');
	return false;
	notify('步骤重置了');
	var action_url = null;
	useLocal(function(local){

		var task_types = ['search','xss','payment','home'];

		if(task_types.indexOf(local.taskType) != -1){
			var task = local.task;
			var step = local.host_step;

			if(step == 0){
				notify("当前状态为0, 10秒后重新开始");
				setTimeout(startHost, 10000);
				return false;
			}

			if(!task){
				//平台标志不存在,不处理
				startHost();
				return false;
			}

			action_url = _host_step_action_url[step][task.business_slug][(task.is_mobile=='1'||task.is_mobile=='2')?1:0];

			if(task.worldwide == '1'){
				action_url = _worldwide_host_step_action_url[step][task.business_slug][(task.is_mobile=='1'||task.is_mobile=='2')?1:0];
			}

			if(step == 3){
				action_url = task.promotion_url;
			}else if(step == 4){
				action_url = action_url.replace('{item_id}', task.item_id);
			}

			//已经开始付款的步骤重置
			if(step>=7&&(local.host_status>=1413099 || local.host_status==1000001)&&local.host_status!=1502005){
				//已经到达跳转银行,如果这个时候超时那么认为付款失败了,要先设置付款失败状态,然后进行步骤重置,继而能够继续付款
				setHostStatus(1502005, watchDogTimeOut);
				return false;
			}

			if(!action_url){ console.log("action_url 错误", action_url); return false;}
			//action_url = "https://order.jd.com/center/list.action";

			//步骤重置,从原来的关闭tab转到关闭windows,解决异常关闭没有重新打开的问题
            create_incognito_window({url: action_url})
		}else{
			//非sd 重置
			TC.done(local.taskType);
		}

	});

}

function currTaskReset(){
	TC.resetCurrentTask();
}


/**
 * 保存平台用户cookie到远程服务器
 * @param domain
 */
function saveUserCookiesToRemote(domain, callback){
	console.log("set cookies", domain);
	var details = {domain: domain};
	getCookies(details, function (cookies) {console.log(cookies);
		useLocal(function (local) {

			var username_right = false;
			var required_cookies = new Array();

			for(var i in cookies){
				var cookie = cookies[i];
				//console.log(cookie.name+'='+cookie.value);
				if(cookie.name == 'pin'  || cookie.name == 'pt_pin'){

					if(decodeURI(cookie.value) == local.account.username || decodeURIComponent(cookie.value) == local.account.username){
						username_right = true;
						// continue;
					}
				}

				//指定name 保存需要的cookie
				if(cookie.name == 'pinId' || cookie.name == 'thor' || cookie.name == 'pin'){
					required_cookies.push(cookie);
				}
				//M端
				if(cookie.name == 'pt_key' || cookie.name == 'sid' || cookie.name == 'USER_FLAG_CHECK' || cookie.name == 'pt_pin'){
					required_cookies.push(cookie);
				}

			}

			var old_cookies = local.account.cookies ? local.account.cookies:null;

			var new_cookies = new Array();

			if(old_cookies && old_cookies.length > 0){
				for(var i in required_cookies){
					for(var ii in old_cookies){
						if(old_cookies[ii].name == required_cookies[i].name){
							old_cookies.splice(ii,1);
							break;
						}
					}

					// new_cookies.push(required_cookies[i]);
				}
				new_cookies = old_cookies.concat(required_cookies);
			}else{
				new_cookies = required_cookies;
			}

			// var required_cookies = {'m':required_cookies_for_m,'pc':required_cookies_for_pc};
			console.log('new_cookies' , new_cookies);

			if(!username_right){

				notify('保存cookies 账号 不一致，不保存cookie');
				callback && callback();
				return false;
			}

			var API = new Api();
			console.log(cookies);

			var data =
			{
				account_id:local.accountId,
				// cookies:JSON.stringify(cookies)
				cookies:JSON.stringify(new_cookies)
			};
			API.saveAccountCookies(data,
				function(ret){
					if(ret == 1){
						console.log('cookies~ 保存成功');
						setLocalTask({cookies:new_cookies},function(){
							callback && callback();
						})

					}else{
						console.log('cookies~ 保存失败');
						callback && callback();
					}
				},
				function(){
					console.log('请求保存cookie接口失败,重新保存');
					setTimeout(function(){
						saveUserCookiesToRemote(domain, callback);
					},5000)
				});
			// var params = {
			//     action: 'task_order_save_cookies',
			//     host_id: local.host_id,
			//     username: local.username,
			//     password: local.password,
			//     task_order_id: local.task.task_order_id,
			//     cookies: cookies
			// };
			// API.setParams(params);
			// API.ajaxPOST(function (ret) {
			//     if(ret.success == 1){
			//         notify('cookies 保存成功');
			//         callback && callback();
			//     }else{
			//         notify(ret.message + " <不保存>");
			//         callback && callback();
			//     }
			// }, function(){
			//     notify("cookie保存失败，重新保存");
			//     saveUserCookiesToRemote(domain, callback);
			// });
		})

	})
}


function setUserCookiesToWindows(cookies) {
	var cookies_string = 'thor,areaId,ipLoc-djd,ipLocation';
	//console.log('下单不在设置cookie');
	//return false;
	if (cookies === null || cookies === undefined) {
		console.log('cookies is null or undefined');
		return false;
	}
	if (cookies === null) {
		console.log("cookies is null");
		return false;
	}
	console.log("set cookies", cookies);

	var cookies_length = cookies.length;
	while(cookies_length--){
		var fullCookie = cookies[cookies_length];
		if(fullCookie.domain === '.passport.jd.com'){ console.log('.passport.jd.com OUT'); continue; }
		//if(fullCookie.name !== 'thor'){ console.log('非登陆状态项，不设置'); continue; }
		//去掉不设置的，全都设置
		//if(cookies_string.indexOf(fullCookie.name) == -1){ console.log('非需要设置项，不设置'); continue; }
		//seesion, hostOnly 值不支持设置,
		var newCookie = {};
		//var host_only = fullCookie.hostOnly == "false" ? false : true;
		var host_only = typeof(fullCookie.hostOnly)=='string'?(fullCookie.hostOnly=='false'?false:true):(fullCookie.hostOnly?true:false);
		newCookie.url = "http" + ((fullCookie.secure) ? "s" : "") + "://" + fullCookie.domain + fullCookie.path;
		newCookie.name = fullCookie.name;
		newCookie.value = fullCookie.value;
		newCookie.path = fullCookie.path;
		//newCookie.httpOnly = fullCookie.httpOnly == "false" ? false : true;
		newCookie.httpOnly = typeof(fullCookie.httpOnly)=='string'?(fullCookie.httpOnly=='false'?false:true):(fullCookie.httpOnly?!0:!1);
		//newCookie.secure = fullCookie.secure == "false" ? false : true;
		newCookie.secure = typeof(fullCookie.secure)=='string'?(fullCookie.secure=='false'?false:true):(fullCookie.secure?true:false);
		if(!host_only){ newCookie.domain = fullCookie.domain; }
		if (fullCookie.session === "true" && newCookie.expirationDate) { newCookie.expirationDate = parseFloat(fullCookie.expirationDate); }

		console.log(newCookie);
		setCookies(newCookie);
	}

}


/**
 * 标记订单异常， 增加备注信息
 */
function orderException(message,sender) {

	setHostStatus(3);//报道订单异常状态
	API = new Api();
	useLocal(function(local){

		var data = {
			"host_id": local.host_id,
			"task_order_id": local.task.task_order_id,
			"remark": message
		};
		API.setException(data,function (ret) {
			if (ret.success == 1) {
				notify('订单异常标记完成，重新开始任务', 'error');
				//任务重新开始
                console.warn('订单异常标记');
				startHost();
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message,'error');
				chrome.storage.local.set({isRunning: false}, function() {
					console.log("插件暂停");
				});
				return false;
			}else{
				notify(ret.message+"订单异常标记失败，10秒后重试");
				setTimeout(function(){
					orderException(message);
				},10000);
			}

		}, function () {
			notify('订单标记异常出现错误，5秒后刷新重试', 'error');
			setTimeout(function(){
				// location.reload();
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})

}

function orderGroupException(message,sender,task_order_id,orderIds) {
	setHostStatus(3);//报道订单异常状态
	API = new Api();
	useLocal(function(local){

		var data = {
			'order_group_id':local.task.id,
			"host_id": local.host_id,
			"task_order_id": task_order_id ? task_order_id : local.tasks[local.taskId].id,
			"remark": message
		};

		if(orderIds && orderIds.length >0){
			data.order_ids = orderIds;
		}	

		if(message == '未付款找不到订单' || message.indexOf('发票选择有误') != -1){
			var order_ids = [];
			local.task.orders.forEach(function(order){
				order_ids.push(order.id);
			})

			console.log('order_ids' , order_ids);
			data.order_ids = order_ids;
		}

		API.setGroupException(data,function (ret) {
			if (ret.success == 1) {
				notify('订单异常标记完成，重新开始任务', 'error');
				//任务重新开始
                console.warn('订单异常标记');
				startHost();
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message,'error');
				chrome.storage.local.set({isRunning: false}, function() {
					console.log("插件暂停");
				});
				return false;
			}else{
				notify(ret.message);
				notify('订单异常标记失败，10秒后重试');
				setTimeout(function(){
					orderGroupException(message,sender,task_order_id,orderIds)
				},10000);
			}

		}, function () {
			notify('订单标记异常出现错误，5秒后刷新重试', 'error');
			setTimeout(function(){
				// location.reload();
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})

}

function getBusinessAccountPassword(sender){
	useLocal(function(local){
		var API = new Api();
		var params = {
			"account_id": local.accountId
		};
		API.getBusinessAccountPassword(params,function (ret) {
			if (ret.success == 1) {
				console.log('获取最新账号信息');
				useLocal(function(local){
					var task = local.task || {};
					task.username = ret.data.username;
					task.password = ret.data.password;
					task.pay_password = ret.data.pay_password;
					task.email = ret.data.email;
					setLocal({task: task}, function(){
						chrome.tabs.sendMessage(sender.tab.id, {act: 'business_account_ready'});
					})
				})

			}else{
				console.log("获取最新账号信息失败，自动刷新重试", 'error');
				chrome.tabs.reload(sender.tab.id);
			}

		}, function () {
			console.log('获取最新账号信息错误，自动刷新重试', 'error');
			chrome.tabs.reload(sender.tab.id);
		});
	});
}



/**
 * m端操作结束切换到pc
 * @constructor
 */
function MChangedToPc(url){
	useLocal(function(local){
		var task = local.task;
        create_incognito_window({url: "about:blank"}, function(openTab){
			// if(task.cookies && false){
			if(task.cookies){
				var cookies_set = true;
				var username_label = 'pin';
				var cookies_length = task.cookies.length;
				while(cookies_length--){
					var cookie = task.cookies[cookies_length];
					if(cookie.name == username_label){
						if(decodeURIComponent(cookie.value) != task.username){
							cookies_set = false;
							break;
						}
						break;
					}
				}

				if(cookies_set){

					setUserCookiesToWindows(task.cookies);

					//cookie 的登录日志
					saveAccountLoginLog('cookie', 'pc');
				}


			}

			chrome.tabs.create({windowId: openTab.windowId, url: url});


		});
	});
}

/**
 * tab窗口选中
 * @param url
 */
// function tabsUpdateSelectedByUrl(url){
// 	chrome.tabs.query({url: url}, function(tabs){
// 		if(tabs.length > 0){
// 			var tab_id = tabs[0].id;
// 			chrome.tabs.update(tab_id, {selected: true}, function(details){
// 				console.log(details);
// 			});
// 		}
// 	});
// }

/**
 * 删除对应域下的cookies but排除
 * @param domain
 * @param but_str
 * @param callback
 */
function removeDomainCookies(domain, but_str, callback){
	//var domain = 'jd.com';
	//var but_str = '__jda';
	var details = {domain: domain};
	getCookies(details, function (cookies) {
		console.log(cookies);
		var cookies_length = cookies.length;
		while(cookies_length--){
			var c = cookies[cookies_length];
			console.log(c);
			if(but_str.indexOf(c.name) == -1){
				var c_url = "http" + ((c.secure) ? "s" : "") + "://" + c.domain + c.path;
				chrome.cookies.remove({url: c_url, name: c.name});
			}
		}

		setTimeout(function(){
			callback && callback();
		}, 7000);
	})
}

function removeDomainCookieByName(domain, names, callback){
	//var domain = 'jd.com';
	//var but_str = '__jda';

	var nameLen = names.length;
	while(nameLen--){
		var name = names[nameLen];
		var details = {domain: domain, name: name};
		getCookies(details, function (cookies) {
			console.log(cookies);
			var cookies_length = cookies.length;
			while(cookies_length--){
				var c = cookies[cookies_length];
				var c_url = "http" + ((c.secure) ? "s" : "") + "://" + c.domain + c.path;
				chrome.cookies.remove({url: c_url, name: c.name});
			}
		})
	}

	setTimeout(function(){
		callback && callback();
	}, 7000);

}

function changeIdCard(reason,callback){
	useLocal( function (local) {
		var API = new Api();
		var task = local.task;
		getSharedTaskWorks(function(sTask){

			var account_info = sTask.account_info;

			var params = {
				"host_id": local.host_id,
				"business_account_id": task.business_account_id ? task.business_account_id : null,
				"disabled_reason": reason
			};
			
			if(sTask.is_group){
				params.business_account_id = local.task.account_id;
			}
		
			
			API.changeIdCard(params , function (IdCard) {
				notify("已成功更换身份证");
				if (IdCard.success == 1) {
					var identity = IdCard.data;
					task.consignee.name = identity.name || task.consignee.name;
					task.identity_name = identity.name ? identity.name : task.identity_name;
					task.identity_card = identity.identity_card ? identity.identity_card : task.identity_card;
					account_info.identity_name = identity.name ? identity.name : task.identity_name;
					account_info.identity_card = identity.identity_card ? identity.identity_card : task.identity_card;
					setSharedTaskWorks({account_info:account_info},function(){	
					})

					var order_address = local.order_address;
					order_address.name = identity.name;
					setLocal({order_address:order_address},function(){
						setLocal({"task": task}, function(){
							callback && callback(identity)
						});
					})

					
				} else {
					notify('更换身份证失败');
					notify(IdCard.message);

					//setTimeout(function(){
					//    changeIdCard(reason, callback)
					//}, 10000);
				}
			}, function () {
				//setTimeout(function(){
				//    changeIdCard(reason, callback)
				//}, 10000);
			});
		})

	})
}



/**
 * 发送附加信息到tracker
 * @param note
 * @param callback
 */
function send_notes_to_tracker(note, callback) {
    if (note) {
        useLocal(function (local) {
            var extension_id = tracker_env[local.env];
            message_export(extension_id, {act: 'notes', data: note}, function () {
                callback && callback();
            }, null);
        });
    }else{
        callback && callback();
	}
}

/**
 * 发送基础信息到tracker
 * @param obj
 * @param callback
 */
function send_trackerInfo_to_tracker(data) {
    if (data) {
        useLocal(function (local) {
            var extension_id = tracker_env[local.env];
            message_export(extension_id, {act: 'trackerInfo', data: data}, null, null);
        });
    }
}


/**
 * 发送storage数据到tracker
 */
function send_storage_to_tracker(callback){
	useLocal(function(local){

		getSharedTaskWorks(function(shareTask){


			var extension_id = tracker_env[local.env];

			var manifest = chrome.runtime.getManifest();
			var task_details = {};
			if(local.task){
				task_details.order_id = local.task.task_order_oid;
				task_details.account_name = local.task.username;
				task_details.jd_order_id = shareTask.jd_order_id;
			}

			task_details.type = manifest.name;
			task_details.type_version = manifest.version;

			message_export(extension_id, {act: 'task_details', data: task_details}, function(){
				callback && callback();
			}, function(){
				notify("没有 赤兔，跑不起来！尝试重启赤兔", 10, function(){
                    chrome.management.getAll(function(extensionInfo){
                    	var extensionInfo_tracker = extension_id;
                    	var extensionInfo_tracker_find = false;
                        for(var i in extensionInfo){
                        	console.log('extensionInfo',extensionInfo[i].id);
                        	if(extensionInfo[i].id == extensionInfo_tracker){
                                extensionInfo_tracker_find = true;
                                chrome.management.setEnabled(extensionInfo_tracker, false, function() {
                                    chrome.management.setEnabled(extensionInfo_tracker, true,function () {
                                    	console.log('启用赤兔成功');
                                        setTimeout(function () {
                                            send_storage_to_tracker(callback)
                                        },20e3)
                                    });
                                });
                                break;
							}
						}
						if(extensionInfo_tracker_find==false){
                        	notify('奥迪自动关闭');
                            extension_self_disabled();
                        }
					})
				});
				// callback && callback();
			});
		});

	})
}

/**
 * 告诉tracker让他更新
 * @param callback
 */
function update_check_to_tracker(callback){
	useLocal(function(local){
		var extension_id = tracker_env[local.env];
		message_export(extension_id, {act: 'update_check', data: local}, function(){
			callback && callback();
		}, null);
	});
}

/**
 * 插件消息输出(输出到其他的插件)
 * @param extension_id
 * @param data
 * @param success
 * @param error
 */
function message_export(extension_id, data, success, error){

	chrome.management.get(extension_id, function(info){
		if(info && info.enabled){
			console.log('message export request', extension_id, data);
			chrome.runtime.sendMessage(extension_id, data, function(response){
				console.log('message export response', response);
				if(response === undefined || response.success != 1){
					message_export(extension_id, data, success, error);
				}else{
					success && success();
				}
			});
		}else{
			error && error();
		}
	})
}

/**
 *
 * @param installed
 * @param required
 * @returns {boolean} true可用，false不可用
 */
function compare_versions(env, installed, required){

	//var installed = '0.4.21';
	//var required = '0.4.30';

	if(installed === required){
		return true;
		//当前版本可以试用无需更新，或者无更新版本
	}

	var a = installed.split('.');
	var b = required.split('.');

	if(parseInt(a[0]) < parseInt(b[0])) return false;
	if(parseInt(a[0]) > parseInt(b[0])) return true;

	if(parseInt(a[1]) < parseInt(b[1])) return false;
	if(parseInt(a[1]) > parseInt(b[1])) return true;

	if(parseInt(a[2]) < parseInt(b[2])) return false;
	if(parseInt(a[2]) > parseInt(b[2])) return true;

	if(parseInt(a[3]) < parseInt(b[3])) return false;
	if(parseInt(a[3]) > parseInt(b[3])) return true;

	return true;
}

/**
 * 删除jd cookies 去登陆
 * @param sender
 */
function wxRemoveCookieLoginStart() {

	//删除jd.com cookies
	removeDomainCookies("jd.com", '', function () {
		//设置步骤为 打开推广连接
		setHostStep(_host_step.open_promotion_link, function () {
			//进行步骤重置
			watchDogTimeOut();

		})
	});

}

/**
 * 任务开始
 */
function taskStart(){

    //任务需要自动开始
    setLocal({isRunning: true}, function(){
        send_trackerInfo_to_tracker({
			"ext_name":chrome.runtime.getManifest().name,
			"ext_version":chrome.runtime.getManifest().version
		});
		useLocal(function(local){
			if(local.clearCookies){
				// setLocal({clearCookies:false},taskStartBoot)
				taskStartBoot();
			}else{
				startHost();
			}
		})
        
    });

	// //任务开始,去获取任务
	// //taskStartBoot();
    //
	// //检查插件活动状态,等待返回结果后在执行,boot
	// checkActiveExtension();
    //
    //
	// setTimeout(function(){
	// 	if(!checkActiveExtensionFinished){
	// 		checkActiveExtension();
	// 	}
	// }, 10000);
}



/**
 * 检查插件活动状态, 之后等待返回结果
 */
function checkActiveExtension(){
	checkActiveExtensionFinished = false;
	useLocal(function(local) {
		var extensionId = tracker_env[local.env];
		message_export(extensionId, {act: "extension_active_check"});
	});
}

/**
 * 插件活动状态检查完毕,任务开始执行
 */
function checkActiveExtensionFinish(){
	console.log("checkActiveExtensionFinish");
	checkActiveExtensionFinished = true;

    //任务需要自动开始
    setLocal({isRunning: true}, function(){
        taskStartBoot();
    });
}

/**
 * 生成两位连续的一串数字
 * @returns {string}
 */
function randomSerialNumber(){
	var digit = random(2,10);
	var segmentNumber = Math.ceil(digit/2);
	var segment = '';
	while(segmentNumber--){
		var r = random(0,9);
		var s = r==9?0:r+1;
		segment+=(r.toString() + s.toString());
	}
	console.log(digit, segment);
	return segment;
}
/**
 * 生成一个随机数
 * @param {start} [随机数的开始值]
 * @param {end} [随机数的结束值]
 * @returns {number} [返回随机的结果]
 */
function random(start, end) {
	return Math.round(Math.random() * (end - start) + start);
}

function sendMessageToTab(msg, sender){

	if(msg.tabId>0){
		msg.data.sender = sender;
		chrome.tabs.sendMessage(msg.tabId, msg.data);
	}else{
		console.log(msg);
		chrome.tabs.query({url: msg.url}, function(tabs){
			console.log(tabs);
			if(tabs.length > 0){
				var tabId = tabs[0].id;
				msg.data.sender = sender;
				chrome.tabs.sendMessage(tabId, msg.data);
			}else{
				chrome.tabs.sendMessage(sender.tab.id, {act: 'url_error', data: msg.data});
			}
			//chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: 123, text: 'text'});
		});
	}
}


function reportHostRunningMessage(message, timeout){
	var API = new Api();

	useLocal(function(local){
		var data = {
			host_id: local.host_id,
			running_text: message,
			expect_time: timeout
		};
		API.setHostRuningMessage(data, function(ret){
			console.log("host running message ok");
		}, function(){
			console.log('报告状态,放弃重试');
			//setTimeout(function(){
			//    reportHostRunningMessage(message, timeout);
			//}, 2000);
		});
	});

}

function reportHostMessage(message, timeout){

    // if(applicationName != 'worm' && applicationName != 'group_worm' && applicationName != 'app_group_payment' && applicationName != 'app_payment'){ //不是worm 不报告
    //     console.log(applicationName, 'hostmessage not report');
    //     return false;
    // }

	var API = new Api();
	useLocal(function(local){
		var orderDetails = {};
		if(local.task){
			orderDetails.order_id = local.task.task_order_id;
			orderDetails.order_oid = local.task.task_order_oid;
			orderDetails.start_at = local.task.start_at;
			orderDetails.item_amount = local.task.amount;
			orderDetails.is_mobile = local.task.is_mobile;
			orderDetails.worldwide = local.task.worldwide;
			orderDetails.expect_order_time = local.task.expect_order_time;
			orderDetails.bank_user = local.task.bank_user;
			orderDetails.card_no = local.task.card_no;
			orderDetails.admin_real_name = '';
		}else{
			orderDetails.order_id = '';
			orderDetails.order_oid = '';
			orderDetails.start_at = '';
			orderDetails.item_amount = '';
			orderDetails.is_mobile = '';
			orderDetails.worldwide = '';
			orderDetails.expect_order_time = '';
			orderDetails.bank_user = '';
			orderDetails.card_no = '';
			orderDetails.admin_real_name = '';

		}
		var sharedTaskWorks = local.sharedTaskWorks || {};
		var data = {
			id:sharedTaskWorks.hostMessageId,
			type:sharedTaskWorks.hostMessageType,
			user_id: sharedTaskWorks.admin_id,
			body: message,
			timeout:  timeout > -1 ? timeout : undefined,
			host_id: local.host_id
		}
		//data = $.extend(data, orderDetails);// 主机列表已修改，不在使用其他信息
		data.user_id = data.user_id || 0;
		API.reportHostMessage(data, function(){
			console.log("host message ok");
		}, function(){
			console.log('报告状态,放弃重试');
			//setTimeout(function(){
			//    reportHostMessage(message, timeout);
			//}, 1000)
		})
	});
}
function fullDateTime(){
	var dt = new Date();
	var y = dt.getFullYear();
	var m = (dt.getMonth() + 1);
	var d = dt.getDate();

	var h = dt.getHours();
	var i = dt.getMinutes();
	var s = dt.getSeconds();

	this.format = function(num){
		return num<10 ? '0' + num.toString() : num;
	}

	return y + '-' + this.format(m) + '-' + this.format(d) + ' ' + this.format(h) + ':' + this.format(i) + ':'+ this.format(s);
}

/**
 * 保存修改生日的时间
 */
function updateAccountBirthday(){
	var API = new Api();
	API.updateAccountBirthday();
}

/**
 * 保存修改昵称的时间
 */
function updateAccountNikename(){
	var API = new Api();
	API.updateAccountNikename();
}


/**
 * 补充hk的登录cookie 值
 */
function add360hkCookieThor(callback){
	chrome.cookies.getAll({domain:'jd360.hk', name: 'thor'}, function(thor360JdHkCookie){
		if(thor360JdHkCookie.length==0){
			chrome.cookies.getAll({domain:'jd.com', name: 'thor'}, function(thorJdCookie){
				console.log(thorJdCookie);
				if(thorJdCookie.length>0){
					var thor360JdCookie = thorJdCookie[0];
					thor360JdCookie.domain = '.jd360.hk';
					delete thor360JdCookie.session;
					delete thor360JdCookie.hostOnly;
					thor360JdCookie.url = "http" + ((thor360JdCookie.secure) ? "s" : "") + "://" + thor360JdCookie.domain + thor360JdCookie.path;
					setCookies(thor360JdCookie, function(){
						//chrome.cookies.getAll({domain:'360jd.hk', name: 'thor'}, function(cc){console.log('123',cc)})
						callback && callback();
					})
				}else{
					console.log("add360hkCookieThor jd thor not found ");
					callback && callback();
				}
			});
		}else{
			console.log("hk thor", thor360JdHkCookie);
			callback && callback();
		}
	})
}

//保存是否使用cookie登录
// function saveLoginType(){

//     useLocal(function(local){
//         if(local.task.is_mobile != 1){
//             var name = 'thor';
//         }else{
//             var name = 'pt_key';
//         }
//         var task_cookies = local.task.cookies;
//         console.log('task_cookies:' ,task_cookies);
//         if(task_cookies.length > 0){
//             getCookies({domain:'.jd.com',name:name},function(cookies){
//                 var use_cookies = 0;
//                 console.log('this cookies:',cookies);
//                 for(var t_c in task_cookies){
//                     console.log(task_cookies[t_c]);
//                     if(task_cookies[t_c].name == name && task_cookies[t_c].domain == '.jd.com'){
//                         console.log('duibi' + cookies[0].value + '=' + task_cookies[t_c].value);
//                         if(cookies[0].value == task_cookies[t_c].value){
//                             //使用coolie登录
//                             console.log('使用cookies登录');
//                             use_cookies = 1;
//                         }else{
//                             console.log('未使用cookies登录');
//                         }
//                     }

//                 }
//                 doSaveLoginType(use_cookies);
//             })
//         }else{
//             doSaveLoginType(0);
//         }


//    })
// }

//保存登录方式
function doSaveLoginType(use_cookies){
	var api = new Api();
	api.saveLoginType({use_cookies:use_cookies,login_kind:1},
		function(ret){
			if(ret == 1){
				console.log('保存登录方式成功');
			}else{
				console.log('保存登录方式失败');
			}
		},
		function(){
			console.log('请求保存登录方式接口失败');
			setTimeout(function(){
				doSaveLoginType(use_cookies);
			},3000);
		}
	)
}

//获取支付信息
function getPaymentInfo(sender){

	useLocal(function(local){
		var task = local.task;
		if(task.get_payment_info_last_time && task.get_payment_info){
			var between_time = parseInt(new Date().getTime() - task.get_payment_info_last_time);
			if(between_time > 3*180*1000){
				//间隔时间大于3分钟
				console.log('大于3分钟重新分配');
				doGetPaymentInfo(sender);
			}else{
				//小于三分钟的直接返回已有数据
				console.log('小于三分钟的直接返回已有数据');
				chrome.tabs.sendMessage(sender.tab.id, {act: 'get_payment_info_result', data:task.get_payment_info,error:0});
			}
		}else{
			doGetPaymentInfo(sender);
		}


	})
}

function doGetPaymentInfo(sender){
	useLocal(function(local){
		var task = local.task;
		var api = new Api();
		api.getPaymentInfo(
			function(ret){
				console.log(ret);
				if(ret.success == 1){
					console.log('获取银行信息成功');
					task.get_payment_info_last_time = new Date().getTime();
					task.get_payment_info = ret.data;

					setLocal({task:task},function(){
						chrome.tabs.sendMessage(sender.tab.id, {act: 'get_payment_info_result', data:ret.data,error:0});
					})

				}else{
					console.log(ret.message);
					notify(ret.message + ', 10S后重新获取');
					setHostStatus(1414001);
					setTimeout(function(){
						doGetPaymentInfo(sender);
					},10*1000);
					//chrome.tabs.sendMessage(sender.tab.id, {act: 'get_payment_info_result', error:1});
				}
			},
			function(){
				console.log('请求获取银行信息接口失败');
				setTimeout(function(){
					doGetPaymentInfo(sender);
				},3000);
			}
		)

	})
}

//获取支付信息
function getPayState(sender){
	var api = new Api();
	api.getPayState(
		function(ret){
			console.log(ret);
			if(ret.success == 1){
				console.log('获取付款状态成功');
				chrome.tabs.sendMessage(sender.tab.id, {act: 'get_pay_state_result', data:ret.data,error:0});
			}else{
				console.log(ret.message);

				getTaskWorks(function(task){

					if(ret.message.indexOf('未找到订单对应的有效付款流水') != -1){

	                    task.no_pay_data_nums = task.no_pay_data_nums || 0;
	                    if(task.no_pay_data_nums > 10){
	                        setSharedTaskWorks({payment_clicked:false},function(){
	                            console.log('未找到订单对应的有效付款流水,重置点击付款标识');
	                            // do_reload_page();
	                            setTaskWorks({no_pay_data_nums:0},function(){
	                            	chrome.tabs.reload(sender.tab.id);
	                            })
	                            
	                        })
	                    }else{
	                        setTaskWorks({no_pay_data_nums:++task.no_pay_data_nums},function(){
	                            // do_reload_page();
	                            chrome.tabs.reload(sender.tab.id);
	                        })
	                    }
	                    
	                    
	                }else{
	                	notify(ret.message);
	                	setTimeout(function(){
							chrome.tabs.reload(sender.tab.id);
						},3000)
	                }

                })
				// notify(ret.message);
				// notify(ret.message + ', 10S后重新获取');
				// setTimeout(function(){
				// 	chrome.tabs.reload(sender.tab.id);
				// },3000)

				// setHostStatus(1414001);
				// setTimeout(function(){
				//     getPayState(sender);
				// },10*1000);
				//chrome.tabs.sendMessage(sender.tab.id, {act: 'get_payment_info_result', error:1});
			}
		},
		function(){
			console.log('请求获取付款状态接口失败');
			setTimeout(function(){
				getPayState(sender);
			},3000);
		}
	)
}

function getGroupPayState(sender){
	var api = new Api();
	api.getGroupPayState(
		function(ret){
			console.log(ret);
			if(ret.success == 1){
				console.log('获取付款状态成功');
				chrome.tabs.sendMessage(sender.tab.id, {act: 'get_pay_state_result', data:ret.data,error:0});
			}else{
				console.log(ret.message);

				getTaskWorks(function(task){

					if(ret.message.indexOf('未找到') != -1 && ret.message.indexOf('对应的有效付款流水') != -1){

	                    task.no_pay_data_nums = task.no_pay_data_nums || 0;
	                    if(task.no_pay_data_nums > 10){
	                        setSharedTaskWorks({payment_clicked:false},function(){
	                            console.log('未找到订单对应的有效付款流水,重置点击付款标识');
	                            // do_reload_page();
	                            setTaskWorks({no_pay_data_nums:0},function(){
	                            	chrome.tabs.reload(sender.tab.id);
	                            })
	                        })
	                    }else{
	                        setTaskWorks({no_pay_data_nums:++task.no_pay_data_nums},function(){
	                            // do_reload_page();
	                            chrome.tabs.reload(sender.tab.id);
	                        })
	                    }
	                    
	                    
	                }else{
	                	notify(ret.message);
	                	setTimeout(function(){
							chrome.tabs.reload(sender.tab.id);
						},3000)
	                }

                })
				// notify(ret.message);
				// notify(ret.message + ', 10S后重新获取');
				// setTimeout(function(){
				// 	chrome.tabs.reload(sender.tab.id);
				// },3000)

				// setHostStatus(1414001);
				// setTimeout(function(){
				//     getPayState(sender);
				// },10*1000);
				//chrome.tabs.sendMessage(sender.tab.id, {act: 'get_payment_info_result', error:1});
			}
		},
		function(){
			console.log('请求获取付款状态接口失败');
			setTimeout(function(){
				getGroupPayState(sender);
			},3000);
		}
	)
}

//保存账号级别
// function saveAccountLevel(data){
// 	useLocal(function(local){
// 		data.id = local.accountId;
// 		var api = new Api();
// 		api.saveAccountLevel(data,
// 			function(ret){
// 				console.log(ret);
// 				if(ret.success == 1){
// 					console.log('保存账号等级成功');
// 				}else{
// 					console.log(ret.message);
// 					setTimeout(function(){
// 						saveAccountLevel(data);
// 					},10*1000);
// 				}
// 			},
// 			function(){
// 				console.log('请求保存账号等级接口失败');
// 				setTimeout(function(){
// 					saveAccountLevel(data);
// 				},3000);
// 			}
// 		)
// 	})
// }

//保存收货地址到服务器
function saveAddressToRemote(sender){
	var API = new Api();
	useLocal(function(local){

		if(local.order_address !== undefined) {
			notify("保存到服务器后跳转");
			var params = {
				"action": "task_save_consignee",
				"host_id": local.host_id,
				"username": local.username,
				"password": local.password,
				"task_order_id": local.task.task_order_id,

				"name": local.order_address.name,
				"mobile": local.order_address.mobile,
				"province": local.order_address.province,
				"city": local.order_address.city,
				"area": local.order_address.area,
				"street": local.order_address.street,
				"address": local.order_address.short_address
			};
			// API.setParams(params);
			API.saveConsignee(params,function (ret) {

				if (ret.success == 1) {
					notify('地址更新成功', 'success');

				}else if(ret.message == 'host_id不一致'){
					setHostStatus(4);//host_id 不一致
					notify(ret.message);
					chrome.storage.local.set({isRunning: false}, function() {
						notify("插件暂停", true);
					});
					return false;

				}else{
					notify("地址更新失败! 5秒刷新后重试"+ret.message);
					setTimeout(function(){
						// location.reload();
						chrome.tabs.reload(sender.tab.id);
					},5*1000);
				}

			}, function () {
				notify("地址更新出错，5秒刷新后重试", 'error');
				setTimeout(function(){
					chrome.tabs.reload(sender.tab.id);
				},5*1000);
			});
		}else{
			notify("保存到服务器de收货地址没找到，未修改");
			console.log("保存到服务器的收货地址是",local.order_address);
			// callback && callback();
		}
	})
}

//保存订单包收货地址
function saveGroupAddressToRemote(sender){
	var API = new Api();
	useLocal(function(local){

		if(local.order_address !== undefined) {
			notify("保存到服务器后跳转");
			var params = {
				'order_group_id':local.task.id,
				"host_id": local.host_id,
				"name": local.order_address.name,
				"mobile": local.order_address.mobile,
				"province": local.order_address.province,
				"city": local.order_address.city,
				"area": local.order_address.area,
				"street": local.order_address.street,
				"address": local.order_address.short_address
			};
			// API.setParams(params);
			API.saveGroupConsignee(params,function (ret) {

				if (ret.success == 1) {
					notify('地址更新成功', 'success');

				}else if(ret.message == 'host_id不一致'){
					setHostStatus(4);//host_id 不一致
					notify(ret.message);
					chrome.storage.local.set({isRunning: false}, function() {
						notify("插件暂停", true);
					});
					return false;

				}else{
					notify("地址更新失败! 5秒刷新后重试"+ret.message);
					setTimeout(function(){
						// location.reload();
						chrome.tabs.reload(sender.tab.id);
					},5*1000);
				}

			}, function () {
				notify("地址更新出错，5秒刷新后重试", 'error');
				setTimeout(function(){
					chrome.tabs.reload(sender.tab.id);
				},5*1000);
			});
		}else{
			notify("保存到服务器de收货地址没找到，未修改");
			console.log("保存到服务器的收货地址是",local.order_address);
			// callback && callback();
		}
	})
}

//获取订单包信息
function getGroupOrdersInfo(sender){
	var API = new Api();
	useLocal(function(local){

		API.getGroupOrdersInfo(function (ret) {

			if (ret.success == 1) {
				notify('获取订单包信息成功', 'success');

			}else{
				notify(ret.message);
			}

			var username_right = false;

			var details = {domain:'jd.com'};
			getCookies(details,function(cookies){
				for(var i in cookies){
					var cookie = cookies[i];
					//console.log(cookie.name+'='+cookie.value);
					if(cookie.name == 'pin'){

						if(decodeURI(cookie.value) == local.account.username || decodeURIComponent(cookie.value) == local.account.username){
							username_right = true;
							// continue;
						}
					}

				}
			

				if(local.task.is_mobile == 1){

					if(username_right){
						// delCookies();
						chrome.tabs.sendMessage(sender.tab.id,{act:'get_group_orders_info_result',ret:ret});
					}else{
						notify('账号登录与任务账号不一致，任务账号：' + local.account.username + ',3S后清除cookie');
						// chrome.cookies.remove(details);
						// delCookies();
						setTimeout(delCookies,3*1000);
						return false;
					}

				}else{
					chrome.tabs.sendMessage(sender.tab.id,{act:'get_group_orders_info_result',ret:ret});
				}

			})		

		}, function () {
			notify("请求获取订单包信息接口，5秒刷新后重试");
			setTimeout(function(){
				getGroupOrdersInfo(sender)
				// chrome.tabs.reload(sender.tab.id);
			},5*1000);
		});
		
	})
}

//保存订单包信息
// function saveGroupBusinessOrdersInfo(group_business_oids){
// 	var API = new Api();
// 	useLocal(function(local){

// 		API.saveGroupBusinessOrdersInfo({order_info:group_business_oids},function (ret) {

// 			if (ret.success == 1) {
// 				notify('保存订单包信息成功', 'success');

// 			}else{
// 				notify(ret.message);
// 				saveGroupBusinessOrdersInfo(group_business_oids);
// 			}

// 			// chrome.tabs.sendMessage(sender.tab.id,{act:'get_group_orders_info_result',ret:ret});

// 		}, function () {
// 			notify("请求保存订单包信息接口，5秒刷新后重试");
// 			setTimeout(function(){
// 				saveGroupBusinessOrdersInfo(group_business_oids);
// 				// chrome.tabs.reload(sender.tab.id);
// 			},5*1000);
// 		});
		
// 	})
// }



function reportProductStockout(message,sender){
	notify(message+"标记异常");
	var API = new Api();
	useLocal(function(local){

		var data = {
			"host_id": local.host_id,
			"task_order_id": local.task.task_order_id,
			"remark": message
		};
		API.setException(data,function (ret) {
			if (ret.success == 1) {
				notify('产品无货标记完成，重新开始任务');
				//callback && callback();
				//任务重新开始
                console.warn('产品无货标记完成');
				startHost();
				// tabCommon.sm(taskVars, '.startHost');
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message);
				chrome.storage.local.set({isRunning: false}, function() {
					alertify.log("插件暂停");
				});
				return false;
			}else{
				notify(ret.message + "产品无货标记失败，10秒后重试");
				setTimeout(function(){
					reportProductStockout(message,sender);
				},10000);
			}

		}, function () {
			notify('产品无货标记出现错误，5秒后刷新重试');
			setTimeout(function(){
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})
}

function reportGroupProductStockout(message,sender){
	notify(message+"标记异常");
	var API = new Api();
	useLocal(function(local){

		var data = {
			'order_group_id':local.task.id,
			"host_id": local.host_id,
			"task_order_id": local.tasks[local.taskId].id,
			"remark": message
		};
		API.setGroupException(data,function (ret) {
			if (ret.success == 1) {
				notify('产品无货标记完成，重新开始任务');
				//callback && callback();
				//任务重新开始
                console.warn('产品无货标记完成');
				startHost();
				// tabCommon.sm(taskVars, '.startHost');
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message);
				chrome.storage.local.set({isRunning: false}, function() {
					alertify.log("插件暂停");
				});
				return false;
			}else{
				notify(ret.message + "产品无货标记失败，10秒后重试");
				setTimeout(function(){
					reportGroupProductStockout(message,sender)
				},10000);
			}

		}, function () {
			notify('产品无货标记出现错误，5秒后刷新重试');
			setTimeout(function(){
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})
}




/**
 * 任务添加备注
 * @param message
 */
function addTaskOrderRemark(message,sender, callback){
	notify("添加备注"+message);
	var API = new Api();
	useLocal(function(local){

		var data = {
			"host_id": local.host_id,
			"task_order_id": local.task.task_order_id,
			"remark": message
		};
		API.setException(data,function (ret) {
			if (ret.success == 1) {
				notify('添加备注完成');
				callback && callback();
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message);
				chrome.storage.local.set({isRunning: false}, function() {
					alertify.log("插件暂停");
				});
				return false;
			}else{
				notify("添加备注失败，10秒后重试");
				setTimeout(function(){
					addTaskOrderRemark(message,sender,callback);
				},10000);
			}

		}, function () {
			notify('添加备注出现错误，5秒后刷新重试');
			setTimeout(function(){
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})

}

/**
 * 任务添加备注
 * @param message
 */
function addGroupTaskOrderRemark(message,sender, callback){
	notify("添加备注"+message);
	var API = new Api();
	useLocal(function(local){

		var data = {
			'order_group_id':local.task.id,
			"host_id": local.host_id,
			"task_order_id": local.tasks[local.taskId].id,
			"remark": message
		};
		API.setGroupException(data,function (ret) {
			if (ret.success == 1) {
				notify('添加备注完成');
				callback && callback();
			}else if(ret.message == 'host_id不一致'){
				setHostStatus(4);//host_id 不一致
				//console.log(ret.message);
				notify(ret.message);
				chrome.storage.local.set({isRunning: false}, function() {
					alertify.log("插件暂停");
				});
				return false;
			}else{
				notify("添加备注失败，10秒后重试");
				setTimeout(function(){
					addGroupTaskOrderRemark(message,sender, callback)
				},10000);
			}

		}, function () {
			notify('添加备注出现错误，5秒后刷新重试');
			setTimeout(function(){
				chrome.tabs.reload(sender.tab.id);
			},5*1000)
		});

	})

}

//获取订单付款信息（急速保存用到）
function getOrderPaymentInfo(sender){
	useLocal(function(local){
		var api = new Api();
		api.getOrderPaymentInfo(
			function(ret){
				chrome.tabs.sendMessage(sender.tab.id, {act: 'get_order_payment_info_result', ret: ret});
			},
			function(){
				console.log('请求获取订单付款信息接口失败');
				setTimeout(function(){
					getOrderPaymentInfo(sender);
				},3000);
			}
		)
	})
}


//保存订单审核状态
function saveBusinessCheckOrder(data){
	var api = new Api();


	useLocal(function(local){
		data.business_account_id = local.accountId;

		getSharedTaskWorks(function(sTask){
			if(!sTask.is_group){
				data.order_id = local.task.task_order_id;
				data.towh_id = local.task.towh_id;
			}
		

			if(data.towh_id && data.order_id){
				console.log('保存订单审核状态');
			}else{
				console.log('保存订单包母订单审核状态');
				data.order_group_work_id = local.task.order_group_work_id;
				data.order_group_id = local.task.id;
			}
			
			api.saveBusinessCheckOrder(data, function(ret){
				if(ret.success == 1){
					console.log("保存检测订单审核状态成功");
				}else{
					console.log(ret.message);
				}

				// callback && callback();
			}, function(){
				console.log("请求保存检测订单审核状态接口失败,重试");
				setTimeout(function(){
					saveBusinessCheckOrder(data)
				}, 5000);
			});

		})
	})
}

// function checkIsLogin(client_code){
//     var url = 'https://ai.jd.com/index_new.php?app=Newuser&action=isNewuser&callback=jsonpCallbackIsNewuser&_=' + new Date().getTime();
//     $.ajax({
//         type: "GET",
//         url: url,
//         dataType: "text",
//         success:function(msg){
//             var reg = /\((.+)\)/;
//             if(reg.exec(msg).length > 1){
//                 var res = JSON.parse(reg.exec(msg)[1]);
//                 if(res.islogin == true){
//                     saveLoginType(client_code)
//                 }else{
//                     login_type = null;//清除登录方式标识
//                 }
//             }else{
//                 console.log('登录方式获取失败');
//                 return false;
//             }
//         }
//     })
// }

//检测登录
// function checkLoginType(domain,client_code){

//     if(login_type){
//         //有登录方式标识
//         checkIsLogin(client_code);

//     }else{
//         console.log('暂无登录方式标识');
//     }
//     // useLocal(function(local){
//     //     var username_right = false;
//     //     var details = {domain:domain};
//     //     getCookies(details,function(cookies){
//     //         for(var i in cookies){
//     //             var cookie = cookies[i];
//     //             if(cookie.name == 'pin' || cookie.name == 'pt_pin'){

//     //                 if(decodeURI(cookie.value) == local.task.username || decodeURIComponent(cookie.value) == local.task.username){
//     //                     username_right = true;
//     //                     continue;
//     //                 }
//     //             }
//     //         }

//     //         if(username_right){
//     //             //确定是登录了 并且是任务的账号
//     //             if(login_type){
//     //                 //有登录方式标识
//     //                 saveLoginType(client_code);
//     //             }else{
//     //                 console.log('暂无登录方式标识');
//     //             }

//     //         }else{
//     //             console.log('暂无登录或与当前登录账号不一致');
//     //         }
//     //     })

//     // })
// }

//保存登录方式
function saveAccountLoginLog(type, clientCode){

	var loginTypes = {account: 1, cookie: 2};

	useLocal(function(local){
		var api = new Api();
		var data = {
			account_id: local.accountId,
			type: loginTypes[type],
			client_code: clientCode,
			host_code: local.host_id,
			ext_name: local.applicationName,
			targetable_id: local.tasks[local.taskId].id

		};

		api.saveLoginType(data,
			function(ret){
				if(ret.success == 1){
					console.log('保存登录方式成功');
				}else{
					console.log('保存登录方式失败' + ret.message);
				}
			},
			function(){
				console.log('请求保存登录方式接口失败');
				setTimeout(function(){
					saveAccountLoginLog(type, clientCode);
				},3000);
			}
		)
	})
}


//获取点点任务
function getClankTask(order_id, callback){
	var API = new Api();
	API.getClankTask(order_id, function (data) {
		if(data.success == 1){
			if(data.data.type == 'look_order'){
				setLocal({lookOrderTask:data.data, accountId: data.data.account_id},callback)
			}else if(data.data.type == 'product_detail'){
				setLocal({productDetailTask:data.data, accountId: data.data.account_id},callback)
			}else if(data.data.type == 'product_category_2'){
				setLocal({productCategory2Task:data.data, accountId: data.data.account_id},callback)
			}else if(data.data.type == 'product_category_3'){
				setLocal({productCategory3Task:data.data, accountId: data.data.account_id},callback)
			}else if(data.data.type == 'front_page'){
				setLocal({frontPageTask:data.data, accountId: data.data.account_id},callback)
			}else{
				console.error('不支持的点点任务类型');
				callback&&callback();
			}
		}else{
			console.log('no clank task', data.message);
			callback&&callback();
		}
	},function (err) {
		console.error('获取点点任务失败');
		console.error(err);
		callback&&callback();
	})
}


function getUUID() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
}

function getSearchAlikeKeywordGroup(keyword,callback){
	this.genPvid = function() {
		var a = "";
		return a || (a = Math.round(1e12 * Math.random())),
			(a.toString(36) + "." + (new Date).getTime().toString(36)).split("").reverse().join("")
	}

	var k = keyword;
	var q = this.genPvid();
	var keyword = encodeURIComponent(k);
	var time = (new Date).getTime();
	var r = "https://dd-search.jd.com/?ver=2&zip=1&key="+keyword+"&pvid=" + q + "&t="+time+"";

	$.ajax({
		url: r,
		dataType: "json",
		scriptCharset: "utf-8",
		//jsonp: "callback",
		cache: !0,
		success: function(b) {
			callback(b)
		}
	})

}

//获取远程保存的账号cookie
function getJdAccountDetails(accountId, callback){

	var API = new Api();
	API.getBusinessAccountPassword({account_id: accountId},function (ret) {
		if (ret.success == 1) {
			//console.log('获取最新账号信息');
			var account = {
				id: accountId,
				username: ret.data.username,
				password: ret.data.password,
				pay_password: ret.data.pay_password,
				cookies: []
			}

			setLocal({account: account}, function(){
				API.getAccountCookies(accountId, function(cookies){
					if(cookies){
						account.cookies = cookies;
						setLocal({account: account}, function(){
							callback&&callback(account);
						});
					}else{
						console.log('account no cookie');
						callback&&callback();
					}
				}, function(request){
					console.warn('get account cookie json error');
					console.log(request);
					callback&&callback();
				})
			})

		}else{
			console.error("获取最新账号信息失败");
			callback&&callback();
		}

	}, function () {
		console.log('获取最新账号信息错误');
        getJdAccountDetails(accountId, callback);
	});
}

//获取ua
function getUADetails(accountId, _clientType, callback){
    notify('当前 _clientType 为 '+_clientType);

    var API_ua = new Api();
    API_ua.getAccountUA(_clientType,accountId,function (ret) {
        if (ret) {
        	if(_clientType=='pc' && ret.ua.indexOf('iPad')>-1){
                //清理掉pc下iPad
                saveUADetails(accountId,_clientType)
                callback && callback();
			}else if(ret.ua.indexOf('MSIE')>-1){
        		//清理掉ie的ua
                saveUADetails(accountId,_clientType)
                callback && callback();
			}else{
                setUserAgent(ret.ua);
                setTimeout(function () {
                    callback && callback();
                },1000)
			}
        }else{
            saveUADetails(accountId,_clientType)
            callback && callback();
        }
    }, function (e) {
        saveUADetails(accountId,_clientType)
        callback && callback();
    });
}

function saveUADetails(accountId,clientType) {
    if(clientType=='m'){
        var ua_l = _MobileUserAgent.length;
        var i = parseInt(Math.random()*ua_l);
        var _saveUserAgent = _MobileUserAgent[i];
    }else if(clientType=='pc'){
        var ua_l = useragents.length;
        var i = parseInt(Math.random()*ua_l);
        var _saveUserAgent = useragents[i];
    }
    console.warn('设置ua: ',_saveUserAgent);
    setUserAgent(_saveUserAgent);
    var API_ua = new Api();
    API_ua.saveAccountUA(clientType,{account_id:accountId,userAgent:_saveUserAgent});
}

//保存订单评价状态saveCommentState
function saveCommentState(data){
	var api = new Api();
	useLocal(function(local){

		api.saveCommentState(data, function(ret){
			if(ret.success == 1){
				console.log("保存评价状态成功");
			}else{
				console.log(ret.message);
				setTimeout(function(){
					saveCommentState(data)
				}, 5000);
			}

			// callback && callback();
		}, function(){
			console.log("请求保存评价状态接口失败,重试");
			setTimeout(function(){
				saveCommentState(data)
			}, 5000);
		});
	})
}

//保存未找到订单数据
function saveNoFoundOrder(business_oid){
	var api = new Api();
	getSharedTaskWorks(function(sTask){

	useLocal(function(local){
		var data = {
			business_oid:business_oid,
			// order_id:local.task.task_order_id,
			towh_id:local.task.towh_id,
			type:2
			
		}

		//区分是不是订单包
		if(sTask.is_group){
			data.order_group_id = local.task.id;
			data.order_group_work_id = local.task.order_group_work_id;
			data.business_account_id = local.task.account_id;
		}else{
			data.order_id = local.task.task_order_id;
			data.sku_code = local.task.item_id;
			data.business_account_id = local.task.business_account_id;
		}
		

		if(local.tasks[local.taskId].is_mobile == '1'){
			data.client_code = 'm';
		}else{
			data.client_code = 'pc';
		}

		api.saveNofoundOrder(data, function(ret){
			if(ret.success == 1){
				console.log("保存未找到订单信息成功");
			}else{
				console.log(ret.message);
				if(ret.message != '订单信息已经提交'){
					setTimeout(function(){
						saveNofoundOrder(business_oid);
					}, 5000);
				}
				
			}

			// callback && callback();
		}, function(){
			console.log("请求保存未找到订单接口失败,重试");
			setTimeout(function(){
				saveNofoundOrder(business_oid)
			}, 5000);
		});
	})

	})
}

/**
 * app禁用账号
 * @param remark
 * @param sender
 */
function appDisableAccount(remark, sender){
	//先截图
	screenCapture(function(url){//账号禁用之前
		var API = new Api();
		useLocal(function(local){
			var data = {
				account_id: local.accountId,
				locked_channel_code: local.applicationName,//使用当前任务类型
				locked_remark: remark,
				locked_message: remark,
				ext_name: local.applicationName,
				image: url
			};

			if(local.tasks[local.taskId].is_mobile == '1'){
				data.client_code = 'm';
			}else{
				data.client_code = 'pc';
			}

			API.disabledAccount(data, function(){

				//当前任务 done
				//点点
				//startHost();
				//TC.done(sender, {status:4, finished_result: remark});
				M[TC.currentTaskType].accountFail(remark,sender);
			}, function(){
				console.log("禁用账号失败,重试");
				setTimeout(function(){
					appDisableAccount(remark, sender);
				}, 10000);
			});
		})
	})
}

/**
 * 订单重置账号接口
 * @param remark
 * @param sender
 */
function appDisableAccountOrderReset(remark, sender) {

	getSharedTaskWorks(function(sTask){
		if(sTask.is_group){
			appGroupDisableAccountOrderReset(remark,sender);
			return false;
		}else{
			//先截图
			screenCapture(function (url) {//账号禁用之前

				setHostStatus(2);//报道订单重置状态
				remark = remark ? remark : '未知';
				var API = new Api();
				useLocal(function (local) {

					getSharedTaskWorks(function(shareTask){

						if (shareTask.jd_order_id !== undefined) {
							notify("已经提交订单不能进行账号重置", true);
							chrome.storage.local.set({isRunning: false}, function () {
								notify("插件暂停", true);
							})
							return false;
						}

						var params = {
							task_order_id: local.task.task_order_id,
							remark: remark,
							image: url,
							locked_channel_code: local.applicationName,
							ext_name: local.applicationName
						}

						if(local.tasks[local.taskId].is_mobile == '1'){
				            params.client_code = 'm';
				        }else{
				            params.client_code = 'pc';
				        }
				        
						API.accountReset(params, function (ret) {
							if (ret.success == 1) {
								console.log('账号重置成功，，重置账号');
								notify("账号重置成功！");

								//重置账号，，成功后，任务重新开始

								startHost();
								// TC.done(sender);
							} else {
								notify("账号重置 失败(" + ret.message + ")");
								setTimeout(function(){
									appDisableAccountOrderReset(remark, sender);
								}, 10000);
							}

						});

					});

				})
			});
		}
	})
	
}

function appGroupDisableAccountOrderReset(remark, sender) {
	//先截图
	screenCapture(function (url) {//账号禁用之前

		setHostStatus(2);//报道订单重置状态
		remark = remark ? remark : '未知';
		var API = new Api();
		useLocal(function (local) {

			getSharedTaskWorks(function(shareTask){

				// if (shareTask.jd_order_id !== undefined || local.task.business_oid) {
				// 	notify("已经提交订单不能进行账号重置", true);
				// 	chrome.storage.local.set({isRunning: false}, function () {
				// 		notify("插件暂停", true);
				// 	})
				// 	return false;
				// }

				if(shareTask.saved_card_pay){
					notify('付款流水已存在不能进行账号重置',true);
					chrome.storage.local.set({isRunning: false}, function () {
						notify("插件暂停", true);
					})
					return false;
				}

				var params = {
					// task_order_id: local.task.task_order_id,
					order_group_id:local.task.id,
					remark: remark,
					image: url,
					locked_channel_code: local.applicationName,
					ext_name: local.applicationName
				}

				if(local.tasks[local.taskId].is_mobile == '1'){
		            params.client_code = 'm';
		        }else{
		            params.client_code = 'pc';
		        }
		        	
	        	API.accountGroupReset(params, function (ret) {
					if (ret.success == 1 || ret.message.indexOf('重置订单包帐户失败，设置为0失败') != -1) {
						console.log('账号重置成功，，重置账号');
						notify("账号重置成功！");

						//重置账号，，成功后，任务重新开始

						startHost();
						// TC.done(sender);
					} else {
						notify("账号重置 失败(" + ret.message + ")");
						setTimeout(function(){
							appGroupDisableAccountOrderReset(remark, sender);
						}, 10000);
					}

				});

		     

		      
		        
				

			});

		})
	});
}


function setStartPage(taskType,url) {
    TaskRunInfo.taskTypes.forEach(function (tType) {
        if(tType.taskType == taskType){
            tType.startPage = url;
        }
    })
}

function getStartPage(taskType) {
	for (var i=0;i<TaskRunInfo.taskTypes.length;i++){
        if(TaskRunInfo.taskTypes[i].taskType == taskType){
            return TaskRunInfo.taskTypes[i].startPage;
        }
	}
}

//创建隐身窗口
/**
 * 创建隐身窗口 by url
 * @param url
 * @param f
 */
function create_incognito_window_by_url(url) {
    create_incognito_window({
		url: url
	});
}

function create_incognito_window(config,callback) {
    chrome.windows.create({
        url: "about:blank",
        incognito: true
    },function (w) {
        chrome.windows.getAll({}, function (windows) {
        	for(var i=0;i<windows.length;i++){
        		if(windows[i].id!=w.id){
                    chrome.windows.remove(windows[i].id)
                }
			}
            setCookiesAndUA(function () {
                chrome.windows.update(w.id, {state: "maximized"});
                chrome.tabs.create($.extend(config,{windowId:w.id}), function (incognitoWindow) {
                    callback && callback(incognitoWindow);
                });
            });
        })
    })
}

function setCookiesAndUA(callback) {
	var setFlag = 0;
	useLocal(function (local) {
        if (local.accountId) {

            var _clientType='pc';
            if(TC.currentTask!==''){
                if(+local.tasks[TC.currentTask]['is_mobile']===0){
                    var _clientType='pc';
                }else if(+local.tasks[TC.currentTask]['is_mobile']===1){
                    var _clientType='m';
                }
            }

            //获取账号cookies
            getJdAccountDetails(local.accountId, function (account) {
                if (account && account.cookies && account.cookies.forEach) {

                    //cookies 信息和当前账号是否一致
                    var setCookie = false;
                    account.cookies.forEach(function (i) {
                        if (i.name == 'pin' && i.value == account.username) {
                            setCookie = true;
                        }
                    });

                    //设置cookie
                    if (setCookie) {
                        console.log('set account cookies');

                        // setUserCookiesToWindows(account.cookies);
                        setTimeout(function () {
							setFlag++;
                        },3e3)
                        //cookie 的登录日志
                        if(local.task) saveAccountLoginLog('cookie', _clientType);
                    }else{
                        setFlag++;
					}
                }else{
                    setFlag++;
				}
            },function () {
				setFlag++;
            });
            //获取ua
            getUADetails(local.accountId,_clientType,function () {
                setFlag++;
            });
            var _setInterval = setInterval(function () {
                if(setFlag>=2){
                	console.warn('写入cookie和ua ',setFlag);
                	clearInterval(_setInterval);
                    callback && callback();
				}
            },1000);
        }else{
        	console.error('丢失了帐号信息');
        	notify('丢失了帐号信息 程序停止',30);
		}
    })
}


function labelWatcher() {
	setInterval(function () {
		//console.warn('labelWatch',labelWatch)
		useLocal(function (local) {
            if(TC.currentTask) labelWatch.retryTimes[TC.currentTask] = labelWatch.retryTimes[TC.currentTask] || 0;
            if(local.isRunning && labelWatch.time!='' && TC.currentTask){
            	if(labelWatch.retryTimes[TC.currentTask]<6){
                    var _time = parseInt((new Date().getTime() - labelWatch.time)/1000)
                    if(_time>labelWatch.timeout){
                        console.error('超时了',_time,labelWatch.message)
                        notes('labelWatcher' + labelWatch.message);
                        labelWatch.retryTimes[TC.currentTask] = +labelWatch.retryTimes[TC.currentTask]+1;
                        labelWatch.time = new Date().getTime();

                        chrome.tabs.query({}, function(tabs){
                            console.log(tabs);
                            if(tabs.length > 0){
                                var _sendSuccess = false;
                                tabs.forEach(function (tab) {
                                    if(tab.id == labelWatch.tab.id){
                                        _sendSuccess = true;
                                        smToTab(labelWatch.tab.id,'global.labelTimeout')
                                    }
                                })
                                if (_sendSuccess == false || labelWatch.retryTimes[TC.currentTask]>3) {
                                    create_incognito_window({
                                        url: getStartPage(TC.currentTaskType)
                                    }, function (openTab) {
                                        closeOtherTabs({tab:openTab})
                                    });

                                }
                            }else{
                                create_incognito_window_by_url(getStartPage(TC.currentTaskType));
                            }
                        });
                    }
				}else{
            		if(topTaskConfig.taskAutoStart == true || TC.currentTask.indexOf('selfOrder') != -1){
                        labelWatch.time = '';
            			startHost();
					}
				}
            }
        })
    },5000)
}

// function closeOtherTabs(sender){
//     //关闭sender中其他的tab
//     if (sender && sender.tab) {
//         chrome.tabs.query({}, function (tabs) {
//             for (var i = 0; i < tabs.length; i++) {
//                 var tab_id = tabs[i].id;
//                 var tab = tabs[i];
//                 if (tab_id != sender.tab.id) {
//                     try {
//                         chrome.tabs.remove(tab_id);
//                     } catch (e) {
//                         console.log("remove tab not found", tab.url, tab.title, tab);
//                     }
//                 }
//             }

//         });
//     }
// }

function closeOtherTabs(sender){
    //关闭sender中其他的tab
    if (sender && sender.tab) {
		//console.log(sender);
        chrome.windows.getAll({populate:true},function(wins){
            //console.log(wins);
            if(wins.length >0){
	            wins.forEach(function(win){
		            //console.log(win);
		            if(win.id != sender.tab.windowId){
			            //console.log('remove window', win);
			            chrome.windows.remove(win.id);
		            }else{
			            win.tabs.forEach(function(tab){
				            if(tab.id != sender.tab.id ){
					            //console.log('remove tab', tab);
					            chrome.tabs.remove(tab.id);
				            }
			            });
		            }
	            })
            }
        })
    }
}