console.log('socketio');
socketio = (function(){
	return function(host_id){

		const url = 'https://host-notification.disi.se/';
		// const url = 'http://192.168.2.40:3000/';
		
		const socket = {};
		socket.socketActions = {};
		socket.sendMessageCallbackFunctions = {};
		socket.io = io(url);

		console.log(socket.io);

		socket.io.on('connect', function(){
			this.emit('name', host_id);
		});

		socket.io.on('disconnect',function(data){
		    //连接断开
		    console.log('连接断开');
		});

		socket.io.on('error',function(){
			console.log('连接出现错误');
		})

		socket.io.on('reconnect',function(re_nums){
			console.log('重新连接次数',re_nums);
		})

		socket.io.on('reconnect_failed',function(){
			console.log('重新连接失败');
		})

		socket.io.on('reconnecting',function(){
			console.log('正在重新连接');
		})

		// socket.io.on('connect_failed', function(){
		//     console.log('Connection Failed');
		// });

		// socket.io.on('error', function(){
		//     console.log('Connection error');
		// });

		socket.io.on('name', function(data){
			console.log('name', data);
			this.name = data;
		});

		socket.io.on('data', function(data){
			console.log(data);
			socket.onMessage(data);
		})

		socket.onMessage = function(data){
			if(data.action && this.socketActions[data.action]){
				this.socketActions[data.action](data);
				this.dataCallback(data.action, data.form, {value: 'ok'});
				if(data.action.indexOf('__callback')!=-1) delete this.socketActions[data.action];
			}else{
				this.dataCallback(data.action, data.form, {value: '不支持的命令', oldData:data});
			}
		}

		socket.sendMessage = function(action, to, data, callback){
			var message = {action: action, to: to, from: this.io.name, data: data};
			if(message.action){
				if(typeof(callback)==='function') this.addAction(message.action+'__callback', callback);
				this.io.emit('data', message);
			}else{
				console.error('sendMessage not found action', message);
				this.dataCallback(action, to, {value: 'sendMessage not found action', oldData: data});
			}
		}
		socket.addAction=function(action, callback){
			this.socketActions[action] = callback;
		}
		socket.dataCallback = function(action, to, data){
			this.sendMessage(action+'__callback', to, data);
		}

		return socket;
	}
})();

// const code = 'man:'+new Date().getMilliseconds();
// const code = 'hn_001:pacgjeifepfilgalcbpkcghmebbmgafc';
// var sock = new socketio(code);

// sock.addAction('restart', function(){
// 	// console.log('重新开始任务了');
// 	startHost();
// });
