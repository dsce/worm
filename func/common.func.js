/*****************worm*****************
 * @describe	页面公用方法库文件
 * @file 		common.func.js
 * @version		2015-5-7 16:40:28 1.0
 *****************worm*****************/
var checkedLoginType = false;
var observers=[];
var taskVars={};
/**
 * 页面开始运行任务
 * @param callback
 * @constructor
 */
function Task(callback) {
    chrome.storage.local.get(null, function (local) {
        if (local.isRunning) {
            console.log("XSS init ... ");
            if (local.task) {
                console.log('XSS task init ... ');

                callback && callback(local);
            } else {
                console.log("XSS 没有任务");
            }
        } else {
            console.log("XSS task 暂停");
        }

    });
}


//检测账号登录类型
function accountLoginLog(type, callback){
    var href = location.href;
    if(href.indexOf('m.jd.com') != -1 || href.indexOf('m.jd.hk') != -1 ){
        var client_code = 'm';
    }else{
        var client_code = 'pc';
    }

    tabCommon.sm(taskVars, '.saveAccountLoginLog', {loginType: 'account', clientCode: client_code}, function(){
        callback && callback();
    });
}

/**
 * 页面监控 继续执行使用 ，增加有延迟时间
 * @param func
 * @constructor
 */
function Run(func) {
    chrome.storage.local.get(null, function (local) {
        if (local.isRunning) {//运行中
            lazy(function () {
                func(local);
            });
        } else {
            console.log("暂停，监控操作停止-Run");
        }

    });
}

/**
 * 页面监控 继续执行使用 ，不增加有延迟时间
 * @param func
 * @constructor
 */
function r(callback) {
    chrome.storage.local.get(null, function (local) {
        if (local.isRunning) {//运行中
            callback(local);
        } else {
            console.log("暂停-r");
        }

    });
}

/**
 * 暂停操作
 * @param callback
 */
function pauseRun(callback){
    setLocal({isRunning: false}, function(){
        callback && callback();
    });
}

/**
 * 使用stroage中的local数据
 * @param callback
 */
function useLocal(callback){
    chrome.storage.local.get(null, function (local) {
        callback && callback(local);
    });
}

/**
 * 向storage的local中保存数据
 * @param i
 * @param callback
 */
function setLocal(i, callback) {
    chrome.storage.local.set(i, function () {
        callback && callback();
    });
}
function setLocalTask(item, callback){
    useLocal(function(local){
        var task = local.task;
        task = $.extend(task, item);
        setLocal({task: task}, function(){
            callback && callback();
        })
    })
}
/**
 * 删除storage中local里的某一个数据
 * @param k
 * @param callback
 */
function localDel(k, callback) {
    chrome.storage.local.remove(k, function () {
        callback && callback();
    });
}

/**
 * local 输出
 */
function consoleLocal(){
    useLocal(function(local){
        console.log('Local');
        console.log(local);
    })
}

/**
 * 当前任务进度 标记
 * @param message
 * @param timeout
 */
function label(message, timeout, func){
    if(message){
        var value = {message: message};
        if(timeout && parseInt(timeout)>=0) {
            value.timeout = parseInt(timeout);
        }
        tabCommon.messageListener.global.labelTimeout = func || tabCommon.messageListener.global.labelTimeoutDefault;
        tabCommon.sm(taskVars, '.label', value);
    }else{
        console.log('label message null');
    }
}

/**
 * 生成一个随机数
 * @param {start} [随机数的开始值]
 * @param {end} [随机数的结束值]
 * @returns {number} [返回随机的结果]
 */
function random(start, end) {
    return Math.round(Math.random() * (end - start) + start);
}
//延迟操作
function lazy(callback, time) {
    if (time == undefined) {
        var t = random(_t.lazy_rand_time_start, _t.lazy_rand_time_end);
    } else {
        var t = time;
    }

    alertify.log(':)  ' + t, 'log', t * 1000);
    setTimeout(function () {
        callback();
    }, t * 1000);
}

//是否在数组中
function in_array(find, array) {
    for (var i = 0; i < array.length; i++) {
        if (find == array[i]) {
            return true;
        }
    }
    return false;
}

/**
 * 是否直辖市
 * @param province [省份]
 * @returns {boolean}
 */
function is_municipality(province) {
    //如果是直辖市
    if (province == '北京' || province == '上海' || province == '天津' || province == '重庆') {
        return true;
    } else {
        return false;
    }

}

/**
 * background sendMessageTobackground ，tab发送数据到background
 * @param act
 * @param val
 */
function sendMessageToBackground(act, val, callback) {
    var data = {
        'act': act,
        'val': val == undefined ? 'val' : val
    };
    chrome.runtime.sendMessage(data, function (response) {
        callback && callback(response);
    });
}
/**
 * background sendMessageTobackground ，tab发送数据到background
 * @param act
 * @param val
 */
function sendDataToBackground(act, data, callback) {
    data.act = act;
    chrome.runtime.sendMessage(data, function (response) {
        callback && callback(response);
    });
}

function sendMessageToTabByUrl(url, act, data, callback){
    data = data ? data : {};
    data.act = act;
    var message = {act: 'send_message_to_tab', url: url, data: data};
    chrome.runtime.sendMessage(message, function (response) {
        callback && callback(response);
    });
}
function sendMessageToTabByTabId(tabId, act, data, callback){
    data = data ? data : {};
    data.act = act;
    var message = {act: 'send_message_to_tab', tabId: tabId, data: data};
    chrome.runtime.sendMessage(message, function (response) {
        callback && callback(response);
    });
}
/**
 * 发送数据到background 关闭当前的tab页面
 */
function close_this_tab() {
    tabCommon.sm(taskVars, '.closeThisTab');
}

/**
 * 报告运行状态
 * @param v
 */
function updateHostStatus(v, t,callback) {
    //tabCommon.sm(taskVars, '.setHostStatus', {hostStatus: v, timeout: t});
    if((v >= 1502001 && v <= 1502005) || v == 1301000){
        setSharedTaskWorks({payment_clicked:false},function(){

        })
    }

    useLocal(function(local){
        if(local.taskType == 'selfOrder'){
            var text = '[自营]' + _host_status[v].text;
        }else{
            var text = _host_status[v].text;
        }

        label(text, t||_host_status[v].timeout,callback);
    })
    
    
}

/**
 * 更新主机当前步骤
 * @param v
 */
function updateHostStep(v) {
    tabCommon.sm(taskVars, '.hostStep', v);
}

/**
 * 从当前步骤重置主机运行
 * @param v
 */
function resetHostByStep() {
    tabCommon.sm(taskVars, '.resetHostByStep');
}


/**
 * 重置看门狗计时器
 */
function wangwang(){
    tabCommon.sm(taskVars, '.wangwang');
}
function resetWatchDogTimer() {
    wangwang();
}

function closeMe(seconds){
    if(seconds){
        setTimeout(function () {
            tabCommon.sm(taskVars, '.closeThisTab');
        },seconds*1000);
    }else{
        tabCommon.sm(taskVars, '.closeThisTab');
    }
}


/**
 * 发送消息到background 进行桌面提醒
 * @param v
 */
function notifyMessage(v) {
    //chrome.runtime.sendMessage({act: 'notify', val: v}, function (response) {  });
    tabCommon.sm(taskVars, '.notify', {message: v});
}
/**
 * 发送消息到background 固定标签页面
 */
function pinnedThisTab() {
    tabCommon.sm(taskVars, '.setTabPinned');
}

/**
 * tabs页面增加message监听，
 * @param {callback}
 */
function addListenerMessage(callback) {
    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
            callback && callback(request);
        }
    );
}

/**
 * 页面元素变动监控
 * @param target
 * @param callback
 * @param config
 */
function addMutationObserver(target,callback,config){
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            // console.log(mutation.type);
            // console.log(mutation.target);
            // console.log(mutation);

            callback(mutation);

        });
    });

    if(config){
        var observer_config = config;
    }else{
        var observer_config = {
            attributes: true,
            childList: true,
            characterData: true,
            attributeOldValue: true,
            characterDataOldValue: true,
            subtree: true
        }
    }

    observer.observe(target, observer_config);

    //var observer_config = {
    //    attributes: true,
    //    childList: true,
    //    characterData: true,
    //    attributeOldValue: true,
    //    characterDataOldValue: true,
    //    subtree: true
    //}
    //addMutationObserver($("#p-box")[0],function(mutation){
    //    // console.log(mutation.type);
    //    // console.log(mutation.target);
    //    // console.log(mutation);
    //},observer_config);
}

/**
 * 页面直接插入js
 * @param b
 */
function insertScript(b){
    var a = document.createElement("script");
    a.innerHTML = "(" + b.toString() + ")(1);";
    a.addEventListener("load", function () {
        document.documentElement.removeChild(a)
    }, !0);
    document.documentElement.insertBefore(a, document.documentElement.firstChild || null)
}

/**
 * 提示信息 alertify
 * @param {message} [提示内容]
 * @param {type} [提示类型，log，error，success]
 * @param {sec} [提示停留时间，默认不关]
 */
function clue(message, type, sec) {
    type = type ? type : 'log';
    sec = sec && sec > 0 ? sec : 0;
    alertify.log(message, type, sec);
    tabCommon.sm(taskVars, '.sendNotesToTracker', {message: message});
}

//触发 对象的change事件，js原生对象
function change_event(o) {
    var changeEvent = document.createEvent("MouseEvents");
    changeEvent.initEvent("change", true, true);
    o.dispatchEvent(changeEvent);
}
function click_event(o) {
    var changeEvent = document.createEvent("MouseEvents");
    changeEvent.initEvent("click", true, true);
    o.dispatchEvent(changeEvent);
}

//解除银行账号占用状态
function bank_make_over(local) {

    //检查是否自动登陆
    if (!local.pay.autologin_boc) {
        clue("中行自动登陆，未开启，无需解除");
        return false;
    }

    //填用户名，
    var bank_user = local.pay.boc_username;
    var bank_code = local.pay.bank;

    //排队，获取密码状态
    var API = new Api();
    var params = {
        action: 'set_use_status',
        host_id: local.host_id,
        username: local.username,
        password: local.password,
        bank_code: bank_code,
        bank_user: bank_user
    };

    API.setParams(params);
    API.BankAjaxPOST(function (ret) {
        if (ret.success == 1) {
            notifyMessage("[" + bank_user + "] 已解除占用");
        } else {
            //解除占用失败，稍后再试
            notifyMessage("[" + bank_user + "] 解除占用失败," + ret.message);
        }
    }, function () {
        notifyMessage("[" + bank_user + "] 解除占用失败," + ret.message);
    });

}

/**
 * 输入内容
 * @param {target} [需要输入内容的对象jq]
 * @param {string} [输入的字符串]
 * @param {callback} [输入完成后，回调函数]
 * @returns {boolean} [错误，]
 */
function writing(target, string, callback) {
    //string = "伊子凡2015春装新款 春秋装 韩版女装衣服时尚修身两件套雪纺条纹背心连衣裙";
    if (target.length <= 0) {
        console.log("wirte object not exist.", 'error');
        return false;
    }

    if (string.length <= 0) {
        console.log("string error", 'error');
        return false;
    }

    var arr = string.split('');
    var len = string.length;

    var eventClick = new MouseEvent('click', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventMove = new MouseEvent('mousemove', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventDown = new MouseEvent('mousedown', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventUp = new MouseEvent('mouseup', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventBlur = new MouseEvent('blur', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventKeydown = new MouseEvent('keydown', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventKeyup = new MouseEvent('keyup', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventchange = new MouseEvent('change', {'view': window, 'bubbles': true, 'cancelable': true});
    var eventfocus = new MouseEvent('focus', {'view': window, 'bubbles': true, 'cancelable': true});

    this.get = function () {
        if (arr.length > 0) {
            var str = arr.shift();
            setTimeout(function () {
                this.setValue(str);
            }, random(_t.write_rand_msec_start, _t.write_rand_msec_end));

        } else {
            target[0].dispatchEvent(eventBlur);
            callback && callback();
        }
    };
    this.setValue = function (str) {
        var value = target.val();
        if(value.length < len && string.indexOf(value) == 0){
            var val = value + str;
        }else{
            var val = string.substr(0,string.indexOf(arr.toString().replace(/,/g,""))) + str;
        }

        //target.val(value.length < len && string.indexOf(value) == 0 ? value + str : str);
        target.val(val);

        target[0].dispatchEvent(eventKeydown);
        target[0].dispatchEvent(eventKeyup);

        this.get();
    };

    target[0].dispatchEvent(eventMove);
    target[0].dispatchEvent(eventDown);
    target[0].dispatchEvent(eventClick);
    target[0].dispatchEvent(eventUp);
    target[0].dispatchEvent(eventfocus);

    target.val('');
    setTimeout(function () {
        target.val('');
        this.get();
    }, 2000);
}

/**
 * 点击
 * @param {object} [需要点击的对象]
 */
function clicking(object) {

    var evt_over = document.createEvent("MouseEvents");
    evt_over.initEvent("mouseover", true, true);

    //鼠标按下事件
    var evt_down = document.createEvent("MouseEvents");
    evt_down.button = 0;
    evt_down.initEvent("mousedown", true, true);
    //click
    var evt_click = document.createEvent("MouseEvents");
    evt_click.initEvent("click", true, true);

    //鼠标 弹起
    var evt_up = document.createEvent("MouseEvents");
    evt_up.button = 0;
    evt_up.initEvent("mouseup", true, true);

    if (object.length > 0) {
        object[0].dispatchEvent(evt_over);
        //去掉点击延迟,立即执行
        //setTimeout(function () {
            object[0].dispatchEvent(evt_down);
            object[0].dispatchEvent(evt_click);
            object[0].dispatchEvent(evt_up);
        //}, 500);


    } else {
        console.log("clicking object not exist.", 'error');
    }
}

/**
 * 检查订单是否已经提交
 * @param local
 * @returns {boolean}
 */
function taskOrderExist(task_type,callback) {
    getSharedTaskWorks(function(shareTask){

        useLocal(function(local){


        if(shareTask.is_group){
            var business_oid = local.task.business_oid;
        }else{
            var business_oid = local.tasks[local.taskId].business_oid;
        }
        
        if (shareTask.jd_order_id === undefined && !business_oid) {
            callback && callback();
            return false;
        } else {
            
            clue('已经提交过订单', 'error');
            //clue('需要重新下单，请【重新开始】');

            //已经提交过订单直接从 付款开始
            setLocal({host_step: _host_step.payment}, function(){
                // setTimeout(resetHostByStep, 2000);
                updateHostStatus(1413099);
                tabCommon.report(taskVars, task_type);
                return true;
            })

            return true;
        }

    })

    })
}

//保存收货地址到服务器
function saveAddressToRemote(callback){

    tabCommon.sm(taskVars, '.saveAddressToRemote', {}, function(){
        callback && callback();
    });

    
    // var API = new Api();
    // useLocal(function(local){

    //     if(local.order_address !== undefined) {
    //         clue("保存到服务器后跳转");
    //         var params = {
    //             "action": "task_save_consignee",
    //             "host_id": local.host_id,
    //             "username": local.username,
    //             "password": local.password,
    //             "task_order_id": local.task.task_order_id,

    //             "name": local.order_address.name,
    //             "mobile": local.order_address.mobile,
    //             "province": local.order_address.province,
    //             "city": local.order_address.city,
    //             "area": local.order_address.area,
    //             "street": local.order_address.street,
    //             "address": local.order_address.short_address
    //         };
    //         // API.setParams(params);
    //         API.saveConsignee(params,function (ret) {

    //             if (ret.success == 1) {
    //                 clue('地址更新成功', 'success');
    //                 callback && callback();

    //              }else if(ret.message == 'host_id不一致'){
    //                     updateHostStatus(4);//host_id 不一致
    //                     clue(ret.message,'error');
    //                     chrome.storage.local.set({isRunning: false}, function() {
    //                       notify("插件暂停", true);
    //                     });
    //                     return false;

    //             }else{
    //                 clue("<p>地址更新失败! 5秒刷新后重试 </p><p>"+ret.message+"</p>", 'error');
    //                 lazy(function(){
    //                     location.reload();
    //                 },5);
    //             }

    //         }, function () {
    //             clue("地址更新出错，5秒刷新后重试", 'error');
    //             lazy(function(){
    //                 location.reload();
    //             },5);
    //         });
    //     }else{
    //         clue("保存到服务器de收货地址没找到，未修改");
    //         console.log("保存到服务器的收货地址是",local.order_address);
    //         callback && callback();
    //     }
    // })
}

/**
 * 订单缺货不刷标记异常
 * @param message
 */
function reportProductStockout(val){

     var data = {
        'act': 'set_exception',
        'val': val == undefined ? 'val' : val
    };

    chrome.runtime.sendMessage(data, function (response) {
        callback && callback(response);
    });

    // clue("<p>"+message+"</p><p>标记异常</p>", "error");
    // var API = new Api();
    // useLocal(function(local){

    //     var data = {
    //         "host_id": local.host_id,
    //         "task_order_id": local.task.task_order_id,
    //         "remark": message
    //     };
    //     API.setException(data,function (ret) {
    //         if (ret.success == 1) {
    //             clue('产品无货标记完成，重新开始任务', 'error');
    //             //callback && callback();
    //             //任务重新开始
    //             tabCommon.sm(taskVars, '.startHost');
    //         }else if(ret.message == 'host_id不一致'){
    //             updateHostStatus(4);//host_id 不一致
    //             //console.log(ret.message);
    //             clue(ret.message,'error');
    //             chrome.storage.local.set({isRunning: false}, function() {
    //               alertify.log("插件暂停");
    //             });
    //             return false;
    //         }else{
    //             clue("<p>"+ret.message+"</p><p>产品无货标记失败，10秒后重试</p>", 'error');
    //             setTimeout(function(){
    //                 reportProductStockout(message);
    //             },10000);
    //         }

    //     }, function () {
    //         clue('产品无货标记出现错误，5秒后刷新重试', 'error');
    //         lazy(function(){
    //             location.reload();
    //         },5)
    //     });

    // })
}

function reportGroupOrderException(val,task_order_id,order_ids){

     var data = {
        'act': 'order_exception',
        'val': val == undefined ? 'val' : val,
        'task_order_id':task_order_id,
        'order_ids':order_ids || {}
    };

    chrome.runtime.sendMessage(data, function (response) {
        callback && callback(response);
    });

}



/**
 * 任务添加备注
 * @param message
 */
function addTaskOrderRemark(val, callback){

    var data = {
        'act': 'add_remark',
        'val': val == undefined ? 'val' : val
    };

    chrome.runtime.sendMessage(data, function (response) {
        callback && callback(response);
    });

    // clue("<p>添加备注</p><p>"+message+"</p>");
    // var API = new Api();
    // useLocal(function(local){

    //     var data = {
    //         "host_id": local.host_id,
    //         "task_order_id": local.task.task_order_id,
    //         "remark": message
    //     };
    //     API.setException(data,function (ret) {
    //         if (ret.success == 1) {
    //             clue('添加备注完成');
    //             callback && callback();
    //         }else if(ret.message == 'host_id不一致'){
    //             updateHostStatus(4);//host_id 不一致
    //             //console.log(ret.message);
    //             clue(ret.message,'error');
    //             chrome.storage.local.set({isRunning: false}, function() {
    //               alertify.log("插件暂停");
    //             });
    //             return false;
    //         }else{
    //             clue("<p>添加备注失败，10秒后重试</p>", 'error');
    //             setTimeout(function(){
    //                 addTaskOrderRemark(message,callback);
    //             },10000);
    //         }

    //     }, function () {
    //         clue('添加备注出现错误，5秒后刷新重试', 'error');
    //         lazy(function(){
    //             location.reload();
    //         },5)
    //     });

    // })
    
}

/**
 * 实现自动喂狗功能
 * @param time {int}[自动喂狗时长,毫秒数]
 */
function autoResetWatchDogTimer(time){

    time = time == undefined ? 0 : time;
    setTimeout(function(){
        var spare = time - 30000;
        if(spare > 0){
            resetWatchDogTimer();
            autoResetWatchDogTimer(spare);
        }else{
            resetWatchDogTimer();
        }

    },30000)

}

function getBlobImg(img){
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var dataURL = canvas.toDataURL("image/png");

    return dataURL;

    //dataURL to Blob
    var arr = dataURL.split(',');
    var mime = arr[0].match(/:(.*?);/)[1];
    var bstr = atob(arr[1]);
    var n = bstr.length;
    var u8arr = new Uint8Array(n);

    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

function base64ToBlob(dataURL){
    //dataURL to Blob
    var arr = dataURL.split(',');
    var mime = arr[0].match(/:(.*?);/)[1];
    var bstr = atob(arr[1]);
    var n = bstr.length;
    var u8arr = new Uint8Array(n);

    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

/**
 * 获取url get参数信息，返回对象
 * @returns {{}}
 */
function urlGet(){
    var url = window.document.location.href.toString();
    var u = url.split("?");
    if(typeof(u[1]) == "string"){
        u = u[1].split("&");
        var get = {};
        for(var i in u){
            var j = u[i].toString().split("=");
            get[j[0]] = j[1];
        }
        return get;
    } else {
        return {};
    }
}

//京东地址直辖市处理
function jdMunicipality(consignee) {
    var province = consignee.province;
    var city = consignee.city;
    if (province == '北京' || province == '上海' || province == '天津' || province == '重庆') {
        //北京 北京
        if(city.indexOf(province) != -1){

            consignee.city = consignee.area;
            consignee.area = consignee.street;
            consignee.street = '';
        }

        if(city.indexOf(province) == -1 && consignee.area != null){
            consignee.street = consignee.area;
            consignee.area = consignee.city;
            consignee.city = province;
        }
    }
    return consignee;
}

function openCart(){
    window.open('http://cart.jd.com/cart/cart.html');
    close_this_tab();
}

/**
 * 获取插件version
 * @returns {*}
 */
function extensionVersion(){
    var manifest = chrome.runtime.getManifest();
    return manifest.version;
}

/**
 * 当前所在页面是否在iframe中
 */
function windowsTopNotSelf(){
    if(top != self){
        //步骤重置
        notifyMessage('页面 嵌入框架 错误，步骤重置');
        resetHostByStep();
    }
}


/**
 * 链接劫持问题处理
 */
function verifyUnionUrl(callback, union_url){

    //增加页面监听、、核对cookie时返回的结果
    addListenerMessage(function(request){
        if(request.act == 'tab_get_cookies_response'){
            useLocal(function(local){
                var promotionUrlCode = local.task.promotionUrlCode;//推广链接标志201504
                var cookies = request.cookies;
                console.log(cookies);
                if(cookies.length == 0 ){ errorUnionUrl(); return false;}


                var kidnap = true;
                for(var i=0;i<cookies.length;i++){
                    console.log(cookies[i].value);
                    if(cookies[i].value.indexOf(promotionUrlCode) != -1){
                        kidnap = false;
                        break;
                    }

                }
                if(!kidnap){
                    console.log('正常链接');
                    //Task(index);
                    callback && Task(callback);
                }else{
                    errorUnionUrl();
                }
            })



        }
    });

    useLocal(function(local){
        if(local.unpromotion === true){
            errorUnionUrl();
            return false;
        }

        var promotionUrlCode = local.task.promotionUrlCode;//推广链接标志201504
        var url = union_url ? union_url : location.href;

        if(url.indexOf('yiqifa.com') != -1){
            var details = {domain: 'yiqifa.com', name: 'yiqifa_euid'};
            tabCommon.sm(taskVars, '.tabs_get_cookies', {details: details});
        }else if(url.indexOf('chanet.com.cn') != -1){
            var details = {domain: 'chanet.com.cn'};
            tabCommon.sm(taskVars, '.tabs_get_cookies', {details: details});
        }else if(url.indexOf('p.egou.com') != -1){
            //p.egou.com 相关cookies在当前地址上存在，直接检查地址上的值即可
            if(url.indexOf(promotionUrlCode) != -1){
                console.log('正常链接');
                //Task(index);
                callback && Task(callback);
            }else{
                var details = {domain: 'yiqifa.com', name: 'yiqifa_euid'};
                tabCommon.sm(taskVars, '.tabs_get_cookies', {details: details});
                // errorUnionUrl();//异常推广链接，
            }
        
        }else{
            //异常推广链接，
            errorUnionUrl();
            return false;
        }
    })

}

/**
 * 错误的联盟推广链接
 */
function errorUnionUrl(){
    notifyMessage('链接劫持');
    updateHostStatus(1401001);
    setTimeout(resetHostByStep, 2000);
}

//检查是否有活动url
function checkActiveUrl(task){
    if(task.active_url){
        window.location.href = task.active_url;
    }
}

/**
 * 关键词搜索
 */
function searchKeywordsByJDPC(local){

    // checkCart(function(){     })

            //状态 推广链接打开
            updateHostStatus(1401000);

            checkRandClick(local);

            var task = local.currentTask;

            // checkActiveUrl(task);

            //是否已经提交订单
            // if(taskOrderExist(local.task_type)){
            //     return false;
            // }

            //searchPc(task.keyword, task.item_id);

            if(task.active_url){
                window.location.href = task.active_url;
            }else{
                //避免超值购空白页面
                if($("#shortcut-2014:visible").length > 0 || $("#shortcut").length >0){
                    readySearch(local);
                }else{
                    document.location.reload(true);
                }
            }


}

var pcSearch = {
    ready: function(){
        wangwang();

        useLocal(function(local){  
        getTaskWorks(function(works) {
            var searchGroup = works.searchGroup;
            var searchGroupItems = works.searchGroupItems;
            if (searchGroup === true) {//已经是批量搜索
                pcSearch.nextKeyword();//下一个关键词
            } else {
                searchGroup = true;
                searchGroupItems = [];

                //random(0,1);
                var searchGroupOn = 1;//相似关键词开关
                //设置随机比例
                var r = random(0,100);
                if(r > 90){
                    searchGroupOn = 0;
                }
                if (searchGroupOn) {
                    getSearchAlikeKeywordGroup(function (groupKeywords) {
                        console.log('相似关键词', groupKeywords);
                        if (groupKeywords) {
                            groupKeywords.shift();//移除第一个,第一个是输入的关键词
                            //随机1-3个
                                var m = local.tasks[local.taskId].similar_search || 0;
                       
                                // var m = random(1, 3);//0-1,0510
                                m = groupKeywords.length > m ? m : groupKeywords.length;
                                clue('随机相似关键词个数' + m);

                                for (var i = 0; i < m; i++) {
                                    var l = groupKeywords.length;
                                    var start = random(1, l);
                                    var g = groupKeywords.splice(start - 1, 1);
                                    console.log(i, "长" + l, "取" + start, g, '余下' + groupKeywords.length, groupKeywords);
                                    if (g[0].key && g[0].key.indexOf('自营') == -1) {
                                        searchGroupItems.push(g[0].key);
                                    } else {
                                        console.log("相似关键词没有找到", g);
                                    }
                                }
                                console.log('随机相似关键词', searchGroupItems);

                            
                        }

                      

                        //推入最后一个当前的关键词
                        searchGroupItems.push(works.keyword);
                        clue('关键词个数: ' + searchGroupItems.length);
                        setTaskWorks({searchGroup: searchGroup, searchGroupItems: searchGroupItems}, function () {
                            pcSearch.nextKeyword();
                        })
                    })
                }else{
                    //推入最后一个当前的关键词
                    searchGroupItems.push(works.keyword);

                    setTaskWorks({searchGroup: searchGroup, searchGroupItems: searchGroupItems}, function () {
                        pcSearch.nextKeyword();
                    })
                }


            }
        })

    })
    }, nextKeyword: function(){
        wangwang();
        getTaskWorks(function(works){
            var searchGroupItems = works.searchGroupItems;

            //批量搜索
            if(searchGroupItems.length>0){//继续搜索,批量已准备,关键词是没问题的,开始搜索吧
                //startSearch();

                var keyword = searchGroupItems.shift();
                setTaskWorks({searchGroupItems: searchGroupItems}, function(){
                    searchPc(keyword, works.item_id);
                })

            }else{
                //进行批量搜索,但是没有相似关键词,清除信息执行
                console.log("searchGroupItems length 0", searchGroupItems);
                setTaskWorks({searchGroup: null, searchGroupItems: null}, pcSearch.ready);
            }

        })
    }
};


function readySearch(){
    randGoLogin(30,function(){
        pcSearch.ready();
    })
    
}


function startSearch(){
    pcSearch.nextKeyword();
}

//创建M端搜索区域
function createSearchAreaByM(local){

    if($("#index_search_main").length > 0){
        $("#index_search_main").remove();
    }

    clue('搜索按钮不存在，创建搜索区域');

    //创建搜索框区域
    
    $('<div id="index_search_main" search_land_searchtransformation_show="true" class="jd-header-home-wrapper"><div class="jd-search-container on-focus" id="index_search_head"><div class="jd-search-box-cover" style="opacity: 0;"></div><div class="jd-search-box"><div class="jd-search-tb"><div class="jd-search-icon"><span class="jd-search-icon-logo"><i class="jd-sprite-icon"></i></span><span id="index_search_bar_cancel" class="jd-search-icon-cancel"><i class="jd-sprite-icon"></i></span></div><form action="/ware/search.action" id="index_searchForm" class="jd-search-form"><div class="jd-search-form-box"><span class="jd-search-form-icon jd-sprite-icon"></span><div class="jd-search-form-input"><input type="hidden" name="sid" value="6e30b00f9707923e86b9a53a9824f260"><input type="text" maxlength="20" autocomplete="off" id="index_newkeyword" name="keyword" value="" placeholder="" class="hilight1"><input id="index_category" name="catelogyList" type="text" style="display:none"></div><a href="javascript:void(0);" class="jd-search-icon-close jd-sprite-icon" id="index_clear_keyword"></a><a href="javascript:void(0)" id="index_search_submit" class="jd-search-form-action"><span class="jd-sprite-icon"></span></a></div></form><div class="jd-search-login login-ed"><a href="http://home.m.jd.com/myJd/home.action" class="J_ping" page_name="index" report-eventparam="" report-eventid="MHome_MyJD"><span class="jd-search-icon-logined"><i class="jd-sprite-icon"></i></span></a></div></div></div></div></div>').insertAfter($("#layout_top"));

}

//创建搜索区域PC端
function createSearchArea(keyword, itemId,callback){

    console.log(keyword,itemId);
    if($("#search-2014").length > 0){
        $("#search-2014").remove();   
    }

    clue('3s后,创建搜索区域');
    setTimeout(function(){
        $('<div id="search-2014"><ul class="hide" id="shelper" style="display: none;"></ul><div class="form"><input type="text" clstag="shangpin|keycount|toplist1|b02" class="text" accesskey="s" id="key" autocomplete="off" onkeydown="javascript:if(event.keyCode==13) search(\'key\');"><button clstag="shangpin|keycount|toplist1|b03" class="button cw-icon" onclick="search(\'key\');return false;"><i></i>搜索</button></div></div>').insertAfter("#logo-2014");
        callback && callback(keyword, itemId);
    },3000); 

}

function searchPc(keyword, itemId){
    
    if (keyword == '') {
        clue("关键词为空");
        window.location.href = "http://item.jd.com/"+itemId+".html";
    }else {
        updateHostStatus(1404000);

        if($("#key").length>0){
            $("#key").focus();
            writing($("#key"),keyword,function(){
                if($("#key").val() != keyword){
                    $("#key").val(keyword);
                }
                var search_2014 = $("#search-2014 .form .button:contains('搜索')");//正常搜索按钮
                var reKeySearchBtn = $("#reKeySearchBtn");//热卖搜索按钮
                var searchAllSiteBtn = $("#search-2014 .form .button:contains('搜全站')");//商品详情搜全站
                var searchBtn = $("#search .button");//兼容出现显示器电脑搜索框

                if(search_2014.length > 0){
                    clicking(search_2014);
                }else if(reKeySearchBtn.length > 0){
                    clicking(reKeySearchBtn);
                }else if(searchAllSiteBtn.length>0){
                    clicking(searchAllSiteBtn);
                }else if(searchBtn.length >0){
                    clicking(searchBtn);
                }else{
                    clue('没有找到搜索按钮');

                    //createSearchArea(keyword, itemId,doSearch);
                    indexJdPc();
                }
            });
        }else if($("#search_2015:visible").length>0){
            clue('超值购');
            //jd超值购
            writing($("#search_2015 .txt"), keyword, function(){
                if($("#search_2015 .txt").val() != keyword){
                    $("#search_2015 .txt").val(keyword);
                }

                clicking($("#search_2015 .btn:contains('搜索')"));
            })
        }else{
            clue("重新搜索没有输入框,回首页");
            indexJdPc();
        }


    }
}

//创建区域后提交搜索
function doSearch(keyword, itemId){

    if (keyword == '') {
        clue("关键词为空");
        window.location.href = "http://item.jd.com/"+itemId+".html";
    }else {
        updateHostStatus(1404000);

        writing($("#key"),keyword,function(){
            if($("#key").val() != keyword){
                $("#key").val(keyword);
            }
            if($("#search-2014 .form .button").length > 0){
                $("#search-2014 .form .button")[0].click();
            }
        })
    }
}

/**
 * 进入京东首页
 */
function indexJdPc(){
    useLocal(function(local){
        if(local.task.unpromotion){
            var ttbarHome = $("#ttbar-home a:contains('京东首页')");
            var logo2014 = $("#logo-2014 a:contains('京东')");
            if(ttbarHome.length > 0){
                clicking(ttbarHome);
            }else if(logo2014.length > 0){
                clicking(logo2014);
            }else{
                clue("没有找到首页入口,3秒直接进入");
                setTimeout(function(){
                    location.href = "//www.jd.com/";
                }, 3000);
            }
        }else{
            document.location.href=local.task.promotion_url;
        }
    })

}

/**
 * 获取跨域验证码的base64
 * @param src
 * @param callback
 */
function getCrossDomainAuthCodeBase64(src, callback){
    var img = new Image,
        canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        imgSrc = src; // insert image url here

    img.crossOrigin = "Anonymous";

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage( img, 0, 0 );
        var base64 = canvas.toDataURL("image/png");
        //console.log(base64);
        callback && callback(base64);
    }
    img.src = imgSrc;
}

/**
 * 获取驻商品页面停留时间最大值
 * @param jdPrice
 * @returns {number}
 */
function getItemWaitTimeEnd(jdPrice){ 
    var priceTime = parseInt(jdPrice/100)*60;
    if(priceTime < _t.mainProductWaitRandTimeEndMin){
        priceTime = _t.mainProductWaitRandTimeEndMin;
    }else if(priceTime > _t.mainProductWaitRandTimeEndMax){
        priceTime = _t.mainProductWaitRandTimeEndMax;
    }
    console.log(priceTime);
    return priceTime;
}

/**
 * 获取关键词的相似关键词
 */
function getSearchAlikeKeywordGroup(callback){
    useLocal(function(local){
        this.genPvid = function() {
            var a = parseInt($.cookie("__jdu") || "");
            return a || (a = Math.round(1e12 * Math.random())),
                (a.toString(36) + "." + (new Date).getTime().toString(36)).split("").reverse().join("")
        }

        var k = local.tasks[local.taskId].keyword;
        var q = this.genPvid();
        var keyword = encodeURIComponent(k);
        var time = (new Date).getTime();
        var r = "//dd-search.jd.com/?ver=2&zip=1&key="+keyword+"&pvid=" + q + "&t="+time+"";

        $.ajax({
            url: r,
            dataType: "json",
            scriptCharset: "utf-8",
            //jsonp: "callback",
            cache: !0,
            timeout: 10*1000,
            success: function(b) {
                callback(b);
            },error: function(){
                callback();
            }
        })
    })

}

function reloadHttps(){
    var href = document.location.href;
    document.location.href = href.replace('http://', 'https://');
}
/**
 * 黄金比例 61.8%的比例返回true
 * @returns {boolean}
 */
function isGoldenRatio(){
    var r = random(1,1000);
    return r>618?!1:!0;
}

/**
 * 页面滚动到对象位置
 * @param target
 * @param callback
 */

function windowScrollToTarget(id,callback){
    $.extend($.scrollTo.defaults, {
        duration: random(3000,5000)
    });
    $.scrollTo(id, {
        onAfter: callback
    });
}

var scrolling = false;
function windowScrollToTarget_bak(id, callback){
    if(scrolling){
        return null;
    }
    scrolling = true;
    var target = id;
    windowScroll(id, target.offset().top -  window.scrollY, callback);
}

/**
 * 滚动条滚动
 * @param m 滚动多少
 * @returns {boolean}
 */
function windowScroll(id, m, c){
    var s=20;
    if(Math.abs(m)<10){
        var target = id;
        scrolling=false;
        if(Math.abs(target.offset().top -  window.scrollY) > s){
            console.log('未完继续');
            windowScrollToTarget(id, c);
        }else{
            console.log("滚动完成");

            c && c();
        }
        return false;
    }

    if(m>0){
        m = m-s;
        window.scrollTo(0, window.scrollY + s);
    }else{
        m = m+s;
        window.scrollTo(0, window.scrollY - s);
    }

    setTimeout(function(){
        windowScroll(id, m, c);
    }, 30);
}

//检测商品下柜、无货 刷新页面次数控制
function checkProductExceptionReloadNums(type,callback){
    chrome.storage.local.get(null, function(local){ 
        //console.log(local);
        var task = local.task;
        //console.log('task-2' + task);
        var exception_type  = '';
        switch(type){
            case 'xgui':
            exception_type = '下柜';
            break;
            case 'whuo':
            exception_type = '无货';
            break;
            case 'no_active':
            exception_type = '在活动页面找不到';
            break;
        }
        var exception_type_reload_nums = type + '_reload_nums';
        var reload_nums = task[exception_type_reload_nums] ? task[exception_type_reload_nums] : 0;
        if(reload_nums < 3){
            task[exception_type_reload_nums] = parseInt(reload_nums + 1);
            setLocal({task:task},function(){
                clue('当前商品' + exception_type + '，10S后刷新看看');
                clue('已刷新次数：' + reload_nums);
                setTimeout(function(){
                    window.location.reload();
                },10*1000);
            })
            
        }else{
            clue('已刷新3次,需标记异常');
            callback && callback();
        }
    });
   
}

//打码错误状态码统一输出
function getErrorNo(request,base,act){
     //云打码失败
    if(request.type == 1){
        var api_type = '云打码';
        if(request.errno == -1007){
            clue(api_type + '余额不足');
        }else if(request.errno <= -1001){
            clue(api_type + '密码错误,或秘钥有误');
        }else if(request.errno == 'timeOut'){
            clue(api_type + '打码超时！');
        }
        //使用优优云平台
        clue('云打码失败喽，使用优优云平台');

        chrome.extension.sendMessage({act: act, base: base});
    }

    
    if(request.type == 2){
        var api_type = '优优云';
        if(request.errno == '-6'){
            clue(api_type + '用户点数不足，请及时充值');
        }else if(request.errno == '-4'){
            clue(api_type + '账户被锁定');
        }else if(request.errno == '-1'){
            clue(api_type + '软件ID或KEY无效或者用户名为空或密码为空');
        }else if(request.errno == 'timeOut'){
            clue(api_type + '打码超时！');
        }
        clue('优优云平台打码也失败，3分钟后刷新');
        setTimeout(function(){
            window.location.reload();
        },3*60*1000);
    }
                
}


//随机点击
function randClick(local){
    //过滤url
    var filter_url = ['www.jd.com/intro/','www.jd.com/contact/','vc.jd.com','jtz.jd.com','app.jd.com','club.jd.com','media.jd.com','sale.jd.com','gongyi.jd.com','en.jd.com','joycenter.jd.com/','surveys.jd.com','b.jd.com','xue.jd.com','jrhelp.jd.com','https://myjd-crm.jd.com/','https://myjd.jd.com/','chat.jd.com','www.jd.com'];

    var urls = '';
    for(var i in filter_url){
       urls += "a[href*='" + filter_url[i] + "'],"; 
    }

    var a_hrefs = $("a[href*='.jd.com']").not($(urls.replace(/,$/,'')));

    if(a_hrefs.length >0){

        var randIndex = random(0,a_hrefs.length);

        var click_url = a_hrefs.eq(randIndex).attr('href').replace(/http[s]*:/,'');

        tabCommon.sm(taskVars, '.create_tab_by_rand_click_url', {url: 'http:' + click_url});

        var task = local.task;
        task.randClicked = true;
        setLocal({task:task},function(){
            
        })   
    }

}

//检查是否要点击
function checkRandClick(local){

    var account_click_percentage = local.task.account_click_percentage;

    var s = random(0, 99);
    if(!local.task.randClicked && account_click_percentage && account_click_percentage > s){
        //随机点击
        randClick(local);
    }
}


//chrome.runtime.onMessage.addListener(
//    function (request, sender, sendResponse) {
//        if(request.cmd === '__index'){
//            useLocal(function (local) {
//                try{
//                    eval(local.taskType+'__index'+(local));
//                }catch (e){
//                    common__index(local,e);
//                }
//            })
//        }
//    }
//);

function findAndClick(ele,callback) {
    if(ele.length){
        ele[0].click();
    }else{
        callback && callback();
    }
}

function common__index(local) {
    lazy(function () {

        // 老版
        // var current_url = ['https://' + location.host + location.pathname, 'http://' + location.host + location.pathname];
        // var taskStartPage = local['tasks'][local.taskId]['config']['startPage']
        // if (current_url.indexOf(taskStartPage)>-1
        //     || (location.href.indexOf('details.jd.com/normal/item.action?')!=-1 && local.taskType=='payment')) {

        // 使用match 新
        var myMatch = null;
        if(local['tasks'][local.taskId]['config']['urlMatch']){
            var myMatch = new UrlMatch.default(local['tasks'][local.taskId]['config']['urlMatch']);
        }
        
        if (myMatch && myMatch.test(location.href)) {
            //location.reload();
            (function () {
                tabCommon.start(local.tasks[local.taskId].config.taskType, tabCommon.startCallback[local.tasks[local.taskId].config.taskType])
            })()
        } else {
            if (location.href.indexOf('m.jd.com') != -1 && location.href.indexOf('item.jd.com') == -1) {
                location.href = 'https://m.jd.com';

            } else if (location.href.indexOf('m.jd.hk') != -1 && location.href.indexOf('item.jd.hk') == -1) {
                location.href = 'https://m.jd.hk';
                
            } else if (location.href.indexOf('jd.com') != -1) {
                if (location.pathname != '/' || location.href.indexOf('www.jd.com/?cdn') != -1) {
                    findAndClick($('.logo a'),function () {
                                location.href = 'https://www.jd.com/';
                            }
                        )
                    
                    // findAndClick($('.logo'),
                    //     findAndClick($('.logo_tit_lk'),
                    //         findAndClick($('a:contains("京东首页")')),
                    //         function () {
                    //             location.href = 'https://www.jd.com/';
                    //         }
                    //     )
                    // )
                } else {
                    console.log('this is jd index');
                    // location.reload();
                    location.href = 'https://www.jd.com/';
                }
            } else if (location.href.indexOf('jd.hk') != -1) {
                if (location.pathname != '/' || location.host != 'www.jd.hk') {
                    clue('点击首页hk');
                    findAndClick($('.logo-head a')),
                        function () {
                            location.href = 'https://www.jd.hk/';
                        }
                } else {
                    console.log('this is jd index');
                    // location.reload();
                    clue('是首页，刷新');
                    location.href = 'https://www.jd.hk/';
                }
            }
        }
    })
}

function randomString() {
    return Math.random().toString(36).substr(2);
}

/**
 * 运行， 多任务公用时使用，一般任务不能用
 * @param callback
 */
function autoRun(callback) {
    useLocal(function (local) {
        if(local.isRunning){
            callback(local)
        }else{
            clue('暂停状态')
        }
    })
}

/**
 * 延迟运行，多任务公用的时候用，一般任务不能用
 * @param callback
 */
function lazyAutoRun(callback) {
    useLocal(function (local) {
        if(local.isRunning){
            lazy(function () {
                callback(local)
            })
        }else{
            clue('暂停状态')
        }
    })
}
function checkCart(callback){
    useLocal(function(local){
        //打开购物车页面
        var cart_cleared = local.task.cart_cleared;
        if(cart_cleared === undefined){

            // var cart_btn = $("#nav a:contains('我的购物车')");
            var cart_btn = $("#settleup a:contains('购物车')");
            if(cart_btn.length > 0){
                setLocalTask({cart_cleared: false}, function(){
                    setTimeout(function(){
                        clicking(cart_btn);
                        callback && callback();
                    },3*1000)
                    
                });
            }else{
                clue('购物车入口改变，不能进行清空操作', 'error');
            }

        }else{
            callback && callback();
        }
    })
    
}

function clearChangeUseragent(callback){
    tabCommon.sm(taskVars, '.removeUseragentListener', {}, callback);
}
//去登陆
function to_login_message() {
    updateHostStatus(1412000);
    //重新打开隐身窗口
    tabCommon.sm(taskVars, '.MChangedToPc', {url: 'https://order.jd.com/center/list.action'});

    setTimeout(function () {
        
        clue('正在打开窗口');
        //已经打开窗口，关闭当前窗口
        clue('3秒后，自动关闭本窗口');
        setTimeout(close_this_tab, 3000);
    }, 2000);
}

var auto_scroll_nums = 0;
var auto_scroll_interval = null;
//查看商品评价
function clickProductCommented(){

    clue('点击查看评价');

    window.scrollTo(0, 1024);

    if($(".tab-main ul li:contains('评价')").length >0){
        $(".tab-main ul li:contains('评价')")[0].click();
    }

    //随机点击查看评价下tab
    setTimeout(function(){
        if($(".filter-list li[data-tab='trigger']").length >0){
            var tab_index = random(0,$(".filter-list li[data-tab='trigger']").length-1);
            if($(".filter-list li[data-tab='trigger'] a").eq(tab_index).text().match(/\d+/)[0] >0){
               $(".filter-list li[data-tab='trigger']")[tab_index].click() 
            }
        }else if($(".m-tab-trigger li").length >0){
            var tab_index = random(0,$(".m-tab-trigger li").length-1);
            if($(".m-tab-trigger li a").eq(tab_index).text().match(/\d+/)[0] >0){
                $(".m-tab-trigger li")[tab_index].click();
            }
            
        }
    },5*1000)

    //自动滚动
    setTimeout(auto_scroll,6*1000);
    // auto_scroll(5);

    var r_page = random(0,100);
    //随机点击分页
    setTimeout(function(){
        if($("div[id*='comment-']:visible").find(".ui-page a").length >0){
            var page_index = random(0,$("div[id*='comment-']:visible").find(".ui-page a").length-1);
            if(r_page > 50){//50的比例点击分页
                $("div[id*='comment-']:visible").find(".ui-page a").eq(page_index)[0].click()
            } 
        }  
    },15*1000)

    setTimeout(function(){
        clue('查看评价结束');
        goto_top();
    },20*1000);
}

function goto_top() {
    window.scrollTo(0, 0);
}

//自动滚动
function auto_scroll(cnt){
    var cnt = cnt ? cnt:5;
    auto_scroll_interval = setInterval(function(){
        if(auto_scroll_nums > cnt){
           clearInterval(auto_scroll_interval);
        }else{
            window.scrollTo(0, 1024 + auto_scroll_nums*200);
            auto_scroll_nums++;
        }
    },1000)
}


/**自营使用start**/
function changeCategoryObserverForNew(){
    // if($(".JS_popCtn").length == 0){
    //     location.href = "https://www.jd.com/";
    //     return false;
    // }
    var observer_config = {
        attributes: true,
        childList: true,
        characterData: true,
        attributeOldValue: true,
        characterDataOldValue: true,
        subtree: true
    }
    addMutationObserver($(".J_cate")[0],function(mutation){
        // console.log(mutation.type);
        // console.log(mutation.target);
        // console.log(mutation);
        if(mutation.type == 'childList'){
            if(mutation.target.className == 'JS_popCtn cate_pop' && mutation.addedNodes.length > 0){
                //导航增加,可以选择
                Run(function(){
                    categoryRandom();
                });

            }
        }
    },observer_config);
}

function categoryRandom(){

    useLocal(function(local){
        var API = new Api();
        var items = $(".dorpdown-layer .item-sub[id*=category-item] a[href*='list.jd.com/list.html?cat=']");
        var items_new = $(".JS_popCtn .cate_part[id*=cate_item] a[href*='//list.jd.com/list.html?cat=']");
        // chrome.runtime.sendMessage({act:'get_recent_category'});
        tabCommon.sm(taskVars,'businessAccountRecentCategory');
        addListenerMessage(function(request){
            if(request.act == 'get_recent_category_response'){
                var categoryCodes = request.data.category_code;

                for(var i in categoryCodes){
                    var c = categoryCodes[i];
                    if(c && items_new.length >0){
                        $(".JS_popCtn .cate_part[id*=cate_item] a[href*='//list.jd.com/list.html?cat="+c+"']").remove();
                    }else if(c && items.length >0){
                        $(".dorpdown-layer .item-sub[id*=category-item] a[href*='list.jd.com/list.html?cat="+c+"']").remove();
                    }
                }

                // categoryCodes.forEach(function(c){

                //     if(c!='' && items_new.length >0){
                //         $(".JS_popCtn .cate_part[id*=cate_item] a[href*='//list.jd.com/list.html?cat="+c+"']").remove();
                //     }else if(c!='' && items.length >0){
                //         $(".dorpdown-layer .item-sub[id*=category-item] a[href*='list.jd.com/list.html?cat="+c+"']").remove();
                //     }
                // })
                
                if(items.length >0){
                    var item = items[random(0, items.length-1)];
                    item.click();
                }else if(items_new.length >0){
                    var item_new = items_new[random(0, items_new.length-1)];
                    item_new.click();
                }else{
                    clue('找不到随机的类目');
                    return false;
                }
                
                close_this_tab();
            }
        })
        // API.businessAccountRecentCategory(local.host_id, local.task.business_account_id, function(ret){
            
        // }, function(){
        //     categoryRandom();
        // });

    })

}

function changeCategory(){
    clue('切换类目');

    var items = $(".dorpdown-layer .item-sub[id*=category-item] a[href*='//list.jd.com/list.html?cat=']");
    var items_new = $(".JS_popCtn .cate_part[id*=cate_item] a[href*='//list.jd.com/list.html?cat=']");
    if(items.length > 0 || items_new.length >0){
        categoryRandom();
    }else{
        //换类目,随机选择进入自营类目
        var target = $("#categorys-2014").length >0 ? $("#categorys-2014") : $(".cate_menu") ;
        //获取类目信息
        var eventOver = new MouseEvent('mouseover', {'view': window, 'bubbles': true, 'cancelable': true});
        target[0].dispatchEvent(eventOver);
    }
}

function changeCategoryObserver(){
    if($("#categorys-2014").length == 0){
        location.href = "https://www.jd.com/";
        // setTimeout(close_this_tab,2*1000);
        return false;
    }
    var observer_config = {
        attributes: true,
        childList: true,
        characterData: true,
        attributeOldValue: true,
        characterDataOldValue: true,
        subtree: true
    }
    addMutationObserver($("#categorys-2014")[0],function(mutation){
        // console.log(mutation.type);
        // console.log(mutation.target);
        // console.log(mutation);
        if(mutation.type == 'attributes'){
            if(mutation.target.className == 'dd'){
                //导航增加,可以选择
                Run(function(){
                    categoryRandom();
                });

            }
        }
    },observer_config);
}
/**selforder-END**/

//随机登录操作
function randGoLogin(scale,callback){
    var s = scale || 0;
    var r = random(0,100);
    if(r < s){
        tabPc.goLogin();
    }

    callback && callback();
}

