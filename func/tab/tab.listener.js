chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        useLocal(function (local) {
            console.log('tabListener',request, sender);
            try {
                if(request.act.indexOf('global.')==0){
                    var _f = request.act.replace('global.','');
                    tabCommon.messageListener.global[_f](request.val);
                }else{
                    tabCommon.messageListener[local.taskType][request.act](request.val);
                }
            }catch (e){
                console.log('监听事件,找不到需要执行的函数'+local.taskType+'.'+request.act,e);
            }
        })
    }
);

//修正一些奇葩问题
useLocal(function(local){
    if(local.taskType.indexOf('rear') != -1){
        urlFix();
    }else if(local.taskType == 'search' || local.taskType == 'group_xss'){
        if(location.href.indexOf('returnurl') != -1 && location.href.indexOf('home.m.jd.com/myJd/newhome.action') != -1){
            location.href = 'https://plogin.m.jd.com/user/login.action';
        }        
    }
})

function urlFix() {
    if(top.location.href.indexOf('club.jd.com//afterComments')>-1){
        top.location.href = top.location.href.replace('//afterComments','/afterComments');
    }
}

// label('打开新页面',600);