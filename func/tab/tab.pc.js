var tabPc = {

    /**
     * pc搜索
     * @param keyword
     * @returns {*}
     */
    search: function (keyword) {
        return tabCommon.func(function (resolve, reject) {
            if (keyword == '') {
                clue("关键词为空");
                reject('keywordIsNull');
            } else {
                if ($("#key").length > 0) {
                    writing($("#key"), keyword, function () {
                        if ($("#key").val() != keyword) {
                            $("#key").val(keyword);
                        }
                        var search_2014 = $("#search-2014 .form .button:contains('搜索')"); //正常搜索按钮
                        var reKeySearchBtn = $("#reKeySearchBtn"); //热卖搜索按钮
                        var searchAllSiteBtn = $("#search-2014 .form .button:contains('搜全站')"); //商品详情搜全站
                        var searchBtn = $("#search .button"); //兼容出现显示器电脑搜索框

                        if (search_2014.length > 0) {
                            clicking(search_2014);
                        } else if (reKeySearchBtn.length > 0) {
                            clicking(reKeySearchBtn);
                        } else if (searchAllSiteBtn.length > 0) {
                            clicking(searchAllSiteBtn);
                        } else if (searchBtn.length > 0) {
                            clicking(searchBtn);
                        } else {
                            //clue('没有找到搜索按钮');
                            reject('searchButtonLost');
                            //createSearchArea(keyword, itemId,doSearch);
                            //indexJdPc();
                        }
                    });
                } else if ($("#search_2015:visible").length > 0) {
                    clue('超值购');
                    //jd超值购
                    writing($("#search_2015 .txt"), keyword, function () {
                        if ($("#search_2015 .txt").val() != keyword) {
                            $("#search_2015 .txt").val(keyword);
                        }

                        clicking($("#search_2015 .btn:contains('搜索')"));
                    })
                } else {
                    clue("重新搜索没有输入框,回首页");
                    reject('KeywordInputLost')
                    //indexJdPc();
                }
            }
        })
    },

    /**
     * 评论浏览
     * @param maxpage 最大页数
     * @returns {*}
     */
    commentView: function (maxpage) {
        clue('查看评论')
        return tabCommon.func(function (success, fail) {
            maxpage = maxpage || 3;
            tabPc.findCommentMainMenu()
                .then(function () {
                    clue('点击评论分页')
                    NextPage();

                    function NextPage() {
                        var _nowPage = +$('a.ui-page-curr').text();
                        clue('当前页码 ' + _nowPage);
                        if ($('a.ui-pager-next').length) {
                            if (_nowPage <= 3) {
                                windowScrollToTarget($('a.ui-pager-next'), function () {
                                    $('a.ui-pager-next')[0].click()
                                    setTimeout(NextPage,3e3);
                                })
                            } else {
                                clue('翻页结束')
                                success();
                            }
                        }else{
                            clue('找不到翻页')
                            success();
                        }
                    }

                }, function () {
                    fail()
                })
        })
    },

    /**
     * 辅助函数 支持浏览评论
     * @returns {*}
     */
    findCommentMainMenu: function () {
        return tabCommon.func(function (resolve, reject) {
            var _ele = $("li:contains('商品评价')");
            if (_ele.length) {
                clue('找到了评论菜单 滚动到')
                windowScrollToTarget(_ele, function () {
                    lazy(function () {
                        clue('点击评论菜单')
                        _ele[0].click();
                        tabCommon.waitElementReady(function () {
                                return $('a.ui-page-curr')
                            })
                            .then(function () {
                                resolve();
                            }, function () {
                                reject();
                            })
                    })
                })
            } else {
                reject();
            }
        })
    },

    /**
     * 点击进入店铺 有多种入口
     */
    shopClick: function () {
        clue('点击店铺')
        var _ele = [
            $('div.name').find('a'),
            $("a:contains('进店逛逛')")
        ];
        tabCommon.clickElementArr(_ele);
    },

    /**
     * 点击导航上的我的订单
     */
    myOrderListClick: function () {
        clue('我的订单')
        var _ele = $("a:contains('我的订单')");
        tabCommon.clickElement(_ele);
    },

    myHome: function () {
        clue('我的京东')
        var _ele = $("a:contains('我的京东')");
        tabCommon.clickElement(_ele);
    },

    loginOut:function () {
        clue('退出登录')
        var _ele = $("a:contains('退出')");
        tabCommon.clickElement(_ele);
    },

    //个人信息
    userInfo: function () {
        clue('个人信息')
        var _ele = $("#menu a:contains('个人信息')");
        tabCommon.clickElement(_ele);
    },
    //我的级别
    myLevel: function () {
        clue('我的级别')
        var _ele = $("#menu a:contains('我的级别')");
        tabCommon.clickElement(_ele);
    },

    //我的购物车
    myCart: function () {
        clue('我的购物车')
        var _ele = $("a:contains('我的购物车')");
        tabCommon.clickElement(_ele);
    },

    jdIndex: function () {
        clue('京东首页')
        var _ele = $("a:contains('京东首页')");
        tabCommon.clickElement(_ele);
    },

    goLogin:function(){
        clue('去登录');
        var _ele = $("#ttbar-login a:contains('请登录')");
        tabCommon.clickElement(_ele);
        // if($("#ttbar-login a:contains('请登录')").length >0){
        //     $("#ttbar-login a:contains('请登录')")[0].click();
        // }
    },
    /**
     * 找到订单列表中的某个订单
     * @param orderId
     */
    findOrder:function (orderId) {
        var ele = $('#track' + orderId);
        return tabCommon.func(function (success,fail) {
            if (tabCommon.findElement(ele)) {
                clue('找到了目标产品 滚动到之后 点击订单详情');
                success();
            } else {
                clue('找不到订单 准备翻页')
                lazy(function () {
                    var _pageEle = $('a.next')
                    if (_pageEle.length && +$('a.current').text() < 3) {
                        windowScrollToTarget(_pageEle,function () {
                            lazy(function () {
                                tabCommon.clickElement(_pageEle);
                            })
                        })
                    } else {
                        clue('失败 找不到目标订单');
                        fail();
                    }
                })
            }
        })
    },

    /**
     * 点击打开订单详情
     * @param orderId
     */
    clickOrderDetailLink:function (orderId) {
        var ele = $('#track' + orderId);
        var clickEle = ele.find('a:contains("订单详情")');
        return tabCommon.func(function (success,fail) {
            if(tabCommon.findElement(clickEle)){
                windowScrollToTarget(clickEle,function () {
                    lazy(function () {
                        success();
                        tabCommon.clickElement(clickEle);
                    })
                })
            }else{
                fail();
            }
        })
    },

    /**
     * 点击搜索商品页面的目标商品
     * @param itemId
     */
    clickSearchGoodsItem:function (item_id,keyword) {
        
        return tabCommon.func(function (success,fail) {

            var urlget = urlGet();
            var search_key = decodeURIComponent(urlget.keyword);
            var keyword = keyword.replace(/^\s+|\s+$/g,"");

            //检测是否有错误关键词提示
            if($(".check-error").length >0){
                if($(".check-error .key2:contains('点击查看')").length == 0){
                    var search_key = $(".check-error .key2").text();
                }
            }

            if(search_key != keyword){
                clue('不是目标关键词 准备重新搜索')
                searchPc(keyword,itemId)
            }else{
                if ($("li[data-sku='" + item_id + "']").length > 0 && $("li[sku='" + item_id + "'] .p-img a[href*='item.jd.com']").length>0) {
                    clue("主商品存在列表中,直接点击");
                    success();
                    clicking($("li[data-sku='" + item_id + "'] .p-img a"));
                }else {

                    var itemId = item_id;
                    var positionSearch = random(0, 59);console.log('positionSearch', positionSearch);
                    //var itemId = '1023124131';
                    var itemsUl = $("#J_goodsList ul");
                    var itemsLi = $("#J_goodsList ul li");
                    if(itemsLi.length > 0){
                        var tempItem = itemsLi.first();
                        var tempItemId = tempItem.attr('data-sku');
                        var tempItemHtml = tempItem[0].outerHTML;
                        var mainItem = $(tempItemHtml);
                        var mainItemLink = mainItem.find("a[onclick*='searchlog'][href*='item.jd.com']");
                        mainItemLink.each(function(i,o){
                            var strLinkOnclick = $(this).attr('onclick');
                            var arrLinkOnclick = strLinkOnclick.split(',');
                            arrLinkOnclick[2] = positionSearch.toString();
                            $(this).attr('onclick', arrLinkOnclick.join(','));
                        });
                        mainItemLink.eq(random(0,mainItemLink.length-1)).attr("id", "xss-product-link");
                        mainItemLink.eq(0).attr("id", "xss-product-link");
                        mainItem.attr("id", "xss-product");
                        var mainItemHtml = mainItem[0].outerHTML;
                        mainItemHtml = mainItemHtml.replace(new RegExp(tempItemId, 'g'), itemId);
                        itemsUl.prepend(mainItemHtml);

                        clue("已增加模拟商品");
                        success();
                        setTimeout(function(){
                            clicking($("#xss-product-link"));
                        }, 5000);

                    }else{
                        clue("商品在哪儿");
                        fail();
                    }
                }
            }

        })
    },

    rear:{
        findOrder:function (order_id,sku_id,callback,taskVars) {
            console.log("判断是否有订单在此页面");
            clue(order_id);
            var $order_tr = $('#track' + order_id);
            if($order_tr.length > 0){
                clue("找到订单");
                if($order_tr.find('span').filter(':contains("已取消")').length > 0){
                    console.log("订单已取消");
                    clue("订单已取消");
                    tabCommon.sm(taskVars, 'reportFail',{message:"订单已取消"});
                }else if($order_tr.find(".td-01 .order-statu").filter(':contains("正在出库")').length > 0){
                    console.log("未发现确认收货，订单正在出库");
                    clue("订单正在出库");
                    tabCommon.sm(taskVars, 'reportFail',{message:"订单正在出库",delay:3600*24});
                }else if($order_tr.find('.order-status:contains("等待厂商处理")').length >0){
                    clue("等待厂商处理");
                    tabCommon.sm(taskVars, 'reportFail',{message:"等待厂商处理",delay:3600*24});
                }else{
                    //正常可进行下一步
                    console.log("订单状态正常");
                    clue('判断订单正常 继续操作');
                    lazy(function () {
                        callback && callback();
                    })
                }
            }else{
                if(location.href.indexOf('order.jd.com/center/search.action') != -1){
                    //检查下是否是拆分订单
                    if($(".parent-" + order_id).length >0 && $(".parent-" + order_id).find(".p-" + sku_id).length >0){
                        var business_oid = $(".parent-" + order_id).attr('id').split('-')[1];
                        setTaskWorks({business_oid:business_oid},function () {
                            location.reload();
                        })
                    }else{
                        clue('搜索页也未找到订单');
                        clue('延迟执行');
                        setTimeout(function(){
                            tabCommon.sm(taskVars, 'reportFail',{message:'搜索页也未找到订单',delay:24*3600});
                        },3*1000);
                    }
                }else{
                    console.log("本页没有找到订单，去搜索页");
                    clue("没有找到订单，去搜索页");
                    searchOrder();
                }
            }
            function searchOrder() {
                var search_input = $('#ip_keyword');
                if(search_input.length > 0){
                    clue("搜索框搜索订单");
                    if(search_input.val() == order_id){
                        var search_btn = $('a.btn-13');
                        if(search_btn.length > 0){
                            search_btn[0].click();
                        }else{
                            window.open('//order.jd.com/center/search.action?keyword=' + order_id);
                        }
                    }else{
                        search_input.val(order_id);
                        setTimeout(function(){
                            searchOrder();
                        },2000);
                    }
                }else{
                    window.open('//order.jd.com/center/search.action?keyword=' + order_id);
                }
            }
        },
        getButtons:function (order_id,name) {
            var _buttons = $('#operate' + order_id).find('a');
            if(_buttons.length>0){
                for(var i=0;i<_buttons.length;i++){
                    // clue('按钮:' + _buttons.eq(i).text().trim());
                    if(_buttons.eq(i).text().trim().indexOf(name)>-1) {
                        clue('找到了按钮 '+name);
                        return true;
                    }
                }
            }
            return false;
        },
        receipt:{

        }
    },


    TabEnd: {}
}