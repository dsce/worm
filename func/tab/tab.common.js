var tabCommon = {

    /**
     * Promise简写
     * @param cb
     * @returns {Promise}
     */
    func: function (cb) {
        return new Promise(cb);
    },

    /**
     * background sendMessageTobackground ，tab发送数据到background
     * @param act
     * @param data
     */
    sm: function(vars,action, val, callback) {
        //处理需要传递的参数
        var varList = ['uuid'];//参数白名单 除去前缀的命名部分
        var _vars = {};
        for (var i=0; i<varList.length; i++){
            _vars[varList[i]] = vars[varList[i]];
        }

        useLocal(function(local){

            //.saveLogintype = global.saveLoginType   //公用的加 .
            //saveOrderInfo = submit_order.saveOrderInfo //默认不加

            if(action.indexOf('.')==0){
                action = 'global'+action;
            }else{
                action = local.taskType + '.' + action;
            }
            console.warn('sm', action, vars, _vars);
            var data = {'act': action, 'val': val || {}, 'bgTaskVars':_vars };
            chrome.runtime.sendMessage(data, function (response) {
                callback && callback(response);
            });
        });

    },
    messageListener:{
        global:{
            appTaskStart:function () {
                useLocal(function (local) {
                    // clue('收到了执行任务的通知 本页面如果不跳转 将在30s后关闭')
                    common__index(local);
                    setTimeout(function () {
                        // close_this_tab()
                    },30e3)
                })
            },
            backgroundScreenShotResult:function (request) {
                console.log('截屏结果',request);
                if(request.img){
                    uploadImage(request.img,
                        function(url){
                            tabCommon.sm(taskVars, '.imageUploadResult',{url:url,order_id:request.order_id,task_type:request.task_type});
                        },
                        function(){
                            tabCommon.sm(taskVars, '.imageUploadResult',{url:null});
                        }
                    );
                }else{
                    console.error('截图失败');
                }
            },
            labelTimeoutDefault:function () {
                getTaskWorks(function (local) {
                    clue('labelTimeoutDefault');
                    local.reflush = local.reflush ||0
                    local.reflush++;
                    if(local.reflush>3){
                        setTaskWorks({reflush:0},function () {
                            tabCommon.sm(taskVars, '.currentTaskReset');
                            clue('超时刷新3次喽。。')
                        })
                    }else{
                        setTaskWorks({reflush:local.reflush},function () {
                            top.location.reload()
                        })
                    }
                })
            }
        }
    },

    addMessageListener:function (act,cb) {
        useLocal(function (local) {
            tabCommon.messageListener[local.taskType] = tabCommon.messageListener[local.taskType] || {}
            tabCommon.messageListener[local.taskType][act] = cb
            console.log(tabCommon.messageListener)
        })
    },

    startCallback:{},

    start:function (taskType,cb) {
        var _whiteList = ['login']
        useLocal(function (local) {
            window[taskType] = true;
            tabCommon.startCallback[taskType] = cb;
            if(local.isRunning == true && (local.taskType == taskType || _whiteList.indexOf(taskType) != -1)){
                local.currentTask = local.tasks[local.taskId];
                var task_types = ['search','xss','payment','home'];
                getSharedTaskWorks(function(sTask){
                    if(!sTask.is_group){
                        if(task_types.indexOf(taskType) != -1){
                            local.currentTask = local.task;
                        }
                    }

                    console.log(local.currentTask)
                    local.taskVars={};
                    local.taskVars.uuid = local.taskType +'#'+ local.taskId;
                    console.log(local)
                    // clue('uuid: ' + local.taskVars.uuid)
                    cb(local)
                })
                
                
            }
        })
    },
    observers:{},

    /**
     *
     * @param element
     * @param option
     */
    observer:function (element,option) {
        var observer_config = {
            attributes: true,
            childList: true,
            characterData: true,
            attributeOldValue: true,
            characterDataOldValue: true,
            subtree: true
        }
        var obId=randomString();
        tabCommon.observers[obId] = [];
        var option = option?option:observer_config
        addMutationObserver(element,function(mutation){
            tabCommon.observers[obId].push(mutation)
        },option);
        return obId;
    },

    mutation:function (obId,cb) {
        if(obId){
            while (tabCommon.observers[obId].length>0){
                cb(tabCommon.observers[obId].shift())
            }
            setTimeout(function () {
                tabCommon.mutation(obId,cb)
            },100)
        }
    },

    each:function (arrs,callback) {
        if(arrs.length!='undefined'){
            for (var i in arrs){
                callback(arrs[i])
            }
        }else{
            arrs.forEach(function (p1, p2, p3) {
                callback(p1)
            })
        }
    },

    changeURLPar:function (destiny, par, par_value) {
        var pattern = par+'=([^&]*)';
        var replaceText = par+'='+par_value;
        if (destiny.match(pattern))
        {
            var tmp = '/\\'+par+'=[^&]*/';
            tmp = destiny.replace(eval(tmp), replaceText);
            return (tmp);
        }
        else
        {
            if (destiny.match('[\?]'))
            {
                return destiny+'&'+ replaceText;
            }
            else
            {
                return destiny+'?'+replaceText;
            }
        }
        return destiny+'\n'+par+'\n'+par_value;
    },

    dama:function (imgSrcFunc,inputFunc,afterInput,retryTimes) {
        var retryTimes = typeof retryTimes === 'undefined'?3:retryTimes;
        if (retryTimes--<0) {
            clue('打码失败')
            return;
        }
        //打码前,开启监听
        tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginListener', null, function(){
            tabCommon.addMessageListener('https_tabs_verify_code_result_tabCommon',function (data) {
                console.log(data)
                writing(inputFunc(),data.text,function () {
                    afterInput()
                        .then(function () {
                            clue('验证码通过');
                        },function () {
                            tabCommon.dama(imgSrcFunc,inputFunc,afterInput,retryTimes);
                        })
                })
            })
            tabCommon.addMessageListener('https_tabs_verify_code_result_errno_tabCommon',function (data) {
                console.log(data)
            })
            getCrossDomainAuthCodeBase64(imgSrcFunc(), function(base64){
                console.log(base64)
                tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginRemoveListener', null, function(){
                    //关闭监听,准备打码
                    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                });
            });
        });
    },

    getUUID: function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    },

    screenShot:function (callback,autoUpload){
        $('#panel-XSS').remove();
        $('#alertify-logs').remove();
        var autoUpload = autoUpload?autoUpload:false;
        tabCommon.addMessageListener('screenShotResult',function (request) {
            if(request.img){
                if(autoUpload){
                    uploadImage(request.img,
                        function(url){
                            callback && callback(url)
                        },
                        function(){
                            callback && callback();
                        }
                    );
                }else{
                    callback && callback(request.img);
                }
            }else{
                console.error('截图失败');
            }
        })
        tabCommon.sm(taskVars, '.screenShot');
    },

    reloadPage:function (maxTimes,success,fail) {
        getTaskWorks(function (taskWorks) {
            var _times = taskWorks.reloadPageMaxTimes || 0;
            _times++
            if(_times>=maxTimes){
                setTaskWorks({reloadPageMaxTimes:_times},function () {
                    fail && fail();
                })
            }else{
                setTaskWorks({reloadPageMaxTimes:_times},function () {
                    lazy(function () {
                        location.reload(true);
                    })
                    success && success();
                })
            }
        })
    },

    //字母+数字混排,随机插入横线
    randPasswordStr: function(){
        var a = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z', 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        var b = ['0','1','2','3','4','5','6','7','8','9'];

        var str = '';
        var len = 12;
        var gLen = random(2, 4);//横线有几个
        var abLen = len - gLen;

        for(var i=0; i<abLen; i++){
            if(random(0,1)){
                str += a[random(0, a.length-1)];
            }else{
                str += b[random(0, b.length-1)];
            }
        }

        for(var i=0; i<gLen; i++){
            var l = random(0, str.length-1);
            str = str.substr(0, l) + '-' + str.substr(l);
        }

        return str;
    },

    /**
     * 任务中心汇报任务
     * @param taskType
     */
    report: function (taskVars, taskType) {
        tabCommon.sm(taskVars, '.next_app_task', {taskType:taskType});
    },

    gotoUrl: function (url) {
        window.location.href = url;
    },

    /**
     * 找元素
     * @param ele
     * @returns {boolean}
     */
    findElement: function (ele) {
        if (ele.length) return true;
        else return false;
    },

    /**
     * 等待元素 一般ajax的时候需要使用 每次try使用一秒钟
     * @param ele
     * @param tryTimes
     * @returns {*|Promise}
     */
    waitElementReady: function (funcEle, tryTimes) {
        clue('等待元素加载完成')
        var _ele = funcEle();
        tryTimes = tryTimes || 5;
        return tabCommon.func(function (resolve, reject) {
            if (_ele.length) {
                clue('元素加载完成')
                resolve();
            } else {
                var _intCount = 0;
                var _int = setInterval(function () {
                    clue('等待...' + _intCount)
                    var _ele = funcEle();
                    _intCount++;
                    if (_ele.length) {
                        clearInterval(_int)
                        clue('元素加载完成')
                        resolve();
                    } else if (_intCount >= tryTimes) {
                        clearInterval(_int)
                        clue('元素加载失败')
                        reject();
                    }
                }, 1e3);
            }
        })
    },

    /**
     * 延迟执行
     * @param timer
     * @returns {*|Promise}
     */
    delay: function (timer) {
        return tabCommon.func(function (success, fail) {
            setTimeout(function () {
                success()
            }, timer)
        })
    },

    /**
     * 判断当前链接是否含有某字符串
     * @param str
     * @returns {boolean}
     */
    findStrByHref: function (str) {
        if (window.location.href.indexOf(str) != -1) return true;
        else return false;
    },

    /**
     * 元素存在 就点击元素
     * @param ele
     */
    clickElement: function (ele) {
        if (tabCommon.findElement(ele)) {
            ele[0].click();
        }
    },

    /**
     * 传入多个可以点击的元素 点击第一个可以点到的
     * @param eleArr
     */
    clickElementArr: function (eleArr) {
        var _isClick = false;
        eleArr.forEach(function (p1, p2, p3) {
            if (tabCommon.findElement(p1)) {
                if (_isClick == false) {
                    _isClick = true
                    tabCommon.clickElement(p1);
                }
            }
        })
    },

    checksum:function (chars){
        var sum = 0;
        for (var i=0; i<chars.length; i++)
        {
            var c = chars.charCodeAt(i);
            if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f))
            {
                sum++;
            }
            else
            {
                sum+=2;
            }
        }

        return Math.floor(sum/2);
    },

    TabEnd: {}
}