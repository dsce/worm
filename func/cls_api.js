//接口类
function Api(){

    var _domain = "https://www2017.disi.se/";
    var _domain = "{api_domain}";
    var _api_env = new Array;
    _api_env[0] = "https://www2017.disi.se/";
    _api_env[1] = "http://b22.poptop.cc/";
     _api_env[2] = "http://127.0.0.1:8080/";
     //_api_env[2] = "http://192.168.1.111:8000/";

    _api_env[2] = "http://mars.com/";
    _api_env[2] = "http://192.168.2.172:8099/";
    // _api_env[2] = "https://192.168.2.172/";
    // _api_env[2] = "http://192.168.2.21/";


    //接口地址
    var _url = _domain+'index.php/Admin/ClientApi/';
    var _band_url = _domain+'index.php/Admin/ClientBankApi/';
    var _bank_pay_local_url = _domain+"index.php/Admin/OrderBankPayApi/";

    //add新接口地址配置20160331
    // var _new_domain = 'http://192.168.3.111:2000/';
    var _new_api_env = new Array;
    _new_api_env[0] = 'https://wwwphpapi-0.disi.se/';
    _new_api_env[2] = 'http://192.168.2.21:3000/';
    _new_api_env[2] = 'http://192.168.2.170:8099/';
    // _new_api_env[2] = 'http://192.168.2.170:8099/';

    // _new_api_env[2] = 'https://192.168.2.170/';

    //add end

    //领单使用的域名,其他不用
    var _php_api_0_env = new Array;
    // _php_api_0_env[0] = 'https://phpapi-0.disi.se/';
    _php_api_0_env[0] = 'https://wwwphpapi-0.disi.se/';
    _php_api_0_env[2] = 'http://192.168.2.21:3000/';
    _php_api_0_env[2] = "http://192.168.2.170:8099/";

    //var _bank_pay_local_url = "http://192.168.1.111:91/index.php/Admin/OrderBankPayApi/";


    var _key = 'F$~((kb~AjO*xgn~';
    var _params = {};
    var _api = this;


    this.data = {version: chrome.runtime.getManifest().version, extension: chrome.runtime.id, extension_name: chrome.runtime.getManifest().name, app_secret: _key};

    //设置接口等信息
    this.func_init_cfg = function(){
        
    };

    this.retryTimes = 1;
    this.setRetryTimes = function (times) {
        _api.retryTimes = times||1e8;
    }
    
    this.setParams = function(params){

        var manifest = chrome.runtime.getManifest();

        var base_pms = {
            t: _api.timestamp(),
            version: manifest.version,
            extension: chrome.runtime.id,
            app_secret: _key
        };

        for(var i in base_pms){
            params[i] = base_pms[i];
        }

        _params = params;
    }
    
    //get请求
    this.get = function(func){

        //增加行为参数
        var url = _url + _params.action;

        chrome.storage.local.get(null, function(local) {
            var site = _api_env[local.env] == undefined ? _api_env[0] : _api_env[local.env];
            //增加host_id和api_token
            _params = $.extend(_params, {host_id: local.host_id, api_token: local.key});
            url = url.replace('{api_domain}', site);

            $.get(url,_params,function(ret){
                urlResult(ret,func);
            },'json').error(function(){
                setTimeout(function(){
                    _api.get(func);
                }, 10000);
            });
        });


    }

    //post请求
    this.post = function(func){

        //增加行为参数
        var url = _url + _params['action'];
        chrome.storage.local.get(null, function(local) {
            var site = _api_env[local.env] == undefined ? _api_env[0] : _api_env[local.env];
            url = url.replace('{api_domain}', site);
            //增加host_id和api_token
            _params = $.extend(_params, {host_id: local.host_id, api_token: local.key});

            $.post(url,_params,function(ret){
                urlResult(ret,func);
            },'json');
        });

        
    }

    this.ajaxPOST = function(success,error) {

        //增加行为参数
        var url = _url + _params['action'];

        chrome.storage.local.get(null, function(local) {

            var site = _api_env[local.env] == undefined ? _api_env[0] : _api_env[local.env];
            url = url.replace('{api_domain}', site);
            if(local.host_id){
                //增加host_id和api_token
                _params = $.extend(_params, {host_id: local.host_id, api_token: local.key});
            }

            $.ajax({
                type: "POST",
                url: url,
                data: _params,
                dataType: "JSON",
                success: function (ret) {
                    //请求成功
                    console.log('api ajaxPOST success');
                    urlResult(ret, success);
                },
                error: function (Request, textStatus, errorThrown) {
                    //出错
                    var status = Request.status;
                    if (status == 0) {//adsl拨号未成功 ，稍后重试
                        _api.lose_adsl_exist();
                        if (error) {
                            error();
                        }
                    } else if (status == 401) {//账号认证失败
                        _api.lose_auth();
                    } else {
                        if (error) {
                            error();
                        }
                    }

                    console.log('api ajaxPOST error');
                    console.log('Request:' + Request);
                    console.log(Request);
                    console.log('textStatus:' + textStatus);
                    console.log('errorThrown:' + errorThrown);
                }
            });
        });

    }
    
    var urlResult = function(ret,func){
        if(ret.success == 1){//成功，
            func(ret)
        }else{
            console.error(ret);
            func(ret);
        }
    }

    /*****银行本地支付数据保存*******/
    this.BankPayAjaxPOST = function (success,error){
        _url = _bank_pay_local_url;
        this.ajaxPOST(success,error);
    }
    /*****银行本地支付数据保存*******/

    /*****银行密码状态*******/
    this.BankAjaxPOST = function (success,error){
        _url = _band_url;
        this.ajaxPOST(success,error);
    }
    /*****银行密码状态*******/

    /******写入密码 ************************************************/
    //{type: "boc-login",password: 123456}
    this.WritePassword = function (p,f,err){
        var url = "https://127.0.0.1:2886/";
        var params = p;
        $.ajax({
            type: "GET",
            url: url,
            data: params,
            dataType: "TEXT",
            success: function(ret){
                console.log(ret);
                if(ret == 'true'){
                    f();
                }else{
                    notifyMessage('写入密码失败');
                    if(err){
                        err();
                    }
                }
            },
            error:function(Request, textStatus, errorThrown){
                //出错
                var status = Request.status;
                if(status == 0){
                    notifyMessage('没有正确开启输入密码工具！');
                }else{
                    notifyMessage('写入密码错误，状态：'+status);
                }

                console.log('error');
                console.log('Request:'+Request);
                console.log(Request);
                console.log('textStatus:'+textStatus);
                console.log('errorThrown:'+errorThrown);
            }
        });
    }
    /******写入密码 ************************************************/

    //时间戳
    this.timestamp = function() {
        var timestamp = Math.round(new Date().getTime() / 1000);
        return timestamp;
    }

    //账号认证失败
    this.lose_auth = function(){
        console.log('账号认证错误，检查账号信息');
        this.notify('后台账号输入错误，请检查后台账号信息！');

        var item = {isRunning: false,host_status: 0};
        chrome.storage.local.set(item,function(){
            alert("后台账号输入错误，请检查后台账号信息！");
        });
    }

    //adsl没有拨号成功
    this.lose_adsl_exist = function(){
        console.log('adsl没有链接');
        _api.notify('无法链接到服务器，请确认ADSL是否正常连接。');
    }

    this.notify = function(message){

        if(typeof(notifyMessage) === 'function'){
            notifyMessage(message);
        }else if(typeof(notify) === 'function'){
            notify(message);
        }

    }

    //获取当前主机编号
    this.background_get_file_client_id = function(callback){
        var client_id_url = "file:///C:/client_id.txt";
        chrome.runtime.getPlatformInfo(function(info){
            if(info.os == "mac"){
                client_id_url = "file:///Users/liutao/extensions/client_id.txt";
            }

            console.log(client_id_url);
            $.get(client_id_url).success(function(ret){
                console.log('获取主机信息成功 判断远程信息',ret);
                _api.setParams({host_id: ret});
                _api.getHostInfo(_params,function(result){
                    console.log('获取远程信息成功',result);
                    if(result.success == 1){
                        var host = result.data;
                        var usage = host.application_id == 16 ? 1 : 0;//app付款用途为 1
                        var item = {
                            key: host.api_token,
                            worker: host.admin,
                            host_id: host.host_id,
                            //username: host.admin.name,
                            host_details: host,
                            usage:usage
                        }
                        setLocal(item, function(){
                            console.log("host_id=" + host.host_id);
                            chrome.extension.setUpdateUrlData("host_id=" + host.host_id);
                            var real_name = '';
                            if(host.real_name && host.admin.real_name){
                                real_name = ' - '+host.admin.real_name;
                            }
                            notify(host.host_id+"-"+host.host_province+host.host_city + "-" + host.useage + real_name, 10);
                            callback && callback();
                        })
                    }else{
                        notify(result.message, 10, function(){
                            chrome.management.getSelf(function(selfInfo){
                                chrome.management.setEnabled(selfInfo.id, false, null);
                            });
                        });
                    }
                }, function(){
                    console.log('获取远程信息失败');
                    notify("无法获取主机信息，检查链接是否正常，需再次启用", 10, function(){
                        // chrome.management.getSelf(function(selfInfo){
                        //     chrome.management.setEnabled(selfInfo.id, false, null);
                        // });
                        chrome.windows.create({
                            url: 'adsl:adsl'
                        }, function (win) {
                            setTimeout(function () {
                                closeTabByUrl("adsl:adsl");
                            },3e3)
                            setTimeout(function () {
                                setLocal({update_check: true}, function(){
                                    chrome.runtime.reload();
                                })
                            },15e3)
                        });
                    })
                })

            }).error(function(Request, textStatus){
                console.log('获取主机信息失败',Request,textStatus);
                chrome.extension.isAllowedFileSchemeAccess(function(isAllowedAccess){
                    if(isAllowedAccess){
                        //刷新
                        notify("获取不到主机编号,\r\n请务必确保C:/client_id.txt文件正确,\r\n5秒后重新加载", 5, function () {
                            setTimeout(function(){
                                chrome.runtime.reload();
                            }, 10000);
                        });
                    }else{
                        notify("没有访问文件网址权限, 5秒后自动禁用", 5, function () {
                            // setTimeout(function(){
                            //     chrome.management.getSelf(function (selfInfo) {
                            //         chrome.management.setEnabled(selfInfo.id, false, null);
                            //     });
                            // }, 10000);

                        });
                    }
                });
                // notify("C:/client_id.txt 没有获取到主机编号, 5秒后自动禁用", 10, function(){
                //     chrome.management.getSelf(function(selfInfo){
                //         chrome.management.setEnabled(selfInfo.id, false, null);
                //     });

                // });
            });
        });

    }

    

    /**
     * 新的主机列表状态实时报告
     * @param data
     * @param success
     * @param error
     */
    this.reportHostMessage = function(data, success, error){
        var url = "https://hostmessage.disi.se/users";
        $.post(url, data, function(ret){
            success && success(ret);
        }, "TEXT").error(function(){
            error && error();
        });
    }
    //这个接口不验证api_token
    this.setHostRuningMessage = function(data, success, error){
        useLocal(function(local){
            // var url = "https://phpapi-1.disi.se/v1/host/"+local.host_id+"/update/running_expired_at";
            var url = "https://phpapi-1.disi.se/v1/host/update/running_expired_at";
            // var url = "http://192.168.2.21:3000/v1/host/update/running_expired_at";

            $.ajax({
                type: "PATCH",
                url: url,
                data: $.extend(data, _api.data),
                dataType: "JSON",
                success: function (ret) {
                    console.log("request", ret);
                    success && success(ret);
                },
                error: function (Request, textStatus, errorThrown) {
                    error && error();
                }
            });
        });
    }

    //获取主机信息
    this.getHostInfo = function(host,success, error){
       chrome.storage.local.get(null, function(local) {
            var url = '/v1/host/' + host.host_id + '/get';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            var data = $.extend(host, _api.data);

            $.ajax({
                type: 'POST',
                url: host_server + url,
                data: data,
                timeout:15000,
                dataType: "JSON",
                success: function (ret) {
                    console.log("request", ret);
                    success && success(ret);
                },
                error: function (Request, textStatus, errorThrown) {
                    if (Request.status == 0) {//adsl拨号未成功 ，稍后重试
                        _api.lose_adsl_exist();
                    } else if (Request.status == 401) {//账号认证失败
                        _api.lose_auth();
                    }
                    error && error();
                }
            });
        })
    }

    
    //cnapi-获取版本号
    this.get_extension_update_status = function(host_id, success, error){
        chrome.storage.local.get(null, function(local) {
            if(local.env >0){
                var url = 'https://cnapi.disi.se/hashes/extensions/edffiillgkekafkdjahahdjhjffllgjg';
            }else{
                var url = 'https://cnapi.disi.se/hashes/extensions/' + chrome.runtime.id;
            }

            var data = {host_id: host_id};
            _api.request(url,'GET',data,success,error);
        })
    }

    //api-获取版本号
    this.get_extension_update_status_api = function(host_id, success, error){

        chrome.storage.local.get(null, function(local) {
            var url = '/v1/extension/version';
            var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
            var data = {appid: chrome.runtime.id, host_id: host_id};
            _api.request(host_server +url,'GET',data,success,error);
        })
    }

    //cnapi-获取劫持链接列表
    this.getRequestBlockUrl = function(success, error){
        var url = 'https://cnapi.disi.se/strings/chrome_block_lists';
        _api.request(url, 'GET', {}, success, error);
       
    }

    //api系统的-获取劫持链接列表
    this.getRequestBlockUrlAPI = function(success, error){   
       chrome.storage.local.get(null, function(local) {
            var url = '/v1/chromeblock/get/block_lists';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server + url, 'POST', {}, success, error);
        })
    }

    //标记搜索关键词异常
    this.keywordsException = function(success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/taskorder/'+ local.task.task_order_id +'/update/update_keyword_exception';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',{order_id:local.task.task_order_id},success,error);
        })
    }

    //新接口保存劫持链接
    this.saveInterceptedLinks = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = 'v1/chromeblock/create';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'POST',data,success,error);
        })
    }

    //保存帐户是否使用cookie登录
    this.saveLoginType = function(data,success,error){
         chrome.storage.local.get(null, function(local) {
             var url = 'v1/business_account/' + local.accountId +'/save_login_log';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'POST',data,success,error);
        })
    }

    //新接口获取账号cookie
    this.getAccountCookies = function(accountId, success, error){
         var url = 'https://cnapi.disi.se/db/kv/accounts/' + accountId;
         _api.request(url,'GET',{},success,error);

    }
    //新接口保存账号cookie
    this.saveAccountCookies = function(data, success, error){
        var url = 'https://cnapi.disi.se/db/kv/accounts/' + data.account_id;
        _api.request(url,'POST',{value:data.cookies},success,error);
    }
    // this.saveCookies = function(data, success, error){
    //     chrome.storage.local.get(null, function(local) {
    //          var url = 'v1/business_account/' + data.account_id +'/update/cookies';
    //          var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
    //          _api.request(host_server +url,'PATCH',data,success,error);
    //     })
        
    // }

    //新接口获取账号ua
    this.getAccountUA = function(clientType,accountId, success, error){
        var url = 'https://cnapi.disi.se/db/kv/ua_'+clientType+'/' + accountId;
        _api.request(url,'GET',{},success,error);

    }
    //新接口保存账号ua
    this.saveAccountUA = function(clientType,data, success, error){
        var url = 'https://cnapi.disi.se/db/kv/ua_'+clientType+'/' + data.account_id;
        _api.request(url,'POST',{value:JSON.stringify({ua:data.userAgent})},success,error);
    }

    //保存帐号新密码
    this.resetPwdSuccess = function (account_id, password,success,error) {
        useLocal(function (local) {
            var url = _new_api_env[local.env]+'v1/business_account/'+account_id+'/update_login_password';
            _api.request(url,'PATCH',{account_id:account_id,password:password},success,error);
        })
    }

    //修改密码失败
    this.resetPwdFail = function (account_id, message,success,error) {
        useLocal(function (local) {
            var url = _new_api_env[local.env]+'v1/account_reset_task/password_reset_fail';
            _api.request(url, 'PATCH', {account_id: account_id, message: message}, success, error);
        })
    }

    //新接口领单
    this.getXssTaskOrder = function(success,error){
        chrome.storage.local.get(null, function(local) {
             var url = 'v1/host/'+ local.host_id +'/get_task_order';
             var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
             _api.request(host_server +url,'GET',{},success,error);
        })
    }

    // this.getXssTaskClank = function (order_id, success,error) {
    //     chrome.storage.local.get(null, function(local) {
    //         var url = 'v1/host/'+ local.host_id +'/xss_click_task';
    //         var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
    //         _api.request(host_server +url,'GET',{order_id: order_id},success,error);
    //     })
    // }

    this.reportClankTask = function(data,success,error){
        useLocal(function(local){
            var url = 'v1/host/'+ local.host_id +'/update_click_task';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'PATCH',data,success,error);
        });
    }
    
    
    this.getClickTasks = function (para,success,error) {
        useLocal(function (local) {
            var url = 'v1/host/'+ local.host_id +'/new_click_task';
            var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
            _api.request(host_server +url,'GET',para?para:{},success,error);
        })
    }

    this.getReceiptTasks = function (success,error) {
        useLocal(function (local) {
            var url = 'v1/host/'+ local.host_id +'/receipt/get_task';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'GET',{client_id:local.host_id},success,error);
        })
    }

    

    //新api获取分配账号
    this.getXssTaskAssignment = function(data,success, error){
        chrome.storage.local.get(null, function(local) {
            var url = "/v1/taskorder/" + data.order_id +"/task_order_assignment";
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url, 'POST', data, success, error);
         })
    }
    

    //新接口获取推广链接
    this.getXssTaskPromotionUrl = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = 'v1/taskorder/' + data.order_id +'/get_promotion_url';
             var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
             _api.request(host_server +url,'POST',data,success,error);
        })
    }

    this.updateAccountBirthday = function(){
        useLocal(function(local){
            var data = {account_id: local.accountId};
            var url = 'v1/business_account/' + local.accountId +'/update/birthday';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'PATCH',data);
        });
    }

    this.getOrderPaymentInfo = function(success, error){
        useLocal(function(local){
            var data = {order_id:local.task.task_order_id};
            var url = '/v1/taskorder/'+local.task.task_order_id+'/get_payment_info';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'GET',data, success, error);
        });
    }

    this.updateAccountNikename = function(){
        useLocal(function(local){
            var data = {account_id: local.accountId};
            var url = 'v1/business_account/' + local.accountId +'/update/nickname';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'PATCH',data);
        });
    }

    //获取聊天信息
    this.getChatMsg = function(success, error){
        chrome.storage.local.get(null, function(local) {
             // var url = 'v1/chats/get';
             var url = 'v1/taskorder/' + local.tasks[local.taskId].id +'/get_chats';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'GET',{},success,error);
        })
        
    }

    //保存订单信息
    this.saveOrderInfo = function(order_info,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = 'v1/taskorder/'+ local.task.task_order_id +'/save_order_info';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',order_info,success,error);
        })
    }

   

    //获取银行卡信息-new
    this.getPaymentInfo = function(success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/admincard/get_team_useable_card';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];

             getSharedTaskWorks(function(sTask){
                if(sTask.is_group){
                    var order_id = local.task.orders[0].id;
                }else{
                    var order_id = local.task.task_order_id;
                }

                _api.request(host_server +url,'GET',{order_id:order_id},success,error);
             })
             
        })
    }

    //保存待付款信息
    this.saveCardPay = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/taskorder/save_card_pay';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'POST',data,success,error);
        })
    }

    //判断IP重复http://192.168.2.21:3000/v1/host/gd001/get/order_start_ip_no_repeat?app_secret=F$~((kb~AjO*xgn~&host_id=gd001&host_ip=11.11.11.11
    this.orderIpNoRepeat = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/host/' + data.host_id +'/get/order_start_ip_no_repeat';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server +url,'GET',data,success,error);
        })
    },

    //获取付款状态
    this.getPayState = function(success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/taskorder/get_pay_state';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'GET',{task_order_id:local.task.task_order_id},success,error);
        })
    }

    //保存收货地址
    this.saveConsignee = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/taskorder/' + local.task.task_order_id +'/save_consignee';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',data,success,error);
        })
    }

    //重置账号
    this.accountReset = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/taskorder/' + local.task.task_order_id +'/reset';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',data,success,error);
        })
    }

     //缺货异常
    this.setException = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = 'v1/taskorder/set_exception';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',data,success,error);
        })
    }

    //新接口获取账号信息
    this.getBusinessAccountPassword = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/business_account/' + data.account_id  + '/get';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'GET',data,success,error);
        })
    }

    //新接口获取更换身份证信息
    this.changeIdCard = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/business_account/' + data.business_account_id  + '/idcard_change';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',data,success,error);
        })
    }

    //新接口保存任务订单数据
    this.taskOrderSubmit = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/taskorder/' + local.task.task_order_id +'/task_order_submit';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'PATCH',data,success,error);
        })
    }

    //新接口app领付款任务
    this.getAppPayTask = function(data,success,error){
        chrome.storage.local.get(null, function(local) {
             var url = '/v1/host/' + data.host_id +'/get_app_pay_task_order';
             var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
             _api.request(host_server +url,'GET',data,success,error);
        })
    }

    //保存账号级别
    this.saveAccountLevel = function (data,success,error) {
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/business_account/' + data.id +'/update/level';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server + url,'PATCH',data,success,error);
        })     
    },

    //保存订单审核状态数据
    this.saveBusinessCheckOrder = function (data,success,error) {
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/taskorder/jd_order_states';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server + url,'POST',data,success,error);
        })     
    },

    //保存订单评价状态数据
    this.saveCommentState = function (data,success,error) {
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/comment/save/sate';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server + url,'PATCH',data,success,error);
        })     
    },

    //订单JD状态检查并保存
    this.saveNofoundOrder = function (data,success,error) {
        chrome.storage.local.get(null, function(local) {
            var url = '/v1/taskorder/not_found_order/add';
            var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
            _api.request(host_server + url,'POST',data,success,error);
        })     
    },

        //订单JD状态检查并保存
        this.getClientTaskType = function (data,success,error) {
            chrome.storage.local.get(null, function(local) {
                var url = '/v1/host/' + local.host_id +'/get/taskType';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'GET',data,success,error);
            })
        },
        //订单JD状态检查并保存
        this.getXssTaskLists = function (data,success,error) {

            var data = data ? data : {};

            chrome.storage.local.get(null, function(local) {
                var url = '/v1/host/' + local.host_id +'/get/XssMixedTasks';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                if(chrome.runtime.id == 'apkehafjkglmefgenfdhofmflijglclg' || chrome.runtime.id == 'pacgjeifepfilgalcbpkcghmebbmgafc'){
                    data = {is_test:true};
                }
                
                _api.request(host_server + url,'GET',data,success,error);
            })
        },

        //新接口禁用账号
        this.disabledAccount = function(data, success, error){
            chrome.storage.local.get(null, function(local) {
                var url = 'v1/account/disable_type_channel';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'POST',data,success,error);
            })
        },

        //新接口启用账号
        this.enableAccount = function(data, success, error){
            chrome.storage.local.get(null, function(local) {
                var url = 'v1/account/enable';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'POST',data,success,error);
            })
        },

        this.updateAvatar = function(account_id, success, error){
            chrome.storage.local.get(null, function(local) {
                var url = '/v1/business_account/'+account_id+'/update/avatar';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'PATCH',{account_id:account_id},success,error);
            })
        },

        this.saveBusinessOrderToRemote = function (orders, success, error) {
            //保存 订单状态订单
            chrome.storage.local.get(null, function(local) {
                var url = 'v1/taskorder/save_order_states';
                var post_data = {
                    business_orders: orders,
                    source_name: chrome.runtime.getManifest().name,
                    version: chrome.runtime.getManifest().version
                };
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url, 'POST', post_data, success, error);
            })
        },

        this.getHotCommentTagStatistics= function (pid, done, fail) {
            var url = 'http://s.club.jd.com/productpage/p-' + pid + '-s-0-t-3-p-0.html';
            var data = {};
            $.getJSON(url, data, function (data) {
                done && done(data);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                fail && fail();
            });
        },

        this.productTagsSave = function (product_id, tags, success, error) {
            chrome.storage.local.get(null, function(local) {
                var url = 'v1/product/product_tags_save';
                var post_data = {
                    tags: tags,
                    item_id: product_id
                };
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url, 'POST', post_data, success, error);
            })
        },

        //保存截图地址
        /**
         * type 0:未知类型 1收 2评 3晒
         */
        this.saveScreenCaptureUrl = function (order_id,type,image_url, success, error) {
            chrome.storage.local.get(null,function(local){
                var url = 'v1/taskorder/' + order_id + '/save_order_image';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                var post_data ={
                    order_id:order_id,
                    type:type,
                    image_url:image_url
                }
                _api.request(host_server + url, 'POST', post_data, success, error);
            })
        },

        //保存晒单图片结果
        this.saveSharePictureUrl = function(share_id,imgUrl,result,success,error){
            chrome.storage.local.get(null,function(local){
                var url = 'v1/host/'+local.host_id+'/receipt/share_picture';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                var post_data ={
                    id:share_id,
                    url:imgUrl,
                    client_result:result
                }
                _api.request(host_server + url, 'PATCH', post_data, success, error);
            })
        },

        //保存追评中晒单图片结果
        this.saveAppendSharePictureUploadResult = function(data,success,error){
            chrome.storage.local.get(null,function(local){
                var url = 'v1/appended/update/result_code';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url, 'PATCH', data, success, error);
            })
        },

        //保存晒单图片结果
        this.saveSharePictureUploadResult = function(data,success,error){
            chrome.storage.local.get(null,function(local){
                var url = 'v1/share/update/result_code';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url, 'PATCH', data, success, error);
            })
        },

        //保存追评中晒单图片结果
        this.saveAppendCommentCapturePictureUrl = function(appended_id,imgUrl,result,success,error){
            chrome.storage.local.get(null,function(local){
                var url = 'v1/host/'+local.host_id+'/appended/appended_picture';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                var post_data ={
                    id:appended_id,
                    url:imgUrl,
                    client_finish_result:result
                }
                _api.request(host_server + url, 'PATCH', post_data, success, error);
            })
        },

        this.reportReplaceWords = function (business_oid,words,success,error) {
            useLocal(function (local) {
                var url = 'v1/host/'+local.host_id+'/receipt/report_shielding_words';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                var post_data = {
                    words: words,
                    referer: business_oid
                };
                _api.request(host_server + url, 'PATCH', post_data, success, error);
            })
        },


        this.reportRearTask = function (data, success, error) {
            chrome.storage.local.get(null,function(local){
                var url = "v1/host/" + local.host_id+ "/receipt/report_status";
                var post_data = {
                    client_id: local.host_id,
                    cmd: data.type,
                    order_id: data.order_id,
                    delay: data.delay,
                    message: data.message
                };
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'PATCH',post_data,success,error);
            })
        },

        //https://192.168.2.170/v1/business_account/{account_id}/unbundling_mobile
        this.unboundMobile = function(success, error){
            chrome.storage.local.get(null, function(local) {
                var data = {business_account_id:local.accountId};
                var url = '/v1/business_account/' + local.accountId +'/unbund_mobile';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server + url,'PATCH',data,success,error);
            })
        }

        /***新领包接口***/
        //领包
        this.getXssGroupTaskOrder = function(success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/host/' + local.host_id +'/get_task_order_group';
                 var host_server = _php_api_0_env[local.env] == undefined ? _php_api_0_env[0] : _php_api_0_env[local.env];
                 _api.request(host_server +url,'GET',{},success,error);
            })
        }

        //分账号
        this.getXssGroupTaskAssignment = function(data,success, error){
            chrome.storage.local.get(null, function(local) {
                var url = "/v2/ordergroup/" + data.order_group_id +"/order_group_assignment";
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server +url, 'GET', data, success, error);
             })
        }

        //混合任务
        this.getXssGroupTaskLists = function (data,success,error) {

            // var data = data ? data : {};

            chrome.storage.local.get(null, function(local) {
                var url = '/v2/host/' + local.host_id +'/order_group/XssMixedTasks';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                // if(chrome.runtime.id == 'apkehafjkglmefgenfdhofmflijglclg' || chrome.runtime.id == 'pacgjeifepfilgalcbpkcghmebbmgafc'){
                //     data = {is_test:true};
                // }
                
                _api.request(host_server + url,'GET',data,success,error);
            })
        },

        //保存包收货地址
        this.saveGroupConsignee = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + data.order_group_id +'/save_consignee';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'PATCH',data,success,error);
            })
        }

        //保存包订单信息
        this.saveGroupOrdersInfo = function(order_info,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + order_info.order_group_id +'/save_order_group_info';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'PATCH',order_info,success,error);
            })
        }

        //保存包付款信息
        this.saveGroupCardPay = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                var url = '/v2/ordergrouppayment/save_card_pay';
                var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                _api.request(host_server +url,'POST',data,success,error);
            })
        }

        //获取包付款状态
        this.getGroupPayState = function(success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = 'v2/ordergrouppayment/get_pay_state';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'GET',{order_group_id:local.task.id},success,error);
            })
        }

        //提交包数据
        this.taskGroupOrderSubmit = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                     var url = 'v2/ordergroup/' + local.task.id +'/order_group_submit';
                     var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                     _api.request(host_server +url,'PATCH',data,success,error);
                })
        }

        //保存包状态
        this.saveGroupOrderState = function(success,error){
            chrome.storage.local.get(null, function(local) {
                     var url = 'v2/ordergroup/' + local.task.id +'/save_order_group_state';
                     var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                     _api.request(host_server +url,'PATCH',{order_group_id:local.task.id},success,error);
                })
        }

        //包重置账号
        this.accountGroupReset = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + local.task.id +'/reset';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'PATCH',data,success,error);
            })
        }

        //包异常操作
        this.setGroupException = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + local.task.id +'/set_exception';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'PATCH',data,success,error);
            })
        }

        //订单包关键词异常
        this.keywordsGroupException = function(success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + local.task.id + '/update_keyword_exception';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'PATCH',{order_group_id:local.task.id,order_id:local.tasks[local.taskId].id},success,error);
                })
        }

        //获取订单包信息
        this.getGroupOrdersInfo = function(success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + local.task.id + '/orders_info';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 var data = {order_group_id:local.task.id};
                 _api.request(host_server +url,'GET',data,success,error);
            })
        }

        //保存订单包信息
        this.saveGroupBusinessOrdersInfo = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v2/ordergroup/' + local.task.id + '/save_order_business_oid';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 data.order_group_id = local.task.id;
                 _api.request(host_server +url,'PATCH',data,success,error);
            })
        }

        //app包领付款任务
        this.getAppGroupPayTask = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v1/host/' + data.host_id +'/get_app_pay_group';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'GET',data,success,error);
            })
        }

        /**领包接口END**/

        /***search任务新增api***/
        //保存商品位置
        this.savePosition = function(data,success,error){
            chrome.storage.local.get(null, function(local) {
                 var url = '/v1/taskorder/' + data.order_id +'/keyword_position';
                 var host_server = _new_api_env[local.env] == undefined ? _new_api_env[0] : _new_api_env[local.env];
                 _api.request(host_server +url,'POST',data,success,error);
            })
        }

        /**自营使用接口**/
        this.businessAccountRecentCategory = function(hostId, accountId, success, error){
            var url = "/index.php/Admin/ProductApi/business_account_recent_category";
            var postData = {
                host_id: hostId,
                business_account_id: accountId
            };
            this.request(url, 'POST', postData, success, error);
        }

        this.saveSelfSupportOrderToRemote = function(host_id,order, success, error){
            var url = "/index.php/Admin/ReceiptApi/self_task_order_add";
            var post_data = {
                host_id: host_id,
                business_id: 1,
                item_id: order.item_id,
                product_name: order.product_name,
                product_price: order.product_price,
                amount: order.amount,
                business_account_id: order.business_account_id,
                business_oid: order.business_oid,
                business_payment_fee: order.business_payment_fee,
                business_use_jd_beans: order.business_use_jd_beans,
                source: chrome.runtime.getManifest().name
            };
            this.request(url, 'POST', post_data, success, error);
        }

        this.productSave = function(hostId, product, success, error){
            var url = "/index.php/Admin/ProductApi/product_save";
            var postData = {
                host_id: hostId,
                item_code: product.item_code,
                item_url: product.item_url,
                name: product.name,
                sub_name: "",
                category_name: product.categoryName,
                category_code: product.categoryCode,
                price: product.price,
                promotion: "",
                shop_id: 0,
                shop_name: product.shop_name,
                business_id: 1,
                picture: product.picture,
                category_last_code: product.categoryLastCode,
                jd_self: product.jd_self
            }

            this.request(url, 'POST', postData, success, error);
        }

        this.selfOrderTaskUpdate = function(host_id, data, success, error){
            var url = "/index.php/Admin/BusinessAccountApi/self_order_task_update";
            var postData = {
                id: data.id,
                host_id: host_id,
                status: data.status,
                message: data.message
            }
            this.request(url, 'POST', postData, success, error);
        }
        //end

        this.request = function(url, type, data, success, error){
        chrome.storage.local.get(null, function(local) {

            if(url.indexOf('http') == -1){//disi.se
                 var host_server = _api_env[local.env] == undefined ? _api_env[0] : _api_env[local.env];
                 url = host_server + url;
            }else{//phpapi.disi.se
                //新接口
                data.api_token = local.host_details.api_token;
                data.host_id = local.host_id;
            }
            
            data = $.extend(data, _api.data,{api_token: local.key, host_id: local.host_id});
            console.log('request data',data);

            ajaxSend();

            function ajaxSend() {
                _api.retryTimes--;
                console.warn('api请求数据','['+type+']','['+_api.retryTimes+']',url);
                $.ajax({
                    type: type,
                    url: url,
                    data: data,
                    dataType: "JSON",
                    timeout: 30000, //增加超时时间30s
                    success: function (ret) {
                        console.log("apiRequest", ret);
                        var ret_msg = ret.message ? ret.message :'';
                        var request_url = url.replace(/http[s]*:\/\/[\w-]+.disi.se\//,'');
                        send_notes_to_tracker('请求:' + request_url + ' 请求参数：' + JSON.stringify(data) + ' 返回结果:' + JSON.stringify(ret));
                        //success && success(ret);
                        if(_api.retryTimes<=0) {
                            success && success(ret);
                        }else{
                            if(ret.success==1){
                                success && success(ret);
                            }else{
                                setTimeout(ajaxSend,1000);
                            }
                        }
                    },
                    error: function (Request, textStatus, errorThrown) {
                        console.error('apiError:',Request, textStatus, errorThrown);
                        if (Request.status == 0) {//adsl拨号未成功 ，稍后重试
                            _api.lose_adsl_exist();
                        } else if (Request.status == 401) {//账号认证失败
                            _api.lose_auth();
                        }
                        if(_api.retryTimes<=0) {
                            error && error();
                        }else{
                            setTimeout(ajaxSend,1000);
                        }
                    }
                });
            }
        });

    }


    //init
    this.func_init_cfg();
}