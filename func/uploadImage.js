//上传图片文件

var uploaded_nums = 0;

function uploadImage(sourceImage,success,error){
    //上传图片地址
    var upload_path = 'https://afile.disi.se/api/attachment/';
    if(uploaded_nums>0){
        var upload_path = 'https://wwwafile.disi.se/api/attachment/';
    }

    var data = {
            kind_code : 'ce-comment',
            deadline  : new Date().getTime() + 60*60,
            token : '3QP1nk1HQsyzsA2XjY6ZLC8Vl81fvaP9jnzKuAFoFe13ygk3',
            file : imgBase64ToBlob(sourceImage)
    }

    data.sign = $.md5(data.deadline + data.kind_code + data.token);

    console.log(data);

    var formData = new FormData();
    formData.append('kind_code', data.kind_code);
    formData.append('deadline', data.deadline);
    formData.append('token', data.token);
    formData.append('sign', data.sign);
    formData.append('method', 'upload');
    formData.append('file', data.file,'123.png');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', upload_path, true);

    xhr.send(formData);

    xhr.onerror = function(e){
        uploaded_nums++;
        uploadImage(sourceImage,success,error);
    }

    xhr.onload = function(e) {
            console.log(e);
            if (this.readyState==4){
                if (this.status == 200) {
                    console.log('提交图片成功');
                    var result = $.parseJSON(this.response);
                    console.log(result);
                    if(result.url){
                        success && success(result.url);
                        // saveScreenCaptureUrl(result.url);
                    }
                }else{
                    console.log('提交图片未成功');
                    if(uploaded_nums < 3){
                        uploaded_nums  = uploaded_nums + 1;
                        uploadImage(sourceImage,success,error);
                    }else{
                        //继续
                        uploaded_nums = 0;
                        error && error();
                    }

                }
            }else{
                console.log('请求接口失败');
                if(uploaded_nums < 3){
                    uploaded_nums = uploaded_nums + 1;
                    uploadImage(sourceImage,success,error);
                }else{
                    //继续
                    uploaded_nums = 0;
                    error && error();
                }
            }
        };
}

//图片转为2进制
function imgBase64ToBlob(b64){
	//b64 to Blob
	var arr = b64.split(',');
	var mime = arr[0].match(/:(.*?);/)[1];
	var bstr = atob(arr[1]);
	var n = bstr.length;
	var u8arr = new Uint8Array(n);

	while(n--){
	u8arr[n] = bstr.charCodeAt(n);
	}
	return new Blob([u8arr], {type:mime});
}
