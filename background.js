//worm
$(function () {

    //初始化
    init_cfg(function () {
        //chrome系统设置
        chromeContentSetting();

        //页面消息 侦听
        backgroundMessageListener();

        //302控制
        chromeRedirectListener();

        //关闭所有下载
        chromeDownloadsCancel();

        //阻止google链接
        impede_request_listener();

        //拦截jd广告
        //intercept_ad_request_listener();

        //打不开的地址进行重定向,,20170424不使用
        //received_redirect_listener();

        //监听网络请求超时
        listenErrRequest();

        //接收插件外部消息
        messageExternalListener();

        //截获建行请求
        ccbFormDataIntercepted();

        //截获交行请求
        bcomFormDataIntercepted();

        //截获工行数据
        icbcFormDataIntercepted();

        //截获中行请求
        bocFormDataIntercepted();

        //看门狗计时器执行
        //watchDogTimer();

        //监听插件更新是否成功
        // update_check_available_listener();

        //background 开始执行 默认要执行的操作
        background_default();

        labelWatcher();

    });
});

/**
 * 初始化 stroage 数据
 * @param func
 */
function init_cfg(callback) {
    console.log('XSS background init...');
    chrome.storage.local.get(null, function(local) {
        // console.log("host_id=" + local.host_id);
        // chrome.extension.setUpdateUrlData("host_id=" + local.host_id);
        console.log('local.isRunning' ,local.isRunning);
        if(!local.isRunning && local.isRunning == undefined){
            _cfg.isRunning = true;
        }else{
            _cfg.isRunning = local.isRunning;
        }
        
        setLocal(_cfg, function () {
            preCheckExtension(callback);
            // callback && callback();
        });
    });
}


//预先检查插件勾选情况
function preCheckExtension(callback){
    chrome.extension.isAllowedIncognitoAccess(function(isAllowedIncognitoAccess){
        if(isAllowedIncognitoAccess){
            // console.log('隐身模式已启用');
            notify('隐身模式已启用');
            chrome.extension.isAllowedFileSchemeAccess(function(isAllowedFileSchemeAccess){
                if(isAllowedFileSchemeAccess){
                    // console.log('允许访问文件网址已勾选');
                    notify('允许访问文件网址已勾选');
                    callback && callback();
                }else{
                    // console.log('允许访问文件网址未勾选,30S后请求6085');
                    notify('允许访问文件网址未勾选,60S后请求6085');
                    setTimeout(function(){
                        preSetExtension(callback);
                    },90*1000)
                    
                }
            })
        }else{
            // console.log('隐身模式未启用,30S后请求6085');
            notify('隐身模式未启用,60S后请求6085');
            setTimeout(function(){
                preSetExtension(callback);
            },90*1000)
        }
    })
}

//勾选插件设置
function preSetExtension(callback){
    // console.log('准备请求6085');

    var url = 'http://127.0.0.1:6085/check';
    notify('准备请求:' + url);
    $.ajax({url: url,type:'GET',data:{}, timeout: 30000}).done(function (data) {
        console.log(data);
        if(data == '1'){
            notify('请求6085成功，30S后再次检测');
        }else{
            notify('请求6085失败,30S后重新请求');
        }

        setTimeout(function(){
            preCheckExtension(callback);
        },60*1000)

    }).fail(function (Request, textStatus, errorThrown) {
        notify("请求6085接口失败，30S后再次请求");
        setTimeout(function(){
            preSetExtension(callback); 
        },60*1000)

    });
}

//代理拨号
function changeIp(callback){
    // console.log('准备请求6085');

    var url = 'http://127.0.0.1:6085/change';
    console.log('准备请求:' + url);
    $.ajax({url: url,type:'GET',data:{}, timeout: 30000}).done(function (data) {
        console.log(data);
        notify('请求6085代理拨号成功，15S后检测IP');
        callback && callback(true);
        // if(data == '1'){
        //     notify('请求6085代理拨号成功，15S后检测IP');
        //     callback && callback(true);
        // }else{
        //     console.log('6085代理拨号失效，请求ADSL');
        //     // setTimeout(function(){ },15*1000)
        //         // changeIp(callback);
        //         callback(false)
           
        // } 

    }).fail(function (Request, textStatus, errorThrown) {
        console.log("请求6085代理拨号接口失败，请求ADSL");
        // setTimeout(function(){},15*1000)
            // changeIp(callback); 
            callback(false)
        

    });
}

/**
 * 记录
 * @param message
 */
function notes(message){
    console.log(message);
    send_notes_to_tracker(message);
}
/**
 * 桌面提醒
 * @param message
 * @param clear
 */
function notify(message, sec, callback) {
    console.log(message);
    send_notes_to_tracker(message);
    var opt = {
        type: 'basic',
        title: '奥迪',
        message: message,
        iconUrl: 'icon.png'
    };

    chrome.notifications.create('', opt, function (id) {
        sec = parseInt(sec) > 0 ? parseInt(sec) : 5;
        setTimeout(function () {
            chrome.notifications.clear(id, function(){
                callback && callback();
            });
        }, sec * 1000);
    });


    //桌面提醒增加状态报告
    //hostRunningMessage(message, 60);
}

/**
 * 确认ip格式
 * @param value
 * @returns {boolean}
 */
function validate_ip_address(value) {
    var re = new RegExp(/\d+\.\d+\.\d+\.\d+$/)
    return re.test(value)
}

/**
 * 设置一号店cookie  收货地区，选择 ,按照省份设置
 * @param province
 * @param func
 */
function yhd_set_cookie_province_id(province, func) {

    var province_id = gbl_const_yhd_address_map[province];
    if (province_id == undefined) {
        province_id = 1;//如果 没有要设置的cookie值，就默认为 1 上海
        province = "上海"
    }

    if (province_id != undefined) {
        notify("设置一号店收货地区 " + province);
        console.log('设置地址provinceId = ' + province);


        var province_cookie = {
            url: 'http://www.yhd.com',
            name: 'provinceId',
            value: province_id.toString(),
            domain: 'yhd.com',
            path: '/'
        };

        chrome.cookies.set(province_cookie, function () {

            var item_province_cookie = {
                url: 'http://item.yhd.com',
                name: 'provinceId',
                value: province_id.toString(),
                domain: 'yhd.com',
                path: '/'
            };
            chrome.cookies.set(item_province_cookie, function () {

                if (func) {
                    func();
                }
            });

        });

    }
}


//清除cookie 
function clearCookies(func) {
    useLocal( function (data) {
        var OldStorage = data;
        var TmpStorage = {
            "env": OldStorage.env ? OldStorage.env : 0,
            "key": OldStorage.key ? OldStorage.key : '',
            "version_env": OldStorage.version_env ? OldStorage.version_env : 0,
            "worker": OldStorage.worker ? OldStorage.worker : 0,
            "host_details": OldStorage.host_details ? OldStorage.host_details : '',
            "update_check": OldStorage.update_check ? OldStorage.update_check : false,
            "extension_reload": OldStorage.extension_reload ? OldStorage.extension_reload : false,
            "usage": OldStorage.env ? OldStorage.usage : 0,
            "isRunning": OldStorage.isRunning ? OldStorage.isRunning : false,
            "host_id": OldStorage.host_id ? OldStorage.host_id : '',
            "host_status": OldStorage.host_status ? OldStorage.host_status : 0,
            "host_step": OldStorage.host_step ? OldStorage.host_step : 0,
            "reported_status": OldStorage.reported_status ? OldStorage.reported_status : 0,
            "username": OldStorage.username ? OldStorage.username : 0,
            "password": OldStorage.password ? OldStorage.password : 0,
            "pay": OldStorage.pay ? OldStorage.pay : '',
            "last_ip": OldStorage.last_ip ? OldStorage.last_ip : 0,
            "proxy_ip": ''
        };


        //chrome.storage.local.clear(function(){
        var removeLocal = ['rand_search_items', 'around_items', 'order_id', 'order_should_pay', 'orderinfo', 'task', 'task_finish_show', 'temp_pay_fee', 'temp_shipping_fee', 'order_address', 'randItemIds', 'entry_order_info', 'lookOrderTask', 'productDetailTask', 'productCategory2Task', 'productCategory3Task', 'frontPageTask',
            'account', 'accountId', 'taskType', 'tasks','taskWorks','sharedTaskWorks',
            'TaskWorks', 'taskId'
        ];
        chrome.storage.local.remove(removeLocal, function(){
            chrome.browsingData.remove(
                {
                    originTypes: {
                        unprotectedWeb: true,
                        protectedWeb: true
                    }
                }, {
                    cookies: true,
                    appcache: true,
                    cache: true,
                    history: true,
                    indexedDB: true,
                    localStorage: true
                },
                function () {
                    notes('清理storage成功')
                    //setLocal(TmpStorage, func);
                    setLocal({clearCookies:true},function(){
                        func && func();
                    })
                    
                }
            );
        });


    });
}

function delCookies(){
    chrome.cookies.getAll({domain:'.jd.com'}, function(cookies){
        console.log(cookies);
        for(i=0; i<cookies.length;i++) {
           removeCookie(cookies[i]);
        }
    });
}

//删除cookie
function removeCookie(cookie) {
  var url = "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
  console.log(url + cookie.name);
  chrome.cookies.remove({"url": url, "name": cookie.name});
}



//关闭随机链接页面
// function close_rand_click_url(url){
//     chrome.tabs.query({url:'*:' + url},function(tabs){
//         console.log(tabs);
//         if(tabs.length >0){
//             for(var i in tabs){
//                 chrome.tabs.remove(tabs[i].id);
//             }

//             setLocalTask({randClicked:false},function(){});
//         }
//     })
// }
//关闭标签tab
function closeTabByUrl(url) {
    //console.log(sender);
    console.log('close url',url);
     chrome.tabs.query({
         url: url
     }, function (tabs) {
         if (tabs.length > 0) {
             chrome.tabs.remove(tabs[0].id, function () {
            //chrome.tabs.remove(sender.tab.id, function () {

            });
         }

     });
}
//创建随机点标签页
function create_tab_by_rand_click_url(url,sender) {
    chrome.tabs.create({
        url: url
    }, function (tabs) {

        chrome.tabs.update(sender.tab.id,{selected:true},function(details){
            console.log(details);
        })
        var rand_time = random(5,80);
        send_notes_to_tracker('打开随机页面：' + url + ',' + rand_time + 'S后关闭');
        setTimeout(function(){
            chrome.tabs.remove(tabs.id);
            setLocalTask({randClicked:false},function(){

            });
        },rand_time*1000)
    });
}



/**
 * 保存主机订单任务相关信息
 */
function saveHostTaskOrderDetail(sender) {

    API = new Api();
    useLocal( function (local) {


        console.log(local);

        getSharedTaskWorks(function(shareTask){

            if (local.task && shareTask.orderinfo) {
        //if (local.task && local.orderinfo) {
            var submit = {
                host_id: local.host_id,//
                username: local.username,//
                password: local.password,//

                task_order_id: local.task.task_order_id,//   任务 id

                is_handwork: shareTask.orderinfo.is_handwork,//是否极速保存//默认0,极速2

                business_oid: shareTask.orderinfo.order_id,//   订单id
                business_discount_fee: shareTask.orderinfo.discount_fee,// 用券金额(折扣)
                business_payment_fee: shareTask.orderinfo.payment_fee,//  券扣金额(实际银行支付的金额)
                business_freight_fee: shareTask.orderinfo.yunfei,//  运费

                //consignee_province: local.order_consignee.province,//
                //consignee_city: local.order_consignee.city,//
                //consignee_area: local.order_address !== undefined ? local.order_address.area : '',//区，没有为空
                //consignee_short_address: local.order_consignee.short_address,//

                consignee_name: shareTask.orderinfo.consignee_user,// 收货人姓名
                consignee_mobile: shareTask.orderinfo.consignee_mobile,//  收货人电话
                consignee_address: shareTask.orderinfo.consignee_address// 收货人地址
            }

            // API.post(function (ret) {
            API.taskOrderSubmit(submit,function (ret) {
                //清除手机端的ua切换
                change_useragent_removelistener();

                //设置保存成功
                if (ret.success == 1) {

                    setHostStep(9);//保存数据完毕，设置第9步

                    var task = local.task;
                    task.card_no = '';
                    task.bank_user = '';
                    task.task_order_id = '';

                    setLocal({task_finish_show: true,task:task,bank_info:null}, function () {
                        console.log('数据保存成功');
                        notify("数据保存成功");
                        setHostStatus(2000000, function(){
                            sdTaskDone(sender);
                        });//任务完成

                        // setHostStatus(2000000, startHost);//任务完成
                        chrome.tabs.sendMessage(sender.tab.id,{act:'save_host_task_order_detail_result'})
                    });

                 }else if(ret.message == 'host_id不一致'){
                    setHostStatus(4);//host_id 不一致
                    notify(ret.message);
                    chrome.storage.local.set({isRunning: false}, function() {
                      notify("插件暂停", true);
                    });
                    return false;

                } else {
                    console.log(ret.message);
                    notify(ret.message);
                    if (ret.message === '已完成订单不能重复提交') {
                        console.log("已完成订单不能重复提交");
                        setHostStatus(2000000, function(){
                            sdTaskDone(sender);
                        });//任务完成
                        // setHostStatus(2000000, startHost);//任务完成
                        chrome.tabs.sendMessage(sender.tab.id,{act:'save_host_task_order_detail_result'})
                    }else if(ret.message == '提交订单事务异常'){
                        console.log('提交订单事务异常,再次提交');
                        setTimeout(function(){
                            saveHostTaskOrderDetail(sender);
                        },5*1000)

                    }else{
                        var item = {isRunning:0};
                        setLocal(item,function(){
                            notify(ret.message);
                        });
                    }

                }


            },function(){
                console.log('请求保存提交订单接口失败,3S后重新请求保存');
                setTimeout(function(){
                    saveHostTaskOrderDetail(sender);
                },3000);
            });
        } else {
            console.log("数据不完整");
            console.log(local);
        }



    });
    });
}

function sdTaskDone(sender){

    setHostStatus(2000000, function(){
        TC.done(sender,{taskType:'payment'})
    });//任务完成
}

function sdGroupTaskDone(sender,task_sub_order_id){

    getTaskWorks(function(tWorks){
        var finished_sub_orders = tWorks.finished_sub_orders || {};
        finished_sub_orders[task_sub_order_id].finished = true;
        setTaskWorks({finished_sub_orders:finished_sub_orders},function(){
            notify('保存订单数据成功，完成状态已写入');
            setTimeout(function(){
                chrome.tabs.remove(sender.tab.id);
            },3*1000)
            
        })
    })
   
}

function saveGroupHostTaskOrderDetail(sender,sub_order_info) {

    var task_sub_order_id = sub_order_info.sub_task_order_id;

    API = new Api();
    useLocal( function (local) {


        console.log(local);

        getTaskWorks(function(tworks){

            if (local.task && tworks.finished_sub_orders[task_sub_order_id] && tworks.finished_sub_orders[task_sub_order_id].orderinfo) {

        //if (local.task && local.orderinfo) {
            var orderinfo = tworks.finished_sub_orders[task_sub_order_id].orderinfo;

            var submit = {
                host_id: local.host_id,//
                // username: local.username,//
                // password: local.password,//

                // task_order_id: local.task.task_order_id,//   任务 id
                order_id:task_sub_order_id,

                is_handwork: orderinfo.is_handwork,//是否极速保存//默认0,极速2

                business_oid: orderinfo.order_id,//   订单id
                business_discount_fee: orderinfo.discount_fee,// 用券金额(折扣)
                business_payment_fee: orderinfo.payment_fee,//  券扣金额(实际银行支付的金额)
                business_freight_fee: orderinfo.yunfei,//  运费

                order_gifts : sub_order_info.gift_list ? sub_order_info.gift_list : []
                //consignee_province: local.order_consignee.province,//
                //consignee_city: local.order_consignee.city,//
                //consignee_area: local.order_address !== undefined ? local.order_address.area : '',//区，没有为空
                //consignee_short_address: local.order_consignee.short_address,//

                // consignee_name: shareTask.orderinfo.consignee_user,// 收货人姓名
                // consignee_mobile: shareTask.orderinfo.consignee_mobile,//  收货人电话
                // consignee_address: shareTask.orderinfo.consignee_address// 收货人地址
            }

            var data = {order_group_id:local.task.id,order_groups:submit}

            // API.post(function (ret) {
            API.taskGroupOrderSubmit(data,function (ret) {
                //清除手机端的ua切换
                change_useragent_removelistener();

                //设置保存成功
                if (ret.success == 1) {

                    setHostStep(9);//保存数据完毕，设置第9步

                    var task = local.task;
                    task.card_no = '';
                    task.bank_user = '';
                    task.task_order_id = '';

                    setLocal({task_finish_show: true,task:task,bank_info:null}, function () {
                        console.log('数据保存成功');
                        notify("数据保存成功");
                        setHostStatus(2000000, function(){
                            sdGroupTaskDone(sender,task_sub_order_id);
                            return false;
                        });//任务完成

                        // setHostStatus(2000000, startHost);//任务完成
                        // chrome.tabs.sendMessage(sender.tab.id,{act:'save_host_task_order_detail_result'})
                    });

                 }else if(ret.message == 'host_id不一致'){
                    setHostStatus(4);//host_id 不一致
                    notify(ret.message);
                    chrome.storage.local.set({isRunning: false}, function() {
                      notify("插件暂停", true);
                    });
                    return false;

                } else {
                    console.log(ret.message);
                    notify(ret.message);
                    if (ret.message === '已完成订单不能重复提交') {
                        console.log("已完成订单不能重复提交");
                        setHostStatus(2000000, function(){
                            sdGroupTaskDone(sender,task_sub_order_id);
                            return false;
                        });//任务完成
                        // setHostStatus(2000000, startHost);//任务完成
                        // chrome.tabs.sendMessage(sender.tab.id,{act:'save_host_task_order_detail_result'})
                    }else if(ret.message == '提交订单事务异常'){
                        console.log('提交订单事务异常,再次提交');
                        setTimeout(function(){
                            saveGroupHostTaskOrderDetail(sender,task_sub_order_id);
                        },5*1000)

                    }else{
                        var item = {isRunning:0};
                        setLocal(item,function(){
                            notify(ret.message);
                        });
                    }

                }


            },function(){
                console.log('请求保存提交订单接口失败,3S后重新请求保存');
                setTimeout(function(){
                    saveGroupHostTaskOrderDetail(sender,task_sub_order_id);
                },3000);
            });
        } else {
            console.log("数据不完整");
            console.log(local);
        }



    });
    });
}

/**
 * 保存订单信息
 */
function saveOrderInfo(submit_time,tabId) {

    console.log('submit_time',submit_time);
    API = new Api();
    useLocal( function (local) {

        getSharedTaskWorks(function(shareTask){

            getTaskWorks(function(tWorks){

                if (local.task && shareTask.orderinfo) {
                    var order_info = {
                        host_id: local.host_id,//
                        towh_id: local.task.towh_id,//历史表记录
                        task_order_id: local.task.task_order_id,//任务订单id
                        business_oid: shareTask.orderinfo.order_id,//   订单id
                        consignee_address: shareTask.orderinfo.consignee_address,// 收货人地址
                        business_discount_fee: shareTask.orderinfo.discount_fee,// 用券金额(折扣)
                        business_payment_fee: shareTask.orderinfo.payment_fee,//  券扣金额(实际银行支付的金额)
                        business_freight_fee: shareTask.orderinfo.yunfei,//  运费

                        business_order_at:submit_time,//提交订单时间
                        business_ip : local.last_ip,
                        business_total_fee : local.temp_pay_fee,//总费用
                        order_gifts : local.task.order_gift_list ? local.task.order_gift_list :[],//赠品数据

                        consignee_province: local.task.consignee.province,//
                        consignee_city: local.task.consignee.city,//
                        consignee_area: local.task.consignee.area ? local.task.consignee.area : ''//区，没有为空
                       
                    }

                    console.log('order_info',order_info);

                    API.saveOrderInfo(order_info,
                        function(ret){
                            if(ret.success == 1){
                                console.log('保存订单信息成功');
                                last_watchdog_time = new Date().getTime();
                                chrome.tabs.sendMessage(tabId, {act: 'save_order_info_result'}
                                    ,function(request){
                                        console.log(request);
                                    }
                                );
                            }else if(ret.message == 'host_id不一致'){
                                setHostStatus(4);//host_id 不一致
                                //console.log(ret.message);
                                notify(ret.message);
                                chrome.storage.local.set({isRunning: false}, function() {
                                  notify("插件暂停", true);
                                });
                                return false;
                                
                            }else{
                                notify(ret.message);
                            }
                            
                        },
                        function(){
                            console.log('请求保存订单信息接口失败');
                            setTimeout(function(){
                                saveOrderInfo(submit_time,tabId);
                            },10*1000);
                        }
                    );

                    
                }else{
                    console.log("数据不完整");
                    console.log(local);
                }

         })

        })
    });
}

function saveGroupOrderInfo(submit_time,tabId) {

    console.log('submit_time',submit_time);
    API = new Api();
    useLocal( function (local) {

        getTaskWorks(function(tWorks){

        getSharedTaskWorks(function(shareTask){

            if (local.task && shareTask.orderinfo) {
                var order_info = {
                    order_group_id:local.task.id,
                    host_id: local.host_id,//
                    // towh_id: local.task.towh_id,//历史表记录
                    // task_order_id: local.task.task_order_id,//任务订单id
                    business_oid: shareTask.orderinfo.order_id,//   订单id
                    // consignee_address: shareTask.orderinfo.consignee_address,// 收货人地址
                    discount_fee: shareTask.orderinfo.discount_fee,// 用券金额(折扣)
                    total_fee:shareTask.orderinfo.total_fee,//商品总金额
                    freight_fee:shareTask.orderinfo.yunfei,
                    payment_fee: shareTask.orderinfo.payment_fee,//  券扣金额(实际银行支付的金额)
                    // business_freight_fee: shareTask.orderinfo.yunfei,//  运费

                    sku_nums:tWorks.sku_nums,
                    amount_nums:tWorks.amount_nums,

                    business_order_at:submit_time ? submit_time : '',//提交订单时间
                    business_ip : local.last_ip
                    // business_total_fee : local.temp_pay_fee,//总费用
                    // order_gifts : local.task.order_gift_list ? local.task.order_gift_list :[],//赠品数据

                    // consignee_province: local.task.consignee.province,//
                    // consignee_city: local.task.consignee.city,//
                    // consignee_area: local.task.consignee.area ? local.task.consignee.area : ''//区，没有为空
                   
                }

                console.log('order_info',order_info);

                API.saveGroupOrdersInfo(order_info,
                    function(ret){
                        if(ret.success == 1){
                            console.log('保存订单信息成功');
                            last_watchdog_time = new Date().getTime();
                            chrome.tabs.sendMessage(tabId, {act: 'save_order_info_result'}
                                ,function(request){
                                    console.log(request);
                                }
                            );
                        }else if(ret.message == 'host_id不一致'){
                            setHostStatus(4);//host_id 不一致
                            //console.log(ret.message);
                            notify(ret.message);
                            chrome.storage.local.set({isRunning: false}, function() {
                              notify("插件暂停", true);
                            });
                            return false;

                        }else if(ret.message == '订单SKU数不一致' || ret.message == '订单总件数不一致'){
                            notify(ret.message);
                            notify('当前订单sku数 =' + tWorks.sku_nums + ', 订单总件数 =' + tWorks.amount_nums);
                            notify('3S后重新开始');
                            setTimeout(startHost,3*1000);
                            
                        }else{
                            notify(ret.message);
                        }
                        
                    },
                    function(){
                        console.log('请求保存订单信息接口失败');
                        setTimeout(function(){
                            saveGroupOrderInfo(submit_time,tabId);
                        },10*1000);
                    }
                );

                
            }else{
                console.log("数据不完整");
                console.log(local);
            }

        })

    })
    });
}


//订单操作
function client_task_order_operate(act) {
    if (act == 'client_task_order_exception_reset') {
        //使用新方法 异常 + 备注 2016-06-04
        getSharedTaskWorks(function(sTask){
            if(sTask.is_group){
                orderGroupException('XSS 手动标记异常');
            }else{
               orderException('XSS 手动标记异常'); 
            }
        })
        
        //client_task_order_exception_reset();
    } else if (act == 'client_host_status_reset') {
        //重新开始
        startHost();
    }
}

//订单重置账号接口
function client_task_order_reset(remark,url) {
    setHostStatus(2);//报道订单重置状态
    remark = remark ? remark : '未知';
    API = new Api();
    useLocal( function (local) {

        if (local.order_id !== undefined) {
            notify("已经提交订单不能进行账号重置", true);
            chrome.storage.local.set({isRunning: false}, function() {
                notify("插件暂停", true);
            })
            return false;
        }
        //console.log(local);
        var params = {
            task_order_id: local.task.task_order_id,
            remark: remark,
            image:url,
            locked_channel_code:'xss'
        }

        if(local.tasks[local.taskId].is_mobile == '1'){
            params.client_code = 'm';
        }else{
            params.client_code = 'pc';
        }
        
        API.accountReset(params,function (ret) {
            if (ret.success == 1) {
                console.log('账号重置成功，，重置账号');
                notify("账号重置成功！");

                //重置账号，，成功后，任务重新开始
                startHost();
            } else {
                console.log('账号重置失败');
                notify("账号重置 失败(" + ret.message + ")");
            }

        });

    });
}

//订单异常
function client_task_order_exception_reset() {
    setHostStatus(3);//报道订单异常状态
    API = new Api();
    useLocal( function (local) {
        //console.log(local);
        var params = {
            action: 'task_order_exception_reset',
            host_id: local.host_id,
            username: local.username,
            password: local.password,
            task_order_id: local.task.task_order_id
        }

        API.setParams(params);
        API.post(function (ret) {
            if (ret.success == 1) {
                console.log('标记订单异常成功');
                notify("标记订单异常成功");
                startHost();//重新开始
            } else {
                console.log('标记订单异常 失败');
                notify("标记订单异常 失败");
            }

        });

    });
}

//搜索京东激活页面并返回上一页 、 重置账号成功后的操作
function safe_verify_go_back() {
    //http://safe.jd.com/dangerousVerify/index.action
    chrome.tabs.query({url: "*://safe.jd.com/dangerousVerify/index.action*"}, function (tabs) {
        if (tabs.length > 0) {
            var tab_id = tabs[0].id;
            chrome.tabs.executeScript(tab_id, {
                    code: "history.go(-1);",
                    allFrames: true
                }, function (result) {
                    //console.log(result);
                }
            );
        } else {
            console.log('没有找到账号激活页面！');
            notify("没有找到账号激活页面！");
        }

    });

}

//检测当前发送消息tab
function checkActiveTab(senderTabId){
    console.log('senderTabId',senderTabId);
    chrome.tabs.query({active:true},function(tab){
        console.log(tab);
        for(var i in tab){
            if(tab[i].id == senderTabId){
                closeTabsNotActive(tab[i].windowId,senderTabId,function(need_reload){
                    chrome.tabs.sendMessage(senderTabId,{act:'check_active_tab_result',is_active:true,need_reload:need_reload});
                })
                 
            }else{
                chrome.tabs.remove(tab[i].id);
                // chrome.tabs.sendMessage(senderTabId,{act:'check_active_tab_result',is_active:false}); 
            }
        }
    })
}

//关闭当前窗口活动tab之外的tabs
function closeTabsNotActive(windowId,senderTabId,callback){
    chrome.tabs.query({windowId: windowId},function(tabs){
        console.log('tabs',tabs);
        if(tabs.length > 1){
            for(var i = 0;i<tabs.length;i++){
                if(tabs[i].id != senderTabId){
                    chrome.tabs.remove(tabs[i].id);
                }
            }

            callback && callback(true);
        }else{
            callback && callback(false);
        }
    })
}

//截屏
function screenCapture(callback){
    console.log('request screen capture');

    chrome.windows.getCurrent(function(window){
        console.log(window);
        if(window.id > 0){
      
        setTimeout(function(){
            chrome.tabs.captureVisibleTab(window.id,{format:'png'},function(dataUrl){//截屏
                //console.log(dataUrl);
                if(dataUrl){
                    callback(dataUrl)
                    //上传图片文件
                    // uploadImage(dataUrl,
                    //     function(url){
                    //         callback(url);
                    //     },
                    //     function(){
                    //         screenCapture(callback);
                    //     }
                    // );
                }
            })
        },3000);
      }  
    })  
}