$(function(){

	//1.初始化信息
	load_config();

	//2.保存配置
	var $btnSave = $('#btnSave');
	$btnSave.click(function(){

		
		//用户名
		var worm_username = $("#worm_username").val();
		var worm_password = $("#worm_password").val();

		// if(worm_username==''||worm_password==''){
		// 	alert('用户名密码必填');
		// 	return;
		// }
		var pay = {
			"ccb_username": $('#ccb_username').val(),
			"ccb_password": $('#ccb_password').val(),
			"boc_username": $('#boc_username').val(),
			"boc_password": $('#boc_password').val(),
			"cmb_mobile": $('#cmb_mobile').val(),
			"cmb_paycard": $('#cmb_paycard').val(),
			"icbc_paycard": $('#icbc_paycard').val(),
			"bank": $('input[name ="bank"]:checked').val(),
			"autologin_boc": $("#autologin_boc:checked").length>0?true:false,
            "remote_payment_boc": $("#remote_payment_boc:checked").length>0?true:false,
            "use_online_pay": $("#use_online_pay:checked").length>0?true:false

		}

		var key = $.trim($('#key').val());
		if(key.length < 10){
			//alert('口令长度10位,你输入的不太对额,再试试');
			//return ;
			key = '';
		}

		var items = {
            'env': $("#worm_env").val(),
            'version_env': $("#version_env").val(),
            'usage': $("#worm_usage").val(),
			'username':worm_username,
			'password':worm_password,
			'pay': pay

		};

		if(key){
			items.key = key;
		}

		chrome.storage.local.set(items, function(result){
		    alert('保存成功');
		});

	});

});


	//3.初始化信息
function load_config(){

	var items = [
			'key',
			'username',
			'password',
			'host_id',
            'env',
            'version_env',
            'usage',
			'pay'
		];

	chrome.storage.local.get(items, function(result){
        console.log(result);
	    var worm_env = (result['env'] == undefined) ? 0 : result['env'];
	    var version_env = (result['version_env'] == undefined) ? 0 : result['version_env'];
	    var worm_usage = (result['usage'] == undefined) ? 0 : result['usage'];
	    var username = (result['username'] == undefined) ? '' : result['username'];
	    var password = (result['password'] == undefined) ? '' : result['password'];
	    var host_id = (result['host_id'] == undefined) ? '' : result['host_id'];
	    var key = (result['key'] == undefined) ? '' : result['key'];
	    var pay = (result.pay == undefined) ? '' : result.pay;

        $("#worm_env option[value='"+worm_env+"']").prop("selected", true);
        $("#version_env option[value='"+version_env+"']").prop("selected", true);
        $("#worm_usage option[value='"+worm_usage+"']").prop("selected", true);

		if(key!='' && key.length>4 ){
			var showKey = '';
			var lenKey = key.length-4;
			while(lenKey--){
				showKey +='*';
			}
			showKey += key.substr(-4,4);
			console.log(showKey);
			$("#key").attr('placeholder', showKey);
		}
		$("#worm_username").val(username);
		$("#worm_password").val(password);
		$("#host_id").text(host_id);

		$('#ccb_username').val(pay.ccb_username?pay.ccb_username:'');
		$('#ccb_password').val(pay.ccb_password?pay.ccb_password:'');
		$('#boc_username').val(pay.boc_username?pay.boc_username:'');
		$('#boc_password').val(pay.boc_password?pay.boc_password:'');
		$('#cmb_mobile').val(pay.cmb_mobile?pay.cmb_mobile:'');
		$('#cmb_paycard').val(pay.cmb_paycard?pay.cmb_paycard:'');
		$('#icbc_paycard').val(pay.icbc_paycard?pay.icbc_paycard:'');
	    $('#bank_' + pay.bank).attr('checked', true);
	    $('#autologin_boc').attr('checked',pay.autologin_boc);
	    $('#remote_payment_boc').attr('checked',pay.remote_payment_boc);

	    $('#use_online_pay').attr('checked',pay.use_online_pay);

	});
}