// config/background.js
var UserAgent = '';//useragent
var GetTaskStatus = false;//去服务器领单状态
var StartTaskOpenUrl = false;//
var jd_ad_tab_id = 0;//jd广告窗口
var ADSLAfterConnectNubmber = 0;//adsl连接后,获取ip次数
var url_used_nums = 0;//使用拨号地址计数
var url_repeat_nums = 0;//拨号重复次数

var last_watchdog_time = new Date().getTime();
var watchdog_timeout_count = 0;
var watchdog_timeout_time = 60000;
var last_watchdog_timeout_step = null;

var yiqifaReferrer = null;

var request_task_order_nums = 0;//请求领单次数
var request_assign_account_nums = 0;//请求分配账号次数
var request_assign_url_nums = 0;//请求分配推广链接次数
var request_block_cnts = 0;//请求获取阻止链接次数
var request_check_update_nums = 0;//请求检查版本接口次数

var app_task_check_tracker_start = 0;//app领单请求赤兔更新记录开始时间
var app_checked_tracker = false;

var checkActiveExtensionFinished = true;
var login_type = null;//登录方式

//当前任务名称
var applicationName = null;

//message function
var M = {global: {}};

var tracker_env = [
    'npogbneglgfmgafpepecdconkgapppkd',
    'npogbneglgfmgafpepecdconkgapppkd',
    'emeffelceoodhoogpjnngindlklklgdn'
    //'codckobblnkmfcpaknmcpbjmfcfhminf'
];

//京东修改收货地址的登录地址
var jd_revise_address_login_url = "https://passport.jd.com/uc/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action#none";
var jd_revise_address_login_url = "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action";
//一号店修改收货地址的登录地址
var yhd_revise_address_login_url = "https://passport.yhd.com/passport/login_input.do?returnUrl=http%3A%2F%2Fmy.yhd.com%2Fmember%2Faddress%2FaddressBook.do";

//登录策略改变，不直接进入登录页面，直接进入修改地址页面，看cookies是否生效
var jd_revise_address_login_url = 'https://easybuy.jd.com/address/getEasyBuyList.action';
var yhd_revise_address_login_url = "http://my.yhd.com/member/address/addressBook.do";


var jd_task_start_url = 'http://i.jd.com/user/info';

//需要阻止打开的链接
var requestBlockUrls = [
    "*://ccc-x.jd.com/*",
    "*://sale.jd.com/*",
    "*://c-nfa.jd.com/*",
    "*://*.bijiatu.com/*",
    "*://*.juutuu.com/*",
    "*://*.jiafe.com/*"
];

//默认配置信息，设置到storage
var _cfg = {
	
	// 'host_status': 40001,
    // 'reported_status': 0,
    // 'isRunning': true,
    //'last_ip':'',
    'env':0,
    'worker': null,
	'host_status': 0,
    'host_step': 0,
	'reported_status': 0,
	'isRunning': false
};

//超时需要重新开始
var appTimeOutRestartTaskLists = [
    'lookOrder'
]

//默认推广链接地址
var defaultPromotionUrls = {
    //pc
    '0': {
        '0': 'https://www.jd.com',
        '1': 'https://www.jd.hk'
    },
    //m
    '1': {
        '0': 'https://m.jd.com',
        '1': 'https://m.jd.hk'
    }
};

//一号店地址
var gbl_const_yhd_address_map = {
    '上海':1, '北京':2, '天津':3, '河北':4, '江苏':5, '浙江':6, '重庆':7, '内蒙古':8, '辽宁':9, '吉林':10, '黑龙江':11, '四川':12, '安徽':13, '福建':14, '江西':15, '山东':16, '河南':17, '湖北':18, '湖南':19, '广东':20, '广西':21, '海南':22, '贵州':23, '云南':24, '西藏':25, '陕西':26, '甘肃':27, '青海':28, '新疆':29, '宁夏':30, '山西':32
};

var labelWatch={
    message:'',
    time:'',
    timeout:60,
    tab:{},
    isSend:false,
    retryTimes:{}
}

//插件支持的任务类型
var taskTypes= [

    {
        'apiTaskType':'product_search',
        'taskType':'search',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        // 'urlMatch':[
        //         '*://m.jd.com/',
        //         '*://m.jd.hk/'
                
        // ],
        disableAccount: 'appDisableAccountOrderReset'
    },

    {
        'apiTaskType':'group_submit_order',
        'taskType':'group_xss',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        'urlMatch':[
                // '*://www.jd.com/',
                '*://www.jd.hk/',
                '*://m.jd.com/',
                '*://m.jd.hk/',
                "*://item.jd.com/*",
                "*://item.m.jd.com/*",
                "*://item.jd.hk/*",
                "*://mitem.jd.hk/*",
                "*://item.yiyaojd.com/*",
                "*://m.yiyaojd.com/*",
                "*://trade.jd.com/shopping/order/getPresalInfo.action*"
        ],
         disableAccount: 'appDisableAccountOrderReset'
    },
    {
        'apiTaskType':'group_order_payment',
        'taskType':'group_payment',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://details.jd.com/normal/item.action*', "*://home.jd.hk/order*","*://dps.ws.jd.com/*"],
         disableAccount: 'appDisableAccountOrderReset'
    },
    {
        'apiTaskType':'submit_order',
        'taskType':'xss',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        'urlMatch':[
                '*://www.jd.com/',
                "*://item.jd.com/*",
                "*://item.m.jd.com/*",
                "*://item.jd.hk/*",
                "*://mitem.jd.hk/*",
                "*://item.yiyaojd.com/*",
                "*://m.yiyaojd.com/*"
        ],
         disableAccount: 'appDisableAccountOrderReset'
    },
    {
        'apiTaskType':'order_payment',
        'taskType':'payment',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://details.jd.com/normal/item.action*', "*://home.jd.hk/order*","*://dps.ws.jd.com/*"],
         disableAccount: 'appDisableAccountOrderReset'
    },
    {
        'apiTaskType':'self_order',
        'taskType':'selfOrder',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
         disableAccount: 'appDisableAccount'
         // timeout:900e3
    },
    {
        'apiTaskType':'look_order',
        'taskType':'lookOrder',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
         disableAccount: 'appDisableAccount',
         timeout:300e3
    },
    {
        'apiTaskType':'coupon_a',
        'taskType':'couponA',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        disableAccount: 'appDisableAccount',
        timeout:300e3
    },
    {
        'apiTaskType':'reset_login_password',
        'taskType':'loginPassword',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        disableAccount: 'appDisableAccount'
    },
    {
        'apiTaskType':'get_coupon',
        'taskType':'getCoupon',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        disableAccount: 'appDisableAccount'
    },
    {
        'apiTaskType':'product_detail',
        'taskType':'productDetail',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
         disableAccount: 'appDisableAccount'
    },
    {
        'apiTaskType':'account_info',
        'taskType':'home',
        'detail':{},
        'steps':{},
        'startPage':'https://home.jd.com/',
        'urlMatch':['*://home.jd.com/'],
         // disableAccount: 'appDisableAccount'
         disableAccount: 'appDisableAccountOrderReset'
    },
    {
        'apiTaskType':'account_check',
        'taskType':'accountCheck',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        'urlMatch':['*://www.jd.com/'],
        disableAccount: 'M.accountCheck.accountFail'
    },
    {
        'apiTaskType':'receipt',
        'taskType':'rearReceipt',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:300e3
    },{
        'apiTaskType':'comment',
        'taskType':'rearComment',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:1500e3
    },
    {
        'apiTaskType':'share',
        'taskType':'rearShare',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:300e3
    },{
        'apiTaskType':'appended',
        'taskType':'rearAppended',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:900e3
    },

    {
        'apiTaskType':'order_receipt',
        'taskType':'rearReceipt',
        'detail':{},
        'steps':{},
        'startPage':'https://www.jd.com/',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:300e3
    },{
        'apiTaskType':'order_comment',
        'taskType':'rearComment',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:1500e3
    },
    {
        'apiTaskType':'order_share',
        'taskType':'rearShare',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:300e3
    },{
        'apiTaskType':'order_appended_comment',
        'taskType':'rearAppended',
        'detail':{},
        'steps':{},
        'startPage':'https://order.jd.com/center/list.action',
        'urlMatch':['*://order.jd.com/center/list.action*','*://order.jd.com/center/search.action?keyword=*'],
        disableAccount: 'appDisableAccount',
        timeout:900e3
    }
]

var _host_status = {
    "0": {text: "..."},
    "1": {text: "暂停"},
    "2": {text: "账号重置"},
    "3": {text: "订单异常"},
    "4": {text: "当前主机与订单主机不一致"},

    "1000001": {text: "[红]提交过订单，需人工核对", timeout: 0},
    "1000002": {text: "[红]账号异常,需要验证", timeout: 0},

    "1001000": {text: "audi start"},

    "1101000": {text: "关闭Chrome"},
    "1102000": {text: "ADSL", timeout:120},
    "1103000": {text: "get ip"},
    "1103001": {text: "IP地址重复，等待重新拨号"},
    "1103002": {text: "IP地址错误，等待重新拨号"},
    "1103003": {text: "IP地址已使用，等待重新拨号"},
    "1104000": {text: "IP地址正常,准备请求服务器任务"},

    "1201000": {text: "获取"},
    "1201001": {text: "分配订单"},
    "1201002": {text: "分配账号"},
    "1201003": {text: "分配推广链接"},

    "1202000": {text: "得到服务器任务"},
    "1202001": {text: "请求服务器任务失败"},
    "1202002": {text: "领单无任务"},
    "1202003": {text: "收评无任务"},
    "1202004": {text: "点点无任务"},
    "1203000": {text: "任务开始执行"},

    "1301000": {text: "登录", timeout: 120},
    "1301001": {text: "[绿]登录,出现验证码"},
    "1302000": {text: "核对收货地址", timeout: 120},
    "1303000": {text: "添加收货地址"},
    "1304000": {text: "清空购物车"},

    "1401000": {text: "打开带后缀链接"},
    "1401001": {text: "[红]链接劫持,注意重单"},
    "1401002": {text: "[红]后缀链接重复打开"},
    "1401003": {text: "[红]提交过订单，需人工核对"},
    "1401004": {text: "[红]超值购重新开始"},

    "1402200": {text: "手机端登录"},
    "1402201": {text: "手机登录出现验证码"},
    "1402202": {text: "[红]打码服务歇菜, 手动打码", timeout: 0},
    "1402203": {text: "[红]打码服务欠费, 需充值", timeout: 0},
    "1403200": {text: "手机登录自动打码"},

    "1404000": {text: "搜索关键词"},
    "1404001": {text: "[红]提交过订单，需人工核对", timeout: 0},
    "1405000": {text: "货比三家"},
    "1405001": {text: "打开货比商品"},
    "1406000": {text: "打开主商品", timeout:120},
    "1406001": {text: "[红]无货", timeout: 0},
    "1406002": {text: "[红]下柜", timeout: 0},
    "1406003": {text: "[红]无货或下柜", timeout: 0},
    "1406004": {text: "[红]库存不足", timeout: 0},
    "1406005": {text: "[红]该地区不支持配送", timeout: 0},
    "1406006": {text: "[红]同一IP限购1件", timeout: 0},
    "1406007": {text: "[红]同账户或同IP只能抢购一件", timeout: 0},
    "1406008": {text: "[红]，店铺入口错误", timeout: 0},
    "1406009": {text: "[红]，店铺关注按钮错误", timeout: 0},
    "1406010": {text: "[红]收货地址有误，删除重新添加", timeout: 0},
    "1406011": {text: "[红]收货地址有误，地址库需要更新", timeout: 0},
    "1406012": {text: "[红]由京东发货", timeout: 0},
    "1406013": {text: "[红]找不到关注店铺按钮", timeout: 0},
    "1406014": {text: "[红]找不到关注商品按钮", timeout: 0},
    "1407000": {text: "等待加入购物车"},
    "1408000": {text: "主商品加入购物车"},
    "1409000": {text: "购物车,提交结算", timeout:120},
    "1409101": {text: "[红]赠品无货", timeout: 0},
    "1409102": {text: "[红]购物车商品数量错误", timeout: 0},
    "1409201": {text: "[红]主产品不存在", timeout: 0},
    "1409202": {text: "[红]存在其他产品", timeout: 0},
    "1409203": {text: "[红]数量不一致", timeout: 0},
    "1410000": {text: "准备提交订单"},
    "1410101": {text: "[红]主产品不存在", timeout: 0},
    "1410102": {text: "[红]存在其他产品", timeout: 0},
    "1410103": {text: "[红]数量不一致", timeout: 0},
    "1410104": {text: "[红]支付密码错误", timeout: 0},

    "1410005": {text: "[红]提交订单错误", timeout: 0},
    "1410006": {text: "[红]出现多包裹", timeout: 0},
    "1410007": {text: "[红]提交订单有验证码", timeout: 0},
    "1410208": {text: "[红]提交订单商品或赠品无货", timeout: 0},
    "1410009": {text: "[红]运费超过50元", timeout: 0},
    "1410010": {text: "[红]提交订单找不到在线支付", timeout: 0},

    "1411000": {text: "提交订单成功"},
    "1411001": {text: "京东收银台"},

    "1412000": {text: "手机PC登录，付款"},
    "1413000": {text: "我的订单去支付"},
    "1413001": {text: "订单列表 找单"},
    "1413002": {text: "订单确认"},
    "1413099": {text: "订单详情,付款"},

    "1414000": {text: "跳转银行", timeout:120},
    "1414001": {text: "[红]当前没有可使用的银行卡信息", timeout: 0},
    //"1414002": {text: "银行信息返回监听成功", timeout:120},

    "1501000": {text: "银行付款", timeout: 80},
    "1501001": {text: "[红]中行账号信息设置错误", timeout: 0},
    "1501002": {text: "[红]工行账号信息设置错误", timeout: 0},
    "1501003": {text: "[绿]登录出现验证码"},
    "1502000": {text: "[绿]已发送手机口令"},
    "1502001": {text: "付款失败"},
    "1502002": {text: "付款失败，非网银在线，重新付"},
    "1502003": {text: "支付失败，重新跳转银行"},
    "1502004": {text: "网银跳转失败，重新跳转银行"},
    "1502005": {text: "付款的时候时间超时,认为付款失败,自动步骤重置"},
    "1503000": {text: "付款成功"},
    "1504000": {text: "完成付款"},

    "1601000": {text: "付款失败重新来"},
    "1602000": {text: "保存订单数据"},
    "1603000": {text: "推送订单数据到服务器"},


    "2000000": {text: "任务完成"}
};

//手机端  useragent 配置
var _MobileUserAgent = [
//ipad
'Mozilla/5.0 (iPad; CPU OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3',
'Mozilla/5.0 (iPad; CPU OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53',
'Mozilla/5.0 (iPad; CPU OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D201 Safari/9537.53',
'Mozilla/5.0 (iPad; CPU OS 7_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D167 Safari/9537.53',
'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; zh-cn) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10',
'Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53',
'Mozilla/5.0 (iPad; CPU OS 7_0_2 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A501 Safari/9537.53',
'Mozilla/5.0 (iPad; CPU OS 7_0_3 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B511 Safari/9537.53',
'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; zh-cn) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10',
'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 6_1_2 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B146 Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
'Mozilla/5.0 (iPad; CPU OS 7_0_6 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B651 Safari/9537.53',
'Mozilla/5.0 (iPad; CPU OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B141 Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3',
'Mozilla/5.0 (iPad; CPU iPhone OS 501 like Mac OS X) AppleWebKit/534.46 (KHTML like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3',
'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 6_0_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8536.25',
'Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko ) Version/5.1 Mobile/9B176 Safari/7534.48.3',
//iphone
'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25',
'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25',
'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; zh-cn) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D201 Safari/9537.53',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D167 Safari/9537.53',
'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_2 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5',
'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; zh-cn) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53',
'Mozilla/5.0 (iphone; CPU iphone os 7_0_2 like mac os x) Applewebkit/537.51.1 (khtml, like gecko) version/7.0 mobile/11a501 safari/9537.53',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_3 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B511 Safari/9537.53',
'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25',
'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_2 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B146 Safari/8536.25',
'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_6 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B651 Safari/9537.53',
'Mozilla/5.0 (iphone; U; CPU iPhone OS 4_3_5 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5',
'Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3',
'Mozilla/5.0 (iPad; CPU iPhone OS 501 like Mac OS X) AppleWebKit/534.46 (KHTML like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3',
//android
'Mozilla/5.0 (Linux; U; Android 2.2; zh-cn; Desire_A8181 Build/FRF91) App3leWebKit/53.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn; MIDC410 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.0.4; zh-cn; MIDC409 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
'Mozilla/5.0 (Linux; Android 4.1.1; Nexus 7 Build/JRO03D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Safari/535.19',
'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-N7100 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; SM-T210R Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.0.2; zh-cn; Galaxy Nexus Build/ICL53F) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
 'Mozilla/5.0 (Linux; Android 4.0.4; DROID RAZR Build/6.7.2-180_DHD-16_M4-31) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; Android 4.4.4; zh-cn; SAMSUNG SM-E7000 Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Version/2.0 Chrome/34.0.1847.76 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; Android 4.4.4; SM-E7000 Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.89 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; MI 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 MQQBrowser/5.6 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; MI 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Mobile Safari/537.36 XiaoMi/MiuiBrowser/2.1.1',
'Mozilla/5.0 (Linux; Android 4.1.1; MI 2 Build/JRO03L) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.117 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; Android 4.4.4; SM-N910V Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.93 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; GT-I9300 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
'Mozilla/5.0 (Linux; Android 4.4.2; LG-D415 Build/KOT49I.D41510e) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.93 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; Android 5.0.1; Nexus 5 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36',
'Mozilla/5.0 (Linux; Android 4.2.1; Nexus 7 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Safari/535.19',
'Mozilla/5.0 (Linux; Android 4.2.1; Nexus 4 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19',
//HTC
'Mozilla/5.0 (Linux; U; Android 2.2; zh-cn; Desire_A8181 Build/FRF91) App3leWebKit/53.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.3.5; zh-cn; HTC_DesireHD_A9191 Build/GRJ90) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.3.5; zh-cn; HTC_DesireS_S510e Build/GRJ90) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn; HTC_Desire_C Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn; HTC_Desire_VC_T328d Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
'Mozilla/5.0 (Linux; U; Android 2.1-update1; zh-cn; HTC Desire 1.19.161.5 Build/ERE27) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari',
'Mozilla/5.0 (Linux; U; Android 2.2.2; zh-cn; HTC_Desire_A8181 Build/FRG83G) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.2.1; zh-cn; HTC_DesireZ_A7272 Build/FRG83D) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.2; zh-cn; HTC_DesireHD_A9191 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.3.4; zh-cn; HTC Desire Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.3.3; zh-cn; HTC_DesireS_S510e Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 4.2.2; zh-cn; Desire HD Build/JDQ39E) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 CyanogenM',
'Mozilla/5.0 (Linux; U; Android 2.3.3; zh-cn; HTC_DesireZ_A7272 Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
'Mozilla/5.0 (Linux; U; Android 2.2.2; zh-cn; HTC Desire Build/FRG83G) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'

];


var _host_step_action_url = {
    1: {
        "jd": [
            "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Fi.jd.com%2Fuser%2Fuserinfo%2FshowBaseInfo.action",
            "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Fi.jd.com%2Fuser%2Fuserinfo%2FshowBaseInfo.action"
            // "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action",
            // "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action"
        ],
        "yhd": [
            "https://passport.yhd.com/passport/login_input.do?returnUrl=http%3A%2F%2Fmy.yhd.com%2Fmember%2Faddress%2FaddressBook.do",
            "https://passport.yhd.com/passport/login_input.do?returnUrl=http%3A%2F%2Fmy.yhd.com%2Fmember%2Faddress%2FaddressBook.do"
        ]
    },
    2: {
        "jd": [
            "https://i.jd.com/user/info",
            "https://i.jd.com/user/info"
        //"https://easybuy.jd.com/address/getEasyBuyList.action", "https://easybuy.jd.com/address/getEasyBuyList.action"
        ],
        "yhd": ["http://my.yhd.com/member/address/addressBook.do", "http://my.yhd.com/member/address/addressBook.do"]
    },
    3: {
        "jd": ["{promotion_url}"],
        "yhd": ["{promotion_url}"]
    },
    4: {
        "jd": ["http://item.jd.com/{item_id}.html", "http://item.m.jd.com/product/{item_id}.html"],
        "yhd": ["http://item.yhd.com/item/{item_id}", "http://item.m.yhd.com/item/{item_id}"]
    },
    5: {
        "jd": ["http://cart.jd.com/cart/cart.html", "http://p.m.jd.com/cart/cart.action"],
        "yhd": ["http://cart.yhd.com/cart/cart.do", "http://cart.m.yhd.com/cart/showCart"]
    },
    6: {
        "jd": ["http://trade.jd.com/shopping/order/getOrderInfo.action", "http://p.m.jd.com/norder/order.action"],
        "yhd": ["http://buy.yhd.com/checkoutV3/index.do", "http://buy.m.yhd.com/checkout/order.do"]
    },
    7: {
        "jd": ["https://order.jd.com/center/list.action", "https://order.jd.com/center/list.action"],
        "yhd": ["http://my.yhd.com/order/myOrder.do", "http://my.yhd.com/order/myOrder.do"]
    },
    8: {
        "jd": ["https://order.jd.com/center/list.action", "https://order.jd.com/center/list.action"],
        "yhd": ["http://my.yhd.com/order/myOrder.do", "http://my.yhd.com/order/myOrder.do"]
    },
    9: {
        "jd": ["https://usergrade.jd.com/user/grade", "https://usergrade.jd.com/user/grade"],
        "yhd": ["http://my.yhd.com/order/myOrder.do", "http://my.yhd.com/order/myOrder.do"]
    }
};

var _worldwide_host_step_action_url = {
    1: {
        "jd": [
            "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action",
            "https://passport.jd.com/new/login.aspx?ReturnUrl=http%3A%2F%2Feasybuy.jd.com%2Faddress%2FgetEasyBuyList.action"
        ]
    },
    2: {
        "jd": [
            "https://easybuy.jd.com/address/getEasyBuyList.action",
            "https://easybuy.jd.com/address/getEasyBuyList.action"
        ]
    },
    3: {
        "jd": ["{promotion_url}"]
    },
    4: {
        "jd": [
            "http://item.jd.hk/{item_id}.html",
            "http://item.m.jd.com/product/{item_id}.html"
        ]
    },
    5: {
        "jd": [
            "http://cart.jd.hk/",
            "http://p.m.jd.com/cart/cart.action"
        ]
    },
    6: {
        "jd": [
            // "http://trade.jd360.hk/shopping/order/getOrderInfo.action",
            "http://trade.jd.hk/shopping/order/getOrderInfo.action",
            "http://p.m.jd.com/norder/order.action"
        ]
    },
    7: {
        "jd": [
            "https://order.jd.com/center/list.action",
            "https://order.jd.com/center/list.action"
        ]
    },
    8: {
        "jd": [
            "https://order.jd.com/center/list.action",
            "https://order.jd.com/center/list.action"
        ]
    },
    //https://usergrade.jd.com/user/grade
    9: {
        "jd": [
            "https://usergrade.jd.com/user/grade",
            "https://usergrade.jd.com/user/grade"
        ]
    }
};
