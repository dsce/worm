//操作面板 panel/index.js

getSharedTaskWorks(function(sTask){
	if(sTask.is_group){
		groupPanelIndex();
	}else{
		panelIndex();
	}
})


function panelIndex(){
	var position_html = '<div id="panel-XSS" class="position-XSS">\
							<div class="title-XSS"><b id="extension_name"></b> <b id="extension_version"></b> <span id="pageTime"></span></div>\
							<div class="panel-XSS">\
							  <div>\
							    <p>\
							      <a href="javascript:void(0);" id="runing"></a>\
							    </p>\
							    <p>\
							      <a href="javascript: void(0);">\
                                    <input type="checkbox" id="client_host_last_task_order_checkbox" style="vertical-align: middle;"/>\
                                    <label for="client_host_last_task_order_checkbox">最后一单&nbsp;</label>\
                                  </a>\
							      <a href="javascript:void(0);" id="client_host_status_reset" title="注意：商品数量，重复下单">重新开始(注)&nbsp;</a>\
							    </p>\
							    <p  class="task_show">\
							      <a href="javascript:void(0);" id="client_task_order_reset" style="display: none;">账号重置&nbsp;</a>\
							      <a href="javascript:void(0);" id="client_task_order_exception_reset">订单异常&nbsp;</a>\
							      <a href="javascript:void(0);" id="client_host_status_finish" class="task_finish_show" >完成&nbsp;</a>\
							    </p>\
							  </div>\
							  <div id="task">\
							  	<p>\
							  		<span id="host_status"></span>\
							  	</p>\
							  </div>\
							  <div>\
							    <p>\
							      <span id="host_id"></span>&nbsp;\
							      <span id="task_order_id"></span>&nbsp;\
							      <span id="business_order_id"></span>&nbsp;\
							    </p>\
							    <p>\
							      <span id="is_mobile"></span>&nbsp;\
                                  <span id="task_item_id"></span>&nbsp;\
                                  <span id="task_promotion_url"></span>&nbsp;\
							      <span id="task_keyword"></span>&nbsp;\
							    </p>\
							    <p>\
							      <span id="task_product_name"></span>&nbsp;\
							      <span id="task_amount"></span>&nbsp;\
							    </p>\
							    <p><span id="task_username"></span>&nbsp;<span id="task_password"></span>&nbsp;<span id="task_pay_password"></span></p>\
							    <p><span id="task_consignee"></span></p>\
							    <p>\
							        <span>订单：<span id="jd_order_id"></span></span>\
							        <span>应付：<span id="temp_pay_fee"></span></span>\
							        <span>运费：<span id="temp_shipping_fee"></span></span>\
                                </p>\
							  </div>\
							</div>\
						</div>';

	$("body").append(position_html);
	$(".title-XSS").click(function(){
		$(".panel-XSS").slideToggle(0);	
	});

    var extension_name_arr = ['嘻唰唰', 'APP付款'];

	chrome.storage.local.get(null,function(local){
		//console.log(local);
		//显示主机 相关信息
    	$("#host_id").html(local.host_id);

        //if(local.host_id == ''){
        //    $(".task_show").first().html("没有设置主机信息");
        //    $(".task_show").first().show();
        //    return false;
        //}
    	//显示任务 相关信息
    	var task = local.task;

    	if(task!=undefined&&task.task_id!=undefined&&task.task_id > 0){
            $("#extension_name").html(extension_name_arr[parseInt(local.usage)]);
            $("#extension_version").html(extensionVersion());
    		$(".task_show").show();
			$("#task_order_id").html(task.task_tid);
			$("#business_order_id").html(task.task_order_oid);

			$("#task_item_id").html(task.item_id);
			$("#task_keyword").html(task.keyword);
			$("#task_promotion_url").html('<a href="'+task.promotion_url+'">首页</a>');

			$("#task_product_name").html(task.product_name);
			$("#task_amount").html('( '+task.amount+' )');

			$("#task_username").html(task.username);
			$("#task_password").html(task.password);
			$("#task_pay_password").html(task.pay_password);
            var address = task.consignee;
			$("#task_consignee").html(address.name+" "+address.mobile+' '+address.province+address.city+(address.area?address.area:'')+(address.street?address.street:'')+address.short_address);

			var is_mobile = '';
			if(task.is_mobile == 1){
				is_mobile = '<img src="http://img12.360buyimg.com/n9/jfs/t280/202/1019167704/223218/622f597c/542d10f5N856a43fd.jpg" />';
			}
			
			$("#is_mobile").html(is_mobile);

            //显示京东订单号
            if(local.order_id){
                $("#jd_order_id").text(local.order_id);
                $("#temp_pay_fee").text(local.temp_pay_fee);
                $("#temp_shipping_fee").text(local.temp_shipping_fee);
            }else{
                $("#jd_order_id").text('还没有提交订单');
            }



			if(local.task_finish_show){
				$(".task_finish_show").show();
			}
    	}

    	//设置 开始停止
    	var runing = $("#runing");
    	if(local.isRunning === true){
    		//运行中
    		runing.text('停止任务');
    	}else{
    		//未运行
    		runing.text('开始任务');
    	}
    	runing.click(function(){
    		var text = runing.text();
    		if(text == '开始任务'){
    			//开始任务
				if (local.host_id=='') {
					alert("没有主机编号");
					return false;
				}else{
					chrome.storage.local.set({isRunning: true}, function() {
                        location.reload(true);
						runing.text('停止任务');
					})
				}
    		}else{
    			//停止任务
    			chrome.storage.local.set({isRunning: false}, function() {
					runing.text('开始任务');
				})
    		}
    	});

    	//运行状态
    	$("#host_status").text("运行状态："+(local.isRunning==true?'运行中：'+_host_status[local.host_status].text:'暂停'));

        //显示最后一单状态
        if('last_order' in local && local.last_order!=undefined){
            $("#client_host_last_task_order_checkbox").prop('checked',local.last_order);
        }
        //最后一单操作，
        $("#client_host_last_task_order_checkbox").click(function(){

            var checked = $(this).prop('checked');
            if(checked){
                chrome.storage.local.set({last_order:true});
            }else{
                chrome.storage.local.remove('last_order');
            }
        });

		//重新开始
		$("#client_host_status_reset").click(function(){
			if(confirm("确定 重新开始 操作？\n >>>重跑注意：商品数量，重复下单<<<")){
				orderOperateSendMessage('client_host_status_reset');
			}
		});

		//账号重置
		$("#client_task_order_reset").click(function(){
			if(confirm("确定重置 账号 操作?")){
				//orderOperateSendMessage('client_task_order_reset');
                tabCommon.sm(taskVars, '.disable_account', '[worm]手动重置panel');
			}
		});
		//订单异常
		$("#client_task_order_exception_reset").click(function(){
			if(confirm("确定 标记  订单异常  操作?")){
				orderOperateSendMessage('client_task_order_exception_reset');
			}
		});

        pageWaitTimeShow();
	});
}

function groupPanelIndex(){
	var position_html = '<div id="panel-XSS" class="position-XSS">\
							<div class="title-XSS"><b id="extension_name"></b> <b id="extension_version"></b> <span id="pageTime"></span></div>\
							<div class="panel-XSS">\
							  <div>\
							    <p  class="task_show">\
							      <a href="javascript:void(0);" id="client_task_order_reset" style="display: none;">账号重置&nbsp;</a>\
							      <a href="javascript:void(0);" id="client_host_status_finish" class="task_finish_show" >完成&nbsp;</a>\
							    </p>\
							  </div>\
							  <div>\
							    <p><span id="task_username"></span>&nbsp;<span id="task_password"></span>&nbsp;<span id="task_pay_password"></span></p>\
							    <p>\
							    <p><span id="task_consignee"></span></p>\
							    <p>\
							      <span id="task_group_orders"></span>&nbsp;\
							    </p>\
							    <p>\
							      <span id="task_product_name"></span>&nbsp;\
							      <span id="task_amount"></span>&nbsp;\
							    </p>\
							    <p>\
							    	<span>订单：<span id="jd_order_id"></span></span>\
							        <span>应付：<span id="temp_pay_fee"></span></span>\
							        <span>运费：<span id="temp_shipping_fee"></span></span>\
                                </p>\
                                <div id="task">\
							  	<p>\
							  		<span id="host_status"></span>\
							  	</p>\
							  </div>\
							  	<p>\
							      <span id="host_id"></span>&nbsp;\
							      <span id="group_order_id"></span>&nbsp;\
							      <span id="promotion_url"></span>&nbsp;\
							    </p>\
							    <p>\
							      <span><a id="open_pannel" style="cursor:pointer;"> &nbsp;折 叠 </a></span>&nbsp;\
							      <a href="javascript:void(0);" id="runing"></a>\
							      <a href="javascript: void(0);">\
                                  </a>\
                                  <a href="javascript:void(0);" id="client_task_order_exception_reset">订单异常&nbsp;</a>\
							      <a href="javascript:void(0);" id="client_host_status_reset" title="注意：商品数量，重复下单">重新开始(注)&nbsp;</a>\
							    </p>\
							  </div>\
							</div>\
						</div>';

	$("body").append(position_html);

	$(".title-XSS,#open_pannel").click(function(e){
		$(".panel-XSS").slideToggle(0);
	});

    var extension_name_arr = ['嘻唰唰', 'APP付款'];

	chrome.storage.local.get(null,function(local){
		//console.log(local);
		//显示主机 相关信息
    	$("#host_id").html(local.host_id);

    	$("#group_order_id").html(local.task.id);

    	if(local.task.promotion_url){
    		$("#promotion_url").html('<a href="'+ local.task.promotion_url+'">首页 </a>');
    	}

        //if(local.host_id == ''){
        //    $(".task_show").first().html("没有设置主机信息");
        //    $(".task_show").first().show();
        //    return false;
        //}
    	//显示任务 相关信息
    	var task = local.task;

    	// if(task!=undefined&&task.task_id!=undefined&&task.task_id > 0){
    	if(task!=undefined&&task.id!=undefined&&task.id > 0){
            $("#extension_name").html(extension_name_arr[parseInt(local.usage)]);
            $("#extension_version").html(extensionVersion());
    		$(".task_show").show();

    		var html_str = '';

    		if(task.orders.length >0){
    			task.orders.forEach(function(order){

    				html_str += order.is_mobile ? '<img src="http://img12.360buyimg.com/n9/jfs/t280/202/1019167704/223218/622f597c/542d10f5N856a43fd.jpg" />' : '';

    				html_str += order.item_id + ' ';

    				html_str += order.keyword + ' ';

    				html_str += ' ' + order.oid + ' <a href="#" class="drop_order" value="' + order.id +'"> 踢单</a> ' + order.task_tid + "<br />";

    				// html_str += '<a href="'+ order.promotion_url+'">首页 </a>';

    				html_str += order.product_name + '( '+  order.amount + ' )';

    				// html_str += '<a href="#" class="drop_order" value="' + order.id +'"> 踢单</a>';
    				
    				html_str += "<br /><br />";
    			})
    		}

    		$("#task_group_orders").html(html_str);
			
   //  		$("#task_order_id").html(task.task_tid);
			// $("#business_order_id").html(task.task_order_oid);

			// $("#task_item_id").html(task.item_id);
			// $("#task_keyword").html(task.keyword);
			// $("#task_promotion_url").html('<a href="'+task.promotion_url+'">首页</a>');

			// $("#task_product_name").html(task.product_name);
			// $("#task_amount").html('( '+task.amount+' )');
			$("#task_username").html(task.username);
			$("#task_password").html(task.password);
			$("#task_pay_password").html(task.pay_password);
            var address = task.consignee;
			$("#task_consignee").html(address.name+" "+address.mobile+' '+address.province+address.city+(address.area?address.area:'')+(address.street?address.street:'')+address.short_address);

			var is_mobile = '';
			if(task.is_mobile == 1){
				is_mobile = '<img src="http://img12.360buyimg.com/n9/jfs/t280/202/1019167704/223218/622f597c/542d10f5N856a43fd.jpg" />';
			}
			
			$("#is_mobile").html(is_mobile);

            //显示京东订单号
            if(local.order_id){
                $("#jd_order_id").text(local.order_id);
                $("#temp_pay_fee").text(local.temp_pay_fee);
                $("#temp_shipping_fee").text(local.temp_shipping_fee);
            }else{
                $("#jd_order_id").text('还没有提交订单');
            }



			if(local.task_finish_show){
				$(".task_finish_show").show();
			}
    	}

    	//设置 开始停止
    	var runing = $("#runing");
    	if(local.isRunning === true){
    		//运行中
    		runing.text('停止任务');
    	}else{
    		//未运行
    		runing.text('开始任务');
    	}
    	runing.click(function(){
    		var text = runing.text();
    		if(text == '开始任务'){
    			//开始任务
				if (local.host_id=='') {
					alert("没有主机编号");
					return false;
				}else{
					chrome.storage.local.set({isRunning: true}, function() {
                        location.reload(true);
						runing.text('停止任务');
					})
				}
    		}else{
    			//停止任务
    			chrome.storage.local.set({isRunning: false}, function() {
					runing.text('开始任务');
				})
    		}
    	});

    	//运行状态
    	$("#host_status").text("运行状态："+(local.isRunning==true?'运行中：'+_host_status[local.host_status].text:'暂停'));

        //显示最后一单状态
        if('last_order' in local && local.last_order!=undefined){
            $("#client_host_last_task_order_checkbox").prop('checked',local.last_order);
        }
        //最后一单操作，
        $("#client_host_last_task_order_checkbox").click(function(){

            var checked = $(this).prop('checked');
            if(checked){
                chrome.storage.local.set({last_order:true});
            }else{
                chrome.storage.local.remove('last_order');
            }
        });

		//重新开始
		$("#client_host_status_reset").click(function(){
			if(confirm("确定 重新开始 操作？\n >>>重跑注意：商品数量，重复下单<<<")){
				orderOperateSendMessage('client_host_status_reset');
			}
		});

		//账号重置
		$("#client_task_order_reset").click(function(){
			if(confirm("确定重置 账号 操作?")){
				//orderOperateSendMessage('client_task_order_reset');
                tabCommon.sm(taskVars, '.disable_account', '[worm]手动重置panel');
			}
		});
		//订单异常
		$("#client_task_order_exception_reset").click(function(){
			if(confirm("确定 标记  订单异常  操作?")){
				orderOperateSendMessage('client_task_order_exception_reset');
			}
		});

		$(".drop_order").click(function(){
			if(confirm('确定要踢单吗？？')){
				drop_order($(this).attr('value'));
			}
		})

        pageWaitTimeShow();
	});
}

//页面停留时间计算
var page_time = 0;
function pageWaitTimeShow(){
    page_time ++;
    var h = Math.floor(page_time/3600);
    var m = Math.floor(page_time/60) % 60;
    var s = page_time % 60;
    $("#pageTime").html(h +':'+ (m<10?'0'+m:m) +':'+ (s<10?'0'+s:s));
    setTimeout(pageWaitTimeShow,1000);
}


//订单操作  
function orderOperateSendMessage(v){
  var msg = {
    act:'order_operate',
    val:v
  };
  chrome.runtime.sendMessage(msg, function(response){
    
  });
}
//状态 改变  修改 host_status
function hostStatusSendMessage(v){
  var msg = {
    act:'status',
    val:v
  };
  chrome.runtime.sendMessage(msg, function(response){
    
  });
}

function panelReload(){
    $("#panel-XSS").remove();
    panelIndex();
}

function drop_order(order_id){
	var msg = {
    act:'drop_order',
    val:order_id
  };
  chrome.runtime.sendMessage(msg, function(response){
    
  });
}