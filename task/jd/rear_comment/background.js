M.rearComment = {

    reportSuccess: function (request, sender) {
        getTaskWorks(function (task) {
            var order_id = task.order_id;
            var message = request.message ? request.message : "评价成功";
            var delay = request.delay ? request.delay : 0;
            var cmd = 'comment_success';
            M.global.report.rear.success({
                cmd: cmd,
                order_id: task.order_id,
                delay: delay,
                message: message
            }, sender)
        })
    },

    reportFail: function (request, sender) {
        getTaskWorks(function (task) {
            var order_id = task.order_id;
            var message = request.message ? request.message : "";
            var delay = request.delay ? request.delay : 0;
            var cmd = 'comment_error';
            M.global.report.rear.error({
                cmd: cmd,
                order_id: task.order_id,
                delay: delay,
                message: message
            }, sender)
        })
    },

    saveCommentedState: function (request, sender) {
        var data = {};
        data.guid = request.guid;
        data.business_oid = request.business_oid;
        data.sku_code = request.sku_code;
        data.state = request.state;
        var _api = new Api();
        _api.saveCommentState(data);
    },

    getHotCommentTagStatistics: function (request, sender) {
        var product_id = request.product_id;
        var callback = function (tags) {
            tags = tags ? tags : null;
            smToTab(sender.tab.id, 'getHotCommentTagStatistics', {
                product_id: product_id,
                tags: tags
            });
        }
        var api = new Api();
        api.getHotCommentTagStatistics(product_id, function (data) {
            console.log(data);
            var hotCommentTagStatistics = data.hotCommentTagStatistics ? data.hotCommentTagStatistics : null;
            if (hotCommentTagStatistics && hotCommentTagStatistics.length > 0) {
                var tags = [];
                for (var i in hotCommentTagStatistics) {
                    var tag = hotCommentTagStatistics[i];
                    if (tag.name.indexOf('一般') != -1) {

                    } else if (tag.name.indexOf('不') != -1 && tag.name.indexOf('不错') == -1) {

                    } else if (tag.name.indexOf('差') != -1) {

                    } else {
                        tags.push(tag.name)
                    }
                }
                console.log(tags);
                callback && callback(tags)
            } else {
                callback && callback()
            }
        }, function () {
            callback && callback()
        })
    },

    product_tags_save:function (request,sender) {
        console.log('保存tags');
        var api = new Api();
        api.productTagsSave(request.product_id, request.product_tags)
    },

    saveImage:function (request,sender) {
        console.log('saveImage');
        var api = new Api();
        api.retryTimes = 3;
        api.saveScreenCaptureUrl(request.order_id, 2 , request.imgUrl)
    },

    reportReplaceWords:function (request,sender) {
        console.log('reportReplaceWords');
        var api = new Api();
        api.reportReplaceWords(request.business_oid, request.filter_words)
    },

    accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        getTaskWorks(function (task) {
            M.global.report.rear.error({
                cmd: 'comment_error',
                order_id: task.order_id,
                delay: 0,
                message: remark
            }, sender)
        })
    }

}