(function () {

    tabCommon.start('rearComment', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('点开我的订单');
        lazy(function () {
            if (document.referrer.indexOf('//club.jd.com/myJdcomments/orderEvaluate.action') != -1) {
                tabCommon.sm(taskVars, '.closeThisTab');
            } else if (document.referrer.indexOf('//details.jd.com/normal/item.action') != -1 && location.href.indexOf('//www.jd.com/error2.aspx') != -1) {
                tabCommon.sm(taskVars, 'reportFail', {
                    message: '找不到查看评价页面'
                });
            } else {
                tabPc.myOrderListClick()
            }
        })
    }

}})();