(function () {

    tabCommon.start('rearComment', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('点开我的订单');
        lazy(function () {

            var task = localTask.currentTask;
            var selfOrder = false;
            var item_id = task.sku_code;
            var mainProduct = $('.product-'+item_id).has('.f-price:contains("¥")');

            if(location.href.indexOf(task.business_oid)==-1){
                clue('不是 需要处理的订单');
                tabCommon.sm(taskVars, '.closeThisTab');
                return false;
            }

            //是否是自营单
            if(location.href.indexOf('dps.ws.jd.com/normal/item.action')!=-1){
                selfOrder = true;
            }

            //检查评价guid
            if(!task.comment_guid){

                var look_comment = $('.order-goods .goods-list .product-'+item_id+' a:contains("查看评价")');
                var self_look_comment = $('#iwo'+item_id+' a:contains("查看评价")');
                look_comment = selfOrder ? self_look_comment : look_comment;
                if(look_comment.length){
                    var arr = look_comment.attr('href').split('_');
                    if(arr[1]){
                        var guid=arr[1];
                        clue(guid);
                        // closeMe(5);
                        //如果有guid 任务成功即可
                        tabCommon.sm(taskVars, 'saveCommentedState',{guid: guid, business_oid: task.business_oid, sku_code: task.item_id, state: 1 });
                        tabCommon.sm(taskVars, 'reportSuccess');
                    }else{
                        closeMe();
                    }
                }else{
                    closeMe();
                }
            }

        })
    }

}})();