(function () {
    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearComment', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('开始评价');
        lazy(function () {
            //订单评价
            var task = null;

            var item_id = null;
            var item_only = null;
            var business_oid = null;

            var hot_tags = null;

            var err_win_checked = false;
            var input_try_times = 0;
            var dialog_showed = false;


            getTaskWorks(function (taskWorks) {
                task = localTask.currentTask;
                task.filter_words = taskWorks.filter_words;
                input_try_times = taskWorks.input_try_times?taskWorks.input_try_times:0;
                index();
            })

            /**
             * 初始化数据
             */
            function index() {
                console.log(localTask);
                item_id = task.item_id ? task.item_id : null;
                item_only = task.comment_gift ? true : false; //只评价主商品
                business_oid = task.business_oid;
                task.order_comments_body = task.body;


                //多个商品存在 检测是否成功
                var successText = $(".mycomment-detail .tip-title").text();
                clue('successText:'+successText);
                // 提交成功，还有商品未评价，可继续评价
                // 京豆将于一天左右返到你的账户中
                if(successText.indexOf('提交成功')!=-1 && successText.indexOf('还有商品未评价')!=-1){
                    label('检测提交状态');
                    if(tabCommon.findElement($('.product-' + item_id))){
                        clue('找到了主商品 继续评价');
                    }else{
                        if(item_only==false){
                            //只主商品评价
                            setTaskWorks({isCommented:true},function () {
                                // tabCommon.sm(taskVars, 'reportSuccess');
                                clue('去获取guid');
                                tabPc.myOrderListClick();
                            })
                            return false;
                        }else{
                            clue('继续评价其他产品');
                        }
                    }
                }

                //获取热门评价标签
                tabCommon.addMessageListener('getHotCommentTagStatistics', function (message) {
                    hot_tags = [];
                    if (message.product_id == item_id && message.tags && message.tags.length > 0) {
                        hot_tags = message.tags;
                    }
                    console.log('hot_tags',hot_tags);
                    doComment();
                })
                tabCommon.sm(taskVars, 'getHotCommentTagStatistics', {
                    product_id: item_id
                });
            }

            function doComment() {
                clue('开始处理评论');
                //评价内容处理
                commentBodyMachining();

                //要使用的评价标签
                var commentTagHtml = commentTagMachining();

                //增加保存商品评价标签
                saveProductTags(item_id);


                //4项评分, 全满分
                $('#activityVoucher .commstar .star5').click();

                //匿名评价
                if (task.anonymous == '1') {
                    $("#check1").attr('checked', true); //匿名评价
                } else {
                    $("#check1").attr('checked', false); //非匿名评价
                }


                var product = $('.product-' + item_id);
                if (product.length == 0) {

                    console.log('主商品不存在');

                    //已评价成功
                    if(task.client_status==3){
                        tabCommon.sm(taskVars, 'reportSuccess');
                    }else{
                        tabCommon.sm(taskVars, 'reportSuccess');
                    }
                } else {

                    //安装服务评分
                    if ($('#installTransportVoucher').length > 0) {
                        $('#installTransportVoucher .commstar-group .star5').click();
                    }

                    label('开始输入评价内容',180)
                    if (item_only==false) { //只评价主商品

                        clue('只评论主商品');

                        //商品满意度 满分
                        product.find('.commstar .star5').click();

                        //买家印象, 存在，追加评价标签
                        var mjyx = product.find(".J-mjyx");
                        if (mjyx.length > 0 && commentTagHtml) {
                            product.find('.m-multi-tag .tag-define').before(commentTagHtml);
                        }

                        //有特殊印象要处理
                        var vTags = product.find('.J-vTags');
                        if (vTags.length > 0) {
                            vTagSelect(vTags);
                        }

                        if(input_try_times>0){
                            writing(product.find('.f-textarea textarea'), ' ', function () {
                                product.find('.f-textarea textarea').val(task.order_comments_body.toString().substr(0, 500));
                                //提交
                                var overTime = random(10, 20) * 1000;
                                setTimeout(commentSubmit, overTime);
                            })
                        }else{
                            //评价内容
                            writing(product.find('.f-textarea textarea'), task.order_comments_body.toString().substr(0, 500), function () {

                                var overTime = random(10, 20) * 1000;
                                setTimeout(wangwang, 30);

                                //提交
                                setTimeout(commentSubmit, overTime);
                            })
                        }


                    } else {
                        //全评价

                        clue('评价全部产品');

                        //商品满意度 满分
                        $('.f-goods .commstar .star5').click();

                        //买家印象
                        var mjyx = $(".f-goods .J-mjyx");
                        if (mjyx.length > 0 && commentTagHtml) {
                            $('.f-goods .m-multi-tag .tag-define').before(commentTagHtml);
                        }

                        //有特殊印象要处理
                        var vTags = $('.f-goods .J-vTags');
                        if (vTags.length > 0) {
                            vTagSelect(vTags);
                        }

                        //评价内容
                        var areas = $('.f-goods .f-textarea textarea');
                        var area_over = 0;

                        inputCommentBody(areas, 0, function () {
                            setTimeout(wangwang, 40);
                            setTimeout(wangwang, 80);
                            setTimeout(wangwang, 100);
                            setTimeout(wangwang, 120);
                            var overTime = random(10, 20) * 1000;
                            //提交
                            setTimeout(commentSubmit, overTime);
                        })

                    }
                }
            }

            //输入评价内容
            function inputCommentBody(areas, index, callback) {
                wangwang();
                index = index > 0 ? index : 0;
                if (areas.length > 0) {
                    clue('input_try_times ' + input_try_times);
                    if (index == 0 && input_try_times == 0) {
                        writing(areas.eq(index), task.order_comments_body.toString().substr(0, 500), function () {
                            if (areas.length - 1 == index) {
                                label('输入完成');
                                callback && callback();
                            } else {
                                label('继续输入')
                                index++
                                inputCommentBody(areas, index, callback);
                            }
                        })
                    }else{
                        writing(areas.eq(index), ' ', function () {
                            areas.eq(index).val(task.order_comments_body.toString().substr(0, 500));
                            if (areas.length - 1 == index) {
                                label('输入完成');
                                callback && callback();
                            } else {
                                label('继续输入')
                                index++
                                lazy(function () {
                                    inputCommentBody(areas, index, callback)
                                });
                            }
                        })
                    }

                }
            }

            function doShare(callback) {
                label('有晒单');
                getSharedTaskWorks(function (shared) {
                    new Show(shared.shareTaskDetail.sku_code,shared.shareTaskDetail.business_oid,shared.shareTaskDetail.anonymous,shared.shareTaskDetail.pictures).submit(function () {
                        tabCommon.addMessageListener('shareDone',function () {
                            callback && callback();
                        })
                    });
                })
            }

            function commentSubmit() {
                doSubmit();
                // 先不晒单
                // getSharedTaskWorks(function (shared) {
                //     if(shared.shareExist){
                //         //防止晒单失败
                //         var share_setTime = setTimeout(function () {
                //             label('晒单45内没有完成,强制提交评价');
                //             doSubmit();
                //         },45e3);
                //         doShare(function () {
                //             clearTimeout(share_setTime);
                //             setSharedTaskWorks({isShared:true},function () {
                //                 doSubmit();
                //             })
                //         });
                //     }else{
                //         doSubmit();
                //     }
                // })

                function doSubmit() {
                    label('开始准备提交数据和截屏');
                    document.body.scrollTop = $('.product-' + item_id).offset().top;
                    tabCommon.screenShot(function (imgUrl) {
                        tabCommon.sm(taskVars, 'saveImage',{order_id:localTask.currentTask.order_id,imgUrl:imgUrl}); //pic only
                        filter_words();//增加敏感词监听
                        setTimeout(function () {
                            $('.mycomment-form .btn-submit')[0].click();
                        },1000);
                    },true);
                }
            }

            var filter_words_added = false;
            function filter_words() {
                tabCommon.mutation(bodyObserver, function (mutation) {
                    console.log(mutation.type);
                    console.log(mutation);

                    if (mutation.oldValue == 'fop-item' && mutation.target.className == 'fop-item z-tip-error') {
                        //错误提示
                        var err_msg = mutation.target.innerText;
                        if (err_msg.indexOf('内容含有敏感词') != -1 && filter_words_added==false) {
                            filter_words_added = true;
                            var pat = /（(.+)）/;
                            if (pat.exec(err_msg)) {
                                getTaskWorks(function (taskWorks) {
                                    var filter_words = taskWorks.filter_words ? taskWorks.filter_words : [];

                                    if(filter_words.length >5){
                                        tabCommon.sm(taskVars, 'reportFail', {message: '敏感词个数多于5个，任务失败；当前敏感词：' + filter_words.join(',')});
                                        return false;
                                    }
                                    filter_words.push(pat.exec(err_msg)[1]);

                                    setTaskWorks({filter_words:filter_words,input_try_times:+input_try_times+1},function () {
                                        clue('已获取到屏蔽词，3S后刷新');
                                        tabCommon.sm(taskVars, 'reportReplaceWords',{filter_words:filter_words,business_oid:business_oid});
                                        setTimeout("window.location.reload()", 5 * 1000);
                                    })
                                })
                            }
                        }
                    } else if (!err_win_checked && mutation.target.className == 'ui-dialog thickbox-skin1') {

                        err_win_checked = true;
                        tabCommon.sm(taskVars, 'reportFail', {message: mutation.target.innerText});
                        return false;
                    }else if(mutation.target.className == 'ui-dialog' && !dialog_showed){
                        if(mutation.target.innerText.indexOf('您未绑定手机号,无法评价') != -1){
                            dialog_showed = true;
                            clue('您未绑定手机号,无法评价,汇报任务失败');
                            tabCommon.sm(taskVars, 'reportFail', {message: '您未绑定手机号,无法评价'});
                            return false;
                        }
                    }
                })
            }

            //选择特殊印象标签
            function vTagSelect(vTags) {
                var tagsSelect = function (tags) {
                    if (tags.length == 1) {
                        tags[0].click();
                    } else if (tags.length > 1) {
                        tags.each(function () {
                            $(this)[0].click();
                        });
                    }
                }

                var tagItem = vTags.find('.fop-label:contains("商品质量")').next('.fop-main').find('.tag-item:contains("好")');
                tagsSelect(tagItem);

                var tagItem = vTags.find('.fop-label:contains("尺码")').next('.fop-main').find('.tag-item:contains("适合")');
                tagsSelect(tagItem);

                var tagItem = vTags.find('.fop-label:contains("描述相符")').next('.fop-main').find('.tag-item:contains("一致")');
                tagsSelect(tagItem);

                var tagItem = vTags.find('.fop-label:contains("描述相符")').next('.fop-main').find('.tag-item:contains("很相符")');
                tagsSelect(tagItem);

                var tagItem = vTags.find('.fop-label:contains("携带保温箱")').next('.fop-main').find('.tag-item:contains("未关注")');
                tagsSelect(tagItem);

            }

            //评价页面 保存可用的评价标签
            function saveProductTags(product_id) {
                var product_tags = [];
                var tag_item = $(".product-" + product_id + " .m-multi-tag .tag-item:visible");
                tag_item.each(function () {
                    console.log(this.innerText);
                    product_tags.push(this.innerText);
                });

                if (product_tags.length > 0) {
                    tabCommon.sm(taskVars, 'product_tags_save',{product_id: product_id, product_tags: product_tags})
                }

            }

            //评价内容处理
            function commentBodyMachining() {

                //剔除空字符
                var comments_body = task.order_comments_body.toString().split('');
                task.order_comments_body = "";
                for (var i in comments_body) {
                    var word = comments_body[i].toString().trim();
                    word = word ? word : ' ';
                    task.order_comments_body += word
                }

                //去除屏蔽词
                var re_str = '\s{2,}';
                task.order_comments_body = task.order_comments_body.replace(new RegExp(re_str, 'gm'), ' ');
                //task.order_comments_body = task.order_comments_body.toUpperCase();//字母进行大写转换。//不需要转换
                var strs = ['天瘦', '买卖', '天猫', 'MD', '一B', 'TM', '口交', 'DIY', 'AV', 'QQ群', 'C4', '——', '~', '～', 'TMD', 'X东', 'TB', 'T猫', 'A片', '白粉', '色女人', '虚假', '狗东', '旺旺', '烟雾弹', '枪'];

                if (task.filter_words) {
                    var new_strs = strs.concat(task.filter_words);
                }
                for (var i in new_strs) {
                    var str = new_strs[i];
                    var rep = new Array(str.length + 1).join('。');
                    task.order_comments_body = task.order_comments_body.replace(new RegExp(str, 'gm'), rep)
                }

                var length = tabCommon.checksum(task.order_comments_body);
                console.log(length);
                if (length == 1) {
                    task.order_comments_body += " "
                }
            }

            function commentTagMachining() {
                var tags = [];
                //评价是否需要使用评价标签
                if (task.tagged && task.tagged == "1") { //需要使用评价标签
                    if (task.user_tags && task.user_tags.length > 0) { //使用用户的标签
                        tags = task.user_tags.split(',');
                    } else {
                        //使用热门标签
                        if (hot_tags && hot_tags.length > 3) {
                            //使用商品热门标签, 随机 1-4
                            tags = randTags(hot_tags);
                        } else {
                            //默认通用好评标签
                            tags = task.custom_tags.split(',');
                        }
                    }
                }

                var html = '';
                tags.forEach(function (name) {
                    if(name && name.toString().replace(/\s+/g,'').length >0){
                        html += '<a href="#none" class="tag-item tag-checked" data-id="0">' + name + '<i class="t-check"></i><i class="t-delete"></i></a>';
                    }
                    
                })
                return html;
            }

            function randTags(tags) {
                var num = new Date().getTime();
                num = num.toString().substr(-2);
                num = num % 4;
                num = parseInt(num) + 1;
                num = random(1, 4);
                if (tags.length > num) {
                    var product_tags = [];
                    var k = 0;
                    do {
                        if (k >= num) {
                            break;
                        }
                        //var tag_index = parseInt(Math.random() * tags.length);
                        var tag_index = parseInt(random(0, tags.length - 1));
                        var tag = tags[tag_index];
                        if ($.inArray(tag, product_tags) == -1) {
                            product_tags.push(tag);
                            k++
                        }
                    } while (1);
                    return product_tags;
                } else {
                    return tags;
                }
            }


        })
    }

}})();