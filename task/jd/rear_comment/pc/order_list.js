(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearComment', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('评价 寻找目标订单');
        //主任务 检查订单状态
        var task = localTask.currentTask;
        var order_id;
        getTaskWorks(function (taskWorks) {
            clue('任务已重试次数:'+task.client_try_times||0);
            task.business_oid = taskWorks.business_oid;
            task.order_comments_body = task.body;
            order_id = task.business_oid;
            if(task.jd_guid){
                tabCommon.sm(taskVars, 'reportSuccess');
            }else{
                tabPc.rear.findOrder(task.business_oid,task.item_id,function () {
                    clue('检测需要完成的动作');
                    if(taskWorks.isCommented){
                        clue('需要去保存guid');
                        saveGuid();
                        setTimeout(function () {
                            tabCommon.sm(taskVars, 'reportSuccess');
                        },30e3);
                    }else{
                        clue('需要去完成评价');
                        checkOrderCommented();
                    }
                },taskVars);
            }
        })

        /**
         * 是否存在评论内容
         * 评论状态是否是3,同时有没有评价按钮
         * 有按钮,可能1:有其他产品,可能2:需要评价
         * 点开评价 没有主商品, 汇报成功; 有主商品,继续评价 赠品参数一起判断
         * 没有评价按钮
         *  有追评或者晒单,已经评价过了
         *  查看详情 查找评价按钮
         */

        function checkOrderCommented() {
            //有评价
            if (task.order_comments_body) {
                clue('有评价内容');
                saveGuid();
                if(task.client_status==3 && tabPc.rear.getButtons(order_id,'评价')==false){
                    clue('任务状态已完成 直接完成任务');
                    setTimeout(function () {
                        tabCommon.sm(taskVars, 'reportSuccess');
                    },30e3);
                }else if(tabPc.rear.getButtons(order_id,'评价')){
                    closeMe(60);
                    //延迟一段时间 给保存guid一些时间
                    setTimeout(function () {
                        tabCommon.clickElement($('#operate' + task.business_oid).find('a:contains("评价")'));
                    },30e3);
                }else{
                    if(tabPc.rear.getButtons(order_id,'追评') || tabPc.rear.getButtons(order_id,'晒单')){
                        clue('按钮加载了 但是没有评价 任务已经评价过了');
                        saveGuid();
                        setTimeout(function () {
                            tabCommon.sm(taskVars, 'reportSuccess');
                        },30e3);
                    }else{
                        tabCommon.reloadPage(6,function () {
                            clue('刷新6次');
                        },function () {
                            clue('评价没有入口，延迟执行');
                            tabCommon.sm(taskVars, 'reportFail', {
                                message: "评价没有入口，延迟执行",
                                delay:3600 * 24
                            });
                        })
                    }
                }
            } else {
                console.log("任务中无评价内容");
                //reportError({type:"comment_error",message:"order_comments_body"});
                if (task.client_status > 0) {
                    tabCommon.sm(taskVars, 'reportFail', {
                        message: "任务中没有评价内容"
                    });
                } else {
                    //只有收获,没有评价,收获任务完成
                    tabCommon.sm(taskVars, 'reportSuccess');
                }
            }
        }

        function saveGuid() {
            clue('去保存下guid');
            var promisejdint = $("#tb-" + task.business_oid).find('.promisejdint');
            //没有guid，去看看guid，同时验证订单详情有没有评价入口
            if (!task.comment_guid) {
                clue('没有guid');
                if (promisejdint.length > 0) {
                    clue('全球购订单');
                    //进入晒单列表找guid
                    var share = [];//todo 这里不明确
                    if (share.length > 0) {
                        share[0].click();
                    } else {
                        clue('全球购订单找不到guid，去评价列表获取');
                        var create_url = 'http://club.jd.com/myJdcomments/orderEvaluate.action?ruleid=' + task.business_oid;
                        tabCommon.sm(taskVars, '.createTabByUrl',{url:create_url});
                    }
                } else {
                    //进订单详情看订单的guid
                    clue('进入订单详情');
                    $("#idUrl" + task.business_oid)[0].click();
                }
            } else {
                clue('有 guid, 已经评价');
                //报告评价成功
                tabCommon.sm(taskVars, 'reportSuccess');
            }
        }

    }

}})();