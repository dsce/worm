(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearShare', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('点开晒单');
        lazy(function () {


            //待晒单
            console.log('jd comment sort');

            var is_showing = false;

            var item_id = null;

            var task = localTask.currentTask;
            index();

            function index(){

                item_id = task.sku_code;
                var business_oid = task.business_oid;

                //顺便保存一下guid
                if(!task.comment_guid){
                    var guid = $('.pro-info[oid='+business_oid+'][pid='+item_id+']').find('a:contains("晒单")').attr('guid');
                    if(guid){
                        tabCommon.sm(taskVars, 'saveCommentedState',{guid: guid, business_oid: task.business_oid, sku_code: task.item_id, state: 1 });
                    }
                }

                if(task.status!=3){
                    console.log('需要晒单');
                    if(!task.pictures){
                        clue('没有晒单图片');
                        tabCommon.sm(taskVars, 'reportFail', {
                            message: "没有可使用的晒单图片"
                        });
                        return false;
                    }

                    //验证主商品是否存在
                    var productInfo = $("div.pro-info[oid='"+task.business_oid+"'][pid='"+task.item_id+"']");
                    if(productInfo.length){
                        showItemComtBox(task.item_id);
                    }else{
                        clue('没有找到要晒单的商品');
                        // setTimeout(tabPc.myOrderListClick(), 5000);
                        var _message = task.client_try_times>3?{message: "没有找到要晒单的商品 第"+task.client_try_times+"次",delay:3600 * 24}:{message: "没有找到要晒单的商品"};
                        tabCommon.sm(taskVars, 'reportFail', _message);
                    }
                }else{
                    clue('不需要晒单，不用进待晒单', 'error');
                    tabCommon.sm(taskVars, 'reportSuccess');
                }
            }

//显示待晒单的box
            function showItemComtBox(productId){
                label('上传晒单图片');
                //晒单操作窗口显示
                var comt_box = $('.comt-box[pid="'+productId+'"]:visible');
                if(comt_box.length > 0){
                    //productComment(productId);

                    var btn = $('.op-btns a[alt="'+ productId +'"]:contains("晒单")');
                    if(btn.length){
                        if(!is_showing){
                            var _anonymous = 1;//强制匿名
                            clue('开始上传');
                            label('上传到jd',300);
                            new Show(task.sku_code,task.business_oid,_anonymous,task.pictures).submit(function () {

                            });
                            is_showing = true;
                        }
                    }else{
                        clue('当前窗口不对');
                        window.location.reload(true);
                    }

                }else{
                    var btn = $('.op-btns a[alt="'+ productId +'"]:contains("晒单")');

                    if(btn.length){

                        btn[0].click();
                        setTimeout(function(){
                            showItemComtBox(productId);
                        }, 5000);
                    }else{
                        clue('没有找到晒单入口', 'error');
                    }
                }
            }




            tabCommon.mutation(bodyObserver,function (mutation) {
                if(mutation.type == 'childList'){
                    //console.log('childList');
                    //console.log(mutation.target);
                    //console.log(mutation);
                    if(mutation.target.className == 'img-list-ul' ) {
                        if(mutation.addedNodes.length > 0){
                            if(task.share.status != 3){
                                sharedImages();
                            }
                        }
                    }
                }
            });


            function sharedImages(){
                var imgs = $('.img-lists .img-list-ul').find('img');
                if(imgs.length > 0){
                    console.log(imgs);
                    var imgNum=imgs.length;
                    var loaded = false;
                    imgs.load(function(){
                        if(!--imgNum){
                            // 加载完成
                            console.log('share images loaded');
                            captureArea()
                        }
                        //if(!loaded){
                        //    loaded = true;
                        //    clue('已经有图片加载完成，5秒后开始截图', 'log', 3000);
                        //    setTimeout(function(){
                        //        console.log('share images loaded');
                        //        captureArea();
                        //    }, 5000);
                        //}

                    }).error(function(){
                        if(!--imgNum){
                            // 加载完成
                            console.log('share images loaded');
                            captureArea()
                        }
                    });
                }
            }
            function captureArea(){
                var scroll_top = document.body.scrollTop;
                var offset = $('.pro-info[pid="'+ item_id +'"]').offset();
                if (scroll_top != offset.top) {
                    document.body.scrollTop = offset.top;
                    setTimeout(function () {
                        captureArea()
                    }, 600)
                } else {
                    tabCommon.screenShot(function(imgUrl){
                        tabCommon.sm(taskVars, 'saveImage',{order_id:task.order_id,imgUrl:imgUrl}); // noUse
                    },true);
                }
            }


        })
    }

}})();