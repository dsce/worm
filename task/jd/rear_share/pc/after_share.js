(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearShare', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('检测晒单结果');
        lazy(function () {

            var task = localTask.currentTask;
            var item_id = localTask.currentTask.item_id;
            
            if(tabCommon.findElement($('.dt-imgshow').find('img'))){
                captureArea();
            }else{
                clue('找不到晒单图片');
                tabCommon.sm(taskVars, 'reportFail', {
                    message: "找不到晒单图片 主商品无",
                    delay:3600 * 24
                });
            }


            function sharedImages(){
                var imgs = $('.dt-imgshow').find('.thumbnail-list').find('img');
                if(imgs.length > 0){
                    console.log(imgs);
                    var imgNum=imgs.length;
                    var loaded = false;
                    imgs.load(function(){
                        if(!--imgNum){
                            // 加载完成
                            console.log('share images loaded');
                            captureArea()
                        }
                        //if(!loaded){
                        //    loaded = true;
                        //    clue('已经有图片加载完成，5秒后开始截图', 'log', 3000);
                        //    setTimeout(function(){
                        //        console.log('share images loaded');
                        //        captureArea();
                        //    }, 5000);
                        //}

                    }).error(function(){
                        if(!--imgNum){
                            // 加载完成
                            console.log('share images loaded');
                            captureArea()
                        }
                    });
                }
            }
            function captureArea(){
                var scroll_top = Math.round(document.body.scrollTop);
                var offset_top = Math.round($('.product-'+item_id).offset().top);
                if (scroll_top != offset_top) {
                    document.body.scrollTop = offset_top;
                    setTimeout(function () {
                        captureArea()
                    }, 600)
                } else {
                    $('#panel-XSS').remove();
                    $('#alertify-logs').remove();
                    tabCommon.screenShot(function(imgUrl){
                        tabCommon.sm(taskVars, 'saveImage',{order_id:task.order_id,imgUrl:imgUrl}); //result share
                    },true);
                }
            }




        })
    }

}})();