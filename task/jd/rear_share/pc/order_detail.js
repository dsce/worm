(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearShare', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('检测晒单结果');
        lazy(function () {

            var task = localTask.currentTask;
            var item_id = localTask.currentTask.item_id;

            var mainProduct = $('.product-'+item_id).has('.f-price:contains("¥")');

            if(mainProduct.length){
                gotoScreenPage();
            }else{
                tabCommon.reloadPage(2,function () {
                    clue('刷新重试下');
                },function () {
                    clue('无法截图追评');
                    tabCommon.sm(taskVars, 'reportFail',{message:'订单详情找不到主商品,无法截图晒单',delay:24*3600});
                })
            }

            function gotoScreenPage(){
                if(tabCommon.findElement(mainProduct.find('a:contains("查看评价")'))){
                    tabCommon.clickElement(mainProduct.find('a:contains("查看评价")'));
                    setTimeout(function () {
                        tabCommon.sm(taskVars, 'reportFail',{message:'截图晒单失败',delay:24*3600});
                    },60e3);
                }else{
                    clue('无法截图晒单');
                    tabCommon.sm(taskVars, 'reportFail',{message:'订单详情找不到查看评价,无法截图晒单',delay:24*3600});
                }
            }

        })
    }

}})();