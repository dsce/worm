(function () {

    tabCommon.start('rearShare', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('晒单 寻找目标订单');
        //主任务 检查订单状态
        var task = localTask.currentTask;
        getTaskWorks(function (taskWorks) {
            clue('任务已重试次数:'+task.client_try_times||0);
            task.business_oid = taskWorks.business_oid;
            tabPc.rear.findOrder(task.business_oid,task.item_id,function () {
                clue('检测需要完成的动作');
                checkOrderShared();
            },taskVars);
        })

//检查晒单状态
        function checkOrderShared(){
            getSharedTaskWorks(function (shared) {
                if(shared.isShared){
                    tabCommon.sm(taskVars, 'reportSuccess');
                }else if(shared.rearIsShared){
                    if(tabCommon.findElement($('#operate' + task.business_oid).find('a:contains("追评")'))){
                        clue('晒过 还没截图, 去追评页面截图')
                        closeMe(3);
                        tabCommon.clickElement($('#operate' + task.business_oid).find('a:contains("追评")'));
                    }else{
                        closeMe(3);
                        window.open("http://club.jd.com/afterComments/orderPublish.action?orderId="+task.business_oid);
                    }
                }else{
                    if(task.pictures.length && task.status ==2){
                        clue('需要晒单 ');

                        var share = $('#operate' + task.business_oid).find('a:contains("晒单")');

                        if(share.length){
                            closeMe(3);
                            share[0].click();
                        }else{
                            clue('没有晒单入口');
                            var comment = $('#operate' + task.business_oid).find('a:contains("评价")');
                            var appendComment = $('#operate' + task.business_oid).find('a:contains("追评")');
                            if(comment.length>0 || appendComment.length>0){
                                clue('没有晒单，有评价或追评，认为已经晒过了');
                                //去截图
                                if(tabCommon.findElement($('#operate' + task.business_oid).find('a:contains("追评")'))){
                                    closeMe(3);
                                    tabCommon.clickElement($('#operate' + task.business_oid).find('a:contains("追评")'));
                                }else{
                                    clue('进入订单详情');
                                    $("#idUrl" + task.business_oid)[0].click();

                                    setTimeout(function () {
                                        clue('找不到晒单入口 增加按钮尝试一下');
                                        $('<a class="" target="_blank" href="//club.jd.com/mycomments.aspx?sort=1" clstag="click|keycount|orderinfo|product_show">晒单</a>').appendTo('#pay-button-' + task.business_oid);
                                        setTimeout(function () {
                                            checkOrderShared();
                                        },3e3);
                                        return false;
                                    },10e3)

                                    // window.open("//club.jd.com/afterComments/productPublish.action?sku="+task.item_id+"&orderId="+task.business_oid);
                                }
                            }else{
                                // clue('5s，退出');
                                // setTimeout(task_done, 5000);

                                clue('找不到晒单入口 增加按钮尝试');
                                $('<a class="" target="_blank" href="//club.jd.com/mycomments.aspx?sort=1" clstag="click|keycount|orderinfo|product_show">晒单</a>').appendTo('#pay-button-' + task.business_oid);
                                setTimeout(function () {
                                    checkOrderShared();
                                },3e3);
                                return false;
                                // setTimeout(function(){
                                //     tabCommon.sm(taskVars, 'reportFail', {
                                //         message: "未找到晒单入口",
                                //         delay:3600 * 24
                                //     });
                                // },5*1000)
                            }
                        }
                    }else if(task.pictures.length && task.status == 3){
                        //晒单成功了
                        var share = $('#operate' + task.business_oid).find('a:contains("晒单")');
                        if(share.length==0){
                            tabCommon.sm(taskVars, 'reportSuccess');
                        }else{
                            clue('找到了晒单入口 但是状态是3');
                        }

                    }else{
                        clue('不需要晒单');
                        tabCommon.sm(taskVars, 'reportFail', {
                            message: "未找到晒单入口",
                            delay:3600 * 24
                        });
                    }
                }
            })
        }
    }

}})();