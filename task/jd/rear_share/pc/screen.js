(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearShare', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('检测晒单结果');
        lazy(function () {

            var task = localTask.currentTask;
            var item_id = localTask.currentTask.item_id;

            var _top = $('input[value="'+item_id+'"]').attr('type','text').offset().top;
            $('input[value="'+item_id+'"]').attr('type','hidden');

            if(tabCommon.findElement($('input[value="'+item_id+'"]'))){
                captureArea();
            }else{
                clue('晒单找不到主商品');
            }

            function captureArea(){
                var scroll_top = document.body.scrollTop;
                if (scroll_top != _top) {
                    document.body.scrollTop = _top;
                    setTimeout(function () {
                        captureArea()
                    }, 600)
                } else {
                    tabCommon.screenShot(function(imgUrl){
                        tabCommon.sm(taskVars, 'saveImage',{order_id:task.order_id,imgUrl:imgUrl}); // share noUse...
                    },true);
                }
            }

        })
    }

}})();