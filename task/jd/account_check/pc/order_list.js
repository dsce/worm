(function () {

    tabCommon.start('accountCheck', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {

        tabCommon.addMessageListener('accountOkDone',function () {
            //tabCommon.taskReport.clank()
            // tabCommon.sm(taskVars, 'taskReport');
            tabCommon.sm(taskVars, '.report.clank.success')
        })

        lazy(function () {
            getSharedTaskWorks(function (local) {
                if(local.loginType=='account'){
                    clue('帐号正常 准备汇报 5s之后继续下一条任务');
                    tabCommon.sm(taskVars, 'accountOk',{})
                }else{
                    clue('丢失登录标记或者是自动登录, 准备重新登录');
                    tabPc.loginOut();
                }
            })
        })
    }

}})();