M.accountCheck = {
    //保存账号状态
    accountOk: function (request, sender) {
        //汇报帐号正常 任务完成
        var _api = new Api();
        _api.setRetryTimes(0);//无限重试
        useLocal(function (local) {
            var postData = {
                admin_id: null,
                account_id: local.accountId,
                client_code:'pc'
            }
            _api.enableAccount(postData, function (ret) {
                console.log('启用帐号成功 汇报任务完成',ret);
                smToTab(sender.tab.id,'accountOkDone');
            })
        })
    },
    accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        M.global.report.clank.error({status:4,finished_result:remark},sender)
    },

    //可以自己汇报 也可以直接 .report.clank.success
    // todo 删除
    taskReport:function (request, sender) {
        M.global.report.clank.success({},sender)
    }

};