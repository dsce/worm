(function () {

    tabCommon.start('rearAppended', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('点开我的订单');

        if(document.referrer.indexOf('details.jd.com/normal/item.action?orderid=')>-1){
            if(local.currentTask.client_try_times>3){
                tabCommon.sm(taskVars, 'reportFail', {message: "点击评价跳转到了首页 延期执行"});
            }else{
                tabCommon.sm(taskVars, 'reportFail', {message: "点击评价跳转到了首页 延期执行",delay:3600 * 24});
            }
            return false;
        }

        if($('.content-r').text().indexOf('您访问的页面失联啦...')>-1){
            clue('点开页面错误 延期任务');
            tabCommon.sm(taskVars, 'reportFail',{message:'页面打开了error2.aspx',delay:24*3600});
        }else{
            lazy(function () {
                tabPc.myOrderListClick()
                closeMe(5);
            })
        }
    }

}})();