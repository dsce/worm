(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearAppended', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('检测晒单结果');
        lazy(function () {

            var finished_result = '';

            $('.dt-content img').css('width',50)

            var task = localTask.currentTask;
            var item_id = localTask.currentTask.item_id;

            var _top = $('input[value="'+item_id+'"]').attr('type','text').offset().top;
            $('input[value="'+item_id+'"]').attr('type','hidden');

            if(tabCommon.findElement($('input[value="'+item_id+'"]'))){
                if(task.picture_urls && task.picture_urls.length >0){
                    if($('.append').next().attr('src')){
                        clue('有图片显示');
                        finished_result = '有图片显示';
                    }else{
                        clue('无图片显示');
                        finished_result = '无图片显示';
                    }
                }
                captureArea();
            }else{
                clue('追评找不到主商品');
                tabCommon.sm(taskVars, 'reportFail',{message:'截图追评找不到主商品',delay:24*3600});
            }

            function captureArea(){
                var scroll_top = document.body.scrollTop;
                if (scroll_top != _top) {
                    document.body.scrollTop = _top;
                    setTimeout(function () {
                        captureArea()
                    }, 600)
                } else {
                    tabCommon.screenShot(function(imgUrl){
                        tabCommon.sm(taskVars, 'saveImage',{order_id:task.order_id,imgUrl:imgUrl,result:finished_result}); //result append
                    },true);
                }
            }

        })
    }

}})();