(function () {

    tabCommon.start('rearAppended', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('评价之后跳转到评价成功页面');
        lazy(function () {

            label('检测提交状态');

            var successText = $(".mycomment-detail .tip-title").text();
            if((successText.indexOf('追评已完成')!=-1 && successText.indexOf('以下商品未追评')!=-1) || (successText.indexOf('提交成功，你竟然全部评完啦')!=-1)){
                //去评价页面截图
                setTaskWorks({appendedSuccess:true},function () {
                    tabPc.myOrderListClick();
                    // tabCommon.sm(taskVars, '.createTabByUrl',{url:"http://club.jd.com/myJdcomments/myJdcomment.action?sort=2"});
                })
            }

        })
    }

}})();
