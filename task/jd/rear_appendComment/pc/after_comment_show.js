
/**
 * 晒单
 * @return void
 */

 var saved_succ_result_count = 0;
 var saved_err_result_count = 0;

function appendedShow(opt,pictures){
    //var sendUrl = 'http://club.jd.com/index.php?mod=OrderBbs&action=ajaxImageUpload';
    var sendUrl = '//club.jd.com/myJdcomments/ajaxUploadImage.action';
    // var front_host = 'http://www.popsd.com';
    var front_host = 'https://enzui.enzui.com';
    // var front_host = 'http://192.168.2.171:83';
    var manage_host = 'https://www2017.disi.se';

    var jd_img_host = '//img30.360buyimg.com/shaidan/';

    this.pictures = pictures;
    this.submitted_imgs = [];

    var item_id = opt.item_id;
    var business_oid = opt.business_oid;

    this.params = {
        productId: opt.item_id,
        // productName: encodeURI($('.pro-info[pid="' + item_id + '"]').find('.p-name a').text()),
        content:encodeURI(opt.comment_content),
        orderId: opt.business_oid,
        anonymousFlag: opt.anonymous,
        imgs: ''
    };

    function reportPictureSuccess(id,url){
        var post_data = {
            id:id,
            url:url
        };
        $.post('',post_data,function(){

        });
    }

    function reportPictureFail(id){
        var post_data = {id:id};
        $.post('',post_data,function(){

        });
    }

    // this.showSubmit = function(){
    //     clue('提交');
    // };

    this.showSubmit = function(){
        var obj = this;
        $.ajax({
            type:"POST",
            url:"//club.jd.com/afterComments/saveAfterCommentAndShowOrder.action",
            data:obj.params,
            dataType:"JSON",
            success: function(e) {
                console.log(e);
                //{"error":"评价提交过快，请稍后再试","resultCode":"11","success":false}
                if(e.success == false){
                    
                    // chrome.extension.sendMessage({cmd: 'share_fail'});
                    tabCommon.sm(taskVars, 'reportFail', {message: JSON.stringify(e),delay:3600 * 24});
                    return false;
                }else{
                    setTimeout(function () {

                        setTaskWorks({appendedSuccess:true},function () {
                            tabPc.myOrderListClick();
                            // location.href = 'http://club.jd.com/afterComments/productPublish.action?sku='+item_id+'&orderId='+business_oid;
                        })

                    },3000);
                }
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        })
    };

    this.getImg = function(i,success,error){
        
        var pic = pictures[i];
        console.log(pic);
        var imgsrc = '';
        if(pic.admin){
            imgsrc = manage_host  + pic.picture_url;
        }else{
            imgsrc = front_host + pic.picture_url;
        }

        console.log('get img');
        var xhr = new XMLHttpRequest();
        var obj = this;
        xhr.open('GET', imgsrc, true);
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            if (this.readyState==4){
                if (this.status == 200 || this.status == 304) {
                    var blob = this.response;
                    console.log(blob);
                    obj.submitImg(i,blob,success,error);//提交图片
                }else{
                    console.warn('get img result error',this.response);
                    obj.getImg(i,success, error);//获取图片失败重新获取
                    //error && error();
                }
            }else{
                console.warn('get img error',this);
                obj.getImg(i,success, error);//获取图片失败重新获取
                //error && error();
            }
        };

        obj.mouseEvent($('.btn-upload'), xhr.send())
    };

    this.mouseEvent = function(obj,callbcak){
        var eventClick = new MouseEvent('click', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventOver = new MouseEvent('mouseover', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventMove = new MouseEvent('mousemove', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventDown = new MouseEvent('mousedown', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventUp = new MouseEvent('mouseup', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventBlur = new MouseEvent('blur', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventKeydown = new MouseEvent('keydown', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventKeyup = new MouseEvent('keyup', {'view': window, 'bubbles': true, 'cancelable': true});

        var eventFocusout = new MouseEvent('focusout', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventEnter = new MouseEvent('enter', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventLeave = new MouseEvent('leave', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventResize = new MouseEvent('resize', {'view': window, 'bubbles': true, 'cancelable': true});
        var eventScroll = new MouseEvent('scroll', {'view': window, 'bubbles': true, 'cancelable': true});

        obj[0].dispatchEvent(eventMove);
        obj[0].dispatchEvent(eventOver);
        obj[0].dispatchEvent(eventDown);
        obj[0].dispatchEvent(eventClick);
        obj[0].dispatchEvent(eventUp);

        obj[0].dispatchEvent(eventBlur);
        obj[0].dispatchEvent(eventKeyup);


        obj[0].dispatchEvent(eventFocusout);
        obj[0].dispatchEvent(eventEnter);
        obj[0].dispatchEvent(eventLeave);
        obj[0].dispatchEvent(eventResize);
        obj[0].dispatchEvent(eventScroll);

        obj[0].click();
        callbcak && callbcak()
    };

    this.submitImg = function(i,file,success,error){
        
        console.log('submit img');
        var formData = new FormData();
        formData.append('name', pictures[i].filename);
        formData.append('PHPSESSID', 'mvpjl6muuk705ipboi3ia0b461');
        formData.append('Filedata', file, pictures[i].filename);
        var xhr = new XMLHttpRequest();
        var obj = this;
        xhr.open('POST', sendUrl, true);
        xhr.onload = function(e) {
            if (this.readyState==4){
                if (this.status == 200) {
                    console.log(this.response);
                    //需要验证提交的晒单图片是否成功
                    var sub_response = this.response;
                    var save_result_data = {
                        appended_picture_id : pictures[i].id
                        // result_code:sub_response,
                    };
                    if(sub_response.length > 0 && sub_response.indexOf('/') != -1){//长度大于0, 包含目录结构
                        //图片上传成功
                        save_result_data.client_result = '图片上传成功';
                        save_result_data.url = sub_response;
                        //生成缩略图
                        var img_id = '#image-upload-' + business_oid +'-' + item_id;
                        $("<div data-rotate='0' class='thumbnail-item'><img src='" + jd_img_host + this.response+"'><div class='thumbnail-operate'><span class='op-edit'></span><span class='op-delete'></span></div></div>").insertBefore(img_id);
                        $(".upload-num em:eq(0)").text($(".thumbnail-item").length);
                        $(".upload-num em:eq(1)").text(parseInt(10-$(".thumbnail-item").length));
                        //保存上传晒单结果
                        // chrome.extension.sendMessage({cmd: 'save_append_share_result',data:save_result_data},function(){});
                        tabCommon.sm(taskVars, 'save_append_share_result',{data:save_result_data})
                        saved_succ_result_count++;

                        pictures[i].res = this.response;
                        console.log('success'+pictures[i].picture_url);
                        //上传晒单图片正常
                        obj.submitted_imgs.push(jd_img_host + this.response);

                        
                        // if(obj.submitted_imgs.length == pictures.length){
                            //晒单图片已经全部上传成功
                            //准备提交晒单
                        if(pictures.length == (obj.submitted_imgs.length + saved_err_result_count)){
                            obj.params.imgs = obj.submitted_imgs.toString();
                            console.log(obj.params);//提交晒单参数
                            obj.showSubmit();
                        }
                        // }else{
                        //     console.log("晒单图片还不够，公共"+obj.submitted_imgs.length+" , 已上传"+pictures.length);
                        // }

                    }else{//结果返回不正常，重新上传晒单图片
                        // obj.submitImg(i,file,success,error);// 再一次 提交图片
                        //获取状态码
                        save_result_data.result_code = sub_response;
                        if(sub_response == 'e20005' || sub_response == 'e20000' ){

                            save_result_data.client_result = sub_response == 'e20005' ? '上传失败，图片中不能包含二维码，请重新上传' : '上传文件格式问题或上传文件为空，上传出错';
                            //保存晒单上传结果
                            // chrome.extension.sendMessage({cmd: 'save_append_share_result',data:save_result_data},function(){});
                            tabCommon.sm(taskVars, 'save_append_share_result',{data:save_result_data})
                            saved_err_result_count++;

                            if((pictures.length == 1 && i == 0) || pictures.length == saved_err_result_count){
                                //只有这张图 || 所有图都没上传成功  则任务失败
                                console.log('图片异常，请检查提交的晒单图片');
                                // chrome.extension.sendMessage({cmd: 'append_share_fail'});
                                tabCommon.sm(taskVars, 'reportFail', {message: "图片异常",delay:3600 * 24});
                            }else if(pictures.length == (obj.submitted_imgs.length + saved_err_result_count)){//当前就是最后一张，提交吧
                                obj.params.imgs = obj.submitted_imgs.toString();
                                console.log(obj.params);//提交晒单参数
                                obj.showSubmit();
                            }
                        }else{
                            
                            save_result_data.client_result = '其他错误';
                            //保存晒单上传结果
                            // chrome.extension.sendMessage({cmd: 'save_append_share_result',data:save_result_data});
                            tabCommon.sm(taskVars, 'save_append_share_result',{data:save_result_data})
                            obj.submitImg(i,file,success,error);// 再一次 提交图片
                        }
                        
                        // if(pictures.length == 1 && i == 0){
                        //     //只有这张图，任务失败
                        //     console.log('图片异常，请检查提交的晒单图片');
                        //     chrome.extension.sendMessage({cmd: 'share_fail'});
                        //     //task_fail('图片异常，请检查该任务下提交的晒单图片');
                        // }else if(i == (pictures.length - 1)){//当前就是最后一张，提交吧
                        //     obj.params.imgs = obj.submitted_imgs.toString();
                        //     console.log(obj.params);//提交晒单参数
                        //     obj.showSubmit();
                        // }
                    }
                }else{
                    //提交未成功
                    console.warn('submit img error',this);
                    obj.submitImg(i,file,success,error);// 再一次 提交图片
                    //error && error();
                }
            }else{
                console.warn('submit img error',this);
                obj.submitImg(i,file,success,error);// 再一次 提交图片
                //error && error();
            }
        };

        xhr.send(formData);

    };

    this.submit = function(func){
        if(this.pictures){
            for(var i in pictures){
                pictures[i].filename = pictures[i].picture_url.toString().split('/').pop();
                this.getImg(i,function(){

                },function(){

                });
            }
        }else{
            console.error('show pictures is undefined');
        }
        func && func();
    };

    this.capture = function(){
        
        chrome.runtime.sendMessage({cmd: 'capture'},function(response){});
    }
}


