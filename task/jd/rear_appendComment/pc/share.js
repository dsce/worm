(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearAppended', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('寻找目标订单');

        //主任务 检查订单状态
        var task = localTask.currentTask;
        var order_id;
        //可能有拆单的问题 这里需要获取下最新的order_id
        lazy(function () {
            getTaskWorks(function (taskWorks) {
                task.business_oid = taskWorks.business_oid;
                order_id = task.business_oid;
                var guid = $('.pro-info[oid="'+task.business_oid+'"]').find('a[guid]').attr('guid');
                if(guid){
                    clue(guid);
                    tabCommon.sm(taskVars, 'saveCommentedState',{guid: guid, business_oid: task.business_oid, sku_code: task.item_id, state: 1 });
                    window.open('//club.jd.com/repay/'+task.item_id+'_'+guid+'_1.html');
                }else{
                    tabCommon.sm(taskVars, 'reportFail',{message:'晒单页面找不到追评入口,延迟追评',delay:24*3600});
                }
            })
        })



    }

}})();