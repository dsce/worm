(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearAppended', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('追评 寻找目标订单');

        //主任务 检查订单状态
        var task = localTask.currentTask;
        var order_id;
        //可能有拆单的问题 这里需要获取下最新的order_id
        lazy(function () {
            getTaskWorks(function (taskWorks) {
                clue('任务已重试次数:'+task.client_try_times||0);
                task.business_oid = taskWorks.business_oid;
                order_id = task.business_oid;
                tabPc.rear.findOrder(order_id,task.item_id,function () {
                    clue('检测需要完成的动作');
                    getTaskWorks(function (taskWorks) {
                        if(taskWorks.appendedSuccess){
                            var promisejdint = $("#tb-" + task.business_oid).find('.promisejdint');
                            if (promisejdint.length > 0) {
                                clue('全球购订单 去晒单找guid');
                                if(tabPc.rear.getButtons(order_id,'晒单')){
                                    clue('有晒单按钮 去找guid');
                                    tabCommon.clickElement($('#operate' + task.business_oid).find('a:contains("晒单")'));
                                }else{
                                    clue('无晒单按钮 延期任务');
                                    tabCommon.sm(taskVars, 'reportFail',{message:'无追评入口,延迟追评',delay:24*3600});
                                }
                                //$('.pro-info[oid="'+task.business_oid+'"]').find('a[guid]').attr('guid')
                            } else {
                                clue('需要去截屏 点开订单详情');
                                tabPc.clickOrderDetailLink(order_id);
                            }
                        }else{
                            clue('需要去做追评');
                            checkOrderAppendComment();
                        }
                    })
                },taskVars);
            })
        })

        function checkOrderAppendComment(){

            //是否有追评内容
            //状态是否是完成 3
            //是否存在追评按钮
            //是否存在其他按钮 评论 晒单
            //是否强制去追评页面
            //是否去截图

            if(task.body){
                clue('有追评内容 检测下追评按钮 存在就去做');
                if(tabPc.rear.getButtons(order_id,'追评')){
                    closeMe(3);
                    tabCommon.clickElement($('#operate' + task.business_oid).find('a:contains("追评")'));
                }else{
                    clue('不存在追评按钮');
                    if(task.client_status==3){
                        clue('获取的任务已经是成功状态');
                        tabCommon.sm(taskVars, 'reportSuccess');
                        return false;
                    }else{
                        if(tabPc.rear.getButtons(order_id,'评价')){
                            //有评价按钮 无法追评
                            clue('发现评价按钮,增加追评按钮,追不了的话延迟追评');
                            // tabCommon.sm(taskVars, 'reportFail',{message:'发现评价按钮,延迟追评',delay:24*3600});
                            $('<a class="" target="_blank" href="https://club.jd.com/afterComments/orderPublish.action?orderId=' + order_id +'" clstag="click|keycount|orderinfo|product_commentAgain">追评</a>').appendTo('#pay-button-' + order_id);
                            setTimeout(function () {
                                checkOrderAppendComment();
                            },3e3);
                            return false;
                        }else{
                            if(tabPc.rear.getButtons(order_id,'晒单')){
                                //有评价按钮 无法追评
                                clue('发现晒单按钮,应该已经追评过了,强制去追评页面,已经成功的话 就会去截屏');
                                $('<a class="" target="_blank" href="https://club.jd.com/afterComments/orderPublish.action?orderId=' + order_id +'" clstag="click|keycount|orderinfo|product_commentAgain">追评</a>').appendTo('#pay-button-' + order_id);
                                setTimeout(function () {
                                    checkOrderAppendComment();
                                },3e3);
                                return false;
                            }else{
                                clue('没有发现按钮 刷新3次 还没有的话汇报找不到追评入口');
                                tabCommon.reloadPage(3,function () {
                                    clue('刷新成功');
                                },function () {
                                    clue('找不到追评入口 增加按钮尝试');
                                    $('<a class="" target="_blank" href="https://club.jd.com/afterComments/orderPublish.action?orderId=' + order_id +'" clstag="click|keycount|orderinfo|product_commentAgain">追评</a>').appendTo('#pay-button-' + order_id);
                                    setTimeout(function () {
                                        checkOrderAppendComment();
                                    },3e3);
                                    return false;
                                    // tabCommon.sm(taskVars, 'reportFail',{message:'找不到追评入口',delay:24*3600});
                                })
                            }
                        }
                    }
                }
            }else{
                clue('没有追评内容');
                tabCommon.sm(taskVars, 'reportFail',{message:'没有追评内容'});
            }
        }

    }

}})();