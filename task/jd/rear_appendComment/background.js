M.rearAppended = {

    reportSuccess: function (request, sender) {
        M.global.report.clank.success({},sender)
    },

    reportFail: function (request, sender) {
        getTaskWorks(function (task) {
            var order_id = task.order_id;
            var message = request.message ? request.message : "";
            var delay = request.delay ? request.delay : 0;
            var cmd = 'append_fail';
            M.global.report.rear.error({
                cmd: cmd,
                order_id: task.order_id,
                delay: delay,
                message: message
            }, sender)
        })
    },

    saveCommentedState: function (request, sender) {
        var data = {};
        data.guid = request.guid;
        data.business_oid = request.business_oid;
        data.sku_code = request.sku_code;
        data.state = request.state;
        var _api = new Api();
        _api.saveCommentState(data);
    },

    saveImage:function (request,sender) {
        console.log('saveImage');
        var api = new Api();
        api.retryTimes = 3;
        api.saveScreenCaptureUrl(request.order_id, 4 , request.imgUrl,function () {
            getTaskWorks(function (taskWorks) {
                var _saveApi = new Api();
                _saveApi.retryTimes = 3;
                _saveApi.saveAppendCommentCapturePictureUrl(taskWorks.id,request.imgUrl,request.result,function () {
                    // M.global.taskDone(request,sender);
                    M.global.report.clank.success({},sender)
                })
            })
        })
    },

    save_append_share_result:function (request,sender) {
        var api = new Api();
        api.saveAppendSharePictureUploadResult(request.data)
    },

    accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        getTaskWorks(function (task) {
            M.global.report.rear.error({cmd: 'append_fail', order_id: task.order_id,delay: 0,message: remark}, sender)
        })
    }

}