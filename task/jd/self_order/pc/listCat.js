(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        changeCategoryObserver();
        lazy(function () {
            label('[自营]随机类目页面',120);
            console.log("listCat");
            var indexed = false;
            
            //Task(index);放在监控执行
            index();
            // setTimeout(function(){
            //     console.log('5s');
            //     // Task(index);
                
            // }, 5000);


            function index() {
                if(indexed) return false;
                indexed = true;
                if($("#plist").length >0){
                    var items = $("#plist .gl-item:contains('自营')");
                }else if($("#J_goodsList").length >0){
                    var items = $("#J_goodsList .gl-item:contains('自营')");
                }else{
                    clue('未找到商品列表');
                    return false;
                }
                
                var products = [];

                if(items.length == 0){
                    console.log("自营数量 0");
                    var jdShiping = $("#J_main #J_filter .f-feature a:contains('京东配送')").not(".selected");
                    if(jdShiping.length > 0){
                        jdShiping[0].click();
                        return false;
                    }else{
                        console.log("京东配送");
                        if(document.location.protocol == 'https:'){
                            console.log('changeCategory');
                            changeCategory();
                        }else{
                            reloadHttps();
                            return false;
                        }
                    }
                }

                items.each(function(i,o){
                    console.log(i,o)
                    var priceText = $(this).find("strong.J_price i").text();
                    if(priceText.length > 0 && priceText.match(/\d+(=?.\d{0,2})/g) && $(this).find('.p-img a').attr('href').indexOf('item.jd.com') != -1){
                        var p = priceText.match(/\d+(=?.\d{0,2})/g)[0];
                        if(parseInt(p) <= 200 ){
                            products.push($(this));
                        }
                        console.log(p);
                    }else{
                        console.log('empty',priceText);
                    }

                });

                if(products.length > 0){
                    var product = products[random(0,products.length-1)];
                    var img = product.find(".p-img a");
                    img[0].click();
                }else{
                    //200的没有,准备排序
                    var filterPriceSort = $("#J_main #J_filter .f-sort a:contains('价格')").not(".up");
                    if(filterPriceSort.length > 0){
                        filterPriceSort[0].click();
                        setTimeout(function(){
                            close_this_tab();
                        }, 5000);
                    }else{
                        console.log("价格排序 没找到,或价格已是生序");
                        console.log('changeCategory');
                        changeCategory();
                    }
                }
            }

            var observer_config = {
                attributes: true,
                childList: true,
                characterData: true,
                attributeOldValue: true,
                characterDataOldValue: true,
                subtree: true
            }
            addMutationObserver($("body")[0],function(mutation){
                //console.log(mutation.type);
                //console.log(mutation.target);
                //console.log(mutation);
                if(mutation.type == 'childList'){
                    //console.log(mutation.target);
                    //console.log(mutation);
                    if(mutation.target.className == 'dorpdown-layer' && mutation.addedNodes.length > 0 && mutation.addedNodes[1].id=='settleup-content'){
                        //可以开始执行
                        Task(index);
                    }
                }
            },observer_config);
                    })
        }

}})();


