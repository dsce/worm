(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        
        lazy(function () {
            
            label('[自营]购物车去结算页面');
            console.log("cart");

            var indexed = false;
            var clearSelfItem = false;
            var clearedSelfItem = false;
            var removedSelfItem = false;

            var observer_config = {
                attributes: true,
                childList: true,
                characterData: true,
                attributeOldValue: true,
                characterDataOldValue: true,
                subtree: true
            }
            addMutationObserver($("body")[0],function(mutation){
                 //console.log(mutation.type);
                 //console.log(mutation.target);
                 //console.log(mutation);
                if(mutation.type == 'childList'){
                    if(mutation.target.className == 'ui-ceilinglamp-1' && mutation.addedNodes.length > 0){
                        //已加载,可以操作
                        Run(selectedItem);
                    }else if(mutation.target.className == "number" && mutation.addedNodes.length > 0){
                        console.log('number');
                        Run(selectedItem);
                        //Run(function(local){
                        //    var checkItem = $("#cart-list .item-list .item-form input[name='checkItem']");
                        //    var checkItemNum = checkItem.length;
                        //    var barItemNum = parseInt($(".cart-filter-bar .switch-cart-item .number").text());
                        //    if(checkItemNum > 0 && $("#toggle-checkboxes_down:checked").length==0){
                        //        //勾选要买的自营商品
                        //
                        //        var selfItemId = local.task.item_id;
                        //
                        //        if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length > 0){
                        //            var selfItemNum = $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val();
                        //            if(barItemNum == selfItemNum){
                        //                //提交结算
                        //                console.log('结算');
                        //                submit();
                        //            }
                        //
                        //
                        //        }else{
                        //            var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']");
                        //            selfItemCheckbox[0].click();
                        //        }
                        //
                        //    }else{
                        //        //点击全选
                        //        $("#toggle-checkboxes_down")[0].click();
                        //
                        //    }
                        //})

                    }else if(mutation.target.className == 'ui-dialog-content' && mutation.addedNodes.length >0 ){
                        Run(function(local){
                            var dialog_content = $(".ui-dialog-content");
                            if(dialog_content.find("h3:contains('删除商品')").length > 0){
                                var select_remove = dialog_content.find('.select-remove');
                                if(select_remove.length > 0){
                                    select_remove[0].click();
                                    clearedSelfItem = true;
                                }else{
                                    clue('没有删除按钮');
                                }

                            }
                        });
                    }
                }
            },observer_config);

            // Task(index);
            index();

            function index(local){console.log(local);
                if(indexed){
                    return false;
                }
                indexed = true;
                //全选,不全选,选中需要的
                getTaskWorks(function(tw){
                    var self_order_info = tw.self_order_info || {};

                    if(!self_order_info.item_id){
                        clue('找不到自营单信息');
                        location.href = 'https://www.jd.com/';
                        return false;
                    }

                    var cartEmpty = $("#container div.message:contains('购物车空空的')")
                    if(cartEmpty.length > 0){
                        clue('购物车已经清空');
                        location.href = 'https://www.jd.com/';
                        return false;
                    }

                    //selectedItem(local);
                    // return false;

                     // var barItemNum = parseInt($(".cart-filter-bar .switch-cart-item .number").text());
                    var barItemNum = parseInt($(".comm-right .amount-sum em").first().text());
                    var selfItemId = self_order_info.item_id;

                    if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length > 0){
                        var selfItemNum = $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val();
                        if(barItemNum == selfItemNum){
                            //提交结算
                            console.log('结算');
                            submit();
                        }else{
                            $("#toggle-checkboxes_down")[0].click();
                        }
                    }else{
                        var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+'1153198'+"']");
                        var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']");
                        selfItemCheckbox[0].click();
                    }

                 })
            }

            function submit(){
                var submit_btn = $("#cart-floatbar a.submit-btn");
                if (submit_btn.length > 0) {
                    submit_btn[0].click();
                }
            }


            /**
             * 选中主商品
             */
            function selectedItem(local){

                getTaskWorks(function(tw){

                    var self_order_info = tw.self_order_info || {};

                    if(!self_order_info.item_id){
                        clue('自营单信息不存在');
                        location.href = 'https://www.jd.com/';
                        return false;
                    }

                    var checkItem = $("#cart-list .item-list .item-form input[name='checkItem']");
                    // var barItemNum = parseInt($(".cart-filter-bar .switch-cart-item .number").text());
                    var barItemNum = parseInt($(".comm-right .amount-sum em").first().text());
                    var selfItemId = tw.self_order_info.item_id;

                    var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']")
                    if(selfItemCheckbox.length == 0){
                        clue("主商品不存在", "error");

                        if(clearSelfItem){
                             //删除自营商品,已勾选自营商品,点击删除所选商品
                            if(removedSelfItem){
                                location.href = 'http://easybuy.jd.com/address/getEasyBuyList.action';
                            }else{
                                var removeBatch = $("#cart-floatbar a.remove-batch");
                                removeBatch[0].click();
                                removedSelfItem = true;
                            }

                        }else{
                            var selfCheckItem = $("#cart-item-list-01 .cart-checkbox input[name='checkItem']");
                            if(selfCheckItem.length > 0){
                                //选中自营商品
                                //$("#cart-item-list-01 .cart-checkbox input[name='checkShop']")[0].click();
                                $("#cart-list .cart-item-list .shop:contains('京东自营')").find(".cart-checkbox input[name='checkShop']")[0].click();
                                clearSelfItem = true;
                            }else{
                                location.href = 'http://easybuy.jd.com/address/getEasyBuyList.action';
                            }
                        }

                        return false;
                    }

                    if(checkItem.length > 0){
                        //有已经勾选的商品,
                        if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length > 0){
                            //主商品已勾选
                            clue("主商品已勾选");
                            var selfItemNum = $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val();
                            if(barItemNum == selfItemNum){
                                //勾选数量和主商品购物车数量相同
                                //提交结算
                                clue("主商品已勾选", 'success');
                                console.log('结算');

                                clue("主商品数量正确,去结算", "success");
                                submit();

                            }else{
                                //已勾选商品数量和主商品购物车数量不一致,说明勾选有其他商品,全选,或全部选
                                clue("已选择商品数量和购物车主商品数量不一致,重新勾选主商品");
                                if($("#toggle-checkboxes_down:checked").length==0){
                                    clue("全选");
                                }else {
                                    clue("全不选");
                                }
                                $("#toggle-checkboxes_down")[0].click();
                            }
                        }else{
                            //主商品未勾选,勾选主商品
                            clue('勾选主商品'+selfItemId);
                            selfItemCheckbox[0].click();

                        }

                    }else{
                        //没有勾选的商品.全选肯定没勾上
                        //勾主商品
                        //$("#toggle-checkboxes_down")[0].click();
                        clue('勾选主商品'+selfItemId);
                        selfItemCheckbox[0].click();
                    }

                })
            }

        })
    }

}})();