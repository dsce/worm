//京东 提交订单 新增收货地址

var county_selected = false;
var saveConsigneeTitleDiv_btn_clicked = false;
var areaTabLoaded = false;
var edited = false;
var selected = false;

Task(index);

//页面监控

var observer = new MutationObserver(function(mutations) { 
  // console.log(mutations);
  mutations.forEach(function(mutation) {
    //console.log(mutation.type);
    //console.log(mutation.target);
    // console.log(mutation);

    if(mutation.type === 'childList'){
      if (mutation.target.className == 'ui-area-tab' && mutation.addedNodes.length > 0) {

        if(!areaTabLoaded){
          areaTabLoaded = true;
          selectAddress();
          console.log("可以编辑");
          edited = true;
        }

      }else if(mutation.target.id == 'span_province'&&mutation.addedNodes.length>0){
        //设置省份
        console.log('span_province');
        Run(set_province);
      }else if(mutation.target.id == 'span_city'&&mutation.addedNodes.length>0){
        //设置城市
        console.log('span_city');
        Run(set_city);
      }else if(mutation.target.id == 'span_county'&&mutation.addedNodes.length>0&&mutation.addedNodes[0].length>1){
        //设置 县区
        console.log('span_county');
        Run(set_county);
      }else if(mutation.target.id == 'span_town'&&mutation.addedNodes.length>0&&mutation.addedNodes[0].length>1){
        //设置 街道
        console.log('span_town');
        Run(set_town);
      }else if(mutation.target.id == 'span_town'&&mutation.addedNodes.length==0&&mutation.removedNodes.length>0){
        //删除街道
        console.log('span_town2');
        if($("#span_town:visible").length == 0){
          //console.log('===============================================================================================');
        }
        
      }

    }else if(mutation.type==='attributes'){
      if(mutation.target.id=='consignee_address'&&mutation.attributeName=="tabindex"&&mutation.oldValue=="6"){
        
        if($("#consignee_town[tabindex='5']:visible").length==0&&county_selected){
          //已经选择 县区，，没有 街道，准备保存
          Run(saveConsigneeTitleDiv_btn);
          // Run(input_cosignee_name)
        }
      }else if(mutation.target.className=='ui-area-text'&&mutation.attributeName=='data-id'&&mutation.oldValue!=''){
        if(edited){
          console.log('完成更新');
          clue('保存');
          setTimeout(function(){
            save();
          }, 3000);
        }
      }
    }

  });

});

var observer_config = { 
  attributes: true,
  childList: true,
  characterData: true, 
  attributeOldValue: true,
  characterDataOldValue: true,
  subtree: true
  }

observer.observe($("body")[0], observer_config);


//运行
function index(local){
  var consignee = local.task.consignee;

  //收货人，详细地址，手机号
  // $("#consignee_name").val(consignee.name);
  // $("#consignee_address").val(consignee.short_address);
  // $("#consignee_mobile").val(consignee.mobile);

  //检查是否有空的地区
  var consignee_province_id = $("#consignee_province").val();
  var consignee_city_id = $("#consignee_city").val();
  var consignee_county_id = $("#consignee_county").val();
  var consignee_town_id = $("#consignee_town").val();

  //if(consignee_county_id == ""){
  //  //城市有问题
  //  set_county();
  //
  //}
  //
  //if(consignee_town_id == ""){
  //  set_town();
  //}
  //
  //save();
  
}



//设置省份
function set_province(local){
  var consignee = local.task.consignee;
  var province = $('#consignee_province option').filter(function() {
    var text = $(this).text().replace('*', '')
    return text == consignee.province;
  })
  if (province.length > 0) {
    province.prop('selected', true)
    change_event($("#consignee_province")[0]);
  }else{
    clue("不能匹配的省份 "+consignee.province);
  }
}

//设置城市
function set_city(local){
  var consignee = municipality(local.task.consignee);
  var select = $("#consignee_city");
  var options = $("#consignee_city option");

  //var city = options.filter(function() {
  //  var text = $(this).text().replace('*', '')
  //  return text == consignee.city;
  //})
  //if (city.length > 0) {
  //  city.prop('selected', true)
  //  change_event($("#consignee_city")[0]);
  //}else{
    clue('不能匹配的城市 '+ consignee.city + " 随机");

    var index = Math.floor(Math.random()*(options.length-1)+1)
    options.eq(index).prop('selected', true);
    change_event(select[0]);
  //}
  save();
}

//设置县区
function set_county(local){
  var consignee = municipality(local.task.consignee);
  var select = $("#consignee_county");
  var options = $("#consignee_county option");

  //var option = options.filter(function() {
  //  var text = $(this).text().replace('*', '')
  //  return text == consignee.area;
  //})
  //
  //if (option.length > 0) {
  //  option.prop('selected', true)
  //  change_event(select[0]);
  //}else{
    //clue('无法匹配县区');
    //clue('县区，随机');

    var index = Math.floor(Math.random()*(options.length-1)+1)
    options.eq(index).prop('selected', true);
    change_event(select[0]);
  //}

  county_selected = true;

  // if(is_municipality(consignee.province)){
  //   setTimeout(saveConsigneeTitleDiv_btn,2000);
  // }
  save();
}

//设置街道
function set_town(local){
  var select = $("#consignee_town");
  var options = $("#consignee_town option");

  var index = Math.floor(Math.random()*(options.length-1)+1)
  options.eq(index).prop('selected', true);
  change_event(select[0]);

  setTimeout(saveConsigneeTitleDiv_btn,2000);
}

function municipality(consignee){
  var province = consignee.province;

  if(province=='北京'||province=='上海'||province=='天津'||province=='重庆'){
    consignee.city = consignee.area;
  }

  return consignee;
}

//保存收货人信息
function saveConsigneeTitleDiv_btn(){

  if(saveConsigneeTitleDiv_btn_clicked){return false;}
  saveConsigneeTitleDiv_btn_clicked = true;

chrome.storage.local.get(null,function(local){
  

  writing($("#consignee_name"),local.task.consignee.name,function(){
    lazy(function(){
      writing($("#consignee_address"),local.task.consignee.short_address,function(){
        lazy(function(){
          writing($("#consignee_mobile"),local.task.consignee.mobile,function(){
            lazy(function(){
                $("#saveConsigneeTitleDiv:visible")[0].click();

                $("#saveConsigneeTitleDiv:visible").hide();
            });
          });
        });
      });
    });
  });
})

}

function selectAddress() {
  if(selected){
    return false;
  }
  selected = true;
  $("#jd_area .ui-area-text").attr('data-id').match(/\d+/g);
  if($("#jd_area .ui-area-text").length > 0){
    var areaIds = $("#jd_area .ui-area-text").attr('data-id').match(/\d+/g);

    var aArea = $("#jd_area .ui-switchable-panel-selected li a");
    aArea[random(0, aArea.length-1)].click();
    //var countyId = areaIds[2];
    //var townId = areaIds[3];
    //if(countyId > 0){
    //
    //    if(townId){
    //
    //    }else{
    //
    //    }
    //
    //}else{
    //    //随机选区
    //    var aCounty = $("#jd_area .ui-area-content .ui-switchable-panel[data-index='2']").find(".ui-area-content-list a[data-id]")
    //    aCounty[random(0, aCounty.length-1)].click();
    //}
  }
}


function save(){
  $("#saveConsigneeTitleDiv:visible")[0].click();
}
