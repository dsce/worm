//京东商品详情页面
(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(localSelf) {
        label('[自营]商品详情页',120);
        lazy(function () {
            

        //计时
        var count_timer = 0;
        //计时器
        var time_interval = null;


        //底部停留计时
        var bottom_count_timer = 0;
        //底部停留计时器
        var bottom_time_interval = null;

        var jd_rand_main_wait_seconds = random(20, 50);//10-30多长时间放入购物车
        var jd_rand_main_bottom_wait_seconds = 15;//页面底部等待时间

        var jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, _t.main_product_wait_rand_time_end);
        var jd_main_product_wait_time = 2;
        var jd_aux_product_wait_time = random(_t.aux_product_wait_rand_time_start, _t.aux_product_wait_rand_time_end);
        var jd_aux_product_wait_time = 2;

        var indexed = false;
        changeCategoryObserver();
        //Task(index);
        var observer_config = {
            attributes: true,
            childList: true,
            characterData: true,
            attributeOldValue: true,
            characterDataOldValue: true,
            subtree: true
        }
        addMutationObserver($('body')[0],function(mutation){
            //console.log(mutation.type);
            //console.log(mutation.target);
            //console.log(mutation);
            if(mutation.type === 'childList'){
                //console.log(mutation.target);
                //console.log(mutation);
                if(mutation.target.id === 'store-prompt' && mutation.addedNodes.length > 0){
                    //$('#InitCartUrl').hide();
                    //$('#InitCartUrl').after('加入购物车');
                    Run(readyIndex);
                }else if(mutation.target.id == 'choose-btn-easybuy' && mutation.addedNodes.length > 0){
                    //$('#choose-btn-easybuy').hide();
                }else if(mutation.target.id == 'm-itemover-content' && mutation.addedNodes.length > 0){
                    //下柜后显示的类似商品 监控，不会与store-prompt共存
                    Run(readyIndex);
                }
            }else if(mutation.type == 'attributes'){
                if(mutation.target.id == "product-intro" && mutation.attributeName == "class" && mutation.target.className.indexOf('product-intro-itemover') != -1){
                    //下柜
                    Run(readyIndex);
                }
            }else if(mutation.type == 'characterData'){
                //console.log(mutation.target);
                //console.log(mutation);

                //关注店铺
                if(mutation.oldValue != undefined && mutation.oldValue == '关注店铺'){
                    //关注店铺成功，关注商品
                    Run(function(local){
                        followProduct(localSelf.currentTask.follow_product);
                    });

                }
            }

        },observer_config);


        // Task(function(local){

        //     //页面嵌入框架
        //     if(top != self){
        //         console.error('页面嵌入框架，错误');
        //     }else{
        //         console.log('页面没有嵌入框架，正常');
        //     }

        //     // if(document.location.protocol != 'https:'){
        //     //     reloadHttps();
        //     // }


        //     if($(".m-itemover-title:contains('已下柜')").length > 0){
        //         lazy(function(){
        //             readyIndex(local);
        //         }, 5);
        //     }
        // })

        readyIndex();
        function readyIndex(local){
            console.log('readyIndex');

            if(location.href.indexOf('item.yiyaojd.com') != -1){
                changeCategory();
                return false;
            }

            if(location.href.indexOf('item.jd.hk') != -1 || location.href.indexOf('eduonline.jd.com') != -1){
                clue('不支持的商品，商品链接：' + location.href);
                setTimeout(function(){
                    location.href = 'https://www.jd.com';
                },3*1000)
            }
            //var itemId = location.pathname.match(/\d+/g);
            //var lastItemId = local.rand_search_items.pop();
            index(local);
        }


        function index(local) {
            console.log('index');
            if(indexed){ return false;}
            indexed = true;
            var task = localSelf.currentTask;

            // if (taskOrderExist(local)) {
            //     return false;
            // }

            var itemId = location.pathname.match(/\d+/g)[0];

            //切换主商品显示
            // sendMessageToBackground('tab_update_selected_by_url', 'http://item.jd.com/'+task.item_id+'.html');

            if (itemId) {
                updateHostStatus(1406000);

                task.item_id = itemId;
                //正常的商品

                //主商品计算页面停留时间
                //if($("#jd-price").length > 0){
                //    var jdPrice = $("#jd-price")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
                //    var priceTime = getItemWaitTimeEnd(jdPrice);
                //    //jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, priceTime);
                //}else{
                //    console.log('没有获取到价格');
                //}


                //地区
                var area_text = $('#store-selector .text').text();

                //主产品下柜
                var item_over = $("#itemInfo .m-itemover-title:visible:contains('下柜')");
                if (item_over.length > 0) {
                    clue("主商品[" + task.item_id + "]下柜", 'error');
                    updateHostStatus(1406002);
                    changeCategory();
                    //reportProductStockout("商品[" + task.item_id + "]下柜");
                    return false;
                }

                var store_prompt = $("#store-prompt:visible:contains('无货')");
                if (store_prompt.length > 0) {
                    clue("主商品[" + task.item_id + "]无货", 'error');
                    updateHostStatus(1406001);
                    changeCategory();
                    //reportProductStockout("商品[" + task.item_id + "][" + area_text + "]无货");
                    return false;
                }

                var store_prompt = $("#store-prompt:visible:contains('有货，仅剩')");
                if(store_prompt.length > 0){
                    var remain = store_prompt[0].innerText.match(/\d+/);
                    if(remain < task.amount){
                        clue("主商品[" + task.item_id + "]仅剩"+remain+"件，库存不足", 'error');
                        updateHostStatus(1406004);
                        changeCategory();
                        //reportProductStockout("商品[" + task.item_id + "]仅剩"+remain+"件，库存不足");
                        return false;
                    }
                }

                //该地区不支持配送
                var region_unsupport_delivery = $("#store-prompt:visible:contains('不支持配送')");
                if(region_unsupport_delivery.length > 0){
                    var region = $("#store-selector .text").text();
                    var message = region+'该地区暂不支持配送'+task.task_order_oid;
                    updateHostStatus(1406005);
                    changeCategory();
                    //addTaskOrderRemark(message, function(){
                    //    sendMessageToBackground('task_order_set_exception');
                    //});
                    return false;
                }

                updateHostStatus(1407000);
                clue('等待' + jd_main_product_wait_time + '秒后，放入购物车');
                //到最底部停留后，到最顶部
                //scroll_bottom_wait_right(function () {
                //    goto_top();
                //});

                //显示时间
                // show_wait_time(function(){
                autoResetWatchDogTimer(jd_main_product_wait_time * 1000);
                setTimeout(function () {
                    // clue("关闭上一个搜索页面");
                    // sendMessageToTabByUrl(document.referrer, "search_stop", null, function(){
                        right_product_buy(task);
                    // });
                }, jd_main_product_wait_time * 1000)
                //正常购买商品


                // });

            } else {

                var itemId = location.pathname.match(/\d+/g)[0];
                clue('货比三家 '+itemId);
                //clue(local.rand_search_items.toString());
                scrollBottom(function(){
                    //当前的商品编号
                    useLocal(function(local){
                        if (local.rand_search_items != undefined) {
                            //clue(local.rand_search_items.toString());
                            //是货比三家商品
                            if (in_array(itemId, local.rand_search_items)) {
                                var lastItemId = local.rand_search_items.pop();
                                if(itemId==lastItemId && localSelf.currentTask.searchGroupItems.length > 0){
                                    clue('搜索相似关键词 ');
                                    var ref = document.referrer;
                                    var key = $("#search-2014 .form #key");

                                    if(key.length > 0){
                                        clue("当前页面搜索");
                                        clue("关闭上一个搜索页面");
                                        sendMessageToTabByUrl(ref, "search_stop", null, function(){
                                            readySearch(local);
                                        });


                                    }else{
                                        clue("当前页面不能完成搜索,上一个搜索结果页,再打开一个");
                                        sendMessageToTabByUrl(ref,"next_product",null, function(){
                                            setTimeout(function(){
                                                close_this_tab();
                                            }, random(20, 40)*1000);
                                        });
                                    }

                                }else{
                                    close_this_tab()
                                }
                            }else{
                                close_this_tab();//部分关不掉问题
                            }
                        }else{
                            close_this_tab();
                        }
                    });
                });
            }
        }


        //正常的商品
        function right_product_buy(data) {

            //设置购买数量
            $('#buy-num').val(data.amount)

            var event = new KeyboardEvent('keyup');
            event.key = data.amount;
            document.querySelector('#buy-num').dispatchEvent(event);

            updateHostStatus(1408000);
            addCart();
            //关注店铺
            //favoriteShop(data.favorite_shop, function(){
                //关注商品
            //followProduct(data.follow_product)
            //})

        }


        //到最底端，然后继续
        function scrollBottom(callback) {
            goto_bottom();
            clue('浏览'+jd_aux_product_wait_time + '秒后，继续');
            setTimeout(function () {
                //关闭页面
                callback && callback();
            }, jd_aux_product_wait_time * 1000);
        }


        //正常商品，到最底端，等待，然后到最顶端
        function scroll_bottom_wait_right(func) {


            bottom_time_interval = setInterval(function () {

                //到最底部
                goto_bottom();

                bottom_count_timer++;

                //达到等待时间
                if (bottom_count_timer >= jd_rand_main_bottom_wait_seconds) {
                    func();
                    clearInterval(bottom_time_interval);
                }

            }, 1000);

        }


        //到顶端
        function goto_top() {
            window.scrollTo(0, 0);
        }

        //到底端
        function goto_bottom() {
            window.scrollTo(0, document.body.scrollHeight);
        }

        /**
         * 关注店铺
         * @param favorite_shop
         * @param callback
         */
        function favoriteShop(favorite_shop, callback){

            if(favorite_shop == '1'){
                clue('关注店铺');
                var shop = $("#extInfo .btn-shop-follower");
                if(shop.length > 0){
                    if(shop.not('.followed').length > 0){
                        shop[0].click();
                    }else{
                        clue('已经关注过该店铺');
                        lazy(callback, 3);
                    }
                }else{
                    clue('没有找到关注店铺按钮', 'error');
                    callback && callback();
                }
            }else{
                callback && callback();
            }

        }

        /**
         * 关注商品
         * @param follow_product
         * @param callback
         */
        function followProduct(follow_product){
            if(follow_product == '1'){
                clue('关注商品');
                var follow_button = $("#choose-btn-coll");
                if (follow_button.length > 0) {
                    if(follow_button.not('.followed').length > 0){
                        follow_button[0].click();
                    }else{
                        clue('已经关注过该商品');
                        //lazy(addCart, 3);
                    }

                } else {
                    clue('没有找到关注按钮', 'error');
                    //addCart();
                }
            }else{
                //addCart();
            }


        }

        /**
         * 商品加入购物车
         */
        function addCart(){

            if($('#InitCartUrl:visible').length == 0){
                clue('找不到购物车');
                changeCategory();
                return false;
            }

            if($("#InitCartUrl").text().indexOf('立即冲印') != -1 || $("#InitCartUrl").attr('href').indexOf('cart.jd.com/gate.action') ==-1){
                clue('不符合条件的自营商品，商品链接：' + location.href);
                changeCategory();
                return false;
            }

            //是否随机使用一键购
            //var easyBuy = $("#btn-easybuy-submit");
            //if(easyBuy.length>0){
            //    if(isGoldenRatio()){
            //        clue("使用一键购");
            //        //执行一键购
            //        easyBuy[0].click();
            //        return false;
            //    }
            //}

            var category = $("#root-nav .breadcrumb a[clstag*='mbNav-3']");
            var itemName = $("#name h1").text();
            //兼容另外一种情况
            var category_others = $(".crumb-wrap a[clstag*='mbNav-3']");
            var itemName_others = $(".sku-name").text();
            

            if(category.length > 0 && itemName!=''){
                var InitCartUrl = $('#InitCartUrl');
                if (InitCartUrl.length >= 1) {
                    var itemId = location.pathname.match(/\d+/g)[0];

                    var product = {
                        item_code: itemId,
                        item_url: location.href,
                        name: itemName,
                        sub_name: "",
                        categoryName: category.text(),
                        categoryCode: category[0].href.match(/\d+/g).join(','),
                        price: $("#jd-price").text().match(/\d+(=?.\d{0,2})/g)[0],
                        promotion: "",
                        shop_id: 0,
                        shop_name: $("#extInfo .brand-logo img").attr("title"),
                        business_id: 1,
                        picture: $("#spec-n1 img").attr('src'),
                        categoryLastCode: category[0].href.match(/\d+/g)[2],
                        jd_self: 1
                    };


                } else {
                    clue('没有找到加入购物车按钮', 'error');
                }

            }else if(category_others.length > 0 && itemName_others !=''){

                var InitCartUrl = $('#InitCartUrl');
                if (InitCartUrl.length >= 1) {
                    var itemId = location.pathname.match(/\d+/g)[0];

                    var product = {
                        item_code: itemId,
                        item_url: location.href,
                        name: itemName_others,
                        sub_name: "",
                        categoryName: category_others.text(),
                        categoryCode: category_others[0].href.match(/\d+/g).join(','),
                        price: $(".summary-price .p-price").text().match(/\d+(=?.\d{0,2})/g)[0],
                        promotion: "",
                        shop_id: 0,
                        shop_name: $('.crumb-wrap .name a').text(),
                        business_id: 1,
                        picture: $("#spec-n1 img").attr('src'),
                        categoryLastCode: category_others[0].href.match(/\d+/g)[2],
                        jd_self: 1
                    };

                    // useLocal(function(local){});
                        // var task = local.task;
                        
                    

                } else {
                    //clue('没有找到加入购物车按钮', 'error');
                }

                

            }else{
                location.reload(true);
            }

            useLocal(function(local){
                var self_order_info = {
                    item_id : itemId,
                    product:product
                }

                var tasks = local.tasks;
                var task = tasks[local.taskId];
                task.item_id = itemId;
                task.product = product;

                setLocal({tasks:tasks},function(){
                    setTaskWorks({self_order_info: self_order_info}, function(){
                        followProduct(task.follow_product);
                       
                       getTaskWorks(function(tworks){
                            var add_cart_nums = tworks.add_cart_nums || 0;
                            if(add_cart_nums <3){
                                add_cart_nums++;
                                setTaskWorks({add_cart_nums:add_cart_nums},function(){
                                     InitCartUrl[0].click();
                                })
                            }else{
                                clue('加入购物车次数超过3次，重新随机类目');
                                setTaskWorks({add_cart_nums:0},function(){
                                    changeCategory();
                                })
                                
                                return false;
                            }
                            
                       })
                        
                     
                       
                    });
                })

            })
        }

        })
    }

}})();