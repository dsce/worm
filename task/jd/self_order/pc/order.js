//京东 提交订单
(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('[自营]提交订单页面',120);
        lazy(function () {
            
            var new_consignee = false;//新增收货地址
            var payment_selected = false;//选择在线支付
            var invoice_selected = false;//不开发发票
            var jdbean_use = false;//使用京豆
            var order_submit_checked_number = false;//提交订单检查商品数量
            var order_submit_checked_product = false;//提交订单检查商品
            var pay_password_error = false;//用豆，支付密码错误

            var pay_fee = 0;
            var shipping_fee = 0;
            var bean_fee = 0;

            var order_check_code_img = true;
            var verify_cid = 0;

            // Task(index);
            index();

            //页面监控
            wangwang();
            var observer = new MutationObserver(function (mutations) {
                // console.log(mutations);
                mutations.forEach(function (mutation) {
                    // console.log(mutation.type);
                    // console.log(mutation.target);
                    // console.log(mutation);


                    if (mutation.type === 'childList') {
                        //if (mutation.target.id == "jdBeans" && mutation.addedNodes.length > 0) {
                        if (mutation.target.id == "jdBeans-new" && mutation.addedNodes.length > 0) {
                            //填写使用京豆数量
                            Run(set_jdbeans);
                        } else if (mutation.target.id == 'checkCodeDiv' && mutation.addedNodes.length > 0) {
                            Run(function(local){
                                if($("#orderCheckCodeImg:visible").length == 0){
                                    return false;
                                }
                                 //有验证码
                                 order_check_code_img = false;

                                // var imgsrc = $("#orderCheckCodeImg")[0].src;
                                $("#checkcodeTxt").focus();
                                updateHostStatus(1410007);

                                $('#checkcodeTxt').on('keyup', function () {
                                    if ($('#checkcodeTxt').val().length >= 4) {
                                        clue("4位 自动登录");
                                        lazy(function () {
                                            useLocal(function(local){
                                                if(local.isRunning){
                                                    order_check_code_img = true;
                                                }else{
                                                    setLocal({isRunning: true}, function(){
                                                        order_check_code_img = true;
                                                    });
                                                }
                                            });

                                        }, 2);

                                    }
                                }).focus();

                                //打码结果
                                addListenerMessage(function (request) {
                                    console.log(request);
                                    if (request.act == 'https_tabs_verify_code_result') {
                                        verify_cid = request.cid;
                                        $("#checkcodeTxt").val(request.text);
                                        order_check_code_img = true;
                                    }else if(request.act == 'tab_get_cookies_response'){
                                        //if(request.cookies.length > 0){
                                        //    var imgsrc = location.origin + $("#captcha-img img").attr('src') + "&v="+Math.LN2;
                                        //    var plogin_cookie = "lsid="+request.cookies[0].value;
                                        //    chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc, cookie: plogin_cookie});
                                        //}else{
                                        //    clue("cookie 不存在，不能打码");
                                        //}
                                    }else if(request.act == 'https_tabs_verify_code_result_errno'){
                                        var imgsrc = $("#orderCheckCodeImg")[0].src;
                                        var act = 'https_tabs_verify_code_from_uuyun';
                                        getErrorNo(request,imgsrc,act);
                                    }
                                });
                                //发送打码
                                tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginListener', null, function(){
                

                                     var rid = Math.random().toString() + "_" + Math.random().toString();
                                     var encryptClientInfo = $("#encryptClientInfo").val();
                                     var checkCodeUrl = "//captcha.jd.com/verify/image?acid=" + rid + "&srcid=trackWeb&is=" + encryptClientInfo;
                                     console.log(checkCodeUrl);
                                     $("#checkcodeRid").val(rid);

                                        getCrossDomainAuthCodeBase64(checkCodeUrl, function(base64){
                                            //console.log(base64);
                                            tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginRemoveListener', null, function(){
                                                //关闭监听,准备打码
                                                chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                                            });

                                        });
                                });


                                // var base64 = getBlobImg($("#orderCheckCodeImg")[0]);
                                //console.log(blob);
                                // chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                                // chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc});
                                // chrome.extension.sendMessage({act: 'https_tabs_verify_code_from_uuyun', imgsrc: imgsrc});
                                
                            });



                        } else if (mutation.target.id == 'submit_message' && mutation.addedNodes.length > 0) {
                            //提交订单的错误提示
                            var submit_message_text = $("#submit_message")[0].innerText;
                            if(submit_message_text.indexOf('支付密码不正确') != -1){
                                //支付密码错误//不使用京豆
                                clue('支付密码错误，不使用京豆');
                                pay_password_error = true;
                                var cancel_btn = $("#jdBeans a.a-link:visible:contains('取消使用')");
                                if(cancel_btn.length > 0){
                                    Run(set_jdbeans);
                                }else{
                                    $("#orderBeanItem a:contains('使用京豆')")[0].click();
                                }

                            }else if(submit_message_text.indexOf('省份无法购买商品') != -1){
                                clue(submit_message_text, 'error');
                                clue('该地区无法购买商品', 'error');
                                //clue('任务自动异常');
                                Run(function(local){
                                    updateHostStatus(1406005);
                                    location.href = "https://www.jd.com/"
                                    //addTaskOrderRemark('当前省份无法购买商品(PC)'+local.currentTask.task_order_oid, function(){
                                        //sendMessageToBackground('task_order_set_exception');
                                    //});
                                });

                            }else if(submit_message_text.indexOf("收货人信息不对") != -1 || submit_message_text.indexOf("区镇选择有误") != -1 || submit_message_text.indexOf("地址升级") != -1){
                                //提交订单收货人地址有误，或者地址库待更新，address_update true,进行删除地址，重新添加 ,
                                //你当前的收货人信息不对,2016-3-11
                                Run(function(local){
                                    $(".ui-switchable-panel-selected a:contains('编辑')")[0].click();
                                    //updateHostStatus(1406010);
                                    //if(local.address_update == undefined || local.address_update == true){
                                    //    setLocal({address_update: true}, function(){
                                    //        location.href = 'http://easybuy.jd.com/address/getEasyBuyList.action';
                                    //    });
                                    //}else{
                                    //    updateHostStatus(1406011);
                                    //    clue("新添加的地址过不去，地址库需要更新", 'error');
                                    //}
                                });

                            }else if(submit_message_text.indexOf("无货") != -1){//商品无货,重新
                                location.href = "https://www.jd.com/";

                            }else if(submit_message_text.indexOf('明日可再下单') != -1){
                                clue(submit_message_text, 'error');
                                clue("任务失败,下一个吧");
                                //任务失败了下一个吧
                                sendMessageToBackground('self_order_task_fail', submit_message_text);

                            }else{
                                clue(submit_message_text, 'error');
                            }
                        }
                    }

                    if (mutation.type === 'attributes') {
                        if (mutation.target.id == 'paypasswordPanel' && mutation.attributeName === 'style') {
                            //填写支付密码
                            if($("#txt_paypassword:visible").length > 0){
                                Run(set_paypassword);
                            }
                        }
                    }

                });

            });

            var observer_config = {
                attributes: true,
                childList: true,
                characterData: true,
                attributeOldValue: true,
                characterDataOldValue: true,
                subtree: true
            }

            observer.observe(document.querySelector('#mainframe'), observer_config);


            //运行
            function index() {

                //限制app付款下单
                if(local.usage == '1'){
                    notify('APP付款，不能下单')
                    setLocal({isRunning: false},function(){
                        window.location.reload(true);
                    })
                    return false;
                }

                //只有pc单可以下单
                if(local.currentTask.is_mobile == 1){
                    clue("手机单，不能进行pc下", 'error');
                    return false;
                }

                updateHostStep(_host_step.submit_order);//step6 提交订单
                updateHostStatus(1410000);

                // if (taskOrderExist(local)) {
                //     return false;
                // }

                //进入提交订单，先检查一遍商品数量和金额，
                check_self_address(local,function(){
                    order_check();
                    setTimeout(online_payment, 5000);//选择在线支付
                })
            
                
                //order_submit(local);

            }

            //选择在线支付
            function online_payment() {
                if (payment_selected) {
                    return false;
                }
                $("#payment-list .payment-item[payid='4']").parent()[0].click();
                edit_invoice();//修改发票信息

                //使用京豆
                r(use_jdbean_btn);

            }

            //修改发票信息
            function edit_invoice() {
                if (invoice_selected) {
                    return false;
                }
                $('#part-inv .invoice-edit:contains("修改")')[0].click();
            }

            //使用京豆
            function use_jdbean_btn() {
                var task = local.currentTask;
                task.business_use_jd_beans = 0;
                if (task.business_use_jd_beans == 1) {
                    //可以使用京豆
                    setTimeout(function () {
                        $("#orderBeanItem a:contains('使用京豆')")[0].click();
                    }, 2000);

                } else {
                    order_submit(local);
                }
            }

            //设置京豆
            function set_jdbeans(local) {

                var task = local.currentTask;
                task.business_use_jd_beans = 0;
                if (task.business_use_jd_beans == 1 && jdbean_use == false && pay_password_error == false) {

                    if ($('#usedBeans').attr('fmax') > 0) {

                        //最大京豆
                        var maxJd = parseInt($('#usedBeans').attr('fmax'));

                        //转换失败情况
                        if (isNaN(maxJd)) {
                            maxJd = 0;
                        }

                        if (maxJd > 60) {
                            maxJd = Math.ceil(maxJd / 2);
                        }

                        //先判断京豆输入框是否存在, 不存在说明已经输入京豆 ，存在则开始输入
                        if ($('#usedBeans:visible').length > 0) {
                            maxJd = maxJd > 60 ? 60 : maxJd;
                            $('#usedBeans').val(maxJd);
                            setTimeout(function () {
                                $('#orderBeanItem input[type="button"]').trigger("click");
                                jdbean_use = true;

                            }, 3000);
                        }

                        //检查支付密码框是否存在， 存在直接输入支付密码
                        if ($('#txt_paypassword:visible').length > 0) {
                            set_paypassword(local);
                        }

                    } else {
                        order_submit(local);
                    }

                }else{
                    if(pay_password_error == true){
                        //不使用京豆 处理，
                        var cancel_btn = $("#jdBeans a.a-link:visible:contains('取消使用')");
                        if(cancel_btn.length > 0){
                            cancel_btn[0].click();
                            order_submit(local);
                        }
                    }
                }

            }

            //设置支付密码
            function set_paypassword(local) {
                var task = local.currentTask;
                task.business_use_jd_beans = 0;
                if (task.business_use_jd_beans == 1) {
                    addListenerMessage(function (request) {
                        console.log(request);
                        if (request.act == 'business_account_ready') {
                            useLocal(function(local){
                                $('#txt_paypassword').val(local.currentTask.pay_password);
                                order_submit(local);
                            });
                        }
                    });
                    sendMessageToBackground('get_business_account_password');
                    //$('#txt_paypassword').val(local.currentTask.pay_password);
                    //order_submit(local);
                }
            }

            //核对订单
            function order_check() {

                //检查主商品是否存在
                if ($(".goods-list").html().indexOf(local.currentTask.item_id) > 0) {
                    order_submit_checked_product = true;
                } else {
                    clue('主商品[' + local.currentTask.item_id + ']不存在', 'error');
                    order_submit_checked_product = false;
                }

                var number = $('.order-summary .list span em').text().match(/\d+/);
                if (number>0) {
                //    clue('商品数量不一致', 'error');
                //    cartReture();
                //    order_submit_checked_number = false;
                //
                //} else {
                    clue('商品数量正确', 'success');
                    order_submit_checked_number = true;
                }

                //金额
                pay_fee = $("#sumPayPriceId").text();//应付款金额
                shipping_fee = $("#freightPriceId").text();//运费
                bean_fee = $("#usedJdBeanId").text().replace('-', '');//京豆优惠金额

                clue("<p>优惠：" + bean_fee + "</p><p>运费：" + shipping_fee + "</p><p>应付总额：" + pay_fee + "</p>");


            }

            //提交订单
            function order_submit() {

                //多包裹，
                if($("#shopping-lists .shopping-list").length > 1){
                    clue("多包裹，核对主商品，注意多余商品",'error');
                    updateHostStatus(1410006);//多包裹，核对主商品，注意多余商品
                    return false;
                }

                if($("#shopping-lists .shopping-list .goods-items .goods-item:visible").length > 1){
                    clue("多商品存在，核对主商品，注意多余商品",'error');
                    updateHostStatus(1410102);//多包裹，核对主商品，注意多余商品
                    return false;
                }

                //检查主商品是否存在
                if ($("#shopping-lists").html().indexOf(local.currentTask.item_id) == -1) {
                    updateHostStatus(1410101);//主商品不存在
                    return false;
                }

                //var number = $('.order-summary .list span em').text().match(/\d+/);
                //if (number != local.currentTask.amount) {
                //    updateHostStatus(1410103);//商品数量错误
                //    cartReture();
                //    return false;
                //}

                // if (taskOrderExist(local)) {
                //     return false;
                // }

                order_check();
                if (order_submit_checked_number && order_submit_checked_product) {


                    //核对商品数量
                    var goodsItems = $("#shopping-lists .goods-items .goods-item:visible");
                    if(goodsItems.length > 1 ){
                        console.log("数量不对,回购物车清空");
                    }else{
                        if(goodsItems.length == 1){
                            //1种商品才对
                            var amount = goodsItems.find(".p-num")[0].innerText.match(/\d+/g)[0];
                            if(amount>0) {

                                var task_order_self_support = {};
                                task_order_self_support.item_id = goodsItems.attr("goods-id");

                                task_order_self_support.product_name = goodsItems.find(".p-name a")[0].innerText;
                                task_order_self_support.product_price = goodsItems.find(".jd-price")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
                                task_order_self_support.amount = amount;
                                task_order_self_support.business_oid = '';
                                //task_order_self_support.business_payment_fee = $("#payPriceId").text().match(/\d+(=?.\d{0,2})/g)[0];
                                task_order_self_support.business_payment_fee = $("#sumPayPriceId").text().match(/\d+(=?.\d{0,2})/g)[0];
                                task_order_self_support.business_use_jd_beans = 0;

                                var account_id = local.currentTask.account_id;

                                useLocal(function (local) {
                                    task_order_self_support.business_account_id = account_id;
                                    var tasks = local.tasks;
                                    var task = tasks[local.taskId];
                                    task.task_order_self_support = task_order_self_support;
                                    setLocal({tasks: tasks}, function () {

                                        lazy(function(){
                                            order_submit_click();
                                        }, 10)
                                    })
                                })
                            }else{
                                //数量错误,库存不足重新开始
                                location.href = "https://www.jd.com/";
                            }
                        }else{
                            console.log('购物车没有商品');
                        }
                    }

                    //var items = {
                    //    temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
                    //    temp_shipping_fee: shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
                    //}
                    //setLocal(items ,function(){
                    //    console.log('提交订单');
                    //    lazy(function () {
                    //        order_submit_click();
                    //    }, 10);
                    //});

                }
            }

            //返回购物车
            function  cartReture(){
                lazy(function(){
                    $("#cartRetureUrl")[0].click();
                },3);
            }

            //提交订单
            function order_submit_click(){

                if(!order_check_code_img){
                    console.log('等待验证码');
                    setTimeout(order_submit_click, 3000);
                    return false;
                }
                console.log('order submit');
                $("#order-submit")[0].click();
            }

        })
    }

}})();