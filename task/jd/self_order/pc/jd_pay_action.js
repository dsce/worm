//京东支付页面
(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('[自营]提交订单成功页面');
        lazy(function () {
            

        var btnSubmitNum = 0;
        var btnSubmit = $('#paySubmit');

        // Task(index);
        index();

        function index(){
            updateHostStatus(1414000);

            $('#paySubmit').hide();//隐藏 支付按钮
            var oid = get_page_order_id();
            useLocal(function(local){
                var tasks = local.tasks;
                var task = tasks[local.taskId];
                task.task_order_self_support.business_oid = oid;
                task.self_support = 0
                setLocal({tasks: tasks}, function(){
                    // sendMessageToBackground('self_support','', function(){
                    //     console.log("任务完成");
                    // });
                    clue('下单完成，去订单列表检测下');
                    setTimeout(function(){
                        location.href = '//order.jd.com/center/list.action';
                    },3*1000)
                });
            });


            return false;


            //识别来源页面，提交订单页面，跳转订单列表页面
            var referrer = document.referrer;
            if(referrer.indexOf('trade.jd.com/shopping/order') != '-1'){
                document.location.href = 'http://order.jd.com/center/list.action?s=1';
                return false;
            }

            if(local.order_id === undefined){
                document.location.href = 'http://order.jd.com/center/list.action?s=1';
                return false;
            }

            //保存订单号  先进订单详情保存订单号
            //var order_id = {order_id:$("#orderId").val()};
            //setLocal(order_id,function(){
            //    updateHostStep(_host_step.payment);//step7 去付款;
            //});

            var task = local.task;
            var pay = local.pay;
            if(pay !=''){

                if('bank' in pay&&pay.bank=='null'){
                    clue('手动选择付款');
                    $('#paySubmit').show();//显示 支付按钮
                }else{
                    var bank = local.pay.bank;


                    if(bank == 'icbc'){
                        chrome.runtime.sendMessage({act: 'icbc_change_useragent_start'}, function(response){ });//工商银行替换UserAgent
                    }else if(bank == 'ccb'){
                        sendMessageToBackground('intercept_ccb_form');//建行截获form数据
                    }else if(bank == 'boc' && local.pay.remote_payment_boc !== true){
                        sendMessageToBackground('intercept_boc_form');//中行截获form数据
                    }


                    var li_bank = $('#ebankPaymentListDiv li[clstag="payment|keycount|bank|c-'+bank+'"]');
                    clue(li_bank.text());
                    li_bank[0].click();

                    var select_id = $("#seletedAgencyCode").next().attr('id');
                    if(select_id === ("bank-"+bank)){
                        btnSubmitNum++;
                        if(btnSubmitNum == 1){
                            btnSubmit.click();
                        }

                        setTimeout(function(){
                            //保存订单号
                            var order_id = {order_id:$("#orderId").val()};
                            setLocal(order_id,function(){
                                pay_finish();
                            });

                        },2000);
                    }
                }
            }else{
                console.log("没有设置银行用户信息！");
                updateHostStatus(1414001);
                notifyMessage("没有设置银行用户信息！");
            }
        }

        //点击支付完成
        function pay_finish(){
        	
        	var payFinishInterval = setInterval(function(){
        		if($("#wangyinPaySuccess:visible").length > 0){			
        			$("#wangyinPaySuccess .wangyin-payed-success .wps-button a")[0].click();
        			clearInterval(payFinishInterval);
        		}
        	},500);
        }

        function get_page_order_id(){
            var orderId = $("#orderId").val();
            var jdOrderId = $("#jd_order_id").val();
            if($(".main .order .o-tips-item:contains('订单号')").length >0){
                //HK全球购
                var pcashierOrderId = $(".main .order .o-tips-item:contains('订单号')").text().match(/\d+/)[0];
            }else if($(".main .order .o-title:contains('订单号')").length >0){
                var pcashierOrderId = $(".main .order .o-title:contains('订单号')").text().match(/\d+/)[0];
            }

            //兼容新版0329
            orderId=orderId?orderId:jdOrderId;

            //兼容新版0410
            orderId=orderId ? orderId : pcashierOrderId;
            return orderId;
        }

        })
    }

}})();
