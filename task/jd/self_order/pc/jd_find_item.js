//京东搜索页面
(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        
        lazy(function () {
            var nCount = 1, item_id;
var find_interval = null;

var page_interval = null;

//是否找到
var is_find = false;

//task
var task = null;
var jd_rand_compare_count = 2;
var rand_open_product_number = 2;
var rand_open_product_number = random(1,3);//随机打开1-3个
var jdFindItemCloseTabTime = random(20,30);


windowsTopNotSelf();
Task(readyIndex);


function readyIndex(local){
    var searchGroup = local.task.searchGroup;
    var searchGroupItems = local.task.searchGroupItems;

    //先,移除广告位
    //删除公告位置，杜绝使用广告地址
    //$('.m-aside').html('');//左侧广告
    var goods_wrap = $("div[id*='promGoodsWrap']");
    if(goods_wrap.length > 0){
        clue('广告位 '+goods_wrap.length+'处，现在移除');
        goods_wrap.html('这里是广告');
    }else{
        clue("没有找到广告位", 'error');
    }

    //关键词批量搜索
    if(searchGroup === true){
        if(searchGroupItems.length > 0){
            //随机打开1-3个商品
            clue('剩余'+searchGroupItems.length+'个相似关键词');
            clue('随机,打开'+rand_open_product_number+"个");
            //index(local);
            open_rand_product_J_goodsList(local.task.item_id);

        }else{
            //结束了,直接打开主商品
            clue('结束了,可以打开主商品');
            index(local);
        }
    }else{
        //原有流程
        clue('原有流程');
        index(local);
    }
}

function index(local) {

    if (taskOrderExist(local)) {
        return false;
    }

    task = local.task;
    updateHostStatus(1405000);

    if(task.is_mobile == 1){
        clue("手机单，不能进行pc搜索", 'error');
        return false;
    }

    var urlget = urlGet();
    var search_key = decodeURI(urlget.keyword);
    var keyword = task.keyword.replace(/^\s+|\s+$/g,"");

    if ($("#J_goodsList").length > 0) {
        //A
        if(search_key == keyword){
            //open_rand_product_J_goodsList(task.item_id);
            open_product_J_goodsList(task.item_id);
        }else{
            location.href = "http://search.jd.com/Search?keyword="+keyword+"&enc=utf-8&wq="+keyword+"";
        }

    } else {
        //没有搜索结果直接进入
        clue("直接进入商品详情", 'error');
        sendMessageToBackground('send_notes_to_tracker', '直接进入商品详情');
        location.href = task.product_url;
    }

}

//直接跳到商品页
function direct_goto_product(item_id) {

    updateHostStep(_host_step.view_product);//step4 浏览主商品

    if ($("li[sku='" + item_id + "']").length > 0) {
        clicking($("li[sku='" + item_id + "'] .p-img a"));
    }else {

        var index = (Math.random() * 10).toFixed();

        var s = '<li sku="$item_id" done="1" class="">\
<div class="lh-wrap">\
  <div class="p-img">\
    <a id="a_quick_link" target="_blank" href="http://item.jd.com/$item_id.html" onclick="searchlog(1,$item_id,3,2)" title="">\
      <img width="220" height="220" data-img="1" src="http://img5.2345.com/duoteimg/qqbiaoqing/140822032010/12.jpg" class="err-product">\
    </a>\
    <div shop_id="31837" tpl="1"></div>\
  </div>\
  <div class="p-scroll">\
    <a href="javascript:;" class="p-scroll-btn p-scroll-prev">&lt;</a>\
    <div class="p-scroll-wrap">\
    </div>\
    <a href="javascript:;" class="p-scroll-btn p-scroll-next">&gt;</a>\
  </div>\
  <div class="p-name">\
    <a target="_blank" href="http://item.jd.com/$item_id.html" onclick="searchlog(1,$item_id,3,1)" title="">' + item_id + ' </a>\
  </div>\
  <div class="p-price">\
    <em></em><strong class="J_$item_id" done="1"></strong>    <span id="p$item_id"></span>\
  </div>\
  <div class="extra">\
    <span class="star"><span class="star-white"><span class="star-yellow h5">&nbsp;</span></span></span>\
    <a id="comment-$item_id" target="_blank" href="http://item.jd.com/$item_id.html#comments-list" onclick="searchlog(1,$item_id,3,3)"></a>\
    <span class="reputation" style="display:none;"></span>\
  </div>\
  <div class="stocklist">\
    <span class="st33"></span>\
  </div>\
    <div class="hide"><a stock="$item_id" presale="0"></a></div>\
  <div class="product-follow" style="display: none;"><a href="javascript:;" class="btn-coll" id="coll$item_id" onclick="searchlog(1,$item_id,3,5)"></a></div>\
</div>\
</li>';

        s = s.replace(/\$item_id/g, item_id);

        $('#plist').prepend(s);

        $('#a_quick_link')[0].click();

    }
}


//返回当前列表的商品数组
function search_items() {
    var $lis = $('li[sku]');
    if($lis.length == 0){
        $lis = $('li[data-sku]');
    }

    var items_count = $lis.length;

    var items = new Array();

    if (items_count > 0) {

        $lis.each(function (i) {

            var sku = $(this).attr('sku');
            //对比商品，排除是主商品
            if (sku != task.item_id) {
                items.push($(this).attr('sku'));
            }

        });
    }
    return items;
}


//获取随机的商品sku
function random_search_items() {

    var rdm_count = jd_rand_compare_count;

    //所有的商品
    var items = search_items();

    //随机挑出的商品
    var rnd_items = new Array();

    if (items.length > 0 && rdm_count > 0) {


        //循环次数
        var do_count = 0;
        var break_count = 10;

        do {

            //增加执行次数
            do_count++;

            //超过次数跳出
            if (do_count > break_count) {
                break;
            }


            //随机数
            var rnd_num = parseInt(Math.random() * items.length);

            var end = rnd_num;

            //避免start为负值情况
            if (end == 0) {
                end = 1;
            }

            var start = end - 1;

            var sku = items.slice(start, end);

            if (!in_array(find, rnd_items)) {
                //判断sku是否重复
                rnd_items.push(sku);
            }

        } while (rnd_items.length < rdm_count);


        return rnd_items;

    } else {

        //商品数量为0，随机对比商家数量为0
        return rnd_items;

    }
}

//货比三家，打开随机的商品
function open_rand_product() {

    var rnd_items = random_search_items();

    for (var i = 0; i < rnd_items.length; i++) {
        var item_id = rnd_items[i];
        lazy(function () {
            $('li[sku="' + item_id + '"] a:eq(0)')[0].click();
        });


    }

    //记录随机打开的商品
    var items = {
        'rand_search_items': rnd_items
    };

    chrome.storage.local.set(items, function (result) {
        console.log(rnd_items);
    });
}

//J_goodsList 打开随机商品
function open_rand_product_J_goodsList(item_id, callback) {
    var inter = $("#J_goodsList .gl-item:contains('京东国际')");//搜索结果删除京东国际,全球购
    var product_list = $("#J_goodsList .gl-item").not("#jd_find_item").not('[data-type="activity"]').not(inter);
    if (product_list.length == 0) {
        //搜索结果小于0, 下个关键词搜索
        startSearch();

    }else if(product_list.length < rand_open_product_number){
        //结果小于要打开的数量,要打开的数量就等于结果数,全打开
        var around_items = new Array();
        product_list.each(function (i) {
            var pro_id = $(this).attr("data-sku");
            var is_pro = $(this).find(".p-name a[href*='item.jd.com']");//确认直接是商品链接,排除其他
            if (pro_id != item_id && is_pro.length > 0) {//排除主商品
                around_items.push(pro_id);
            }
        });

        useLocal(function(local){
            clue(around_items.toString());
            setLocal({rand_search_items: around_items}, function(){
                around_items.forEach(function(pro_id){
                    setTimeout(function(){
                        console.log(pro_id);
                        var randProduct = $("li[data-sku='" + pro_id + "'] .p-img a");
                        randProduct[0].click();
                    }, random(1,3)*1000);

                });

            });

        });

    }else{

        //先打开主商品， 后进行货比
        //open_product_J_goodsList(item_id);

        //排除主商品外剩下的商品 id 数组
        var product_list_ids = new Array();

        product_list.each(function (i) {
            var pro_id = $(this).attr("data-sku");
            var is_pro = $(this).find(".p-name a[href*='item.jd.com']");//确认直接是商品链接,排除其他
            if (pro_id != item_id && is_pro.length > 0) {//排除主商品
                product_list_ids.push(pro_id);
            }
        });

        //随机要打开 数组
        var randNumArr = new Array();

        var pro_len = product_list_ids.length;
        if (pro_len > 0 && rand_open_product_number > 0) {
            for (var j = 0; j < rand_open_product_number; j++) {
                var randNum = parseInt(Math.random() * pro_len);
                if (!in_array(randNum, randNumArr)) {
                    randNumArr.push(randNum);
                }
            }
            //随机商品数组
            var around_items = new Array();
            for (var r = 0; r < randNumArr.length; r++) {
                var data_sku = product_list_ids[randNumArr[r]];
                //var randProduct = $("li[data-sku='" + data_sku + "'] .p-img a");
                //randProduct[0].click();

                around_items.push(data_sku);
            }

            useLocal(function(local){
                clue(around_items.toString());
                setLocal({rand_search_items: around_items}, function(){
                    around_items.forEach(function(pro_id){
                        setTimeout(function(){
                            console.log(pro_id);
                            var randProduct = $("li[data-sku='" + pro_id + "'] .p-img a");
                            randProduct[0].click();
                        }, random(1,3)*1000);

                    });

                });

            });
        }
    }
}
function open_product_J_goodsList(item_id, callback) {

    updateHostStep(_host_step.view_product);//step4 浏览主商品
    
    if ($("li[data-sku='" + item_id + "']").length > 0) {
        clicking($("li[data-sku='" + item_id + "'] .p-img a"));
    }else {

        var html = '<li data-sku="$item_id" class="gl-item" id="jd_find_item" style="float: inherit;">\
      <div class="gl-i-wrap">\
        <div class="p-img">\
          <a target="_blank" title="" href="http://item.jd.com/$item_id.html" onclick="searchlog(1,$item_id,2,2)">\
            <img width="220" height="220" data-img="1" data-lazy-img="done" class="err-product" src="http://img5.2345.com/duoteimg/qqbiaoqing/140822032010/12.jpg">\
          </a>\
          <div></div>\
        </div>\
        <div class="p-price"></div>\
        <div class="p-name"></div>\
        <div class="p-commit"></div>\
        <div class="p-operate"></div>\
      </div>\
    </li>\
  ';

        html = html.replace(/\$item_id/g, item_id);
        $("#J_goodsList ul").prepend(html);
        setTimeout(function () {
            if ($("#jd_find_item .p-img a").length > 0) {
                $("#jd_find_item .p-img a")[0].click();

                //再打开随机商品
                open_rand_product_J_goodsList(item_id);
            }
        }, 2000);
    }


}

addListenerMessage(function(request){
    console.log(request);
    var act = request.act;
    if(act == 'next_product'){//再打开一个商品
        clue("再来一个");
        rand_open_product_number = 1;
        useLocal(function(local){
            open_rand_product_J_goodsList(local.task.item_id);
        });

    }else if(act == 'search_stop'){//搜索结束,等待关闭
        clue("执行关闭");
        setTimeout(function(){
            close_this_tab();
        }, jdFindItemCloseTabTime*1000);
    }



});
        })
    }

}})();
