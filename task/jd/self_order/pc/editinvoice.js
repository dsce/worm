//京东 提交订单 修改发票信息

(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        
        lazy(function () {
            
        index();

        //运行
        function index(){

          //不开发票
          if($("#electro-invoice-content--1").length > 0){
            $("#electro-invoice-content--1")[0].click();
            clue('不开发票');

          }else{
            clue('默认');

          }
          
          lazy(save_invoice_btn,2);
        }


        //保存发票信息
        function save_invoice_btn(){
            var btn = $(".invoice-thickbox:visible a:contains('保存发票信息'):visible");
            var btn_new = $(".invoice_sendwithgift:visible a:contains('保存')");
            var btn_new_self = $(".form:visible .op-btns:visible a:contains('保存')");
            var btn_group = $(".invoice_pre:visible a:contains('保存')");
            if(btn.length >0){
              btn[0].click();
            }else if(btn_new.length >0){
              btn_new[0].click();
            }else if(btn_new_self.length >0){
              btn_new_self[0].click();
            }else if(btn_group.length >0){
              btn_group[0].click();
            }

            clue('选择的发票内容：' + $(".invoice_content .invoice-item-selected").eq(0).text());
            
            btn.hide();
            btn_new.hide();
            btn_new_self.hide();
        }



        })
    }

}})();