//京东 订单列表 jd/list.js
(function () {

    tabCommon.start('selfOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('[自营]订单列表',120);
        lazy(function () {
        
        var orderToPay = 0;
        var runIndexed = false;
        var amount = 0;

        checkSelfOrders();

        setTimeout(function(){
            if(!runIndexed){
                // Run(index);
            }
        }, 20000);

        var observer_config = {
            attributes: true,
            childList: true,
            characterData: true,
            attributeOldValue: true,
            characterDataOldValue: true,
            subtree: true
        }
        addMutationObserver($('#main')[0], function(mutation){

            if(mutation.type == 'childList'){
                console.log(mutation.type);
                console.log(mutation.target);
                console.log(mutation);

            }
        }, observer_config);

        function index(local) {

            //checkSelfOrders(local);
          
            if(runIndexed){
                return false;
            }
            runIndexed = true;


            //不应该进入订单列表
            location.href = 'https://www.jd.com/';
            return false;

            //如果是app付款，直接进入订单
            if(local.usage == '1'){
                clue("APP付款订单");
                orderUrl(local.task.business_oid);
                return false;
            }

            var goods_item = $('#main .p-'+ local.task.item_id +'').first();
            if(goods_item.length > 0){
                var goods_number = goods_item.next(".goods-number").text().match(/\d+/)[0];

                if(goods_number == local.task.amount){
                    var order_body = goods_item.parents("tbody");
                    var order_id = order_body.find('a[name="orderIdLinks"]')[0].innerText
                    orderUrl(order_id);
                }else{
                    clue("商品数量错误", 'error');
                }
            }else{
                clue('未找到对应订单，3s后刷新');
                lazy(function(){
                    location.reload(true)
                }, 3);
            }
            

            
        }

        function toPayBegin(order_id){

            //var order_id = '10416062022';
            //var order_id = '10415707583';
            var number = 0;
            $("#track"+order_id+" .goods-number").each(function(){
                number += parseInt(this.innerText.match(/\d+/));
            });

            if(amount == 0 || amount != number){
                clue('进入订单进一步核对');
                orderUrl(order_id);
                return false;
            }else{
                clue("商品数量一致", 'success');
            }

            orderUrl(order_id);
            lazy(close_this_tab);

            //var pay_button = $("#pay-button-" + order_id + " a");
            //pay_button = pay_button.length > 0 ? pay_button : $("#operate"+order_id+" .btn-pay");
            //
            //if (pay_button.length > 0) {
            //    updateHostStatus(1413000);
            //    lazy(function(){
            //        //pay_button[0].click();
            //        orderUrl(order_id);
            //        lazy(close_this_tab);
            //    });
            //} else {
            //    useLocal(function(local){
            //        var statu_payment = $("#track"+order_id).find(":contains('等待付款')");
            //        if(local.host_step == _host_step.save_order_info || statu_payment.length == 0){
            //            clue("查看订单");
            //            orderUrl(order_id);
            //        }else{
            //            clue('没有找到订单付款按钮');
            //        }
            //    });
            //
            //}
        }

        function orderUrl(order_id){

            useLocal(function(local){

                //全球购手机单替换进入链接
                var order_url = $("#idUrl"+order_id);
                if(local.task.worldwide == '1' && local.task.is_mobile == '1'){

                    var hk_url = order_url.attr('href');
                    if(hk_url.indexOf('order.jd360.hk') != -1){
                        var com_url = hk_url.replace('order.jd360.hk', 'gjz.order.jd.com');
                        order_url[0].href = com_url;
                    }
                }


                if(order_url.length > 0){
                    //是否已经入过订单详情
                    if(local.orderinfo !== undefined){
                        //已进入过订单详情，可以在订单列表判断最终状态
                        var entry_order_info = local.entry_order_info === undefined ? 1 : local.entry_order_info;
                        if(entry_order_info > 3){
                            clue('已经多次尝试进入订单详情');
                            clue('订单列表核对订单付款状态');
                            //核对订单付款状态
                            var tb_order = $('#tb-'+ order_id);
                            var order_status = tb_order.find('.order-status').text();
                            if(order_status.indexOf('正在出库') != -1 || order_status.indexOf('付款成功') != -1){
                                clue(order_id + order_status, 'success');

                                clue('正在保存订单数据....');
                                //订单完成，准备保存订单数据
                                updateHostStatus(1603000);//开始保存数据
                                sendMessageToBackground('save_host_task_order_detail');
                            }else{
                                clue(order_id + order_status);

                                clue(_t.order_detail_refresh_hz + "秒后，刷新");
                                setTimeout(function () {
                                    location.reload(true);
                                }, _t.order_detail_refresh_hz * 1000);
                            }

                        }else{
                            entry_order_info ++;
                            clue('再次尝试进入订单详情');
                            setLocal({entry_order_info: entry_order_info}, function(){
                                $("#idUrl"+order_id)[0].click();
                                lazy(close_this_tab);
                            });

                        }


                    }else{
                        $("#idUrl"+order_id)[0].click();
                        lazy(close_this_tab);
                    }

                }else{
                    clue('没有找到订单','error');
                }
            });


        }

        //检测是否已经有订单数据
        function checkSelfOrders(){

            // var task = local.task;
            // task.checkOrderList = true;
            // setLocal({task:task},function(){});

            var business_oid = local.currentTask.task_order_self_support.business_oid || '';

            // var order_dom = $('tbody').first();
            var order_dom = $("tbody[id='tb-" + business_oid +"']");
            console.log(order_dom.length);

            if(order_dom.length > 0){
                var order_time = order_dom.find('.dealtime').attr('title');
                var shop_name = order_dom.find('.order-shop').find('.shop-txt')[0].innerText;

                console.log(order_time);
                console.log(getNowFormatDate());
                console.log(order_time.indexOf(getNowFormatDate()));
                console.log(shop_name);
                console.log($('tbody').first().html());
                console.log(order_dom.find(".p-extra span").attr('data-sku'));

                if(local.currentTask.task_order_self_support && local.currentTask.task_order_self_support.business_oid){

                    if(shop_name != '京东'){
                        clue('非自营店铺,重新下单');
                        useLocal(function(local){
                            var tasks = local.tasks;
                            tasks[local.taskId].task_order_self_support.business_oid = null;
                            setLocal({tasks:tasks},function(){
                                tabPc.jdIndex();
                            })
                        })
                    }else{


                        var order_infos = local.currentTask.task_order_self_support || {};
                    
                        //console.log(product);
                        //自营单任务数据
                        var task_order_self_support = {
                                item_id : order_dom.find(".p-extra span").attr('data-sku'),
                                product_name : order_infos.product_name || order_dom.find(".p-name .a-link")[0].innerText, 
                                product_price : order_dom.find('.amount').find('span')[0].innerText.match(/\d+(=?.\d{0,2})/g)[0],
                                amount : order_dom.find('.goods-number')[0].innerText.match(/\d+/)[0],
                                business_oid : order_dom.find(".number a").attr('id').match(/\d+/)[0],
                                // business_payment_fee:order_dom.find('.amount strong')[1].innerText.match(/\d+(=?.\d{0,2})/g)[0],
                                business_account_id : local.currentTask.account_id,
                                business_use_jd_beans : 0
                        };
                        console.log(task_order_self_support);

                        useLocal(function(local){
                            var tasks = local.tasks;
                            var task = local.tasks[local.taskId];

                            //task.item_id = product.item_code;
                            //task.product = product;
                            task.task_order_self_support = task_order_self_support;
                            setLocal({tasks: tasks}, function(){

                                clue('订单数据获取成功,3S后跳转详情页');
                                setTimeout(function(){
                                    //进入订单详情
                                    order_dom.find("a:contains('订单详情')")[0].click();
                                    //跳转商品详情页
                                    // order_dom.find(".p-name a")[0].click();
                                },3000)
                                
                                // sendMessageToBackground('self_support','', function(){
                                //     console.log("任务完成");
                                // });
                                
                            });
                        });

                    }
                }else{
                    clue('今日还没下单，3s跳转');
                    useLocal(function(local){
                        setTimeout(function(){
                            tabPc.jdIndex();
                            // location.href = 'https://www.jd.com/';
                        },3000)  
                    })  
                }
                
            }else{
                clue('没有订单，3S继续');
                useLocal(function(local){
                    setTimeout(function(){
                        // location.href = local.task.promotion_url;
                        tabPc.jdIndex();
                    },3000)  
                })  
            }

            
        }

        function getNowFormatDate() {
            var date = new Date();
            var seperator1 = "-";
            var seperator2 = ":";
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var currentdate = year + seperator1 + month + seperator1 + strDate;
                    // + " " + date.getHours() + seperator2 + date.getMinutes()
                    // + seperator2 + date.getSeconds();
            return currentdate;
        }


        })
    }

}})();
