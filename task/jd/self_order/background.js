M.selfOrder = {

	accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        M.global.report.clank.error({status:4,finished_result:remark},sender)
    },

    businessAccountRecentCategory:function(request,sender){
	    var API = new Api();
	    useLocal(function(local){
	        // data.business_account_id = local.task.business_account_id;
	        API.businessAccountRecentCategory(local.host_id, local.tasks[local.taskId].account_id, function(ret){
	            if(ret.success == 1){
	                console.log(ret);
	                chrome.tabs.sendMessage(sender.tab.id,{act:'get_recent_category_response',data:ret.data});
	            }else{
	                console.log(ret.message);
	            }
	        }, function(){
	            console.log("请求接口失败,重试");
	            setTimeout(function(){
	               M.selfOrder.businessAccountRecentCategory(request,sender);
	            }, 5000);
	        });
	    })
	},

	/**
	* 保存自营单数据
	*/
	saveSelfSupportOrderToRemote : function(request,sender){
		//保存自营单数据信息
		var API = new Api();
		useLocal(function(local){
		    API.saveSelfSupportOrderToRemote(local.host_id, local.tasks[local.taskId].task_order_self_support, function(){
		        console.log("自营订单数据保存");
		        //任务完成
		        // M.selfOrder.selfOrderTaskSuccess(local.tasks[local.taskId].task_order_self_support.business_oid);
		    }, function(){
		        console.log("自营订单数据保存失败,重试");
		        setTimeout(function(){
		            M.selfOrder.saveSelfSupportOrderToRemote(request,sender);
		        }, 5000);
		    });
		})
	},

	/**
	* 保存自营商品数据
	*/
	productSave : function(request,sender){
		var API = new Api();
		useLocal(function(local){

		// getTaskWorks(function(tw){})

			API.productSave(local.host_id, local.tasks[local.taskId].product, function(){
			    console.log("商品数据保存");
			    // callback && callback();
			}, function(){
			    console.log("自营订单数据保存失败,重试");
			    setTimeout(function(){
			        M.selfOrder.productSave(request,sender);
			    }, 5000);
			});
		

		})
	}

	// selfOrderTaskSuccess:function(request){
	//     M.selfOrder.selfOrderTaskUpdate(3, request, function(){
	//         //任务完成,下一个
	//         // startHost();
	//     });
	// },

	// selfOrderTaskUpdate:function(status, message,success){
	//     var API = new Api();
	//     useLocal(function(local){
	//         API.selfOrderTaskUpdate(local.host_id, {id: local.task.id, status: status, message: message}, function(){
	//             console.log("任务状态更新成功");
	//             success && success();

	//         }, function(){
	//             setTimeout(function(){
	//                 M.selfOrder.selfOrderTaskUpdate(status, message,success);
	//             }, 5000);
	//         });
	//     });
	// }



};