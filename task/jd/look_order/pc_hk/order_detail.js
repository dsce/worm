(function () {

    tabCommon.start('lookOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {
            label('全球购订单详情');
            if (tabCommon.findStrByHref(local.currentTask.business_oid)) {
                clue('是目标订单页面 等到10s后汇报')
                setTimeout(function () {
                    // tabCommon.taskReport.clank()
                    tabCommon.sm(taskVars, '.report.clank.success')
                }, 10e3)
            } else {
                clue('当前不是目标订单 将要点击我的订单')
                lazy(function () {
                    tabCommon.clickElement($("a:contains('我的订单')"))
                })
            }
        })
    }

}})();