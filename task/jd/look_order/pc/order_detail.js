(function () {

    tabCommon.start('lookOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {
            label('打开了订单详情 判断是否目标页面')
            if (tabCommon.findStrByHref(local.currentTask.business_oid)) {
                label('到达目标页面')
                clue('是目标订单页面 等到10s后汇报')
                setTimeout(function () {
                    //tabCommon.taskReport.clank()
                    tabCommon.sm(taskVars, '.report.clank.success')
                    // tabCommon.report(taskVars,'lookOrder');
                }, 10e3)
            } else {
                label('不是目标页面 回到订单列表');
                clue('当前不是目标订单 将要点击我的订单')
                lazy(function () {
                    tabCommon.clickElement($("a:contains('我的订单')"))
                })
            }
        })
    }

}})();