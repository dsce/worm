(function () {

    tabCommon.start('lookOrder', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {
            label('查找需要的订单号');
            tabPc.findOrder(local.currentTask.business_oid)
                .then(function () {
                    tabPc.clickOrderDetailLink(local.currentTask.business_oid)
                        .then(function () {
                            clue('已点开订单详情, 本页不需要动作了')
                        }, function () {
                            clue('点开订单失败 将要汇报任务');
                            //tabCommon.taskReport.clank({status:4,finished_result:'找到了,但是点不开该订单'})
                            tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'找到了,但是点不开该订单'})
                        })
                }, function () {
                    clue('寻找订单失败 准备汇报任务')
                    //tabCommon.taskReport.clank({status:4,finished_result:'订单列表找不到该订单'})
                    tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'订单列表找不到该订单'})
                })
        })
    }

}})();