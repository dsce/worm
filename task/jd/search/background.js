//background.js-单独任务背景页

M.search = {

		//关键词异常
		keywordException: function(){
			var API = new Api();
			getSharedTaskWorks(function(sTask){
				if(sTask.is_group){
					API.keywordsGroupException(
						function(ret){
							if(ret.success == 1){
								console.log('关键词异常标记成功');
								startHost();//重新开始
							}else if(ret.message == 'host_id不一致'){
								setHostStatus(4);
								notify(ret.message);
								chrome.storage.local.set({isRunning: false}, function() {
									notify("插件暂停", true);
								});
								return false;

							}else{
								console.log('关键词异常标记失败',ret.message);
							}
						},
						function(){
							console.log('请求失败，再次尝试');
							setTimeout(function(){
								// keywordsException();
								M.search.keywordsException();
							},5000);
						}
					);
				}else{
					API.keywordsException(
						function(ret){
							if(ret.success == 1){
								console.log('关键词异常标记成功');
								startHost();//重新开始
							}else if(ret.message == 'host_id不一致'){
								setHostStatus(4);
								notify(ret.message);
								chrome.storage.local.set({isRunning: false}, function() {
									notify("插件暂停", true);
								});
								return false;

							}else{
								console.log('关键词异常标记失败',ret.message);
							}
						},
						function(){
							console.log('请求失败，再次尝试');
							setTimeout(function(){
								// keywordsException();
								M.search.keywordsException();
							},5000);
						}
					);
				}
			})
			
		},

	// 	sendMessageToTab: function (request, sender){

	// 		var msg = request;

	// 		if(msg.tabId>0){
	// 			msg.data.sender = sender;
	// 			chrome.tabs.sendMessage(msg.tabId, msg.data);
	// 		}else{
	// 			console.log(msg);
	// 			chrome.tabs.query({url: msg.url}, function(tabs){
	// 				console.log(tabs);
	// 				if(tabs.length > 0){
	// 					var tabId = tabs[0].id;
	// 					msg.data.sender = sender;
	// 					chrome.tabs.sendMessage(tabId, msg.data);
	// 				}else{
	// 					chrome.tabs.sendMessage(sender.tab.id, {act: 'url_error', data: msg.data});
	// 				}
	// 				//chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: 123, text: 'text'});
	// 			});
	// 		}
	// },

	//选中Tab
	tabsUpdateSelectedByUrl :function (request){
		var url = request;
		chrome.tabs.query({url: url}, function(tabs){
			if(tabs.length > 0){
				var tab_id = tabs[0].id;
				chrome.tabs.update(tab_id, {selected: true}, function(details){
					console.log(details);
				});
			}
		});
	},

	//tabs标签页面获取cookie
	tabGetCookies:function(request,sender){
		var msg = request;
		chrome.cookies.getAll(msg.details, function (cookies) {
			console.log(msg, cookies);
			chrome.tabs.sendMessage(sender.tab.id, {act: 'tab_get_cookies_response', cookies: cookies});
		});
	},

	httpsTabsVerifyCodeForChat :function(request,sender){
		console.log('https_tabs_verify_code_result_for_chat');

		var dama = new DaMa();
		dama.submit(request.imgsrc, function (cid, text) {
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', cid: cid, text: text,error:0});
		}, function () {
			chrome.tabs.sendMessage(sender.tab.id, {act: 'https_tabs_verify_code_result', error:1});
			//chrome.tabs.reload(sender.tab.id);
		}, request.cookie);

	},

	//获取聊天信息
	getChatMsg :function (request,sender){
		console.log('get_chat_msg_result');
		getTaskWorks(function(task){
			// var task = local.task;
			var request_nums = task.getChatMsgNums ? task.getChatMsgNums : 1;

			var API = new Api();
			API.getChatMsg(
				function(ret){
					if(ret.success == 1){
						console.log('获取聊天信息成功');
						chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:ret.data});
					}else{
						console.log('获取信息失败 ',ret.message);
						chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:null});
					}
				},
				function(){
					console.log('请求失败，再次尝试');
					if(request_nums < 5){
						setTimeout(function(){
							var getChatMsgNums = parseInt(request_nums + 1);
							setTaskWorks({getChatMsgNums:getChatMsgNums},function(){
								M.search.getChatMsg(sender);//再次请求
							})

						},5000);
					}else{
						console.log('获取信息失败');
						chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:null});
					}

				}
			);

		})

	},
	//保存商品位置信息
	savePosition :function (request,sender){
		console.log('save position');
		getTaskWorks(function(task){
			// var task = local.task;
			var request_nums = task.savePositionNums || 1;

			var API = new Api();
			API.savePosition(request,
				function(ret){
					if(ret.success == 1){
						console.log('保存成功');
						// chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:ret.data});
					}else{
						console.log('保存失败 ',ret.message);
						M.search.savePosition(request,sender);//再次请求
						// chrome.tabs.sendMessage(sender.tab.id, {act: 'get_chat_msg_result', msg:null});
					}
				},
				function(){
					console.log('请求失败，再次尝试');
					if(request_nums < 5){
						setTimeout(function(){
							var savePositionNums = parseInt(request_nums + 1);
							setTaskWorks({savePositionNums:savePositionNums},function(){
								M.search.savePosition(request,sender);//再次请求
							})

						},5000);
					}else{
						console.log('请求接口失败');
						
					}

				}
			);

		})

	}
					

};

