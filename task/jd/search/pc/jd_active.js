(function () {

    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

//jD活动页面

var bottom_time_interval = 0;//计时器
var bottom_count_timer = 0;//底部等待时间计时

var find_target_count = 0;

var href = location.href;

setTimeout(function(){
    Task(function(local){

        //先去掉opened_active_product的验证，出现加入购物车不能成功的情况。所以需要重复进入
        //if(localSearch.currentTask.active_url && !localSearch.currentTask.opened_active_product){
        if(localSearch.currentTask.active_url){
            if(href.indexOf(localSearch.currentTask.active_url.replace(/http[s]*:/,'')) != -1){
                //属于任务的活动链接
                if(document.title == '京东秒杀 - 京东触屏版'){
                    var task = localSearch.currentTask;
                    // task.no_check_referrer = true;
                    setTaskWorks({no_check_referrer:true},function(){
                        scroll_bottom(function(){
                            index_active(local);
                        })
                    })
                }else{
                    scroll_bottom(function(){
                        index_active(local);
                    })
                }
                
                
            }
        }
    });
},3*1000)

var remark = '活动商品找不到';

function index_active(local){
    var item_id = localSearch.currentTask.item_id;
    var p_url = "//item.jd.com/" + item_id +'.html';
    var p_url_hk = "//item.jd.hk/" + item_id +'.html';//量贩团
    var p_url_lf = '//item.yiyaojd.com/' + item_id +'.html';//量贩团

    var m_p_url = "//item.m.jd.com/product/" + item_id +'.html';//M端活动页面
    var m_h5_p_url = "//item.m.jd.com/ware/view.action?wareId=" + item_id;

    //目标商品
    var target_pro_href = $('area[href*="' + p_url +'"],a[href*="' + p_url +'"],area[href*="' + p_url_hk +'"],a[href*="' + p_url_hk +'"],area[href*="' + p_url_lf +'"],a[href*="' + p_url_lf +'"],area[href*="' + m_p_url +'"],a[href*="' + m_p_url +'"],area[href*="' + m_h5_p_url +'"],a[href*="' + m_h5_p_url +'"]');

    if(target_pro_href.length >0){
        clue('找到活动商品');
        updateHostStep(_host_step.view_product);//step4 浏览主商品
        var task = localSearch.currentTask;
        // task.opened_active_product = true;

         setTaskWorks({opened_active_product:true},function(){
            target_pro_href[0].click();
            if(target_pro_href.first().attr('href').indexOf('item.m.jd.com') == -1){
                close_this_tab();
            }
        })
        
    }else{
        
        if(find_target_count > 2){
            //找了3次找不到 刷新吧
            label('找不到活动商品,请及时反馈主机',5*60+10);
            clue('活动商品找不到');
            if(local.task.no_active_reload_nums >= 3){
                tabCommon.sm(taskVars, '.startHost');
                return false;
            }else {
                setTimeout(function(){
                    checkProductExceptionReloadNums('no_active',function(){
                        clue(remark,'error');
                        reportProductStockout(remark);
                    })
                },5*60*1000);
            }
        }else{
            find_target_count++;
            console.log('当前页面已查找次数：' + find_target_count + ',10后继续找');
            setTimeout(function(){
                index_active(local);
            },10*1000)
            
        }
        
    }
    
}

//滚动到最低端
// function scroll_bottom(callback) {

//     // 每次滚动 页面高度／10
//     bottom_time_interval = setInterval(function () {
//         bottom_count_timer++;
//         var scrolltop =  document.body.scrollHeight/10 * bottom_count_timer;

//         //到最底部
//         window.scrollTo(0, scrolltop);

//         //达到等待时间
//         if (scrolltop >= document.body.scrollHeight) {
//             clearInterval(bottom_time_interval);
//             callback&&callback();
//         }
//     }, 1000);
// }

function scroll_bottom(callback) {

     wangwang();
    // 每次滚动 页面高度／10
    // bottom_time_interval = setInterval(function () { }, 1000);
        bottom_count_timer++;
        var scrolltop =  document.body.scrollHeight;

        //到最底部
        window.scrollTo(0, scrolltop);

        //达到等待时间
        setTimeout(function(){
            if (scrolltop == document.body.scrollHeight) {
                clearInterval(bottom_time_interval);
                callback&&callback();
            }else{
               scroll_bottom(callback) 
            }
        },2*1000)
        
   
}

            //匿名函数结束
        })
    }

}})();