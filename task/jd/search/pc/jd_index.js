(function () {

    var task_type = 'search';

    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {

            localSearch.task_type = 'search';
            //匿名函数开始

//京东首页 jd_index.js

//var observer_config = {
//    attributes: true,
//    childList: true,
//    characterData: true,
//    attributeOldValue: true,
//    characterDataOldValue: true,
//    subtree: true
//}
//tabCommon.mutation(observerId,function(mutation){
//    console.log(mutation.type);
//    console.log(mutation.target);
//    console.log(mutation);
//},observer_config);

windowsTopNotSelf();
//Task(utmSource);
//index()
// Task(function(local){});

    verifyUnionUrl(function(){
        taskOrderExist('search',function(){
            searchKeywordsByJDPC(localSearch);
        })
        
    });


function index(local){
    updateHostStatus(1401000);

    var task = localSearch.currentTask;

    //是否已经提交订单
    // if(taskOrderExist('search')){
    //     return false;
    // }
    getSharedTaskWorks(function(sTask){
       if(sTask.is_group){
            var business_oid = local.task.business_oid;
        }else{
            var business_oid = local.tasks[local.taskId].business_oid;
        }

        if(business_oid){
            clue('订单已经存在','error');
            tabCommon.report(taskVars, 'search');
            return false;
        }else{
            if (task.keyword == '') {
                window.location.href = "http://item.jd.com/"+task.item_id+".html";
            }else {
                updateHostStatus(1404000);

                writing($("#key"),task.keyword,function(){
                    search(local);
                });

            }
        }
    })

    
}

function search(local){
    var search_2013 = $("#search-2013 input[type='button']");
    if(search_2013.length > 0){
        if($("#key").val() != localSearch.currentTask.keyword){
            $("#key").val(localSearch.currentTask.keyword);
        }
        clicking(search_2013);
    }else{
        var search_2014 = $("#search-2014 .form .button:contains('搜索')");
        if(search_2014.length > 0){
            if($("#key").val() != localSearch.currentTask.keyword){
                $("#key").val(localSearch.currentTask.keyword);
            }
            clicking(search_2014);
        }else{
            clue('没有找到搜索按钮');
            createSearchArea(localSearch.currentTask.keyword, localSearch.currentTask.item_id,doSearch);
        }
    }

    waitAutoSearch(localSearch.currentTask.keyword);
}


/**
 * 链接劫持问题 处理
 * @param local
 * @returns {boolean}
 */
function utmSource(local){

    //增加页面监听
    addListenerMessage(function(request){
        if(request.act == 'tab_get_cookies_response'){
            var cookies = request.cookies;
            console.log(cookies);
            if(cookies.length == 0 ){ hijackOver(); return false;}

            var kidnap = true;
            for(var i=0;i<cookies.length;i++){
                console.log(cookies[i].value);
                if(cookies[i].value.indexOf('disise201504') != -1){
                    kidnap = false;
                    break;
                }

            }
            if(!kidnap){
                console.log('正常链接');
                index()
            }else{
                hijackOver();
            }


        }
    });

    setTimeout(function(){
        $("#key").val(localSearch.currentTask.keyword);
    },5000);

    var url = location.href;
    if(url.indexOf('yiqifa.com') != -1){
        var details = {domain: 'yiqifa.com', name: 'yiqifa_euid'};
        // chrome.runtime.sendMessage({act:'tabs_get_cookies', details: details});
        tabCommon.sm(taskVars, 'tabGetCookies', {details: details});
    }else if(url.indexOf('chanet.com.cn') != -1){
        var details = {domain: 'chanet.com.cn'};
        // chrome.runtime.sendMessage({act:'tabs_get_cookies', details: details});
        tabCommon.sm(taskVars, 'tabGetCookies', {details: details});
    }else if(url.indexOf('p.egou.com') != -1){
        //p.egou.com 相关cookies在当前地址上存在，直接检查地址上的值即可
        if(url.indexOf('disise201504') != -1){
            console.log('正常链接');
            index()
        }else{
            hijackOver();//异常推广链接，
        }
    }else{
        //异常推广链接，

        //算作正常地址
        //hijackOver();
        //return false;
        index()
    }

}


function waitAutoSearch(keyword){
    setTimeout(function(){
        clue("10秒自动提交");
        location.href = "http://search.jd.com/Search?keyword="+keyword+"&enc=utf-8";
    },10000);

}

function hijackOver(){

    notifyMessage('链接劫持');
    updateHostStatus(1401001);
    setTimeout(resetHostByStep, 2000);
}


            //匿名函数结束
        })
    }

}})();