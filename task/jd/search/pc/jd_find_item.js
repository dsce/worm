(function () {

    if($("#J_container").length >0){
        var observerId = tabCommon.observer($("#J_container")[0])
    }
    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    var urlget = urlGet();
    var curr_search_keyword = decodeURIComponent(urlget.keyword);
    var task_keyword = localSearch.currentTask.keyword.replace(/^\s+|\s+$/g,"");
    run(localSearch);
    function run(localSearch) {
        
        lazy(function () {
            //匿名函数开始

            //京东搜索页面
            var nCount = 1, item_id;
            var find_interval = null;

            var page_interval = null;

            //是否找到
            var is_find = false;

            var indexed = false;
            //task
            var task = null;
            var taskItemId = null;
            var jd_rand_compare_count = 2;
            var rand_open_product_number = 2;
            var rand_open_product_number = random(1,3);//随机打开1-3个
            var rand_open_product_number = 1;//随机打开1-3个
            var jdFindItemCloseTabTime = random(20,30);//搜索结束等待关闭时间

            // var product_contrast_nums = parseInt(localSearch.currentTask.product_contrast_value);

            var product_contrast_value_origin = localSearch.currentTask.product_contrast_value_origin || '1,2';
            if(product_contrast_value_origin.indexOf(',') != -1){
                var p_start = product_contrast_value_origin.split(',')[0] || 1;
                var p_end = product_contrast_value_origin.split(',')[1] || 2;
            }else{
                var p_start = 1,p_end = 2;

            }

            //生成分页数据
            var pageArr = [2,3,4,5,6,7],pages = [],j=random(parseInt(p_start),parseInt(p_end)),nextPageClicking=false,needTryOpenProduct=false,searchOver=false;
            if(j>0){
                rand_open_product_number = j;
                while(j--){
                    pages.push(pageArr.splice(random(0,pageArr.length-1), 1)[0]);
                }
                pages.sort();
            }

            // j=2;

            if(curr_search_keyword == task_keyword){
                clue('当前搜索关键词：' + curr_search_keyword + '==' + task_keyword);
                pages = [1,2,3,4,5];
            }
            
            console.log(pages);
            clue('随机页码 ['+pages.toString()+']');


            var observer_config = {
                attributes: true,
                childList: true,
                characterData: true,
                attributeOldValue: true,
                characterDataOldValue: true,
                subtree: true
            }
            if($("#J_container").length > 0){
                tabCommon.mutation(observerId,function (mutation) {
                    //tabCommon.mutation(observerId,function(mutation){
                    //console.log(mutation.type);
                    //console.log(mutation.target);
                    //console.log(mutation);
                    if(mutation.type == "childList"){
                        //console.log(mutation.target);
                        //console.log(mutation);
                        if(mutation.target.id=='J_bottomPage'&&mutation.addedNodes.length>0){
                            console.log('bottomPage');
                            wangwang();
                            //console.log(mutation.target);
                            //console.log(mutation);
                            //监控向下滚动后分页位置重新加载
                            Run(function(){

                                if(mutation.removedNodes.length>0){
                                    console.log("bottomPage removedNodes");

                                    //尝试打开主商品，（列表中存在主商品的情况）
                                    tryOpenProduct(localSearch.currentTask.item_id);

                                    //商品已加载,随机打开一个
                                    if(curr_search_keyword == task_keyword){
                                        open_rand_product_J_goodsList(localSearch.currentTask.item_id);
                                    }else{
                                        randOpenProduct(localSearch.currentTask.item_id);
                                        searchNextPage();
                                    }
                                    

                                    //进入下一页
                                    // searchNextPage();

                                }else{

                                    //向下滚动
                                    //分页已加载,执行浏览
                                    var searchPage = $("#J_bottomPage .p-num a.curr:visible").text();//当前页数
                                    var totalPage = $("#J_bottomPage .p-skip em b").text();//总页数
                                    clue("现在是 第 "+ searchPage + " 页");

                                    setTimeout(function(){
                                        if($("#J_scroll_loading").text().indexOf('加载失败，请重试') != -1){
                                            // $("#J_scroll_loading a:contains('重试')")[0].click()
                                            pcSearch.nextKeyword();
                                        }
                                    },10*1000)
                                    
                                    //windowScrollToTarget('J_bottomPage');
                                    scrollBottomPage();

                                    if(searchPage==totalPage) {//已经是最后一页,本页商品数量可能不够半页,下半夜就不会加载
                                        searchNextPage();
                                    }else{
                                        setTimeout(function(){
                                            wangwang();
                                            //搜索出问题了,，下一个词吧
                                            if(curr_search_keyword != task_keyword){
                                                searchNextPage();
                                            }
                                            
                                        }, 30*1000);
                                    }

                                }
                            });
                        }else if(mutation.target.id=='J_topPage'){
                            console.log('topPage');
                            wangwang();
                            Run(function(){
                                if($("#J_bottomPage:visible").length==0){
                                    console.log("topPage bottomPage");

                                    //尝试打开主商品，（列表中存在主商品的情况）
                                    tryOpenProduct(localSearch.currentTask.item_id);

                                    //商品已加载,随机打开一个
                                    if(curr_search_keyword == task_keyword){
                                        open_rand_product_J_goodsList(localSearch.currentTask.item_id);
                                    }else{
                                        randOpenProduct(localSearch.currentTask.item_id);
                                        searchNextPage();
                                    }

                                    //进入下一页
                                    // searchNextPage();
                                }
                            })

                        }
                    }
                },observer_config);
            }


            //框架嵌套
            windowsTopNotSelf();
            // Task(readyIndex);


            //开始执行
            // readyIndex();
            randGoLogin(20,readyIndex);

            function setOut(){
                setTimeout(function(){
                    if(indexed == true){
                        window.location.reload();
                    }else{
                        setOut();
                    }
                },60*1000);
            }


            function readyIndex(local){
                wangwang();
                if (taskOrderExist('search')) {
                    return false;
                }

                //不确定
                //setOut();//设置超时

                task = localSearch.currentTask;
                taskItemId = localSearch.currentTask.item_id;
                //updateHostStatus(1405000);
                label('货比三家', 90);

                if(task.is_mobile == 1){
                    clue("手机单，不能进行pc搜索", 'error');
                    return false;
                }

                //是否有活动链接
                if(localSearch.currentTask.active_url){
                    window.location.href = localSearch.currentTask.active_url;
                    return false;
                }

                //先,移除广告 删除公告位置，杜绝使用广告地址
                var goods_wrap = $("div[id*='promGoodsWrap']");
                if(goods_wrap.length > 0){
                    clue('广告位 '+goods_wrap.length+'处，现在移除');
                    goods_wrap.html('这里是广告');
                }else{
                    clue("没有找到广告位", 'error');
                }

                getTaskWorks(function(task){

                    //搜索结果是否异常
                    if ($("#J_goodsList").length==0&&$(".notice-search").length>0) {
                        //当前搜索次数
                        var searchNoResultTimes = task.searchNoResultTimes || 0;
                        if(searchNoResultTimes >=3){
                            //标记异常
                            clue('当前搜索次数：' + searchNoResultTimes);
                            clue('无搜索结果,人工通知! 先人工通知,确认正常可调整为自动','error');
                            // tabCommon.sm(taskVars, '.search_keyword_exception');
                            tabCommon.sm(taskVars, 'keywordException')
                        }else{
                            clue('无搜索结果,3s后重新搜索');
                            //当前次数+1
                            searchNoResultTimes++;
                            setTaskWorks({searchNoResultTimes: searchNoResultTimes},function(){
                                setTimeout(function(){
                                    searchPc(task.keyword);
                                },3000)
                            });
                        }
                        return false;
                    }



                    var searchGroup = task.searchGroup;
                    var searchGroupItems = task.searchGroupItems;
                    //关键词批量搜索
                    if(searchGroup === true){
                        //剩余相似关键词
                        if(searchGroupItems.length > 0){
                            //随机打开1-3个商品
                            clue($("#J_crumbsBar .search-key").text());
                            clue('剩余'+searchGroupItems.length+'个相似关键词');

                            //index(local);

                            //开始滚动
                            //windowScrollToTarget('J_bottomPage');
                            scrollBottomPage();
                        }else{
                            //结束了,当前为主关键词,可以直接打开主商品
                            needTryOpenProduct = true;
                            clue('关键词搜索结束,可以打开主商品');
                            //windowScrollToTarget('J_bottomPage');
                            scrollBottomPage();

                            // index(local);
                        }
                    }else{
                        //原有流程
                        clue('原有流程');
                        index(local);
                    }

                })
            }

            function index(local) {

                indexed = true;
                var urlget = urlGet();
                var search_key = decodeURIComponent(urlget.keyword);
                var keyword = task.keyword.replace(/^\s+|\s+$/g,"");

                if ($("#J_goodsList").length > 0) {
                    //A
                    if(search_key == keyword){
                        clue("关键词: "+search_key);
                        //open_rand_product_J_goodsList(task.item_id);
                        open_product_J_goodsList(task.item_id);
                    }else{
                        clue("关键词不对 - "+search_key);
                        location.href = "http://search.jd.com/Search?keyword="+keyword+"&enc=utf-8&wq="+keyword+"";
                    }

                }else if($(".notice-search").length > 0){
                    var ns = $(".notice-search .ns-content")[0].innerText;
                    if(ns.indexOf('没有找到') != -1 && ns.indexOf('抱歉') != -1){
                        if(search_key == keyword){

                        }
                    }
                }
            }

            //J_goodsList 打开随机商品
            function open_rand_product_J_goodsList(item_id, callback) {
                //updateHostStatus(1405000, 60+30);//货比三家
                label('货比三家 主商品', 90+random(0,30));
                //尝试打开主商品
                tryOpenProduct(item_id);

                clue("随机打开货比商品");
                var inter = $("#J_goodsList .gl-item:contains('京东国际')");//搜索结果删除京东国际,全球购
                var product_list = $("#J_goodsList .gl-item").not("#jd_find_item").not('[data-type="activity"]').not(inter);
                if (product_list.length == 0) {
                    //搜索结果小于0, 下个关键词搜索
                    startSearch();
                }else if(product_list.length < rand_open_product_number){
                    //结果小于要打开的数量,要打开的数量就等于结果数,全打开
                    var around_items = new Array();
                    product_list.each(function (i) {
                        var pro_id = $(this).attr("data-sku");
                        var is_pro = $(this).find(".p-name a[href*='item.jd.com']");//确认直接是商品链接,排除其他
                        if (pro_id != item_id && is_pro.length > 0) {//排除主商品
                            around_items.push(pro_id);
                        }
                    });

                    getTaskWorks(function(local){
                        clue(around_items.toString());
                        setTaskWorks({rand_search_items: around_items}, function(){
                            around_items.forEach(function(pro_id){
                                setTimeout(function(){
                                    console.log(pro_id);
                                    var randProduct = $("li[data-sku='" + pro_id + "'] .p-img a");
                                    randProduct[0].click();
                                }, 2*1000);

                            });

                            setTimeout(searchNextPage, 10000);

                        });

                    });

                }else{

                    //先打开主商品， 后进行货比
                    //open_product_J_goodsList(item_id);

                    //排除主商品外剩下的商品 id 数组
                    var product_list_ids = new Array();

                    product_list.each(function (i) {
                        var pro_id = $(this).attr("data-sku");
                        var is_pro = $(this).find(".p-name a[href*='item.jd.com']");//确认直接是商品链接,排除其他
                        if (pro_id != item_id && is_pro.length > 0) {//排除主商品
                            product_list_ids.push(pro_id);
                        }
                    });

                    //随机要打开 数组
                    var randNumArr = new Array();

                    var pro_len = product_list_ids.length;
                    if (pro_len > 0 && rand_open_product_number > 0) {
                        for (var i = 0; i < rand_open_product_number; i++) {
                            var randNum = parseInt(Math.random() * pro_len);
                            if (!in_array(randNum, randNumArr)) {
                                randNumArr.push(randNum);
                            }
                        }
                        //随机商品数组
                        var around_items = new Array();
                        for (var r = 0; r < randNumArr.length; r++) {
                            var data_sku = product_list_ids[randNumArr[r]];
                            //var randProduct = $("li[data-sku='" + data_sku + "'] .p-img a");
                            //randProduct[0].click();

                            around_items.push(data_sku);
                        }

                        getTaskWorks(function(local){
                            clue(around_items.toString());
                            setTaskWorks({rand_search_items: around_items}, function(){
                                around_items.forEach(function(pro_id){
                                    setTimeout(function(){
                                        console.log(pro_id);
                                        var randProduct = $("li[data-sku='" + pro_id + "'] .p-img a");
                                        randProduct[0].click();
                                    }, 2*1000);

                                });

                                setTimeout(searchNextPage, 20*1000);

                            });

                        });
                    }
                }
            }


            /**
             * 打开主商品
             * @param item_id
             * @param callback
             */
            function open_product_J_goodsList(item_id, callback) {

                getTaskWorks(function(local){
                    var task = localSearch.currentTask;
                    var urlget = urlGet();
                    var search_key = decodeURIComponent(urlget.keyword);
                    var keyword = task.keyword.replace(/^\s+|\s+$/g,"");

                    //检测是否有错误关键词提示
                    if($(".check-error").length >0){
                        if($(".check-error .key2:contains('点击查看')").length == 0){
                            var search_key = $(".check-error .key2").text();
                        }
                    }

                    if(search_key != keyword){
                        readySearch(local);
                        return false;
                    }else{
                        updateHostStep(_host_step.view_product);//step4 浏览主商品

                        if ($("li[data-sku='" + item_id + "']").length > 0 && $("li[data-sku='" + item_id + "'] .p-img a[href*='item.jd.com']").length>0) {
                            clue("主商品存在列表中,直接点击");
                            needTryOpenProduct = false;
                            searchOver = true;
                            clicking($("li[data-sku='" + item_id + "'] .p-img a"));

                            //再打开随机商品
                            // open_rand_product_J_goodsList(item_id);
                            checkPosition(function(){
                                randOpenProduct(item_id,close_this_tab);
                            })
                            
                        }else {

                            var itemId = item_id;
                            var positionSearch = random(0, 59);console.log('positionSearch', positionSearch);
                            //var itemId = '1023124131';
                            var itemsUl = $("#J_goodsList ul.gl-warp");
                            var itemsLi = $("#J_goodsList ul.gl-warp li.gl-item").find("a[href*='item.jd.']").parents('li');
                            if(itemsLi.length > 0){
                                var tempItem = itemsLi.last();
                                var tempItemId = tempItem.attr('data-sku');
                                var tempItemHtml = tempItem[0].outerHTML;
                                var mainItem = $(tempItemHtml);
                                var mainItemLink = mainItem.find("a[onclick*='searchlog'][href*='item.jd.com']");
                                mainItemLink.each(function(i,o){
                                    var strLinkOnclick = $(this).attr('onclick');
                                    var arrLinkOnclick = strLinkOnclick.split(',');
                                    arrLinkOnclick[2] = positionSearch.toString();
                                    $(this).attr('onclick', arrLinkOnclick.join(','));
                                });
                                mainItemLink.eq(random(0,mainItemLink.length-1)).attr("id", "xss-product-link");
                                mainItemLink.eq(0).attr("id", "xss-product-link");
                                mainItem.attr("id", "xss-product");
                                var mainItemHtml = mainItem[0].outerHTML;
                                mainItemHtml = mainItemHtml.replace(new RegExp(tempItemId, 'g'), itemId);
                                itemsUl.prepend(mainItemHtml);

                                clue("已增加模拟商品");
                                needTryOpenProduct = false;
                                wangwang();

                                setTimeout(function(){
                                    
                                    searchOver = true;
                                    clicking($("#xss-product-link"));

                                    //再打开随机商品

                                    // open_rand_product_J_goodsList(item_id);
                                    randOpenProduct(item_id,close_this_tab);
                                }, 5000);

                            }else{
                                clue("商品在哪儿");
                            }
                        }
                    }
                })


            }

            /**
             * 尝试打开主商品
             * @param itemId
             * @param callback
             */
            function tryOpenProduct(itemId){

                if(needTryOpenProduct){

                    if(curr_search_keyword == task_keyword){
                        //主商品在列表,打开主商品带一个随机商品
                        if ($("li[data-sku='" + itemId + "']").length > 0 && $("li[data-sku='" + itemId + "'] .p-img a[href*='item.jd.com']").length>0) {
                            clue("主商品存在列表中,直接点击");
                            needTryOpenProduct = false;
                            searchOver = true;
                            clicking($("li[data-sku='" + itemId + "'] .p-img a"));
                            checkPosition();
                        }else{
                            var currentPageText = $("#J_bottomPage .p-num a.curr:visible").text();
                            console.log('当前第 ' + currentPageText +'页未找到主商品，继续');
                        }

                    }else{
                        clue('搜索关键词已结束，当前关键词不是任务关键词，重新进入');
                        searchPc(task_keyword,itemId);
                    }
                }
            }

            function randOpenProduct(itemId, callback){

                //updateHostStatus(1405000, 60+30);//货比三家
                label('货比三家 随机商品', 90);
                //itemId = 1561211845;
                clue("随机打开1个货比商品");
                var inter = $("#J_goodsList .gl-item:contains('京东国际')");//搜索结果删除京东国际,全球购
                var xssProduct = $("#xss-product");//模拟商品
                var mainItem = $("#J_goodsList .gl-item[data-sku='"+itemId+"']");

                var productList = $("#J_goodsList .gl-item").not('[data-type="activity"]').not(inter).not(xssProduct).not(mainItem);

                var randItem = productList.eq(random(0,productList.length-1));
                var randItemId = randItem.attr("data-sku");
                var randItemLink = randItem.find("a[href*='item.jd.com']:first");
                getTaskWorks(function(local){
                    console.log(local);
                    var randItems = [];
                    if(local.randItemIds){
                        randItems = local.randItemIds;
                    }
                    randItems.push(randItemId);

                    setTaskWorks({randItemIds: randItems}, function(){
                        clicking(randItemLink);

                        setTimeout(function(){
                            callback && callback();
                        },3*1000)
                    });
                });
            }


            addListenerMessage(function(request){
                console.log(request);
                var act = request.act;

                if(act=='next_page'){//下一页
                    // searchNextPage();

                }else if(act == 'next_product'){//再打开一个商品

                    //再来一个直接改为搜索下一个关键词
                    clue("直接下一个关键词");
                    Task(readyIndex);
                    //rand_open_product_number = 1;
                    //getTaskWorks(function(local){
                    //    open_rand_product_J_goodsList(localSearch.currentTask.item_id);
                    //});

                }else if(act == 'search_stop'){//搜索结束,等待关闭
                    clue("执行关闭");
                    close_this_tab();
                }
            });

            /**
             * 准备进入下一页
             */
            function searchNextPage(){
                wangwang();
                indexed = false;
                console.log('searchNextPage');
                if(searchOver){
                    clue("主商品已打开,翻页结束,等待关闭");
                    wangwang();
                    setTimeout(close_this_tab, jdFindItemCloseTabTime*1000);
                }else{

                    //分页浏览
                    if(pages.length > 0){
                        clue('剩余页码 ['+pages.toString()+']');
                        //当前页码
                        var currentPageText = $("#J_bottomPage .p-num a.curr:visible").text();

                        //要进入的页面和当前页码 相同不能正常进入问题避免
                        do{
                            var p = pages.shift();
                            if(currentPageText == p){
                                p = null;
                            }
                        }while(!p && pages.length>0);

                        //分页 页码
                        if(p){
                            var totalPage = $("#J_bottomPage .p-skip em b").text();
                            if($("#J_bottomPage .p-num a:visible").length>0 && totalPage>=p){
                                nextPageClicking = true;
                                var page = $("#J_bottomPage .p-num a:visible:contains("+p+")");
                                var pageFirst = $("#J_bottomPage .p-num a:visible:not(:first):first");
                                var pageLast = $("#J_bottomPage .p-num a:visible:not(:last):last");

                                if(page.text() != p){
                                    pages.unshift(p);
                                    if(p>pageLast.text()){
                                        page = pageLast;
                                    }else{
                                        page = pageFirst;
                                    }
                                }

                                clue("点击进入 第 "+ page.text() + " 页");
                                clicking(page);
                            }else{
                                clue("找不到 第"+p+"页, 总页数"+totalPage);
                                if(needTryOpenProduct){
                                    clue('第'+p+'页 找不到,打开主商品');
                                    open_product_J_goodsList(taskItemId);
                                }else{
                                    clue('第'+p+'页 找不到,进行下个关键词');
                                    setTimeout(function(){
                                        //Task(readySearch);
                                        //Task(readyIndex);
                                        pcSearch.nextKeyword();
                                    },10000)
                                }
                            }
                        }else{
                            //下一页没有了,下一个关键词
                            //Task(readyIndex);
                            pcSearch.nextKeyword();
                        }
                    }else{
                        clue('分页浏览结束');
                        //分页操作完成,下一个关键词
                        if(needTryOpenProduct){
                            clue('打开主商品');
                            open_product_J_goodsList(taskItemId);
                        }else{
                            clue('进行下个关键词');
                            setTimeout(function(){
                                //Task(readySearch);
                                pcSearch.nextKeyword();
                            },10000)

                        }
                    }
                }
            }


            //搜素 下一个关键词


            function scrollBottomPage(){
                tabCommon.sm(taskVars, 'tabsUpdateSelectedByUrl','*://search.jd.com/Search?keyword*');
                indexed = true;
                window.scrollTo(0, $("#J_bottomPage").offset().top/2);
                window.scrollTo(0, $("#J_bottomPage").offset().top);
            }

            //检测主商品位置
            function checkPosition(callback){
                var position;
                $("#J_goodsList .gl-item").each(function(i){
                    if($(this).attr('data-sku') == localSearch.currentTask.item_id){
                        position = i;
                    }
                })

                var curr_page = parseInt($("#J_bottomPage .p-num a.curr:visible").text());

                position = (curr_page-1)*60+position;

                var data = {
                    position:position,
                    sku:localSearch.currentTask.item_id,
                    keywords:localSearch.currentTask.keyword,
                    order_id:localSearch.currentTask.id
                }
                //保存位置信息
                if(data.position && data.position >0){
                    tabCommon.sm(taskVars,'savePosition',data,function(){
                        callback && callback();
                    });
                }else{
                    callback && callback();
                }
                
            }



            //匿名函数结束
        })
    }

}})();