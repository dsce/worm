(function () {
    var observerId1 = tabCommon.observer($('#puller')[0])
    var observerId2 = tabCommon.observer($('#stockStatus')[0])
    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

//jd/m/view.js

var jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, _t.main_product_wait_rand_time_end);
var jd_aux_product_wait_time = random(_t.aux_product_wait_rand_time_start, _t.aux_product_wait_rand_time_end) / 2;

var address = null;
var regin_province_selected = false;
var regin_city_selected = false;
var regin_area_selected = false;
var regin_street_selected = false;

var m_p_price = '';

var reported = false;

windowsTopNotSelf();
resetWatchDogTimer();
index()


var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}
tabCommon.mutation(observerId1,function(mutation){
    //console.log(mutation.type);
    //console.log(mutation.target);
    //console.log(mutation);
    if(mutation.type === 'attributes'){
        //选项内容显示
        if(mutation.target.className == 'sidebar-content' && mutation.attributeName == 'style' && mutation.oldValue.indexOf('display: none') != -1 && !regin_province_selected){
            //选择 省
            Run(function(local){
                var page_address = address.province + address.city + address.area ;
                var commonAddress = $("#jdDeliverList1 .common-address:contains('" + page_address +"')");
                if(commonAddress.length > 0){
                    commonAddress.attr("data");
                    commonAddress[0].click();
                    $.cookie('regionAddress', commonAddress.attr("data"));
                }else{
                    console.log('select province');
                    findRegion($("#jdDeliverList1 li"), address.province, function(){
                        regin_province_selected = true;
                    });
                    // $("#region-back-arrow")[0].click();
                    // beginIndex(local);
                }


            });
        }else if(mutation.target.id == 'jdDeliverList2' && mutation.attributeName == 'class' && regin_province_selected && !regin_city_selected){
            //选择 市
            Run(function(local){
                console.log('select city');
                findRegion($("#jdDeliverList2 li"), address.city, function(){
                    regin_city_selected = true;
                });
            });
        }else if(mutation.target.id == 'jdDeliverList3' && mutation.attributeName == 'class' && regin_city_selected && !regin_area_selected){
            //选择 市
            Run(function(local){
                console.log('select area');
                findRegion($("#jdDeliverList3 li"), address.area, function(){
                    regin_area_selected = true;

                    //beginIndex(local);
                });
            });
        }else if(mutation.target.id == 'jdDeliverList4' && mutation.attributeName == 'class' && regin_area_selected && !regin_street_selected){
            console.log('select street');
            $("#jdDeliverList4 li").eq(0).click();
            regin_street_selected = true;
        }


    }

},observer_config);

//监控库存状态
tabCommon.mutation(observerId2,function(mutation){
    console.log(mutation.type);
    console.log(mutation.target);
    console.log(mutation);
    if(mutation.type == 'childList'){
        if(mutation.target.id == 'stockStatus' && mutation.addedNodes.length > 0){
            Run(function(local){
                var stock_text = $('#stockStatus').text();

                var task_address = localSearch.currentTask.consignee;
                var remarks = "[" + task_address.province + task_address.city + localSearch.currentTask.item_id + "]" ;
                var jd_remarks = stock_text;

                if(stock_text.indexOf('无货') != -1){
                    checkProductExceptionReloadNums('whuo',function(){
                        ////没货
                        console.log('商品无货/下柜');
                        updateHostStatus(1406003);//无货
                        var area_text = $("#btn-select-region .address").text();
                        clue(stock_text, 'error');
                        clue(jd_remarks);
                        reportProductStockout("["+ area_text + "商品" + localSearch.currentTask.item_id  + "]无货");//自动标记异常 .2015-12-14 14:07:39
                    })
                    
                }else if(stock_text.indexOf('下柜') != -1){
                    checkProductExceptionReloadNums('xgui',function(){
                        console.log('商品下柜');
                        updateHostStatus(1406002);
                        clue(remarks + '下柜');
                        //任务备注 + 异常标记 add 2016-06-03
                        // addTaskOrderRemark(remarks + '下柜',function(){});
                            reportProductStockout(remarks + '下柜');//标记异常
                        
                        //reportProductStockout(remarks + '下柜');
                    })
                    
                }else if(stock_text.indexOf('该地区暂不支持配送') != -1){
                    clue(stock_text, 'error');
                    var area_text = $("#btn-select-region .address").text();
                    tabCommon.sm(taskVars, '.order_exception', "商品[" + localSearch.currentTask.item_id + "]["+ area_text +"]该地区暂不支持配送");//订单自动异常
                    clue(area_text + jd_remarks);
                }else{
                    console.log('库存正常');
                    clue(stock_text, 'success');
                    // beginIndex(local);
                }
            });

        }
    }
},observer_config);

//随即打开聊天
function openChat(){
    if($("#imBottom .btm-act-icn").length > 0){
        // if(random(0,1)){
        if(1){
            var task = localSearch.currentTask;
            //请求消息
            // tabCommon.sm(taskVars, '.get_chat_msg');
            tabCommon.sm(taskVars, 'getChatMsg');
            //监听消息
            addListenerMessage(function (request) {
                console.log(request);
                if(request.act == 'get_chat_msg_result'){
                    var msgData = request.msg;
                    console.log('msgData',msgData);
                    if(request.msg){
                        setTaskWorks({chatMsg:request.msg,checkChatted:true},function(){
                            clue('进入聊天'); 
                            setTimeout(function(){
                               clicking($("#imBottom .btm-act-icn")); 
                            },3000)
                            
                        })  
                    }
                }
            });

            // clue('进入聊天');
            // clicking($("#popbox .sprite-im"));
        }
    }
    
}

function index(local) {
    console.log('index');
    var task = localSearch.currentTask;
    var item_id = task.item_id;

    if (taskOrderExist('search')) {
        return false;
    }

    //切换主商品显示
    // tabCommon.sm(taskVars, '.tab_update_selected_by_url', 'http://item.m.jd.com/product/'+task.item_id+'.html');
    tabCommon.sm(taskVars, 'tabsUpdateSelectedByUrl','http://item.m.jd.com/product'+task.item_id+'.html');
    var ware_id = $("#currentWareId").val();

    console.log(ware_id);

    getTaskWorks(function(task){


    if (ware_id == item_id) {
        //打开主商品
        updateHostStatus(1406000);

        if(localSearch.currentTask.chat == '1'){
            //打开聊天
            if(task.checkChatted !== true){
                openChat();
            }else{
                console.log('已经聊过了');
            }
        }

        //检查是否有来源
        if(document.referrer == '' && !task.no_check_referrer && !task.checkChatted){
            clue("没有发现搜索来源,3秒后跳转首页", "error");
            setTimeout(function(){
                if(localSearch.currentTask.worldwide == '1'){
                    location.href = 'https://m.jd.hk';
                }else{
                  clicking($("#m_common_header_shortcut_m_index a"));  
                }
                
            }, 3000);
            return false;
        }


        //判断商品是否下柜
        if($('.yang-pic-price').text().indexOf('下柜') != -1){
            checkProductExceptionReloadNums('xgui',function(){
                console.log('商品下柜');
                updateHostStatus(1406002);
                var task_address = localSearch.currentTask.consignee;
                var remarks = "[" + task_address.province + task_address.city + localSearch.currentTask.item_id + "]" ;
                clue(remarks + '下柜','error');
                clue($('.yang-pic-price').text());
                clue("亲,我认为这个商品下柜了.但我就是个机器,最近在这个地方老是出错,所以,希望亲们给确认一下我是不是可以操作进行标记下柜", "error");
                //任务备注 + 异常标记 add 2016-06-03
                // addTaskOrderRemark(remarks + '下柜',function(){});
                    reportProductStockout(remarks + '下柜');//标记异常
                
                //reportProductStockout(remarks + '下柜');
                return false;
            })
            
        }

        //判断是不是由JD发货
        if($("#serviceFlag .base-txt").length >0){
            if($("#serviceFlag .base-txt").text().indexOf('京东发货') != -1){
                var jd_remarks = $("#serviceFlag").text();
                clue(jd_remarks);
                updateHostStatus(1406012);
                reportProductStockout('京东发货:' + jd_remarks);//标记异常
                return false;    
            }
        }
        

        //主商品计算页面停留时间
        if($(".goods-part .prod-price .yang-pic-price").length > 0){
            if($(".goods-part .prod-price .yang-pic-price")[0].innerText.indexOf('暂无报价') == -1){
                var jdPrice = $(".goods-part .prod-price .yang-pic-price")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
                var priceTime = getItemWaitTimeEnd(jdPrice);
                // jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, priceTime);
                jd_main_product_wait_time =  localSearch.currentTask.page_view_time || 60;
            }else{
                console.log('暂无报价');
            }
            
        }else{
            console.log('没有获取到价格');
        }

        if(jdPrice){
            m_p_price = jdPrice;
            if((jdPrice - localSearch.currentTask.product_price)*localSearch.currentTask.amount  >= 400){
                clue('商品价格差价大于400','error');
                return false;
            }
        }

        address = jdMunicipality(localSearch.currentTask.consignee);

        $("#directorder").hide();
        $("#add_cart").hide();

        lazy(function(){
            clicking($("#btn-select-region"));
            beginIndex(local);
        });

    } else {
        //非主商品
        //updateHostStatus(1405001, jd_aux_product_wait_time+20);//M商品详情页 的货比三家

        var rand_items = task.around_items;
        if (rand_items && in_array(ware_id, rand_items)) {
            //货比三家商品 ，自动返回到搜索列表
            clue('货比，' + jd_aux_product_wait_time + '秒后 关闭');
            setTimeout(function () {
                //$("#backUrl")[0].click();
                close_this_tab();
            }, jd_aux_product_wait_time * 1000);
        } else {
            console.log('非货比 非主商品 ');
        }
    }

    })
}

//商品加入购物车
function add_cart() {
    
    m_p_price ? clue('商品价格:' + m_p_price) : clue('获取不到商品价格');
    clue('放入购物车');
    if($("#add_cart").length > 0){
        if($("#add_cart").text().indexOf('立即预定') != -1 && !reported){//预定按钮直接到了提交订单
            tabCommon.report(taskVars,'search');
            reported = true;
            return false;
        }else{
            $("#add_cart")[0].click();
        }
        
    }else if($("#buyImmediately").length >0){
        $("#buyImmediately")[0].click();
    }else if($(".add_cart:visible").length >0){
        $(".add_cart:visible")[0].click();
    }else if($("#addCartBtm").length >0){
        $("#addCartBtm")[0].click();
    }else if($("#buyImmediately_bottom").length >0){
        $("#buyImmediately_bottom")[0].click();
    }else if($("#makeAppointments_bottom").length >0){
        if($("#makeAppointments_bottom").text().indexOf('立即预约') != -1){
            clue('出现立即预约按钮,不支持下单');
            reportGroupOrderException('出现立即预约按钮,不支持下单');
        }
    }
    
    clue("去结算");
    updateHostStep(_host_step.settle_accounts);//step5 结算
    updateHostStatus(1408000,jd_main_product_wait_time + 20);
    // to_cart();
    setTimeout(to_cart,3*1000);
}
//去结算
function to_cart() {

    if($("#add_cart_spec:visible").length >0){
        $("#add_cart_spec:visible")[0].click();
    }

    lazy(function () {
        if($("#toCart").length >0){
            $("#toCart")[0].click();
        }else if($("#toCartNew").length >0){
            $("#toCartNew")[0].click()
        }    
    }, 2);
}

//触发 change事件
function blur_event(o) {
    var changeEvent = document.createEvent("MouseEvents");
    changeEvent.initEvent("blur", true, true);
    o.dispatchEvent(changeEvent);
}

function beginIndex(local){
    var task = localSearch.currentTask;
    var item_id = task.item_id;

    if ($("#add_cart:not('.btn-fail')").length == 0 && $("#buyImmediately").length == 0 && $(".add_cart").length  == 0) {

        checkProductExceptionReloadNums('whuo',function(){
             //该地区不支持配送
            var region_unsupport_delivery = $("#btn-select-region:contains('不支持配送')");
            if(region_unsupport_delivery.length > 0){
                var message = region_unsupport_delivery.text().match(/\S+/g).toString();
                updateHostStatus(1406005);
                // addTaskOrderRemark(message+task.task_order_oid, function(){});
                    //使用新方法 订单异常 + 备注
                    tabCommon.sm(taskVars, '.order_exception',message);
                    // tabCommon.sm(taskVars, '.task_order_set_exception');
                
                return false;
            }

            //主商品没货或者下柜
            updateHostStatus(1406003);//无货
            clue('主商品[' + item_id + ']无货', 'error');
            clue('主商品[' + item_id + '] XSS认为无货', 'error');
            //reportProductStockout("商品[" + item_id + "]无货");//自动标记异常
            return false;
        })
       
    }

    //主商品，等待，关注，购买
    clue('主商品');
    //检查登陆状态
    var login = $(".login-area .login:contains(登录)");
    if (login.length > 0) {//没有登陆去登陆
        clue("先登录");
        window.location.href = "https://passport.m.jd.com/user/login.action?sid=" + $("#sid").val() + "&returnurl=http://m.jd.com/product/" + $("#currentWareId").val() + ".html";
        return false;
    }


    //关注，收藏

    if (task.follow_product == 1 && $("#focusOn:contains('关注')").length > 0) {
        clue('关注，收藏');
        //$("#attention")[0].click();
        $("#focusOn")[0].click()
    }
    //关注店铺
    if(task.favorite_shop == '1'){
        clue('打开店铺');

        clue('关注店铺， 打开店铺');
        window.open("//shop.m.jd.com/?shopId=" + task.shop_identity);
        // window.open("//ok.jd.com/m/index-"+task.shop_identity+".htm");
        //var shop = $('#shopfooter .J_ping:contains("进入店铺")');
        //var shop_info = $('#mainStay .bran-bar-box-tit');//兼容全球购
        //if(shop.length > 0){
        //    shop.attr('target', '_blank');
        //    shop[0].click();
        //}else if(shop_info.length > 0){
        //    shop_info.attr('target', '_blank');
        //    shop_info[0].click();
        //}else{
        //
        //    var shop_href = $("#jshopkefu a.shop-href")
        //    if(shop_href.length >0 ){
        //        shop_href.attr('target', '_blank');
        //        shop_href[0].click();
        //    }else{
        //        clue('未发现店铺入口', 'error');
        //        updateHostStatus(1406008);
        //        //return false;//没有店铺入口,不处理,进行下一步
        //    }
        //
        //}
    }

    //等待放入购物车
    console.log('放入购物车');

    //购买数量
    $("#number").val(task.amount);
    blur_event($("#number")[0]);
    updateHostStatus(1407000, jd_main_product_wait_time+20);
    clue(jd_main_product_wait_time + '秒后，将放入购物车');

    // clue('搜索任务结束');
    // tabCommon.report(taskVars, 'search');
    // return false;

    autoResetWatchDogTimer(jd_main_product_wait_time * 1000);
    setTimeout(add_cart, jd_main_product_wait_time * 1000);

    tabCommon.sm(taskVars, '.save_user_cookies_to_remote', 'jd.com');//save cookies

}

function findRegion(lis, name, callback){

    // return false;

    if(name){
        name = name.replace(/省|市/g,'');
    }else{
        name = '';//为空选择第一个
    }

    var result = false;

    lis.each(function(){
        var span = $(this).find('span');console.log(span.text());
        var text = span.text().replace(/省|市/g,'');
        if(text.indexOf(name) != -1){

            //clicking(span);
            clicking($(this));
            callback && callback();

            result = true;
            return false;
        }
    });

    if(!result){

        notifyMessage('未找到'+name);
    }

}




            //匿名函数结束
        })
    }

}})();