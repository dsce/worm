(function () {

    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

//jd/m/index_sid.js

windowsTopNotSelf();
index()
resetWatchDogTimer();

function index(local) {
    


    if(localSearch.currentTask.checkedLoginUsername !== true && $(".jd-search-login .jd-sprite-icon").length > 0){
        clue('已登录,3s后检测当前账号');
        setTimeout(function(){
            clicking($(".jd-search-login .jd-sprite-icon"));
        },3000);
        
        return false;
    }

    if(!check_jd_search_box()){
        //检查是否有搜索框
        clue("搜索框未显示，3秒后，跳转https");
        setTimeout(function(){
            location.href = location.href.replace('http','https');
        },3000);

    }else{
        if($("#index_search_main").length == 0 || $("#index_newkeyword").length == 0){
            //创建搜索区域
            createSearchAreaByM(local);
        }
        console.log(local);
        var keyword = localSearch.currentTask.keyword;
        if (taskOrderExist('search')) {
            return false;
        }

        //是否有活动链接
        if(localSearch.currentTask.active_url){
            window.location.href = localSearch.currentTask.active_url;
            return false;
        }

        updateHostStatus(1404000);//首页，搜索关键词

        if (keyword) {
            clicking($("#index_newkeyword"));
            writing($("#index_newkeyword"), keyword, function () {
                clicking($("#index_search_submit"));
                $("#index_searchForm").submit();
            })

        } else {
            alertify.log("没有关键词", 'error', 0);
        }
        
        
    }
    

}

//检查是否有搜索框
function check_jd_search_box(){
    var pro = document.location.protocol;
    if(pro == 'https:'){
        return true;
    }else{
        if($("body").attr("class") == undefined){
            return false;
        }else{
            return true;
        }
    }
    
}



            //匿名函数结束
        })
    }

}})();