(function () {

    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

//jd/m/index.js

windowsTopNotSelf();
index()
resetWatchDogTimer();
function index(local) {

	//检测是否已经登录
	getTaskWorks(function(task){

	var jdSearchLogin = $(".jd-search-login a");
	if($(".jd-search-login:contains('登录')").length>0&&jdSearchLogin.attr('href').indexOf('home.m.jd.com')==-1){
		//未登录先登录
		clicking(jdSearchLogin);
		return false;
	}else{

		//检测登录账号
		if(task.checkedLoginUsername !== true && ($(".jd-search-login .jd-sprite-icon").length > 0 || $(".jd-search-login .jd-search-icon-logined").length >0)){
			clue('已登录,3s后检测当前账号');
			setTimeout(function(){
				if($(".jd-search-login .jd-search-icon-logined").length >0){
					clicking($(".jd-search-login .jd-search-icon-logined"));
				}else{
					clicking($(".jd-search-login .jd-sprite-icon"));
				}
				
			},3000);
			
			return false;
		}
	}

	if(check_jd_search_box()){

		if($("#index_search_main").length == 0 || $("#index_newkeyword").length == 0){
			//创建搜索区域
            createSearchAreaByM(local);
        }
		//setTimeout(myjd, 3000);

		console.log(local);
		var keyword = localSearch.currentTask.keyword;
		if (taskOrderExist('search')) {
			return false;
		}
		//是否有活动链接
		if(localSearch.currentTask.active_url){
			window.location.href = localSearch.currentTask.active_url;
			return false;
		}
		updateHostStatus(1404000);//首页，搜索关键词

		if (keyword) {
			clicking($("#index_newkeyword"));
			writing($("#index_newkeyword"), keyword, function () {
				clicking($("#index_search_submit"));
				$("#index_searchForm").submit();
			})

		} else {
			alertify.log("没有关键词", 'error', 0);
		}

	}else{
		setTimeout(function(){
			location.href = 'https://m.jd.com/';
		},3000);
	}

	})
    
}

//检查是否有搜索框
function check_jd_search_box(){
	var pro = document.location.protocol;
	//clue(pro);
	if(pro != 'https:'){
		if($("body").attr("class") == undefined){
			clue("搜索框未显示，3秒后，跳转https");
			return false;
		}
	}
		return true;
}

function myjd() {
    clue("3秒后，进入我的京东");
    var my_jd = $("a:contains('我的京东')");
    if(my_jd.length > 0){
        my_jd[0].click();
    }else{
        clue('我的京东未找到', 'error');
    }

}

            //匿名函数结束
        })
    }

}})();