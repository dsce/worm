(function () {
    var observerId = tabCommon.observer($("#cj-itemsList")[0])
    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

//M端全球购搜索

var rand_open_product_number = _t.product_rand_open_count;
var opened_rand_product = false;

tabCommon.mutation(observerId,function (mutation) {

        if(mutation.type == "childList"){
            console.log(mutation.target);
            console.log(mutation);
            if(mutation.addedNodes.length>0  && mutation.target.className == "clearfix" && mutation.target.tagName == 'UL'){
            	// Task(index);
            }
        }
});

windowsTopNotSelf();
resetWatchDogTimer();
// Task(index);
index();

function index(local) {
    console.log(local);
    var task = localSearch.currentTask;
    var item_id = task.item_id;

    if(task.is_mobile != 1){
        clue('不是手机单，手动', 'error');
        return false;
    }

    if (taskOrderExist('search')) {
        return false;
    }

    //是否有活动链接
	if(localSearch.currentTask.active_url){
		window.location.href = localSearch.currentTask.active_url;
		return false;
	}

	if(location.protocol == 'https:'){

	    location.protocol = 'http:';
	    return false;
	}

    updateHostStatus(1405000);//货比三家

    //货比三家 商品列表 进行货比
    open_rand_product(item_id);
}

function open_product(){

	updateHostStep(_host_step.view_product);//step4 浏览主商品

	getTaskWorks(function(task){

	console.log('open_product');

    	var item_id = localSearch.currentTask.item_id;
    	var flag = false;//主商品是否存在标志
		if($("#cj-itemsList ul li").length >0){
			$("#cj-itemsList ul li").each(function(i){
				if($(this).attr('skuid') == item_id){
					//主商品存在http://mitem.jd.hk/ware/view.action?wareId=1958845693
					var evt = document.createEvent("MouseEvents");
					evt.initEvent("touchend", true, true);
					this.dispatchEvent(evt);
				}
			})
			// var mainItem = $("#cj-itemsList ul").find("li[skuid=" + item_id +"]");
			if(!flag){
				//主商品不存在
				// var dom_html = $("#cj-itemsList ul li")[0].outerHTML;
				// var first_skuid = $(dom_html).attr('skuid');
				// console.log(first_skuid);
				// var html = dom_html.replace(new RegExp(first_skuid, 'g'), item_id);
				// console.log(html);
				// $("#cj-itemsList ul").prepend(html);

				$("#cj-itemsList ul li").first().attr('skuid',item_id);
				
				var evt = document.createEvent("MouseEvents");
				evt.initEvent("touchend", true, true);
				$("#cj-itemsList ul li")[0].dispatchEvent(evt);
			}
		}else{
			//没有商品，重新搜索
			var urlget = urlGet();
	        console.log(urlget);
	        var search_key = decodeURI(urlget.keyword);
	        var noRightProduct = $(".no-right-product:visible");
	        if($(".notice").length > 0 || noRightProduct.length>0){
	            //var ns = $(".notice")[0].innerText;
	            var ns = $(".notice").text();

	            if((ns.indexOf('暂无搜索结果') != -1) || noRightProduct.length>0){
	                // var task = localSearch.currentTask;
	                //当前搜索次数
	                var now_search_num = task.search_nums?parseInt(task.search_nums):0;
	                if(now_search_num >=3){
	                    //标记异常
	                    clue('当前搜索次数：' + now_search_num);
	                    clue('无搜索结果,人工通知! 先人工通知,确认正常可调整为自动','error');
						tabCommon.sm(taskVars, 'keywordException');
	                }else{
	                    clue('无搜索结果,3s后重新搜索');
	                    //当前次数+1
	                    var search_nums = (now_search_num + 1);
	                    setTaskWorks({search_nums:search_nums},function(){
	                        setTimeout(function(){
	                            writing($("#searchkey"), localSearch.currentTask.keyword, function () {
	                                // clicking($("#layout_search_submit"));
	                                $("#layout-search").submit();
	                            })
	                        },3000)
	                    });
	                    
	                }
	            }
	        }else{
	             clue('不能确认商品列表', 'error');
	        }
		}
    })

	

}

//货比商品
function open_rand_product(item_id){

	var product_list = $("#cj-itemsList ul li");

	if(product_list.length >3){
		var product_list_ids = new Array;
		product_list.each(function(i){
			if($(this).attr('skuid') != item_id){
				product_list_ids.push($(this).attr('skuid'));
			}
		});
		var randNumArr = new Array();
    	var pro_len = product_list_ids.length;
	    
	    if (pro_len > 0 && rand_open_product_number > 0) {
	        for (var j = 0; j < rand_open_product_number; j++) {
	            var randNum = parseInt(Math.random() * pro_len);
	            if (!in_array(randNum, randNumArr)) {
	                randNumArr.push(randNum);
	            }
	        }

	        var around_items = new Array();
	        for (var r = 0; r < randNumArr.length; r++) {
	            var itemBoxId = product_list_ids[randNumArr[r]];
	            product_list.each(function(i){
	            	if($(this).attr('skuid') == itemBoxId){
	            		
						var url = getItemUrl(itemBoxId);

			            tabCommon.sm(taskVars, '.createTabByUrl', {url: url});
						around_items.push(itemBoxId);
						// chrome.tabs.query({active:true},function(t){
						// 	console.log(t);
						// 	if(t.length >0){
						// 		chrome.tabs.create({windowId:t[0].windowId,url:url},function(){
						// 			around_items.push(itemBoxId);
						// 		});
						// 	}
						// })
						
						
	            	}
	            })
	        }

	       	setTaskWorks({around_items: around_items}, function () {
	            lazy(function () {
	                open_product(item_id);
	            });
	        });
	    }

	}else{
		open_product(item_id);//直接打开主商品
        return false;
	}
}


function getItemUrl(e, t) {
 var i = document.location.protocol + "//m.jd.com/product/";
 return i = i + e + ".html?",i += "resourceType=m_destination_page",i = i + "&resourceValue=" + t
}

            //匿名函数结束
        })
    }

}})();