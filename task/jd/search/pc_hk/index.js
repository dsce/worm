(function () {

    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始


windowsTopNotSelf();

 taskOrderExist('search',index);
        

function index(){

    useLocal(function(local){
        if(local.task.unpromotion === true || location.href.replace(/http[s]*:/,'').replace(/\/+/g,'') == 'www.jd.hk'){
            //不使用推广链接
            searchKeywords();
        }else{
            verifyUnionUrl(searchKeywords);
        }

    })
}

// function index(local){
//     updateHostStatus(1401000);

//     var task = localSearch.currentTask;

//     //是否已经提交订单
//     if(taskOrderExist('search')){
//         return false;
//     }

//     if (task.keyword == '') {
//         window.location.href = "http://item.jd.hk/"+ task.item_id +".html";
//     }else {
//         updateHostStatus(1404000);

//         writing($("#key"),task.keyword,function(){
//             search(local);
//         });

//     }
// }

function search(local){
    var search_2013 = $("#search-2013 input[type='button']");
    if(search_2013.length > 0){
        if($("#key").val() != localSearch.currentTask.keyword){
            $("#key").val(localSearch.currentTask.keyword);
        }
        clicking(search_2013);
    }else{
        var search_2014 = $("#search-2014 .form .button");
        if(search_2014.length > 0){
            if($("#key").val() != localSearch.currentTask.keyword){
                $("#key").val(localSearch.currentTask.keyword);
            }
            clicking(search_2014);
        }else{
            clue('没有找到搜索按钮');
        }
    }

    waitAutoSearch(localSearch.currentTask.keyword);
}


/**
 * 链接劫持问题 处理
 * @param local
 * @returns {boolean}
 */
function utmSource(local){

    //增加页面监听
    addListenerMessage(function(request){
        if(request.act == 'tab_get_cookies_response'){
            var cookies = request.cookies;
            console.log(cookies);
            if(cookies.length == 0 ){ hijackOver(); return false;}

            var kidnap = true;
            for(var i=0;i<cookies.length;i++){
                console.log(cookies[i].value);
                if(cookies[i].value.indexOf('disise201504') != -1){
                    kidnap = false;
                    break;
                }

            }
            if(!kidnap){
                console.log('正常链接');
                Task(index);
            }else{
                hijackOver();
            }


        }
    });

    setTimeout(function(){
        $("#key").val(localSearch.currentTask.keyword);
    },5000);

    var url = location.href;
    if(url.indexOf('yiqifa.com') != -1){
        var details = {domain: 'yiqifa.com', name: 'yiqifa_euid'};
        chrome.runtime.sendMessage({act:'tabs_get_cookies', details: details});
    }else if(url.indexOf('chanet.com.cn') != -1){
        var details = {domain: 'chanet.com.cn'};
        chrome.runtime.sendMessage({act:'tabs_get_cookies', details: details});
    }else if(url.indexOf('p.egou.com') != -1){
        //p.egou.com 相关cookies在当前地址上存在，直接检查地址上的值即可
        if(url.indexOf('disise201504') != -1){
            console.log('正常链接');
            Task(index);
        }else{
            hijackOver();//异常推广链接，
        }
    }else{
        //异常推广链接，

        //算作正常地址
        //hijackOver();
        //return false;
        Task(index);
    }

}


function waitAutoSearch(keyword){
    setTimeout(function(){
        clue("10秒自动提交");
        location.href = "http://search.jd.com/Search?keyword="+keyword+"&enc=utf-8";
    },10000);

}

function hijackOver(){

    notifyMessage('链接劫持');
    updateHostStatus(1401001);
    setTimeout(resetHostByStep, 2000);
}

/**
 * 关键词搜索
 */
function searchKeywords(local){

    if(!localSearch.currentTask.worldwide){
        location.href = 'https://www.jd.com';
        return false;
    }

    tabCommon.sm(taskVars, '.save_user_cookies_to_remote', 'jd.hk');//save cookies
    
    //状态 推广链接打开
    updateHostStatus(1401000);

    var task = localSearch.currentTask;

    //是否已经提交订单
    if(taskOrderExist('search')){
        return false;
    }

    if(task.active_url){
        location.href = task.active_url;
        return false;
    }

    if (task.keyword == '') {
        window.location.href = "http://item.jd.hk/"+task.item_id+".html";
    }else {
        updateHostStatus(1404000);

        writing($("#key"),task.keyword,function(){
            var search_2014 = $("#search-2014 .form .button");
            if(search_2014.length > 0){
                if($("#key").val() != localSearch.currentTask.keyword){
                    $("#key").val(localSearch.currentTask.keyword);
                }
                clicking(search_2014);
            }else{
                clue('没有找到搜索按钮');
            }
        });

    }
}


            //匿名函数结束
        })
    }

}})();