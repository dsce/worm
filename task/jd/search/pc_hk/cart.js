(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('search', taskStartRun)

    function taskStartRun(localSearch) {
    var taskVars=localSearch.taskVars;
    run(localSearch);
    function run(localSearch) {
        lazy(function () {
            //匿名函数开始

            var observer_config = {
                //attributes: true,
                childList: true,
                characterData: true,
                //attributeOldValue: true,
                //characterDataOldValue: true,
                subtree: true
            }

            tabCommon.mutation(observerId,function(mutation){

            if(mutation.type === 'childList'){
                console.log(mutation.target);
                console.log(mutation);
                if(mutation.target.className == 'ui-ceilinglamp-1' && mutation.addedNodes.length > 0){
                    //结算
                    
                    Run(function(local){
                       checkCurrTaskItem();
                    });
                }

            }

        },observer_config);




            function checkCurrTaskItem(){
                var item_id = localSearch.currentTask.item_id;

                if($("#product_" + item_id).length >0 || $(".p-name a[href*='" + item_id +"']").length >0){
                    clue('主商品已添加到购物车');
                    tabCommon.report(taskVars,'search');
                }else{
                    clue('找不到主商品，5S重置当前任务','error');

                    setTimeout(function(){
                        tabCommon.sm(taskVars,'.currentTaskReset');
                    },5*1000);
                }
            }

            //匿名函数结束
        })
    }

}})();