//home.js
(function () {

    tabCommon.start('home', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {

        label('修改头像');

        lazy(function () {
            getSharedTaskWorks(function(sTask){

                console.warn(sTask.account_info.is_update_avatar,sTask.account_info.avatar_updated_at)
                if(sTask.account_info.is_update_avatar==1 && !sTask.account_info.avatar_updated_at){
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "https://i.jd.com/user/userinfo/uploadDefaultImg.action",
                        data: {img:random(1,16)},
                        cache: false,
                        success: function (dataResult) {
                            clue('更新头像结果(2=>成功):'+dataResult)
                            if(dataResult==2){
                                tabCommon.sm(taskVars, 'updateAvatar',{account_id:sTask.account_info.account_id});
                            }
                            next()
                        },
                        error: function (XMLHttpResponse) {
                            console.log('error'+XMLHttpResponse);
                            next()
                        }
                    });
                }

                setTimeout(next,30e3);

            })

            function next() {
                setTimeout(function(){
                    $("#menu a:contains('账户安全')")[0].click();
                },3*1000)
            }
        })
    }

}})();