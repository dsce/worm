//home.js
(function () {

    tabCommon.start('home', taskStartRun)

    function taskStartRun(localHome) {
    var taskVars=localHome.taskVars;
    run(localHome);
    label('账户安全');
    function run(localHome) {
        lazy(function () {

            
            if($(".safe-item:contains('您验证的手机')").length == 0){
                //解绑手机了 报告接口
                tabCommon.sm(taskVars, 'unboundMobile');
            }

            //跳转我的级别
            setTimeout(function(){
                // $("#menu a:contains('我的级别')")[0].click();
                tabCommon.report(taskVars, 'home');
            },3*1000)

        })
    }

}})();
