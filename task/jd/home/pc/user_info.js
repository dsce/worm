//home.js
(function () {

    tabCommon.start('home', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);

    label('个人信息');

    var new_nickName = null;
    var new_sex = null;

    if(location.protocol == 'https:'){
        location.protocol = 'http:';
    }


    function run(local) {
        lazy(function () {

            // getRandNickName();
            // return false;

            // getSharedTaskWorks(function(sTask){
            //     updateUserInfo(sTask.account_info, function(){
            //         // compareUsername(sTask.account_info.username);
            //     });
            // })
            getSharedTaskWorks(function(sTask){
            
                updateUserInfo(sTask.account_info,function(){
                    if($(".u-level a:contains('京享值')").length >0){
                        $(".u-level a:contains('京享值')")[0].click()
                    }
                    setTimeout(function(){
                        if(sTask.account_info.is_update_avatar==1 && !sTask.account_info.avatar_updated_at && random(1,100)<25){
                            $("a:contains('头像照片')")[0].click();
                        }else{
                            // $("#menu a:contains('账户安全')")[0].click();
                            // if($(".u-level a").length >0){
                            //     $(".u-level a")[0].click();
                            // }else{
                                $("#menu a:contains('账户安全')")[0].click();
                            // }
                        }
                    },3*1000)

                    // tabPc.userInfo()

                })

            })

        })

    }

    function updateUserInfo(task, callback) {

        if($("#container a:contains('提交')").length == 0){
            window.location.reload();
        }

        if(task.birthday_updated_at && task.nickname_updated_at){
            clue("已修改"+task.birthday_updated_at);
            callback && callback();
            return false;
        }

        //随机生日
        var date,a, b,birthday;
        a=new Date('1970-1-31');
        b=new Date('2000-12-31');
        date = new Date(),
            date.setTime(random(a.getTime(), b.getTime()));
        birthday = date;
        var yearBirthday = birthday.getFullYear();
        var monthBirthday = birthday.getMonth();
        var dayBirthday = birthday.getDay();

        var nickName=new_nickName ? new_nickName :$("#nickName").val();
        var sex= new_sex ? new_sex : $("input[name=sex]:checked").val();
        //var nickName=$("#nickName").val();
        var realName=$("#realName").val();
        var realName = checkRealName(task.consignee.name.replace(/\s+/g, ''));//收货人姓名
        //var sex=$("input[name=sex]:checked").val();
        var birthday=$("#birthdayYear").val() + "-" + $("#birthdayMonth").val() + "-" + $("#birthdayDay").val();
        var birthday=yearBirthday + "-" + monthBirthday + "-" + dayBirthday;
        var province=$("#province").val();
        var city=$("#city").val();
        var county=$("#county").val();
        var address=$("#address").val();
        var code=$("#code").val();
        var rkey=$("#rkey").val();
        var newAliasName =$.trim($("#aliasName").val());
        var oldAliasName = $("#hiddenAliasName").val();

        var hobby="";
        $(".hobul").children().each(function(){
            if($(this).attr("class")=="selected"){
                hobby+=$(this).val()+",";
            }
        });

        var datas = "1=1";
        datas += "&userVo.nickName=" + encodeURI(encodeURI(nickName));
        datas += "&userVo.realName=" + encodeURI(encodeURI(realName));
        datas += "&userVo.sex=" + sex;
        datas += "&userVo.birthday=" + birthday;
        datas += "&userVo.hobby=" + hobby;
        datas += "&userVo.code=" + code;
        ; datas += "&userVo.rkey=" + rkey;
        if(newAliasName != $.trim(oldAliasName)){
            datas += "&newAliasName=" + encodeURI(encodeURI(newAliasName));
        }
        jQuery.ajax({
            type : "post",
            url : "/user/userinfo/updateUserInfo.action",
            data : datas,
            timeout: 10000,
            success : function(html) {
                if (html=="2") {
                    //数据更新成功,保存,或结束
                    console.log("success");
                    clue('个人信息保存成功');
                    tabCommon.sm(taskVars,'.updateNickname','', function(){
                            tabCommon.sm(taskVars,'.updateBirthday', '',function(){
                                callback && callback();
                            });
                    });
                    
                    // setTimeout("location.reload();",10*1000);

                }else if(html != "1"){
                    console.log(html);
                    getTaskWorks(function(tWorks){
                        var rand_nums = tWorks && tWorks.randNickNameNums ? tWorks.randNickNameNums : 0;
                        if(rand_nums < 3){
                            clue("个人信息填写有误，3S后再次保存");
                            var randNickNameNums = rand_nums + 1;
                            clue('保存次数: ' + randNickNameNums);
                            setTaskWorks({randNickNameNums:randNickNameNums},function(){
                                setTimeout(function(){
                                    getRandNickName(callback);//随机昵称
                                },3000);
                            });
                            
                        }else{
                            clue('保存信息失败超过3次，2s后跳过')
                            setTimeout(function(){
                                callback && callback();
                            },2000)
                            
                        }
                  })  
                }else {
                    console.log(html);
                    clue('保存失败');
                    callback && callback();
                }
            }
        });
}



//获取贴吧随机昵称
function getRandNickName(callback){

    getSharedTaskWorks(function(sTask){

        var task = sTask.account_info;

        getTaskWorks(function(tWorks){

            var userNum = random(10000,60000000);
            var rand_nums = tWorks && tWorks.randNickNameNums ? tWorks.randNickNameNums : 0;
            console.log(userNum);
            $.ajax({
                type:"GET",
                dataType:"html",
                // url:"http://tieba.baidu.com/i/" + userNum +"/fans?qq-pf-to=pcqq.c2c",
                // url:"http://user.kdnet.net/index.asp?userid=" + userNum,
                url:"http://i.autohome.com.cn/" + userNum,
                success:function(msg){
                    console.log(msg);
                    // var pat1 = /\<p\sclass=\"aside_user_name\"\>([^\<]+)\<\/p\>/;
                    // var pat2 = /\<p\sclass=\"aside_user_info\"\>\<span\>([^\<]+)\<\/span\>/;
                    // console.log(pat1.exec(msg));
                    // console.log(pat2.exec(msg));
                    // var user_name = $(msg).find(".userid a").text();
                    var user_name = $(msg).find(".user-name b").text().replace(/\s+/,'');
                    if(user_name){
                        new_nickName = user_name;
                        //new_nickName = '刷这是什么';
                        clue('随机到昵称：'+ new_nickName);
                        var reg = /([a-zA-Z\u4E00-\u9FFF])+/;

                        if(new_nickName.indexOf('刷') != -1 || new_nickName.indexOf('淘') != -1 || new_nickName.indexOf('单') != -1){
                            if(rand_nums < 3){
                                clue('含有过滤字符,重新随机昵称');
                                var randNickNameNums = rand_nums + 1;
                                clue('保存次数: ' + randNickNameNums);
                                setTaskWorks({randNickNameNums:randNickNameNums},function(){
                                    setTimeout(function(){
                                        getRandNickName(callback);//随机昵称
                                    },3000);
                                });
                            }
                            
                        }else if(!reg.exec(new_nickName)){
                            //全是数字
                            new_nickName  = new_nickName + task.username.substr(-4);
                        }
                        if(user_name){
                            // new_sex = pat2.exec(msg)[1]=='男'?0:1; 
                            new_sex = random(0,2);
                        }
                        console.log(new_nickName,new_sex);   
                        updateUserInfo(task, callback);
                        
                    }else{
                        
                        
                        if(rand_nums < 3){
                            console.log('未获取到昵称,3s后 go on');
                            var randNickNameNums = rand_nums + 1;
                            clue('保存次数: ' + randNickNameNums);
                            setTaskWorks({randNickNameNums:randNickNameNums},function(){
                                setTimeout(function(){
                                    getRandNickName(callback);//随机昵称
                                },3000);
                            });
                            
                        }else{
                            clue('获取失败超过3次，2s后跳过')
                            setTimeout(function(){
                                callback && callback();
                            },2000)
                            
                        }

                    }
                }   
            })

        })

 })
}

//检查真实姓名
function checkRealName(name){

    if(!name){
        return '真实姓名';
    }
    var reg3 = /([^a-zA-Z\u4E00-\u9FFF])+/;
    //console.log(reg.exec(name));
    console.log(reg3.exec(name));
    if(reg3.exec(name)){
        var new_name = name.replace(reg3.exec(name)[0],'');
    }else{
        return name;
    }
    console.log(new_name.length);
    if(new_name.length  == 0){
        var new_name = '真实姓名';
    }else if(new_name.length == 1){
        var new_name = new_name + new_name;
    }
    console.log(new_name);
    return new_name;
}

}})();