//background.js-单独任务背景页

M.home = {
	//保存账号级别
	saveAccountLevel: function (request) {
		var data = request;
		useLocal(function (local) {
			data.id = local.accountId;
			var api = new Api();
			api.saveAccountLevel(data,
				function (ret) {
					console.log(ret);
					if (ret.success == 1) {
						console.log('保存账号等级成功');
					} else {
						console.log(ret.message);
						setTimeout(function () {
							M.home.saveAccountLevel(request);
						}, 10 * 1000);
					}
				},
				function () {
					console.log('请求保存账号等级接口失败');
					setTimeout(function () {
						M.home.saveAccountLevel(request);
					}, 3000);
				}
			)
		})
	},

	unboundMobile : function (request) {
		var api = new Api();
		api.unboundMobile(
				function (ret) {
					console.log(ret);
					if (ret.success == 1) {
						console.log('保存解绑手机时间成功');
					} else {
						console.log(ret.message);
						setTimeout(function () {
							M.home.unboundMobile(request);
						}, 10 * 1000);
					}
				},
				function () {
					console.log('请求保存解绑手机时间接口失败');
					setTimeout(function () {
						M.home.unboundMobile(request);
					}, 3000);
				}
			)
	},

    updateAvatar : function (request, sender) {
        var _api = new Api();
        _api.updateAvatar(request.account_id);
    }



};

