(function () {

    tabCommon.start('loginPassword', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {

            if (tabCommon.findElement($('h3:contains("修改密码成功")'))) {
                clue('find');
                tabCommon.sm(taskVars, 'resetPwdSuccess');
            } else {
                clue('找不到成功的标识')
                getTaskWorks(function (taskWorks) {
                    var _newPwd = taskWorks.loginPassword;
                    tabCommon.sm(taskVars, 'resetPwdFail','找不到成功的标识 新密码 '+_newPwd+' ' + $('h3:contains("修改密码成功")').text());
                })
            }

        })
    }

}})();