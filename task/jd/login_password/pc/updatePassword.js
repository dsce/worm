(function () {


    tabCommon.start('loginPassword', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {
            label('验证支付密码')
            tabCommon.clickElement($('a:contains("通过支付密码验证")'));
            lazy(function () {
                if(tabCommon.findElement($('span:contains("请输入支付密码：")'))){

                    var observer_payPwd_error = tabCommon.observer(document.getElementById('payPwd_error'));
                    var observer_authCode_error = tabCommon.observer(document.getElementById('authCode_error'));

                    writing($('#payPwd'),local.account.pay_password,function () {
                        tabCommon.dama(function () {
                            return tabCommon.changeURLPar($('#JD_Verification1').attr('src'),'_t', new Date().getTime());
                        },function () {
                            return $('#authCode');
                        },function () {
                            return tabCommon.func(function (success,fail) {
                                tabCommon.clickElement($('#payPwda'));

                                tabCommon.mutation(observer_payPwd_error,function (mutations) {
                                    if(mutations.type = 'childList'){
                                        if($('#payPwd_error').text()){
                                            tabCommon.sm(taskVars, 'resetPwdFail','支付密码异常 ' + $('#payPwd_error').text());
                                        }
                                    }
                                })

                                tabCommon.mutation(observer_authCode_error,function (mutations) {
                                    if(mutations.type = 'childList'){
                                        if($('#authCode_error').text() == '验证码错误'){
                                            fail();
                                        }
                                    }
                                })

                            })
                        })
                    })
                }else if(tabCommon.findElement($('span:contains("请输入登录密码：")'))){

                    var observer_password_error = tabCommon.observer(document.getElementById('password_error'));
                    var observer_authCode_error = tabCommon.observer(document.getElementById('authCode_error'));

                    writing($('#password'),local.account.password,function () {
                        tabCommon.dama(function () {
                            return tabCommon.changeURLPar($('#JD_Verification1').attr('src'),'_t', new Date().getTime());
                        },function () {
                            return $('#authCode');
                        },function () {
                            return tabCommon.func(function (success,fail) {
                                tabCommon.clickElement($('#passworda'));

                                tabCommon.mutation(observer_password_error,function (mutations) {
                                    if(mutations.type = 'childList'){
                                        if($('#password_error').text()){
                                            tabCommon.sm(taskVars, 'resetPwdFail','登录密码异常 ' + $('#password_error').text());
                                        }
                                    }
                                })

                                tabCommon.mutation(observer_authCode_error,function (mutations) {
                                    if(mutations.type = 'childList'){
                                        if($('#authCode_error').text() == '验证码错误'){
                                            fail();
                                        }
                                    }
                                })

                            })
                        })
                    })
                }else{
                    //无法修改密码 停止 看是什么原因
                    tabCommon.sm(taskVars, 'resetPwdFail','无法通过支付密码修改登录密码');
                }
            })

        })
    }

}})();