(function () {
    var observer_password_error = tabCommon.observer(document.getElementById('password_error'));
    var observer_rePassword_error = tabCommon.observer(document.getElementById('rePassword_error'));
    var observer_authCode_error = tabCommon.observer(document.getElementById('authCode_error'));
    tabCommon.start('loginPassword', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        lazy(function () {

            label('设置新的登录密码')
            if(tabCommon.findElement($('span:contains("新的登录密码：")'))){

                var login_password = tabCommon.randPasswordStr();
                //login_password = 'Password@123'
                label('新密码生成 ' + login_password);
                clue('新密码生成 ' + login_password);
                setTaskWorks({loginPassword:login_password},function () {
                    writing($('#password'),login_password,function () {
                        writing($('#rePassword'),login_password,function () {
                            tabCommon.dama(function () {
                                return tabCommon.changeURLPar($('#JD_Verification1').attr('src'),'_t', new Date().getTime());
                            },function () {
                                return $('#authCode');
                            },function () {
                                return tabCommon.func(function (success,fail) {
                                    tabCommon.clickElement($('a.btn-1:contains("提交")'));

                                    tabCommon.mutation(observer_password_error,function (mutations) {
                                        if(mutations.type = 'childList'){
                                            if($('#observer_password_error').text()){
                                                tabCommon.sm(taskVars, 'resetPwdFail','新密码异常 ' + $('#payPwd_error').text());
                                            }
                                        }
                                    })

                                    tabCommon.mutation(observer_rePassword_error,function (mutations) {
                                        if(mutations.type = 'childList'){
                                            if($('#observer_rePassword_error').text()){
                                                tabCommon.sm(taskVars, 'resetPwdFail','重复密码异常 ' + $('#payPwd_error').text());
                                            }
                                        }
                                    })

                                    tabCommon.mutation(observer_authCode_error,function (mutations) {
                                        if(mutations.type = 'childList'){
                                            if($('#authCode_error').text() == '验证码错误'){
                                                fail();
                                            }else if($('#authCode_error').text() == '请输入验证码'){
                                                fail();
                                            }else{
                                                if($('#authCode_error').text()) tabCommon.sm(taskVars, 'resetPwdFail','改密码异常 ' + $('#authCode_error').text());
                                            }
                                        }
                                    })

                                })
                            })
                        })
                    })
                })

            }else{
                tabCommon.clickElement($('a[tid="_MYJD_safe"]'));
            }

        })
    }

}})();