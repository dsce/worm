M.loginPassword = {

    resetPwdSuccess:function (request, sender) {
        var _api = new Api();
        _api.setRetryTimes(0);//无限重试
        useLocal(function (local) {
            getTaskWorks(function (taskWorks) {
                _api.resetPwdSuccess(local.accountId, taskWorks.loginPassword, function (ret) {
                    notify('新密码保存成功');
                    M.global.report.clank.success({},sender)
                })
            })
        })
    },

    resetPwdFail:function (request, sender) {
        M.global.report.clank.error({status:4,finished_result:request},sender)
    },

    accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        M.global.report.clank.error({status:4,finished_result:'帐号无法登录'},sender)
    }

};