(function () {

    tabCommon.start('productDetail', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        var taskInfo = local.currentTask;
        lazy(function () {
            if(tabCommon.findStrByHref(taskInfo.item_code)){
                clue('找到了目标商品 开始评论浏览')

                tabPc.commentView()
                    .then(function () {
                        clue('评论浏览成功 点击店铺首页 30s后本页汇报任务完成')
                    }, function () {
                        clue('浏览评论失败了 尝试点击店铺首页 10s后汇报任务完成')
                    })
                    .then(function () {
                        clue('尝试点击店铺首页 10s后汇报任务完成')
                        tabPc.shopClick;
                        setTimeout(function () {
                            tabCommon.sm(taskVars, '.report.clank.success')
                        },30e3)
                    })
            }else{
                clue('不是目标商品');
                tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'不是目标商品'})
                // lazy(function () {
                //     tabCommon.gotoUrl("http://www.jd.com/");
                // })
            }
        })
    }

}})();