(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('couponA', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {

        var retryTimes = 5;

        label('打开了优惠券');
        lazy(function () {
            windowScrollToTarget($('.cate-more'),function () {
                $('.cate-more')[0].click();
                clue('准备领取');
                setTimeout(randCoupon,3e3);
            })
        })

        function randCoupon() {
            clue('剩余尝试领取次数'+retryTimes);
            if(retryTimes--<=0){
                tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'领取失败'})
                return false;
            }
            label('随机选择类目');
            var _clickIndex = random(0,$('.cate-list').find('a').length - 2);
            tabCommon.clickElement($('.cate-list').find('a').eq(_clickIndex));
            if(tabCommon.findElement($('#footer-2014'))){
                windowScrollToTarget($('#footer-2014'),function () {
                    setTimeout(getCoupon,3e3);
                })
            }else{
                setTimeout(getCoupon,3e3);
            }
        }

        function getCoupon() {
            label('寻找自营领取');
            if(tabCommon.findElement($(".q-range:contains('自营')"))){
                var _ele = $(".q-range:contains('自营')").parent('.q-type').find(".q-opbtns:contains('立即领取')");
                if(tabCommon.findElement(_ele)){
                    var _selected = random(0,_ele.length-1);
                    windowScrollToTarget(_ele.eq(_selected),function () {
                        checkSuccess();//开启成功监控
                        setTaskWorks({couponBatch:_ele.eq(_selected).find('a').attr('data-batch')},function () {
                            tabCommon.clickElement(_ele.eq(_selected).find('a'))
                        })
                    })
                }else{
                    clue('找不到自营中未领取的 重新选择');
                    randCoupon();
                }
            }else{
                clue('找不到自营的 重新选择');
                randCoupon();
            }
        }

        function checkSuccess() {
            tabCommon.mutation(bodyObserver,function (mutation) {
                //恭喜您，领取成功
                // tabCommon.sm(taskVars, '.report.clank.success')
                if(mutation.type == 'childList'){
                    if(mutation.target.className=='ui-dialog'){
                        if(mutation.previousSibling.className=='ui-dialog-content'){
                            console.log(mutation);
                            clue(mutation.target.innerText);
                            if(mutation.target.innerText.indexOf('领取成功')>-1){
                                tabCommon.clickElement($("a:contains('我的优惠券')").eq(1));
                                closeMe(5);
                                // tabCommon.sm(taskVars, '.report.clank.success')
                            }else if(mutation.target.innerText.indexOf('尚未登录')>-1){
                                clue('等待登录完成');
                            }else{
                                clue('无法领取 重新选择');
                                setTimeout(randCoupon,3e3);
                            }
                        }
                    }
                }
            })
        }


    }

}})();