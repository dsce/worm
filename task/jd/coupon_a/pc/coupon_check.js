(function () {

    tabCommon.start('couponA', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {

        label('检测领取结果');
        lazy(function () {
            getTaskWorks(function (taskWorks) {
                if(taskWorks.couponBatch){
                    var _find = false;
                    $('.c-del').each(function (i,ele) {
                        if($(ele).attr('onclick').indexOf("'"+taskWorks.couponBatch+"'")>-1){
                            _find = true;
                        }
                    })
                    if(_find) {
                        tabCommon.sm(taskVars, '.report.clank.success');
                    }else{
                        var checkTimes = taskWorks.checkTimes || 1;
                        if(checkTimes>=2){
                            tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'检测不到领取的优惠券'});
                        }else{
                            setTaskWorks({checkTimes:checkTimes+1},function () {
                                gotoA();
                            })
                        }
                    }
                }else{
                    gotoA();
                }
            })
        })

        function gotoA() {
            if(tabCommon.findElement($("a:contains('领取更多优惠券')"))){
                tabCommon.clickElement($("a:contains('领取更多优惠券')"));
            }else{
                window.open("//a.jd.com");
            }
        }

    }

}})();