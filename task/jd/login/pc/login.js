(function () {
    var observerId = tabCommon.observer($(".login-form")[0])
    tabCommon.start('login', taskStartRun)

    function taskStartRun(loginTask) {
    var taskVars=loginTask.taskVars;
    run(loginTask);
    function run(loginTask) {
        lazy(function () {
            //匿名函数开始

            setSharedTaskWorks({loginType:'account'});
//京东登陆
var submit_username = null;
var submit_password = null;
var user_disabled = false;


var observer_config = {
    //attributes: true,
    childList: true,
    //characterData: true,
    //attributeOldValue: true,
    //characterDataOldValue: true,
    subtree: true
}
// addMutationObserver($(".login-box")[0],function(mutation){
tabCommon.mutation(observerId,function (mutation) {
    console.log(mutation.type);
    console.log(mutation.target);
    console.log(mutation);

    if(mutation.type == 'childList'){
        if(mutation.target.className == 'msg-wrap' && mutation.addedNodes.length > 0){
            lazyAutoRun(function(local){
                var msg_error = $(".msg-wrap .msg-error").text();
                if(msg_error.indexOf("请刷新页面后重新提交") != -1){
                    //存在多余cookie _t
                    clue("等待删除多余cookie, 稍后刷新");
                    tabCommon.sm(taskVars, '.tabs_remove_cookies', {details: {url: document.location.href, name: "_t"}});
                    lazy(function(){
                        location.reload(true);
                    });
                }else if(msg_error.indexOf('账户名不存在，请重新输入') != -1
                    || msg_error.indexOf('你的账号因安全原因被暂时封锁') != -1
                    || msg_error.indexOf("密码错误10次，您可以") != -1) //密码错误10次，您可以找回密码，或20分钟后再试。

                {                    


                    if(msg_error.indexOf('账户名不存在，请重新输入') != -1){

                        getTaskWorks(function(tWorks){
                            if(local.task.email && !tWorks.email_logined){
                                //用的邮箱登录
                                clue(local.task.email + '邮箱登录失败：账户名不存在，使用账号登录试试');
                                setTaskWorks({email_logined:true},function(){
                                    //使用账号登录
                                    setTimeout("location.reload();",3*1000);
                                    return false;
                                })
                            }else{
                                clue('使用账号登录失败:账户名不存在 ' + submit_username);
                                if((local.task.username == submit_username  || local.task.email == submit_username) && local.task.password == submit_password){
                                //账号重置
                                if(!user_disabled){
                                        user_disabled = true;
                                        console.log('账号重置');
                                        tabCommon.sm(taskVars, '.disable_account', msg_error);
                                    }

                                }
                            }
                        })
                        
                    }else{
                        //提示账号密码错误，
                        if((local.task.username == submit_username  || local.task.email == submit_username) && local.task.password == submit_password){
                            //账号重置
                            if(!user_disabled){
                                user_disabled = true;
                                console.log('账号重置');
                                tabCommon.sm(taskVars, '.disable_account', msg_error);
                            }

                        }
                    }
                    
                }else if(msg_error.indexOf("账户名与密码不匹配，请重新输入") != -1){
                    var username_password_unmatch_count = local.task.username_password_unmatch_count != undefined ? local.task.username_password_unmatch_count : 1;

                    if(username_password_unmatch_count > 3){
                        if(!user_disabled){
                            user_disabled = true;
                            console.log('账号重置');
                            tabCommon.sm(taskVars, '.disable_account', msg_error);
                        }
                    }else{
                        local.task.username_password_unmatch_count = username_password_unmatch_count + 1;
                        setLocal({task: local.task}, function(){
                            lazy(function(){
                                location.reload(true);
                            });
                        });
                    }


                }
            });

        }else if(mutation.target.className == "qrcode-img" && mutation.addedNodes.length > 0){
            //切换TAB选账户登录兼容出现默认扫码登录
            $(".login-form .login-tab a:contains('账户登录')")[0].click();
        }

    }
});

addListenerMessage(function (request) {
    console.log(request);
    if (request.act == 'business_account_ready') {
        useLocal(inputUsername);
    }else if(request.act == "https_tabs_verify_code_result"){
        writing($("#authcode"), request.text, null);
    }
});


//Task();
autoRun(sd__index)

function sd__index(local) {
    updateHostStatus(1301000);

    if(document.referrer.indexOf('i.jd.com/user/info') != -1){
        location.href = 'http://i.jd.com/user/info';
    }

    //自动登录
    $("#autoLogin").prop("checked", true);
   

    loginReady();

    $('#authcode').on('keyup', function () {
        if ($('#authcode').val().length >= 4) {
            $('#loginsubmit')[0].click();
            setTimeout(function(){
                console.log('登录超时,刷新');
                location.reload();
            },20000);
        }
    }).focus()

    $('#JD_Verification1').on('click', function () {
        $('#authcode').focus()
    })
}

function inputUsername(local) {
    getTaskWorks(function(tWorks){
        if(tWorks.email_logined){
            var u_name = local.task.username;
        }else{
            var u_name = local.task.email ? local.task.email : local.task.username;
        }
    
    
        clue('u_name : ' + u_name);
        writing($('#loginname'), u_name, function () {

            inputPassword(local);

        });

    })
}
function inputPassword(local) {
    writing($('#nloginpwd'), local.task.password, submit);
}
function submit() {
    if($('#authcode:visible').length > 0){
        updateHostStatus(1301001);

        console.log("有验证码");
        clue("有验证码,等待打码");
        //JD_Verification1

        //getBlobImg()

        //发送打码
        //chrome.runtime.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgSrc});

        //var imgSrc = $("#JD_Verification1").attr("src2") + '&yys='+new Date().getTime();
        //chrome.runtime.sendMessage({act: 'convert_image_url_to_data_url', imgsrc: imgSrc});

        //打码前,开启监听
        tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginListener', null, function(){
            var imgSrc = $("#JD_Verification1").attr("src2") + '&yys='+new Date().getTime();
            getCrossDomainAuthCodeBase64(imgSrc, function(base64){
                //console.log(base64);
                tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginRemoveListener', null, function(){
                    //关闭监听,准备打码
                    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                });

            });
        });



    }
    submit_username = $("#loginname").val();
    submit_password = $("#nloginpwd").val();

    //增加登录标识
    accountLoginLog('pc', function(){
        $('#loginsubmit')[0].click();
    })
    

    setTimeout(function(){
        console.log('登录超时,刷新');
        location.reload();
    },20000);
}

function loginReady(){
    tabCommon.sm(taskVars, '.getBusinessAccount');
}

            //匿名函数结束
        })
    }

}})();