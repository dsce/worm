(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('login', taskStartRun)

    function taskStartRun(loginTask) {
    var taskVars=loginTask.taskVars;
    run(loginTask);
    function run(loginTask) {
        lazy(function () {
            //匿名函数开始

//jd/m/login.js
var verify_code_visible = false;
var verify_code_error = false;
var verify_cid = 0;
var write_full = false;
var get_code_result = false;
var udata = null;
var result = new Array;
var err_rewrite = false;
var api_plat = ['云打码','优优云'];


tabCommon.mutation(observerId,function (mutation) {
    //console.log(mutation.type);
    //console.log(mutation.target);
    //console.log(mutation);
    if(mutation.type == 'childList'){
        //console.log(mutation.target);
        //console.log(mutation);
        if(mutation.target.className == "notice" && mutation.addedNodes.length > 0){
            if($(".notice:contains('验证码错误')").length > 0){
                Run(function(){
                    if(get_code_result == 1){
                        //验证码错误，报错
                        verify_code_error = true;
                        chrome.extension.sendMessage({act: 'https_tabs_verify_fail', cid: verify_cid});
                    }else if(get_code_result == 2){
                        //报告给优优云平台
                        if(udata){
                            chrome.extension.sendMessage({act: 'https_tabs_verify_fail_to_youyouyun', udata: udata});
                        }  
                    }
                    //console.log(result);
                    // var index = parseInt(get_code_result);
                    // //console.log(index);
                    // var key = index == 1 ? 2:1;
                    // if(!err_rewrite && result[key] != undefined){
                    //     clue(api_plat[index-1] + '打码结果错误，使用' + api_plat[key-1] + '结果');
                    //     writing($("#code"), result[key],function(){
                    //         err_rewrite = true;
                    //     });
                    // }else{
                    //     clue('验证码识别错误,2S后刷新');
                    //     setTimeout(function(){
                    //         window.location.reload();
                    //     },2000)
                        
                    // }
                    clue('验证码识别错误,2S后刷新');
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                });

            }else if($(".notice:contains('账户名或密码不正确')").length > 0 || $(".notice:contains('账号或密码不正确')").length > 0 || $(".notice:contains('您的账号存在风险')").length > 0  || $(".notice:contains('您的账号因安全原因被暂时封锁')").length > 0){
                //账户名和密码不匹配  账号重置
                Run(function(){
                    var msg = $(".notice").text();
                    console.log(msg);
                    tabCommon.sm(taskVars, '.disable_account', msg);
                });

            }else{
                //登陆错误问题
                var msg = $(".err-msg").text();

                Run(function(){
                    console.log(msg);

                    if(msg.indexOf('账户名不存在') != -1 || msg.indexOf('账户名或密码不正确')!=-1){

                        if(msg.indexOf('账户名不存在') != -1){

                            getTaskWorks(function(tWorks){
                                if(local.task.email && !tWorks.email_logined){
                                    //用的邮箱登录
                                    clue(local.task.email + '邮箱登录失败：账户名不存在，使用账号登录试试');
                                    setTaskWorks({email_logined:true},function(){
                                        //使用账号登录
                                        setTimeout("location.reload();",3*1000);
                                        return false;
                                    })
                                }else{
                                    clue('使用账号登录失败:账户名不存在 ' + local.task.username);
                                    tabCommon.sm(taskVars, '.disable_account', msg);
                                }

                            })
                        }else{
                            tabCommon.sm(taskVars, '.disable_account', msg);
                        }
                        
                    }

                });
            }
        }else if(mutation.target.className == 'pop-msg' && mutation.addedNodes.length > 0){
            var pop_msg = $(".pop-msg:visible").text();
            if(pop_msg.indexOf('您的账号存在安全风险') != -1 || pop_msg.indexOf('您的账号存在风险，为了账号安全需要短信验证，是否继续') != -1 || pop_msg.indexOf('您尚未绑定手机号，为了您的账号安全，需绑定手机号，是否继续') != -1){
                // clue('您的账号存在安全风险','error');
                // return false;
                Run(function(){
                    //您的账号存在安全风险，请进行短信登录验证您的账号存在风险，为了账号安全需要短信验证，是否继续
                    tabCommon.sm(taskVars, '.disable_account', pop_msg);
                });
            }
        }
    }else if(mutation.type == 'attributes'){
        if(mutation.target.id == 'pop-confirm'){
            var pop_msg = $("#pop-confirm .pop-msg:visible").text();
            if(pop_msg.indexOf('您的账号存在安全风险') != -1){
                Run(function(){
                    //您的账号存在安全风险，请进行短信登录验证
                    tabCommon.sm(taskVars, '.disable_account', pop_msg);
                });
            }
        }
    }
});

Task(sd__index);

function sd__index(local) {
    var task = local.task;

    //地址中包含home.jd.com
    //https://plogin.m.jd.com/user/login.action?appid=100&returnurl=http://home.m.jd.com/myJd/home.action?sid=4fcd62d17b6d3e94cc5912e69663da44
    if(location.href.indexOf('home.m.jd.com') > -1){
        location.href = decodeURIComponent(location.href).replace('http://home.m.jd.com/myJd/home.action', 'http://m.jd.com/');
    }


    updateHostStatus(1402200);//登陆-手机

    addListenerMessage(function (request) {
        console.log(request);
        if (request.act == 'business_account_ready') {
            useLocal(function(local){
                var task = local.task;
                inputLogin(local.task);
            });
        }
    });
    tabCommon.sm(taskVars, '.getBusinessAccount');



    //有验证码，放大一下
    if ($('#imgCode:visible').length > 0) {
        //200*76
        $("#input-code").height(76);
        $("#code").width('50%').height(76);
        $("#input-code .code-box").css('right',100);
        $("#imgCode:visible").width(200).height(76);


        //打码结果
        addListenerMessage(function (request) {
            console.log(request);
            verify_cid = verify_cid ? verify_cid : request.cid;
            udata = udata ? udata : request.udata;
            //2次结果都接收
            if(request.act == 'https_tabs_verify_code_result'){
                result[request.type] = request.text;
            }
            if (request.act == 'https_tabs_verify_code_result') {
                get_code_result = request.type;
                request.type == 2 ? clue('打码来自优优云'):clue('打码来自云打码');
                //add 20160325
                console.log('核对已填写的账号密码');

                var username = $('#username').val();
                var password = $("#password").val();

                //console.log(username + password);
                if(username != task.username){
                    if(task.email != username && task.email){
                        $("#username").val(task.email);
                    }else{
                        clue(task.username);
                        $("#username").val(task.username);
                    }
                    
                }
                if(password != task.password){
                    $("#password").val(task.password);
                }
                //console.log(username + password);
                //add end

                writing($("#code"), request.text);
            }else if(request.act == 'https_tabs_verify_code_result_errno'){

                var base64 = getBlobImg($("#imgCode")[0]);
                var act = 'https_tabs_verify_code_by_base_from_youyouyun';
                getErrorNo(request,base64,act);
                
                //云打码失败
                // var api_type = '云打码';
                // if(request.errno == -1007){
                //     clue(api_type + '余额不足');
                // }else if(request.errno <= -1001){
                //     clue(api_type + '密码错误,或秘钥有误');
                // }else if(request.errno == 'timeOut'){
                //     clue(api_type + '打码超时！');
                // }
                // //使用优优云平台
                // clue('云打码失败喽，使用优优云平台');
                // var base64 = getBlobImg($("#imgCode")[0]);
                // chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base_from_youyouyun', base: base64});

            }else if(request.type == 2 && request.act == 'https_tabs_verify_code_result_errno'){
                // var api_type = '优优云';
                // if(request.errno == '-6'){
                //     clue(api_type + '用户点数不足，请及时充值');
                // }else if(request.errno == '-4'){
                //     clue(api_type + '账户被锁定');
                // }else if(request.errno == '-1'){
                //     clue(api_type + '软件ID或KEY无效或者用户名为空或密码为空');
                // }else if(request.errno == 'timeOut'){
                //     clue(api_type + '打码超时！');
                // }
                // clue('优优云平台打码也失败，3分钟后刷新');
                // setTimeout(function(){
                //     window.location.reload();
                // },3*60*1000);
                
                
            }
        });
    }

    //验证码输入够4位直接登录
    $('#code').on('keyup', function () {
        if ($('#code').val().length >= 4) {
            //$(".btn-login").removeClass('btn-disabled');
            clue("4位 自动登录");
            lazy(function () {
                useLocal(function(local){
                    if(local.isRunning){
                        logining()
                    }else{
                        setLocal({isRunning: true}, function(){
                            logining();
                        });
                    }
                });

            }, 2);

        }
    }).focus()

}

function autoVerifyCode() {
    //准备开始打码
    clue('自动打码');
    updateHostStatus(1402200);

    //拿cookie
    //chrome.extension.sendMessage({act: 'tabs_get_cookies', details: {domain: 'plogin.m.jd.com', name: 'lsid'}});
    //chrome.extension.sendMessage({act: 'tabs_get_cookies', details: {url: location.href, name: 'lsid'}});

    var base64 = getBlobImg($("#imgCode")[0]);
    //console.log(blob);
    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
    //新平台打码
    // chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base_from_youyouyun', base: base64});

    //var imgsrc = location.origin + $("#captcha-img img").attr('src') + "&v="+Math.LN2;
    //console.log('get img');
    //var xhr = new XMLHttpRequest();
    //xhr.open('GET', imgsrc, true);
    //xhr.responseType = 'blob';
    //xhr.onload = function(e) {
    //    if (this.readyState==4){
    //        if (this.status == 200) {
    //            console.log(this);
    //            console.log(e);
    //
    //            var imgblob = this.response;
    //            console.log(imgblob);
    //            $(".qq").append('<img id="qqimg" src="" />');
    //            $("#qqimg")[0].src = URL.createObjectURL(imgblob);
    //            chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_blob', imgblob: imgblob});
    //        }else{
    //            console.warn('get img result error',this.response);
    //            location.reload();
    //        }
    //    }else{
    //        console.warn('get img error',this);
    //        location.reload();
    //    }
    //};
    //xhr.send();

}

function inputLogin(task){
    getTaskWorks(function(tWorks){
        if(tWorks.email_logined){
            var u_name = task.username;
        }else{
            var u_name = task.email ? task.email : task.username;
        }
   
    
        clue('u_name : ' + u_name);
        writing($("#username"), u_name, function () {
            writing($("#password"), task.password, function () {


                if($('#imgCode:visible').length > 0){
                    autoVerifyCode();
                    updateHostStatus(1402201);
                }else{
                    logining();
                }

            })
        })

     })
}

function logining(){
    $("#loginBtn").addClass('btn-active');
    accountLoginLog('m', function(){
        lazy(function(){
            clicking($("#loginBtn"));
        }, 5);

    })
}

            //匿名函数结束
        })
    }

}})();