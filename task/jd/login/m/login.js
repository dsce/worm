(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('login', taskStartRun)

    function taskStartRun(loginTask) {
    var taskVars=loginTask.taskVars;
    run(loginTask);
    function run(loginTask) {
        lazy(function () {
            //匿名函数开始

//jd/m/login.js
var verify_code_visible = false;
var verify_code_error = false;
var verify_cid = 0;
var write_full = false;


tabCommon.mutation(observerId,function (mutation) {
    console.log(mutation.type);
    console.log(mutation.target);
    console.log(mutation);
    if (mutation.type == "attributes") {
        if (mutation.target.id == 'code' && mutation.attributeName == 'src') {
            Run(function(){
                verify_code_visible = true;
                if(verify_code_error){
                    verify_code_error = false;
                    autoVerifyCode();
                }

            });

        }
    }else if(mutation.type == 'characterData'){
        if(mutation.target.data == "图片验证码错误"){
            //验证码错误，报错
            verify_code_error = true;
            chrome.extension.sendMessage({act: 'https_tabs_verify_fail', cid: verify_cid});
        }
    }
});

Task(sd__index);

function sd__index(local) {
    var task = local.task;

    updateHostStatus(1402200);//登陆-手机

    //$("#password").val(task.password);
    //$("#username").val(task.username);

    addListenerMessage(function (request) {
        console.log(request);
        if (request.act == 'business_account_ready') {
            useLocal(inputUsername);
        }else if (request.act == 'https_tabs_verify_code_result') {
            verify_cid = request.cid;
            writing($("#validateCode"), request.text);
        }
    });
    tabCommon.sm(taskVars, '.getBusinessAccount');


    //有验证码，放大一下
    if ($('#code').length > 0) {

    }

    //验证码输入够4位直接登录
    $('#validateCode').on('keyup', function () {
        if ($('#validateCode').val().length >= 4) {
            setTimeout(function () {
                $('#loginSubmit')[0].click()
            }, 2000)

        }
    }).focus()
}

function inputUsername(local){
    var u_name = local.task.email ? local.task.email : local.task.username;
    clue('u_name : ' + u_name);
    writing($("#username"), u_name, function () {
        inputPassword(local.task.password);
    })
}

function inputPassword(password) {
    writing($("#password"), password, function () {
        write_full = true;
        if(verify_code_visible){
            autoVerifyCode();
        }

    })
}

function autoVerifyCode() {
    //准备开始打码
    clue('自动打码');
    updateHostStatus(1402200);
    var imgsrc = location.origin + $("#code").attr('src');
    chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc});
}

            //匿名函数结束
        })
    }

}})();