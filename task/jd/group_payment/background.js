//background.js-单独任务背景页

M.group_payment = {
	//保存包状态
	saveGroupOrderState: function (request,sender) {
		var API = new Api();
        API.saveGroupOrderState(function(ret){
            if(ret.success == 1){
                notify('包状态汇报成功');
                TC.done(sender,{taskType:'payment'});
                // setTimeout(function(){
                //     chrome.tabs.remove(sender.tab.id);
                // },5*1000)
            }else{
                notify('包状态汇报失败：' + ret.message);
            }
        },function(){
            M.group_payment.saveGroupOrderState(request);
        })
	},

    //saveGroupBusinessOrdersInfo

    saveGroupBusinessOrdersInfo: function (request,sender) {
        var API = new Api();
        API.saveGroupBusinessOrdersInfo({order_info:request},function(ret){
            if(ret.success == 1){
                notify('保存订单包平台订单成功');
                
            }else{
                notify('保存订单包平台订单失败：' + ret.message);
            }
        },function(){
            M.group_payment.saveGroupBusinessOrdersInfo(request);
        })
    }

	



};

