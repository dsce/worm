(function () {

    tabCommon.start('group_payment', taskStartRun)

    function taskStartRun(localPay) {
    var taskVars=localPay.taskVars;
    run(localPay);
    function run(localPay) {
        lazy(function () {
            //匿名函数开始
            var opened_at = new Date().getTime();
var indexed = false;
var selected = false;

//var observer = new MutationObserver(function(mutations) {
//    mutations.forEach(function(mutation) {
//        // console.log(mutation.type);
//        // console.log(mutation.target);
//        // console.log(mutation);
//        if(mutation.type === 'attributes' && !indexed){
//            if(mutation.target.className == 'paybox-newcard animate-enter animate-enter-active'){
//               indexed = true;
//               Run(function(){
//                     if($(".pn-new:contains('网银支付')").length >0){
//                        $(".pn-new:contains('网银支付')")[0].click();
//
//                        //获取银行信息
//                        chrome.runtime.sendMessage({act:'get_payment_info'});
//
//                    }
//               })
//
//            }else if(mutation.target.className == 'pay-newUser animate-enter animate-enter-active'){
//                indexed = true;
//                Run(function(){
//                    if($(".pn-c-text:contains('网银支付')").length >0){
//                        $(".pn-c-text:contains('网银支付')")[0].click();
//                    }
//
//                    //获取银行信息
//                    chrome.runtime.sendMessage({act:'get_payment_info'});
//
//                })
//
//            }else if(mutation.target.id == 'modal_newCardWangyin' && !selected){
//                // selected = true;
//                // if($("#modal_newCardWangyin").length >0){
//                //  $("#modal_newCardWangyin .ui-tab-items a:contains('网银支付')")[0].click();
//                //  setTimeout(function(){
//                //      select_bank(bank);
//
//                //  },3*1000)
//
//                // }else{
//                //  clue('找不到付款区域');
//                // }
//
//            }
//        }
//
//    });
//});

//var observer_config = {
//    attributes: true,
//    // childList: true,
//    // characterData: true,
//    // attributeOldValue: true,
//    // characterDataOldValue: true,
//    subtree: true
//}
//
//if($(".pay-load").length >0){
//    observer.observe($(".pay-load")[0], observer_config);
//}



//京东支付页面
var btnSubmitNum = 0;
var btnSubmit = $('#paySubmit');

var bank,bankUsername;

//监听消息
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        console.log(request);
        //clue('监听到返回消息');
        //updateHostStatus(1414002);
        if (request.act == 'get_payment_info_result') {
            if(request.error == 1){
                console.log("没有设置银行用户信息！");
                updateHostStatus(1414001);
                notifyMessage("没有设置银行用户信息！");  
            }else{
                var bank_info = request.data;
                if(bank_info.bank_code && bank_info.card_no){
                    clue('银行类别:' + bank_info.bank_code + ', 卡号:' + bank_info.card_no);
                    bank = bank_info.bank_code.toLowerCase();
                    chrome.storage.local.set({'bank_info':bank_info},function(){
                        //选择银行付款
                        setTaskWorks({card_no:bank_info.card_no,bank_user:bank_info.bank_user},function(){
                            setTimeout(function(){
                                select_bank_to_pay(bank);
                            }, 8000);
                        })
                            
                       
                    })
                    
                }else{
                    notifyMessage("银行用户信息设置不全！");  
                }
            }
            
        }
    }
);

//检测实名制
userDeclare(function(){
    index();
});




function index(){
    updateHostStatus(1414000);

    $('#paySubmit').hide();//隐藏 支付按钮

    // //优先使用接口银行信息
    // if(localPay.currentTask.bank_code && localPay.currentTask.bank_user){
    //     bank = localPay.currentTask.bank_code.toLowerCase();
    // }else{
    //     bank = local.pay.bank;
    // }


    // //识别来源页面，提交订单页面，跳转订单列表页面
    // var referrer = document.referrer;
    // if(referrer.indexOf('trade.jd.com/shopping/order') != '-1'){
    //     document.location.href = 'https://order.jd.com/center/list.action';
    //     return false;
    // }


    if($(".global-error-box:contains('返回商城')").length >0){

        updateHostStatus(1502004);//选银行失败

        if($("a:contains('我的订单')").length >0 && $("a:contains('我的订单')").attr('href').indexOf('order.jd.com') != -1){
            $("a:contains('我的订单')")[0].click();
        }else{
            location.href = 'https://order.jd.com/center/list.action';

        }

        return false;
    }


    //只执行已确认的订单号
    orderId = get_page_order_id();

    clue("订单号: "+orderId);

    //识别来源页面，提交订单页面，跳转订单列表页面
    var referrer = document.referrer;

    getTaskWorks(function(){



        if(!task.jd_order_id){
            
            getSharedTaskWorks(function(shareTask){

                if((shareTask.jd_order_id === undefined && shareTask.jd_order_id!=orderId) || (referrer.indexOf('trade.jd.com/shopping/order') != '-1') ){
                    setSharedTaskWorks({first_submit_order_id:orderId},function(){
                        document.location.href = 'https://order.jd.com/center/list.action';
                        return false;
                    })
                }else{
                    setTaskWorks({jd_order_id:shareTask.jd_order_id},function(){
                        checkPayLoad();
                    })
                }

            })

        }else{
           checkPayLoad(); 
        }

     })
   

    
   
    

    //保存订单号  先进订单详情保存订单号
    //var order_id = {order_id:$("#orderId").val()};
    //setTaskWorks(order_id,function(){
    //    updateHostStep(_host_step.payment);//step7 去付款;
    //});

    
    // addListenerMessage(function (request) {
        
    // });

    
}

function get_page_order_id(){
    var orderId = $("#orderId").val();
    var jdOrderId = $("#jd_order_id").val();
    if($(".main .order .o-tips-item:contains('订单号')").length >0){
        //HK全球购
        var pcashierOrderId = $(".main .order .o-tips-item:contains('订单号')").text().match(/\d+/)[0];
    }else if($(".main .order .o-title:contains('订单号')").length >0){
        var pcashierOrderId = $(".main .order .o-title:contains('订单号')").text().match(/\d+/)[0];
    }

    //兼容新版0329
    orderId=orderId?orderId:jdOrderId;

    //兼容新版0410
    orderId=orderId ? orderId : pcashierOrderId;

    return orderId;
}

function checkPayLoad(){
    if($(".pay-load").length > 0){

        setTimeout(findPaymentBank, 5000);

    }else{
        chrome.runtime.sendMessage({act:'get_payment_info'});
    }
}

function findPaymentBank(){
    //预防超时
    wangwang();
    var pay_btn = $("body a:contains('网银支付'):visible");

    if(pay_btn.length > 0){
        //获取银行信息
        chrome.runtime.sendMessage({act:'get_payment_info'});
    }else{
        if(opened_at+60000<new Date().getTime()){
            location.reload(true);
        }else{
            setTimeout(findPaymentBank, 5000);
        }
    }
}

//选择银行付款
function select_bank_to_pay(bank){
     // var task = localPay.currentTask;
    // var pay = local.pay;

        if(bank == 'icbc'){
            tabCommon.sm(taskVars, '.icbcChangeUseragentStart');//工商银行替换UserAgent
        }else if(bank == 'comm'){
            bank = 'bcom';//交行转换代码
        }

        if($("#modal_newCardWangyin:visible").length >0){
            $("#modal_newCardWangyin .ui-tab-items a:contains('网银支付')")[0].click();
            setTimeout(function(){
                select_bank(bank);
                return false;
            },3*1000)
            
        }

        //银联支付吧
        //if(bank == 'boc' && local.pay.use_online_pay && localPay.currentTask.worldwide != '1'){
        //    //如果是M端清除UA
        //    if(localPay.currentTask.is_mobile == '1'){
        //        tabCommon.sm(taskVars, '.remove_useragent_listener');
        //    }
        //
        //    if($("#ebankPaymentListDiv .bw-more-unionpay a:contains('银联在线支付')").length >0){
        //        $("#ebankPaymentListDiv .bw-more-unionpay a:contains('银联在线支付')")[0].click();
        //    }else{
        //        clue('未找到银联在线支付','error');
        //    }
        //    $('#bank-'+bank)
        //
        //    setTimeout(function(){
        //        pay_finish();
        //    },5000);
        //
        //    return false;
        //}

        var li_bank = $('#ebankPaymentListDiv li[clstag="payment|keycount|bank|c-'+bank+'"]');

        //兼容新版0329
        var span_bank = $('.nm-tab-content-wangyin .bank-logo[id=bank-'+bank+']');

        if(li_bank.length >0){

            clue(li_bank.text());
            //clicking(li_bank);
            li_bank[0].click();//点击银行

            var select_id = $("#seletedAgencyCode").next().attr('id');
            if(select_id === ("bank-"+bank)){
                btnSubmitNum++;
                if(btnSubmitNum == 1){
                    //保存订单号,,不在这个地方保存订单号,统一在订单详情确认订单后保存订单号
                    var orderId = get_page_order_id();
                    getTaskWorks(function(task){
                        if(task.jd_order_id == orderId){
                            clue('订单 ['+orderId+'] 立即支付');
                            var need_pay_price = $(".order .o-price strong").text();
                            clue('应付金额：' + need_pay_price);
                            btnSubmit.click();//立即支付
                            //clicking(btnSubmitNum);
                        }else{
                            clue("订单号错了,回订单列表吧", 'error');
                        }
                    });

                }

                setTimeout(function(){
                    pay_finish();
                },5000);
            }
        }else if(span_bank.length>0){
            //兼容新版0329
            clue(span_bank.text());
            span_bank[0].click();

            setTimeout(function(){
                //submit
                $('.nw-form-wangyin input[type=submit]')[0].click()

                clue('5s后，跳转订单详情');
                setTimeout(function(){
                    location.href = '//order.jd.com/center/list.action';
                },5000);
            },2000);

            return false;

        }else{
            //选择银行失败
            clue('选择银行失败');
            updateHostStatus(1502004);
            // setTaskWorks({payment_clicked:false},function(){})
            close_this_tab();
            
            
            // //中行银联付款
            // if(bank == 'boc' && localPay.currentTask.worldwide != '1'){
            //
            //     //如果是M端清除UA
            //     if(localPay.currentTask.is_mobile == '1'){
            //         tabCommon.sm(taskVars, '.remove_useragent_listener');
            //     }
            //
            //     if($("#ebankPaymentListDiv .bw-more-unionpay a:contains('银联在线支付')").length >0){
            //         $("#ebankPaymentListDiv .bw-more-unionpay a:contains('银联在线支付')")[0].click();
            //     }else{
            //         clue('未找到银联在线支付','error');
            //     }
            //
            //     setTimeout(function(){
            //         pay_finish();
            //     },5000);
            //
            //     return false;
            // }else{
            //     //选择银行失败
            //     clue('选择银行失败');
            //     updateHostStatus(1502004);
            //     close_this_tab();
            // }
        }
        
        // }else{
        //     //选择银行失败
        //     clue('选择银行失败');
        //     updateHostStatus(1502004);
        //     close_this_tab();
    // }
    // if(pay !=''){

        // if('bank' in pay&&pay.bank=='null'){
        //     clue('手动选择付款');
        //     $('#paySubmit').show();//显示 支付按钮
        // }else{

            
        // }
    // }else{
    //     console.log("没有设置银行用户信息！");
    //     updateHostStatus(1414001);
    //     notifyMessage("没有设置银行用户信息！");
    // }
}

function select_bank(bank){
    var li_bank = $(".payment-list:visible span[id=bank-" + bank +"]");

    if(li_bank.length >0){

        clue(li_bank.text());
        //clicking(li_bank);
        li_bank[0].click();//点击银行

        
        //保存订单号,,不在这个地方保存订单号,统一在订单详情确认订单后保存订单号
        var orderId = $(".o-tips-item:contains('订单号')").text().match(/\d+/)[0];
        getTaskWorks(function(task){
            if(task.jd_order_id == orderId){
                clue('订单 ['+orderId+'] 立即支付');
                var need_pay_price = $(".o-price:contains('应付金额') strong").text();
                clue('应付金额：' + need_pay_price);
                
                if($(".ui-form-line .ui-form-group input[value='跳转网银并支付']").length >0){
                    //提交
                    $(".ui-form-line .ui-form-group input[value='跳转网银并支付']")[0].click();
                }
            }else{
                clue("订单号错了,回订单列表吧", 'error');
            }
        });

         

            setTimeout(function(){
                pay_finish();
            },5000);
       

    }else{
            //选择银行失败
            clue('选择银行失败,再次选择');
            select_bank(bank);
            // updateHostStatus(1502004);
            // close_this_tab();
        
    }
}

//点击支付完成
function pay_finish(){

    if(location.href.indexOf('pay.jd.hk') != -1 || (location.href.indexOf('pcashier.jd.com') != -1) && $(".pay-load").length >0 ){
        clue('5s后，跳转订单详情');
        setTimeout(function(){
            location.href = '//order.jd.com/center/list.action';
        },5000);
    }else{
        var payFinishInterval = setInterval(function(){
        if($("#wangyinPaySuccess:visible").length > 0){         
            $("#wangyinPaySuccess .wangyin-payed-success .wps-button a")[0].click();
                 clearInterval(payFinishInterval);
            }
        },500);
    }
    
    
}


//填写实名制信息
function userDeclare(callback){
    if($("#globalUserName:visible").length > 0){
        useLocal(function(local){
            var task = local.task;
            writing($("#globalUserName"),task.identity_name,function(){
                writing($("#globalUserNo"),task.identity_card,
                    function(){
                        if($("#userDeclareModal .um-button a:contains('确认实名')").length >0){
                            $("#userDeclareModal .um-button a:contains('确认实名')")[0].click();
                            
                        }else if($(".ui-button-XL:contains('确认并完成支付')").length >0){
                            $(".ui-button-XL:contains('确认并完成支付')")[0].click();
                        }

                        setTimeout(function(){
                                checkUserDeclareErr(changeIdCard);
                            },3000);  
                    }
                )
            })
        })
    }else{
        callback && callback();
    }
}

//实名验证失败
function checkUserDeclareErr(callback){
    var reason = '';
    if($("#globalUserNameError:visible").length >0){
        reason = $("#globalUserNameError:visible").text();
        callback && callback(reason);
    }else if($("#globalUserNoError:visible").length >0){
        reason = $("#globalUserNoError:visible").text();
        callback && callback(reason);
    }else if($("#globalRealNameErrorInfo:visible").length >0){
        reason = $("#globalRealNameErrorInfo:visible").text();
        callback && callback(reason);
    }else{
        Task(index);
    }  
}

//重置实名信息
function changeIdCard(reason){
    if(reason){
        clue(reason);
        clue('重置实名信息');
        tabCommon.sm(taskVars, '.change_id_card',reason);
    }
}

//监听消息
addListenerMessage(function (request) {
  console.log(request);
  if (request.act == 'id_card_changed') {
    var identity = request.identity;
    var identity_name = identity.name;
    var identity_card = identity.identity_card;

    setTaskWorks({identity_name:identity_name,identity_card:identity_card},function(){
        window.location.reload();
    })
  }
});




            //匿名函数结束
        })
    }

}})();