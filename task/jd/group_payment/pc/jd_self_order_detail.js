(function () {

    tabCommon.start('group_payment', taskStartRun)

    function taskStartRun(localPay) {
    var taskVars=localPay.taskVars;
    run(localPay);
    function run(localPay) {
        lazy(function () {
            //匿名函数开始

//京东订单详情页面

//客户端信息
var client_infos = '';
var order_detail_id = null;
var submit_time = null;

//全局变量
var storage_data = null;

//支付成功
var pay_success = false;

var isHandWork = 0;


// var observer_config = {
//     attributes: true,
//     childList: true,
//     characterData: true,
//     attributeOldValue: true,
//     characterDataOldValue: true,
//     subtree: true
// };
// tabCommon.mutation(observerId,function(mutation){
//     //console.log(mutation.type);
//     console.log(mutation.target);
//     console.log(mutation);
//     if(mutation.type === 'childList'){
//         //console.log(mutation.target);
//         //console.log(mutation);
//         if(mutation.target.id == 'track_time_0' && mutation.addedNodes.length > 0){
//            //监控到提交订单时间
            
           
//         }
//     }
// },observer_config);

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        console.log(request);

        if(request.act == 'save_order_info_result'){
            // var payButtonA = $("#pay-button-" + order_detail_id + " a");
            // var payBtn = $("#pay-button-" + order_detail_id);
            // var hkPayBtn = $(".o-detail .cnt .tips .btn-pay");
            // sendResponse('pay-btn length = ' + payBtn.length);

            // if(payButtonA.length > 0){
            //     clicking(payButtonA);
            // }else if(payBtn.length >0){
            //     clicking(payBtn);
            // }else if(hkPayBtn.length > 0){
            //     clicking(hkPayBtn);//hk
            // }else{
            //     clue('未找到付款按钮','error');
            // }
            
            // close_this_tab();
        }else if(request.act == 'save_host_task_order_detail_result'){
            //提交订单数据成功
            // setTimeout(function(){
            //     if($("#nav .fore-3 .dd a:contains('我的级别')").length >0){
            //         $("#nav .fore-3 .dd a:contains('我的级别')")[0].click();
            //     }else{
            //         location.href = 'https://usergrade.jd.com/user/grade';
            //     }
            // },3*1000)
        }
   
    }
)

//监听付款状态结果
addListenerMessage(function(request){
    if(request.act == 'get_pay_state_result'){
        console.log(request);
        getSharedTaskWorks(function(task){
            if(task.jd_order_id == request.data.business_oid){
                if(request.data.pay_state == 3){//付款成功
                    clue('付款成功了');
                    do_reload_page();
                }else if(request.data.pay_state == 4){//代理失败
                    //标记付款失败
                    clue('付款失败了');
                    tabCommon.sm(taskVars, '.change_proxy');//更换代理

                }else if(request.data.pay_state == 5){//付款失败
                    //标记付款失败
                    clue('付款失败了');
                    updateHostStatus(1502003);
                    setSharedTaskWorks({payment_clicked:false},function(){
                        // setTaskWorks({entry_order_info:0},function(){ })
                            do_reload_page();
                       
                    })
                    // do_reload_page();

                }else{
                    //还没获取到付款状态，继续获取
                    console.log('正在获取付款状态，请等待...');
                    label('正在获取付款状态',180);
                    setTimeout(function(){
                        tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
                    },3000);   
                }
            }else{
                // clue('获取付款状态的订单号不一致');
                // do_reload_page();

                 wangwang();

                if(request.message.indexOf('未找到订单对应的有效付款流水') != -1){

                    task.no_pay_data_nums = task.no_pay_data_nums || 0;
                    if(task.no_pay_data_nums > 10){
                        setSharedTaskWorks({payment_clicked:false},function(){
                            console.log('未找到订单对应的有效付款流水,重置点击付款标识');
                            do_reload_page();
                        })
                    }else{
                        setTaskWorks({no_pay_data_nums:task.no_pay_data_nums++},function(){
                            do_reload_page();
                        })
                    }
                    
                    
                }else{
                    clue('获取付款状态的订单号不一致');
                    do_reload_page();
                }
            }
        })
        
        
    }else if(request.act == 'change_proxy_result'){
        //更换代理成功
        updateHostStatus(1502003);//标记付款失败
        do_reload_page();
    }
})

index();

function index(local) {

    getTaskWorks(function(task){



    getSharedTaskWorks(function(shareTask){

    storage_data = local;
    order_detail_id = $("#orderid").val();
    if(order_detail_id == task.opened_rand_business_oid){
        clue('随机进入的订单详情，3s后去我的订单');
        if($("#shortcut-2014 a:contains('我的订单')").length >0 && $("#shortcut-2014 a:contains('我的订单')").attr('href').indexOf('order.jd.com/center/list.action') != -1){
            setTimeout(function(){
                $("#shortcut-2014 a:contains('我的订单')")[0].click();
                close_this_tab();
            },3000);
        }else{
            setTimeout(function(){
                location.href = 'https://order.jd.com/center/list.action';
            },3*1000)
            
        }

        return false;
    }
    console.log(order_detail_id);
    submit_time = $("#datesubmit-"+ order_detail_id).val();

    //兼容全球购获取订单号
    // if(!order_detail_id && $(".o-detail").attr('orderid')){
    //     order_detail_id = $(".o-detail").attr('orderid');
    // }



    if(shareTask.jd_order_id === undefined){
        //还未确认订单号,核对商品详细信息
        checkOrderItemInfo(local);
        return false;
    }else{
        //核对订单号
        if (shareTask.jd_order_id == order_detail_id) {
            // showPaymentButton(order_detail_id);
            clue('订单号:'+ order_detail_id);
            //付款状态
            var orderState = $('#state-span');
            // var orderStateTxt = $(".order-state .state-txt");
            // var hkOrderStatus = $(".o-detail .cnt .status");

            // if(!shareTask.payment_clicked){
            //      payment();
            //      return false;
            // }


            if(orderState.length > 0){
                var pay_status = orderState.text();
            
            }else{
                clue("不能找到订单状态", "error");
                setTimeout(function(){
                    location.reload();
                },5*1000);
                return false;
            }

            clue("订单状态 :" + pay_status);

             //保存订单当前审核状态数据
            var check_order_data = {
                business_oid : order_detail_id,
                business_order_at : submit_time,
                status : pay_status,
                // status:'已收货',
                type : 2,//pop单
            }
            tabCommon.sm(taskVars, '.save_business_check_order',check_order_data, function(){
               console.log("保存审核状态数据完成");
            });
            
            if (pay_status == "付款成功" || pay_status == "正在出库" || pay_status == '等待厂商处理') {
                //确认付款状态 保存数据
                updateHostStatus(1602000);//确认付款

                pay_success = true;
                //保存数据
                order_ready_to_save();

            } else {
                //固定标签页面
                pinnedThisTab();
                pay_success = false;
                //判断状态 等待 付款完成状态
                if (localPay.host_status > 1501000) {//大于1501000(银行登录) ，银行已登录
                    if (localPay.host_status == 1502001 || localPay.host_status == 1502002 || localPay.host_status == 1502003 || localPay.host_status == 1502004 || localPay.host_status==1502005) {
                        updateHostStatus(1601000);
                        //支付失败，重新去支付
                        clue("支付失败，重新支付");
                        payment();
                        //已重新支付，关闭当前
                        // setTimeout(close_this_tab, 5000);
                        return false;
                    }

                }

                if(!shareTask.payment_clicked){
                     payment();
                     return false;
                }

                //预 保存订单详细数据
                var order_info = orderInfo();

                //保存
                setSharedTaskWorks(order_info, function(){
                    //刷新页面 等待 付款状态


                    //验证订单详情等待次数
                    var orderDetailReloadCount = localPay.currentTask.orderDetailReloadCount?localPay.currentTask.orderDetailReloadCount:0;
                    if(orderDetailReloadCount>8){
                        clue("等待次数超过8次");
                        var API = new Api();
                        API.getOrderPaymentInfo(function(ret){
                            console.log(ret);
                            if(ret.success=='1'){
                                if(ret.data.jd_flash_save=='1'&&ret.data.if_paied=='1'){
                                    clue('付款记录已保存,允许极速保存,3秒后保存数据');
                                    pay_success = true;
                                    //保存数据
                                    isHandWork = 2;//极速保存
                                    order_ready_to_save();
                                }else{
                                    //核对失败,还是刷新吧
                                    orderDetailReloadCount++;
                                    setTaskWorks({orderDetailReloadCount: orderDetailReloadCount}, function(){
                                        reload_page();
                                    })
                                }

                            }else{
                                //核对失败,还是刷新吧
                                orderDetailReloadCount++;
                                setTaskWorks({orderDetailReloadCount: orderDetailReloadCount}, function(){
                                    reload_page();
                                })
                            }

                        }, function(){
                            //核对失败,还是刷新吧
                            orderDetailReloadCount++;
                            setTaskWorks({orderDetailReloadCount: orderDetailReloadCount}, function(){
                                reload_page();
                            })

                        });
                    }else{
                        orderDetailReloadCount++;
                        setTaskWorks({orderDetailReloadCount: orderDetailReloadCount}, function(){
                            reload_page();
                        })
                    }
                })

            }

        } else {
            var m = "不是" + order_detail_id + ",应该是" + shareTask.order_id + ",\r\n" + shareTask.order_id + ' != ' + order_detail_id;
            console.log(m);
            clue(m, 'error');

        }
    }

    })

})
}

//刷新页面之前获取付款状态
function reload_page() {
    tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
}

//刷新当前页面
function do_reload_page(){
    clue(_t.order_detail_refresh_hz + "秒后，刷新");
    setTimeout(function () {

        window.location.reload(true);

    }, _t.order_detail_refresh_hz * 1000);

}

//是否获取完整信息判断
function order_ready_to_save() {


    clue('确认付款, 正在保存数据中 >>>>>>', 'log', 0);
    console.log('订单信息完整，提交订单');
    //保存对账神器数据
    //order_details_save();//功能已弃用

    //保存订单详细数据
    var order_info = orderInfo();

    //保存
    setSharedTaskWorks(order_info, function(){
        updateHostStatus(1603000);//开始保存数据
        tabCommon.sm(taskVars, '.save_host_task_order_detail');
    })


}

//保存信息到服务器
//保存神器数据
// function order_details_save() {

//     var params = {
//         action: "order_detail_save",
//         order_id: $("#orderid").val(),
//         order_url: location.href,
//         order_html: $('html').html(),
//         force_update: 1
//     };

//     //功能已经弃用，可以不用在报告了
//     //var url = 'https://www.disi.se/index.php/Home/Api/order_detail_save';
//     ////var url = 'http://b2.poptop.cc/index.php/Home/Api/order_detail_save';
//     //$.post(url, params, function (ret) {
//     //    console.log('保存神器数据成功！');
//     //}, 'JSON');

//     //完成
//     //finish_sendmsg(params);

// }


//关闭窗口消息
// function send_close_msg() {
//     var msg = {
//         cmd: 'order_detail_close'
//     }
//     //发送消息
//     chrome.runtime.sendMessage(msg, function (response) {

//     });
// }

//保存订单详情数据
//function finish_sendmsg() {
// function finish_order_detail() {

//     //订单状态
//     var order_status = $('#orderstate').find('strong').text();
//     //付款状态
//     var pay_status = $('#orderstate span.ftx14').text();

//     //提交时间
//     var submit_time = $('#track_time_0').text();
//     //订单号
//     var order_id = $("#orderid").val();

//     //订单信息
//     var $lis = $('#orderinfo .fore').find('li');

//     //支付单号
//     var pay_no = $("#ordertrack .fore0 .fore2").text();

//     var user = '';
//     var mobile = '';
//     var address = '';

//     if ($lis.length >= 3) {
//         user = $($lis[0]).text().split('：')[1];
//         address = $($lis[1]).text().split('：')[1];
//         mobile = $($lis[2]).text().split('：')[1];
//     }

//     //金额信息      
//     var $plis = $('div[class="total"]').find('li');
//     var payStr = '';
//     $plis.each(function (i) {

//         var $li = $(this);

//         var span_text = $li.find('span:eq(0)').text();
//         if (span_text != '总商品金额：' && span_text != '- 返现：') {
//             payStr += $.trim($li.text()).replace(/[\s+\+]/g, '');
//         }

//     });


//     //应付金额
//     var should_pay = $('div[class="extra"]').filter(":contains('应付总额')").text().replace(/[\s+\+]/g, '');
//     var pay_result = payStr.replace(/\s/g, '');

//     //计算折扣金额
//     discount_fee = 0;
//     $("#orderinfo .total ul li").each(function (j) {
//         var li = $(this);
//         li_text = li.text();
//         if (li_text.indexOf('-') >= 0 && li_text.indexOf('返现') < 0) {
//             //优惠,,不计算返现
//             discount_fee += parseFloat(li_text.match(/\d+(=?.\d{0,2})/g));
//         }
//     });


//     var items = [
//         {title: "", message: user + ' (' + mobile.match(/\d+/)[0] + ')'},
//         {title: "", message: address},
//         {title: "", message: pay_result},
//         {title: "", message: should_pay}       //应付总额
//     ];

//     should_pay = should_pay.match(/\d+(=?.\d{0,2})/g)[0];

//     var storage_order = {
//         "order_id": order_id,
//         "order_status": order_status,
//         "pay_status": pay_status,
//         "pay_no": pay_no,
//         "submit_time": submit_time,
//         "consignee_user": user,
//         "consignee_mobile": mobile,
//         "consignee_address": address,
//         "pay_result": pay_result,
//         "jingdou": get_jingdou(),//京豆
//         "yunfei": get_yunfei(),//运费
//         "discount_fee": discount_fee,//折扣，，返现
//         "payment_fee": should_pay//应付款金额
//     };
//     //设置一下
//     var order_info = {
//         //'finish_order_id'       :order_id,
//         //'finish_user'           :user,
//         //'finish_mobile'         :mobile,
//         //'finish_address'        :address,
//         //'finish_pay_result'     :pay_result,
//         //'finish_jingdou'        :get_jingdou(),
//         //'finish_yunfei'         :get_yunfei(),
//         //'finish_should_pay'     :should_pay,
//         'orderinfo': storage_order
//     };
//     return order_info;
//     //setSharedTaskWorks(order_info);

// }


//设置信息到storage
// function setSharedTaskWorks(items, callback) {

//     chrome.storage.local.set(items, function (data) {
//         callback && callback();

//         //updateHostStatus(1603000);//开始保存数据
//         //tabCommon.sm(taskVars, '.save_host_task_order_detail');
//     });
// }

/*************** 获取订单上的内容 ******************/

//获取京豆
// function get_jingdou() {

//     //京豆
//     var $li_jingdou = $('div[class="total"] li').filter(":contains('京豆')");

//     if ($li_jingdou.length > 0) {
//         return $li_jingdou.html().match(/(\d+\.\d{2})/)[1];
//     } else {
//         return 0;
//     }
// }

//获取运费
// function get_yunfei() {

//     //运费
//     var $li = $('div[class="total"] li').filter(":contains('运费')");

//     if ($li.length > 0) {
//         return $li.html().match(/(\d+\.\d{2})/)[1];
//     } else {
//         return 0;
//     }
// }

/*************** 获取订单上的内容 ******************/


/**
 * 核对商品详细信息
 * @param local
 */
function checkOrderItemInfo(local){

    var task = localPay.currentTask;
    var plist = $(".p-list");
    // var goodList = $(".order-goods .goods-list");
    // var hkOrderGoodsList = $("table.item-info");//hk

    if(plist.length > 0){
        checkItemInfoByPlist(task);
    }else{
        clue("没有发现商品列表信息,3S后刷新", "error");
        setTimeout(function(){
            window.location.reload();
        },3000);
    }
}

/**
 * check商品信息
 * @param task
 * @returns {boolean}
 */
function checkItemInfoByPlist(task){
    var plist = $(".p-list");
    var item = plist.find("td:contains('"+ task.item_id +"')");
    if(item.length == 0){
        clue('主商品不存在', 'error');
        return false;
    }
    var amount = 0;//该订单商品数量
    plist.find("tbody tr").each(function(){
        var price_txt = $(this).find(".ftx04").text();//价格
        var price = price_txt.match(/\d+(=?.\d{0,2})/g);
        var number = 0;
        if(price > 0){
            number = $(this).find('td').eq(5).text().match(/\d+/g);//数量
        }
        amount += parseInt(number);
    });

    if(amount == parseInt(task.amount)){

        clue("商品数量一致，去付款", "success");

        confirmOrder();
    }else{
        clue("商品数量不一致", "error");
    }
}

// /**
//  * 核对商品信息
//  * @param task
//  * @returns {boolean}
//  */
// function checkItemInfoByGoodsList(task){
//     var goodList = $(".order-goods .goods-list");

//     var item = goodList.find("td:contains('"+ task.item_id +"')");
//     if(item.length == 0){
//         clue('主商品不存在', 'error');
//         return false;
//     }

//     var amount = 0;//该订单商品数量
//     goodList.find("tbody tr").each(function(){
//         var price_txt = $(this).find(".f-price").text();//价格
//         var price = price_txt.match(/\d+(=?.\d{0,2})/g);
//         var number = 0;
//         if(price > 0){
//             number = $(this).find('td').eq(4).text().match(/\d+/g);//数量
//         }
//         amount += parseInt(number);
//     });

//     if(amount == parseInt(task.amount)){

//         clue("商品数量一致，去付款", "success");

//         confirmOrder();

//     }else{
//         clue("商品数量不一致", "error");
//     }

// }

// function checkItemInfoByHkOrderGoodsList(task){
//     var itemsInfo = $("table.item-info");

//     //主商品是否存在
//     var item = itemsInfo.find("a[href*='"+ task.item_id +"']");
//     if(item.length==0){
//         clue('主商品不存在', 'error');
//         return false;
//     }

//     //核对订单商品数量，排除金额为0的赠品
//     var amount = 0;
//     itemsInfo.find("tbody tr").each(function(){
//         var price_txt = $(this).find(".jdPrice").text();//价格
//         var price = price_txt.match(/\d+(=?.\d{0,2})/g);
//         var number = 0;
//         if(price > 0){
//             number = $(this).find('td').eq(2).text().match(/\d+/g);//数量
//         }
//         amount += parseInt(number);
//     });

//     if(amount == parseInt(task.amount)){
//         clue("订单商品数量一致"+amount +"=="+task.amount);
//         clue("商品数量一致，去付款", "success");
//         confirmOrder();
//     }else{
//         clue("商品数量不一致", "error");
//     }
// }

/**
 * 确认订单
 */
function confirmOrder(){
    clue("确认订单 "+ order_detail_id);

    updateHostStep(_host_step.payment);//step7 去付款;
    setSharedTaskWorks({jd_order_id: order_detail_id},function(){
        updateHostStatus(1413099);
        //预 保存订单详细数据
        //var order_info = finish_order_detail();
        var order_info = orderInfo();

        //保存
        setSharedTaskWorks(order_info, function(){
            //保存完，去付款
            //推送数据到赤兔
            tabCommon.sm(taskVars, '.storage_to_tracker', null, function(){
                
            })

            payment();//付款

        })


    });
}

function orderInfo(){

    var orderState = $('#orderstate');
    // var orderStateTxt = $(".order-state .state-txt");
    // var hkOrderState = $(".o-detail .cnt .status");//hk
    if(orderState.length > 0){
        return infoBySelfOrderState();
    }else{
        clue("不能找到订单信息", "error");
    }
}

function infoBySelfOrderState(){
    //订单状态
    var order_status = $("#state-span").text();

    //提交时间
    // var submit_time = $('#track_time_0').text();
    //订单号
    var order_id = $("#orderid").val();

    var consigneeInfo = $("#orderinfo dl:contains('收货人信息')");
    var consigneeInfoItem = consigneeInfo.find('ul li');
    var user = '';
    var mobile = '';
    var address = '';
    if (consigneeInfoItem.length >= 3) {
        user = $.trim($(consigneeInfoItem[0])[0].innerText.split('：')[1]);
        address = $.trim($(consigneeInfoItem[1])[0].innerText.split('：')[1]);
        mobile = $.trim($(consigneeInfoItem[2])[0].innerText.split('：')[1]);
    }

    //应付总额
    //var shouldPay = $(".total li:contains('总商品金额')").text().match(/\d+(=?.\d{0,2})/g)[0];
    var shouldPay = $(".total div:contains('应付总额')").text().match(/\d+(=?.\d{0,2})/g)[0];

    //返现
    var discountFee = $(".total li:contains('返现')").text().match(/\d+(=?.\d{0,2})/g)[0];
    //运费
    var freightFee = $(".total li:contains('运费')").text().match(/\d+(=?.\d{0,2})/g)[0];


    var storage_order = {
        "order_id": order_id,
        "order_status": order_status,
        //"pay_status": pay_status,
        //"pay_no": pay_no,
        //"submit_time": submit_time,
        "consignee_user": user,
        "consignee_mobile": mobile,
        "consignee_address": address,
        //"pay_result": pay_result,
        //"jingdou": get_jingdou(),//京豆
        "yunfei": freightFee,//运费
        "discount_fee": '0.00',//折扣，，返现
        "payment_fee": shouldPay,//应付款金额
        "is_handwork": isHandWork
    };
    //设置一下
    var order_info = {
        'orderinfo': storage_order
    };
    return order_info;
}



// function infoByOrderState(){
//     //订单状态
//     var order_status = $('#orderstate').find('strong').text();
//     //付款状态
//     var pay_status = $('#orderstate span.ftx14').text();

//     //提交时间
//     var submit_time = $('#track_time_0').text();
//     //订单号
//     var order_id = $("#orderid").val();

//     //订单信息
//     var $lis = $('#orderinfo .fore').find('li');

//     //支付单号
//     var pay_no = $("#ordertrack .fore0 .fore2").text();

//     var user = '';
//     var mobile = '';
//     var address = '';

//     if ($lis.length >= 3) {
//         user = $($lis[0]).text().split('：')[1];
//         address = $($lis[1]).text().split('：')[1];
//         mobile = $($lis[2]).text().split('：')[1];
//     }

//     //金额信息
//     var $plis = $('div[class="total"]').find('li');
//     var payStr = '';
//     $plis.each(function (i) {

//         var $li = $(this);

//         var span_text = $li.find('span:eq(0)').text();
//         if (span_text != '总商品金额：' && span_text != '- 返现：') {
//             payStr += $.trim($li.text()).replace(/[\s+\+]/g, '');
//         }

//     });


//     //应付金额
//     var should_pay = $('div[class="extra"]').filter(":contains('应付总额')").text().replace(/[\s+\+]/g, '');
//     var pay_result = payStr.replace(/\s/g, '');

//     //计算折扣金额
//     discount_fee = 0;
//     $("#orderinfo .total ul li").each(function (j) {
//         var li = $(this);
//         li_text = li.text();
//         if (li_text.indexOf('-') >= 0 && li_text.indexOf('返现') < 0) {
//             //优惠,,不计算返现
//             discount_fee += parseFloat(li_text.match(/\d+(=?.\d{0,2})/g));
//         }
//     });


//     var items = [
//         {title: "", message: user + ' (' + mobile.match(/\d+/)[0] + ')'},
//         {title: "", message: address},
//         {title: "", message: pay_result},
//         {title: "", message: should_pay}       //应付总额
//     ];

//     should_pay = should_pay.match(/\d+(=?.\d{0,2})/g)[0];

//     var storage_order = {
//         "order_id": order_id,
//         "order_status": order_status,
//         "pay_status": pay_status,
//         "pay_no": pay_no,
//         "submit_time": submit_time,
//         "consignee_user": user,
//         "consignee_mobile": mobile,
//         "consignee_address": address,
//         "pay_result": pay_result,
//         "jingdou": get_jingdou(),//京豆
//         "yunfei": get_yunfei(),//运费
//         "discount_fee": discount_fee,//折扣，，返现
//         "payment_fee": should_pay,//应付款金额
//         "is_handwork": isHandWork
//     };
//     //设置一下
//     var order_info = {
//         //'finish_order_id'       :order_id,
//         //'finish_user'           :user,
//         //'finish_mobile'         :mobile,
//         //'finish_address'        :address,
//         //'finish_pay_result'     :pay_result,
//         //'finish_jingdou'        :get_jingdou(),
//         //'finish_yunfei'         :get_yunfei(),
//         //'finish_should_pay'     :should_pay,
//         'orderinfo': storage_order
//     };
//     return order_info;
//     //setSharedTaskWorks(order_info);
// }

// function infoByOrderStateTxt(){
//     //订单状态
//     var order_status = $(".order-state .state-txt").text();

//     //提交时间
//     var submit_time = $('#track_time_0').text();
//     //订单号
//     var order_id = $("#orderid").val();


//     var consigneeInfo = $(".order-info .dl:contains('收货人信息')");
//     var consigneeInfoItem = consigneeInfo.find('.item');
//     var user = '';
//     var mobile = '';
//     var address = '';
//     if (consigneeInfoItem.length >= 3) {
//         user = $.trim($(consigneeInfoItem[0])[0].innerText.split('：')[1]);
//         address = $.trim($(consigneeInfoItem[1])[0].innerText.split('：')[1]);
//         mobile = $.trim($(consigneeInfoItem[2])[0].innerText.split('：')[1]);
//     }

//     //应付总额
//     var shouldPay = $(".goods-total li:contains('应付总额')").find('.txt').text().match(/\d+(=?.\d{0,2})/g)[0];
//     //返现
//     var discountFee = $(".goods-total li:contains('返　　现')").find('.txt').text().match(/\d+(=?.\d{0,2})/g)[0];
//     //运费
//     var freightFee = $(".goods-total li:contains('运　　费')").find('.txt').text().match(/\d+(=?.\d{0,2})/g)[0];


//     var storage_order = {
//         "order_id": order_id,
//         "order_status": order_status,
//         //"pay_status": pay_status,
//         //"pay_no": pay_no,
//         //"submit_time": submit_time,
//         "consignee_user": user,
//         "consignee_mobile": mobile,
//         "consignee_address": address,
//         //"pay_result": pay_result,
//         //"jingdou": get_jingdou(),//京豆
//         "yunfei": freightFee,//运费
//         "discount_fee": '0.00',//折扣，，返现
//         "payment_fee": shouldPay,//应付款金额
//         "is_handwork": isHandWork
//     };
//     //设置一下
//     var order_info = {
//         'orderinfo': storage_order
//     };
//     return order_info;
// }

/**
 * 全球购获取订单详情信息
 *
 * */
// function infoByHkOrderStatus(){
//     //$(".o-detail .cnt .status");
//     var detail = $(".o-detail");
//     //订单状态
//     var order_status = $(".o-detail .cnt .status").text();

//     //提交时间
//     var submit_time = $(".o-detail .cnt .eps-process .level-1 .time").text();
//     //订单号
//     var order_id = $(".o-detail").attr('orderid');



//     var consigneeInfo = detail.find(".user-info");
//     var nameMobile = consigneeInfo.find(".row:first .txt").text();

//     var user='', mobile='', address='';
//     var user = $.trim(nameMobile.split('（')[0]);//收货人姓名
//     var mobile = $.trim(nameMobile.split('（')[1].replace('）', ''));//收货人手机号
//     var address = consigneeInfo.find(".row:eq(1) .txt").text();//收货人全地址


//     //应付总额
//     var shouldPay = detail.find(".price-info .total .cost").text().match(/\d+(=?.\d{0,2})/g)[0];
//     //返现
//     var discountFee = detail.find(".price-info .detail .row:contains('返现') .cost").text().match(/\d+(=?.\d{0,2})/g)[0];
//     //运费
//     var freightFee = detail.find(".price-info .detail .row:contains('运费') .cost").text().match(/\d+(=?.\d{0,2})/g)[0];


//     var storage_order = {
//         "order_id": order_id,
//         "order_status": order_status,
//         //"pay_status": pay_status,
//         //"pay_no": pay_no,
//         //"submit_time": submit_time,
//         "consignee_user": user,
//         "consignee_mobile": mobile,
//         "consignee_address": address,
//         //"pay_result": pay_result,
//         //"jingdou": get_jingdou(),//京豆
//         "yunfei": freightFee,//运费
//         "discount_fee": '0.00',//折扣，，返现
//         "payment_fee": shouldPay,//应付款金额
//         "is_handwork": isHandWork
//     };
//     //设置一下
//     var order_info = {
//         'orderinfo': storage_order
//     };
//     return order_info;
// }


/**
 * 跳转付款操作,点击商品详情页面付款按钮
 */

function payment(){

    // console.log('s_time',submit_time);
    // tabCommon.sm(taskVars, '.save_order_info',submit_time,function(){ });

    if($("#shortcut-2013 a:contains('我的订单')").length >0){
        $("#shortcut-2013 a:contains('我的订单')")[0].click();
        return false;
    }
     

    //clue("5秒后刷新");
    //setTimeout(function(){
    //    document.location.reload(true);
    //},5000);
}

/**
 * 保存订单信息
 */
// function payment() {

//     console.log('submit_time',submit_time);
//     API = new Api();
//     useLocal( function (local) {

//         if (localPay.currentTask && local.orderinfo) {
//             var order_info = {
//                 host_id: local.host_id,//
//                 towh_id: localPay.currentTask.towh_id,//历史表记录
//                 task_order_id: localPay.currentTask.task_order_id,//任务订单id
//                 business_oid: local.orderinfo.order_id,//   订单id
//                 consignee_address: local.orderinfo.consignee_address,// 收货人地址
//                 business_discount_fee: local.orderinfo.discount_fee,// 用券金额(折扣)
//                 business_payment_fee: local.orderinfo.payment_fee,//  券扣金额(实际银行支付的金额)
//                 business_freight_fee: local.orderinfo.yunfei,//  运费

//                 business_order_at:submit_time,//提交订单时间
//                 business_ip : local.last_ip,
//                 business_total_fee : local.temp_pay_fee,//总费用

//                 consignee_province: localPay.currentTask.consignee.province,//
//                 consignee_city: localPay.currentTask.consignee.city,//
//                 consignee_area: localPay.currentTask.consignee.area ? localPay.currentTask.consignee.area : ''//区，没有为空
               
//             }

//             console.log('order_info',order_info);

//             API.saveOrderInfo(order_info,
//                 function(ret){
//                     if(ret.success == 1){
//                         console.log('保存订单信息成功');
//                         clue('保存订单信息成功，准备点击付款');
//                         doPayment();
                       
//                     }else{
//                         console.log(ret.message);
//                         clue(ret.message);
//                     }
                    
//                 },
//                 function(){
//                     console.log('请求保存订单信息接口失败');
//                     clue('请求保存订单信息接口失败，10S后重新请求');
//                     setTimeout(function(){
//                         saveOrderInfo();
//                     },10*1000);
//                 }
//             );

            
//         }else{
//             clue("数据不完整");
//             console.log(local);
//         }
//     });
// }

//付款按钮
// function doPayment(){
//     var payButtonA = $("#pay-button-" + order_detail_id + " a");
//     var payBtn = $("#pay-button-" + order_detail_id);
//     if(payButtonA.length > 0){
//         clicking(payButtonA);
//     }else if(payBtn.length >0){
//         clicking(payBtn);
//     }else{
//         clue('未找到付款按钮','error');
//     }
//     close_this_tab();
// }

function showPaymentButton(orderId){

    var payButtonA = $("#pay-button-" + orderId + " a");

    if(payButtonA.length > 0){
        var button = payButtonA;
    }else{
        var button = $("#pay-button-"+orderId);
    }

    button.css('position','fixed');
    button.css('top','0');
    button.css('left','0');
    button.css('margin','0');
    button.css('width','200px');
    button.css('height','20px');
    button.css('line-height','20px');
    button.css('z-index','9999999999999');

}





            //匿名函数结束
        })
    }

}})();