(function () {
    var observerId = tabCommon.observer($('#main')[0])
    tabCommon.start('group_payment', taskStartRun)

    function taskStartRun(localPay) {
    var taskVars=localPay.taskVars;
    run(localPay);
    function run(localPay) {
        lazy(function () {
            //匿名函数开始
            label('订单列表页面',90);
//京东 订单列表 jd/list.js

var orderToPay = 0;
var runIndexed = false;
var amount = 0;
var worldwideOrderId = 0;

var request_pay_state_nums = 0;//获取付款状态请求次数

var first_reload_page = true;

var group_orders = localPay.currentTask.orders;

var checked = false;

// Task(readyIndex);

readyIndex();

setTimeout(function(){
    if(!runIndexed){
        Run(index);
    }
}, 20000);

var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}
tabCommon.mutation(observerId,function(mutation){

    if(mutation.type == 'childList'){
        // console.log(mutation.type);
        // console.log(mutation.target);
        // console.log(mutation);

    }
}, observer_config);

addListenerMessage(function(request){
    if(request.act == "created_360hk_cookie_thor"){
        orderUrl(worldwideOrderId);
    }
});

function readyIndex(){

    getSharedTaskWorks(function(shareTask){
        getTaskWorks(function(task){

        if(localPay.currentTask.business_oid && !shareTask.pay_success){

            var business_oid = localPay.currentTask.business_oid;
            //已经确认过订单 检查是否有拆单
            if($("#parent-" + business_oid).length >0){
                //有拆单 已付款
                setSharedTaskWorks({pay_success:true,jd_order_id:business_oid},function(){
                    setTimeout(function(){
                        location.reload();
                    },3*1000)
                    return false;
                })
                // save_sub_orders(business_oid,check_sub_orders);
            }else{
                //去付款
                // setSharedTaskWorks({payment_clicked:false},function(){})
                    orderUrl(business_oid);
                    return false;
                
            }
        }else{
            if(shareTask.pay_success){

                if(task.sub_orders == undefined){
                    //付款成功，保存子订单数据
                    save_sub_orders(shareTask.jd_order_id,check_sub_orders);
                }else{
                    //之前已经保存过 检查是否保存完毕
                    check_sub_orders();
                }
               
            }else{

                if($("#parent-" + shareTask.jd_order_id).length >0){
                    setSharedTaskWorks({pay_success:true},function(){
                        setTimeout(function(){
                            location.reload();
                        },3*1000)
                        return false;
                    })
                }else{
                    index();
                }
                
            }      
           
        }

        
           
        })

    })
    
}

//保存子订单
function save_sub_orders(business_oid,callback){
    var sub_orders = [];
    var full_orders = [];
    if($("#parent-" + business_oid).length >0){
        //主订单被拆单了 获取子订单
        $("tbody[class*='parent-" + business_oid +"']").each(function(i){

            var mythis = $(this);

            group_orders.forEach(function(order){
                if(mythis.find('.p-' + order.item_id).length >0){
                    var sub_order_id = mythis.attr('id').split('-')[1];
                    sub_orders.push(sub_order_id);
                    full_orders.push({'order_id':order.id,'order_business_oid':sub_order_id});
                    console.log(full_orders);
                }
            })  
        })

        tabCommon.sm(taskVars,'saveGroupBusinessOrdersInfo',full_orders,function(){
            setTaskWorks({sub_orders:sub_orders,full_orders:full_orders},callback)
        });

        

    }else{
        clue('并未拆单,10s后刷新','error');

        getTaskWorks(function(tWorks){
        
            var reload_nums = tWorks.reload_nums ? tWorks.reload_nums :0;
            if(group_orders.length == 1 && reload_nums >1){
                //包只有一单
                sub_orders.push(business_oid);

                full_orders.push({'order_id':group_orders[0].id,'order_business_oid':business_oid});

                // setTaskWorks({sub_orders:sub_orders},callback);

                tabCommon.sm(taskVars,'saveGroupBusinessOrdersInfo',full_orders,function(){
                    setTaskWorks({sub_orders:sub_orders,full_orders:full_orders},callback)
                });


            }

            setTimeout(function(){
                setTaskWorks({reload_nums:++reload_nums},function(){
                    location.reload();
                })
                
            },10*1000)
            // sub_orders.push(business_oid);

            })
    }
    
   
  
}

//检测订单保存状态
function check_sub_orders(){

    runIndexed = true;

    getSharedTaskWorks(function(sTask){

        getTaskWorks(function(tWorks){

            var finished_flag = true;

            var sub_orders = tWorks.sub_orders || [];

            var finished_orders = tWorks.finished_sub_orders || [];
            
            if(tWorks.finished_sub_orders){

                    group_orders.forEach(function(order){
                        if(tWorks.finished_sub_orders[order.id] && tWorks.finished_sub_orders[order.id].finished){
                            console.log('子订单已经完成保存');
                        }else{


                            finished_flag = false;
                            //保存数据不存在
                            //重新获取下子订单，可能拆单不完全
                            
                            if($("tbody[class*='parent-" + sTask.jd_order_id +"']").find('.p-' + order.item_id).length >0 || group_orders.length == 1){

                                if(group_orders.length == 1){//兼容单个单
                                    var sub_order_id = sTask.jd_order_id;
                                }else{
                                    //子订单有拆单商品
                                    var sub_order_id = $("tbody[class*='parent-" + sTask.jd_order_id +"']").find('.p-' + order.item_id).parents("tbody").attr('id').split('-')[1];
                                }
                                
                                //补充子订单
                                if(!in_array(sub_order_id,sub_orders)){
                                    sub_orders.push(sub_order_id);
                                    setTaskWorks({sub_orders:sub_orders},function(){
                                        orderUrl(sub_order_id);
                                    })
                                }else{
                                    orderUrl(sub_order_id);
                                }
                                
                                
                            }
                            
                            // tWorks.sub_orders.forEach(function(business_oid){
                            //     if($("tbody[id*='tb-" + business_oid +"']").find('.p-' + order.item_id).length >0){

                            //         //没保存点击订单详情
                            //         orderUrl(business_oid);
                            //     }
                            // })
                        }
                    })
                     
                    if(finished_flag){
                        //汇报完成
                        wangwang();
                        clue('汇报包完成状态');
                        tabCommon.sm(taskVars, 'saveGroupOrderState');

                        return false;
                    }

            }else{

                finished_flag = false;
                //订单一个没有保存
                tWorks.sub_orders.forEach(function(order_id){
                    orderUrl(order_id);
                })
            }


            clue('正在保存数据，30S后刷新，请等待..');
            wangwang();
            setTimeout(function(){
                if(!finished_flag){
                    //关闭其他多余标签页面
                    tabCommon.sm(taskVars,'.closeOtherTabs',null,function(){
                        location.reload();
                    });
                    
                }
            },30*1000)
        })

    })
}

//检测评价
function randOpenOrderDetail(callback){
    //检测已完成订单
    var finished_orders = $(".order-tb").find("tbody[id*=tb-]").find(".status:contains('已完成')");
    if(finished_orders.length > 0){
        var open_target_index = random(0,parseInt(finished_orders.length - 1));
        var business_oid = finished_orders.eq(open_target_index).parents("tbody").attr("id").match(/\d+/)[0];
        if($("#idUrl"+business_oid).length >0){
                
            setTaskWorks({opened_rand_business_oid:business_oid},function(){
                $("#idUrl"+business_oid)[0].click();
                setTimeout(close_this_tab,3*1000);
                // callback && callback();
            })
            
            
        }else{
            callback && callback();
        }
        

    }else{
        callback && callback();
    }
   
}

function index() {

    if(runIndexed){
        return false;
    }
    runIndexed = true;
    //updateHostStatus(1413001);

    //如果是app付款，直接进入订单
    if(localPay.usage == '1'){
        clue("APP付款订单");
        orderUrl(localPay.currentTask.business_oid);
        return false;
    }

    var goods_item = null;
    var target_order = null;

    group_orders.forEach(function(order){
        if($('#main .p-'+ order.item_id +'').length >0){
            goods_item = $('#main .p-'+ order.item_id +'').first();
            target_order = order;
        }
    })
    

    // var goods_item = $('#main .p-'+ group_orders[0].item_id +'').first();
    if(goods_item && goods_item.length > 0){
        var goods_number = goods_item.next(".goods-number").text().match(/\d+/)[0];

        if(goods_number == target_order.amount){
            var order_body = goods_item.parents("tbody");
            var order_id = order_body.find('a[name="orderIdLinks"]')[0].innerText

            if(localPay.currentTask.worldwide == '1'){
                sendDataToBackground('add_360hk_cookie_thor', {}, function(){
                    clue("检查全球购登录cookie");
                    worldwideOrderId = order_id;
                })
            }else{
                orderUrl(order_id);
            }

        }else{
            clue("商品数量错误", 'error');
        }
    }else{
        clue('未找到对应订单，3s后刷新');

        if(!localPay.currentTask.business_oid){
                getSharedTaskWorks(function(shareTask){
                    if(shareTask.first_submit_order_id){
                        tabCommon.sm(taskVars, '.save_no_found_order',shareTask.first_submit_order_id,function(){
                        });
                        // if(!task.jd_order_id && !task.orderinfo){
                            //提交了订单号，未确认过订单
                            //未保存过付款数据
                            getTaskWorks(function(task){
                                if(!shareTask.saved_card_pay){
                                    var message = '未付款找不到订单';
                                    // var message = '找不到订单，预计订单号(手动标记异常)：' + shareTask.first_submit_order_id;
                                    clue(message);
                                    // tabCommon.sm(taskVars, '.order_exception',message);
                                    reportGroupOrderException(message,group_orders[0].id);
                                    return false;
                                }else{
                                    lazy(function(){
                                        location.reload(true)
                                    }, 3);
                                }
                            })
                        // }else{
                        //     lazy(function(){
                        //         location.reload(true)
                        //     }, 3);
                        // }
                    }else{
                        // lazy(function(){
                        //     location.reload(true)
                        // }, 3);
                        clue('未提交过订单，10S后重新开始');
                        setTimeout(function(){
                            //重新开始
                            tabCommon.sm(taskVars, '.startHost');
                        },10*1000);
                    }
                })

        }else{
            getSharedTaskWorks(function(shareTask){

                   if(!shareTask.saved_card_pay){

                        var message = '未付款找不到订单';
                        // var message = '找不到订单，预计订单号(手动标记异常)：' + localPay.currentTask.business_oid;
                        clue(message);
                        reportGroupOrderException(message,group_orders[0].id);

                        return false;

                    } 

            })
        }
    }
}

function orderUrl(order_id){

    runIndexed = true;

    // getTaskWorks(function(local){   });

        //全球购手机单替换进入链接
        var order_url = $("#idUrl"+order_id);
        if(localPay.currentTask.worldwide == '1' && localPay.currentTask.is_mobile == '1'){

            var hk_url = order_url.attr('href');
            // if(hk_url.indexOf('order.jd360.hk') != -1){
            //     var com_url = hk_url.replace('order.jd360.hk', 'gjz.order.jd.com');
            //     order_url[0].href = com_url;
            // }
        }


        if(order_url.length > 0){

            //进订单详情前获取下单时间
            var task = localPay.currentTask;
            var order_submit_time = $("#datasubmit-" + order_id).val();
            setTaskWorks({order_submit_time:order_submit_time},function(){ })

            //是否已经入过订单详情
            getSharedTaskWorks(function(shareTask){
                if(shareTask.orderinfo !== undefined){
                //已进入过订单详情，可以在订单列表判断最终状态
                    var entry_order_info = localPay.currentTask.entry_order_info === undefined ? 1 : localPay.currentTask.entry_order_info;
                    entry_order_info =1;//不管几次都进入订单详情
                    if(entry_order_info > 3){
                        clue('已经多次尝试进入订单详情');
                        clue('订单列表核对订单付款状态');
                        //核对订单付款状态
                        var tb_order = $('#tb-'+ order_id);
                        var order_status = tb_order.find('.order-status').text();
                        //等待厂商处理
                        if(order_status.indexOf('正在出库') != -1 || order_status.indexOf('付款成功') != -1 || order_status.indexOf('等待厂商处理') != -1){
                            clue(order_id + order_status, 'success');

                            clue('正在保存订单数据....');
                            //订单完成，准备保存订单数据
                            updateHostStatus(1603000);//开始保存数据
                            tabCommon.sm(taskVars, '.save_host_task_order_detail');
                        }else{
                            clue(order_id + order_status);

                            // clue(_t.order_detail_refresh_hz + "秒后，刷新");
                            // setTimeout(function () {
                            //     location.reload(true);
                            // }, _t.order_detail_refresh_hz * 1000);
                            reload_page();
                        }

                    }else{
                        entry_order_info ++;
                        clue('进入' + order_id +'的订单详情');
                        setTaskWorks({entry_order_info: entry_order_info}, function(){

                            var tb_order = $('#tb-'+ order_id);
                            var order_status = tb_order.find('.order-status').text();
                            //等待厂商处理
                            // if(order_status.indexOf('正在出库') != -1 || order_status.indexOf('付款成功') != -1 || order_status.indexOf('等待厂商处理') != -1){
                            //     clue(order_id + order_status, 'success');

                            //     clue('正在保存订单数据....');
                            //     //订单完成，准备保存订单数据
                            //     updateHostStatus(1603000);//开始保存数据
                            //     tabCommon.sm(taskVars, '.save_host_task_order_detail');
                            // }
                            

                            //自营单点付款
                            if(order_url.attr('href').indexOf('dps.ws.jd.com/normal/item.action') != -1){
                                //点击付款按钮
                                if(order_url.parents("tbody").find(".btn-pay:contains('付款')").length >0){
                                    order_url.parents("tbody").find(".btn-pay:contains('付款')")[0].click();
                                }    
                            }else{
                                $("#idUrl"+order_id)[0].click();
                            }


                            if(!shareTask.pay_success){
                                lazy(close_this_tab);
                            }
                            
                        });

                    }


                }else{
                    $("#idUrl"+order_id)[0].click();
                    // lazy(close_this_tab);
                    if(!shareTask.pay_success){
                        lazy(close_this_tab);
                    }
                }
            })
            

        }else{
                clue('没有找到订单 ['+order_id+']','error');

                if(!localPay.currentTask.business_oid){
                    getSharedTaskWorks(function(shareTask){
                        if(shareTask.first_submit_order_id){
                            tabCommon.sm(taskVars, '.save_no_found_order',shareTask.first_submit_order_id,function(){
                            });
                            // if(!task.jd_order_id && !task.orderinfo){
                                //提交了订单号，未确认过订单
                                //未保存过付款数据
                                getTaskWorks(function(task){
                                    if(!shareTask.saved_card_pay){
                                        var message = '未付款找不到订单';
                                        // var message = '找不到订单，预计订单号(手动标记异常)：' + shareTask.first_submit_order_id;
                                        clue(message);

                                        reportGroupOrderException(message,group_orders[0].id);

                                        return false;
                                    }else{
                                        lazy(function(){
                                            location.reload(true)
                                        }, 3);
                                    }
                                })
                           
                        }else{
                           
                            clue('未提交过订单，10S后重新开始');
                            setTimeout(function(){
                                //重新开始
                                tabCommon.sm(taskVars, '.startHost');
                            },10*1000);
                        }
                    })


            }else{

                getSharedTaskWorks(function(shareTask){

                   if(!shareTask.saved_card_pay){

                        var message = '未付款找不到订单';
                        // var message = '找不到订单，预计订单号(手动标记异常)：' + localPay.currentTask.business_oid;
                        clue(message);
                        // sendMessageToBackground('order_exception',message);
                        reportGroupOrderException(message,group_orders[0].id);

                        return false;

                    } 

                })
            }

        }
 


}

function reload_page() {

    if(first_reload_page){
        console.log('第一次请求付款状态延迟10S');
        setTimeout(function(){
            tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
            first_reload_page = false;
        },10*1000);

    }else{
        tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
    }
    
}

//刷新当前页面
function do_reload_page(){
    clue(_t.order_detail_refresh_hz + "秒后，刷新");
    setTimeout(function () {

        window.location.reload(true);

    }, _t.order_detail_refresh_hz * 1000);

}

//监听付款状态结果
addListenerMessage(function(request){
    if(request.act == 'get_pay_state_result'){
        console.log(request);
        getTaskWorks(function(task){
            if(task.jd_order_id == request.data.business_oid){
                if(request.data.pay_state == 3){//付款成功
                    clue('付款成功了');
                    do_reload_page();
                }else if(request.data.pay_state == 4){//代理失败
                    //标记付款失败
                    clue('付款失败了');
                    tabCommon.sm(taskVars, '.change_proxy');//更换代理

                }else if(request.data.pay_state == 5){//付款失败
                    //标记付款失败
                    clue('付款失败了');
                    updateHostStatus(1502003);

                    // setSharedTaskWorks({payment_clicked:false},function(){})
                        setTaskWorks({entry_order_info:0},function(){
                            do_reload_page();
                        })
                    
                    // do_reload_page();
                    

                }else{
                    //还没获取到付款状态，继续获取
                    if(request_pay_state_nums > 20){
                        do_reload_page();
                    }else{
                        request_pay_state_nums++;
                        console.log('正在获取付款状态，请等待...');
                        label('正在获取付款状态',180);
                        setTimeout(function(){
                            tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
                        },3000);   
                    }
                }
            }else{
                // clue('获取付款状态的订单号不一致');
                // do_reload_page();

                 wangwang();

                if(request.message.indexOf('未找到订单对应的有效付款流水') != -1){

                    task.no_pay_data_nums = task.no_pay_data_nums || 0;
                    if(task.no_pay_data_nums > 10){
                        setSharedTaskWorks({payment_clicked:false},function(){
                            console.log('未找到订单对应的有效付款流水,重置点击付款标识');
                            do_reload_page();
                        })
                    }else{
                        setTaskWorks({no_pay_data_nums:task.no_pay_data_nums++},function(){
                            do_reload_page();
                        })
                    }
                    
                    
                }else{
                    clue('获取付款状态的订单号不一致');
                    do_reload_page();
                }
            }
        })
        
        
    }else if(request.act == 'change_proxy_result'){
        //更换代理成功
        updateHostStatus(1502003);//标记付款失败
        setTaskWorks({entry_order_info:0},function(){
            do_reload_page();
        })
        
    }else if(request.act == 'save_host_task_order_detail_result'){
        //提交订单数据成功
        setTimeout(function(){
            if($("#nav .fore-3 .dd a:contains('我的级别')").length >0){
                $("#nav .fore-3 .dd a:contains('我的级别')")[0].click();
            }else{
                location.href = 'https://usergrade.jd.com/user/grade';
            }
        },3*1000)
    }
})

            //匿名函数结束
        })
    }

}})();