// $.cookie('abtest','20170922132527383_20');

(function () {

    if($("#payment").length >0){
        var observerId = tabCommon.observer($("#payment")[0])
    }else if($("#totalNum").length >0){
        var observerId = tabCommon.observer($("#totalNum")[0]);
    }
    


    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/cart.js
// $.cookie('abtest',null);
// $.cookie('abtest','20170922132527383_20');
// $.cookie('abtest','20160316205935713_84');
windowsTopNotSelf();
resetWatchDogTimer();

var num_changed = false;

Task(index);
var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}

var group_orders = localXss.currentTask.orders;

var total_nums = 0;

var check_products_exist = true;

var drop_full_products = [];

tabCommon.mutation(observerId,function(mutation){
     //console.log(mutation.type);
     //console.log(mutation.target);
     //console.log(mutation);
    if(mutation.type == 'characterData'){
        if(mutation.target.parentElement && mutation.target.parentElement.id == 'checkedNum'){
            //结算数量变动
            console.log('结算数量变动');
            // Run(selectedItem);
            Run(function(local){
                num_changed = true;
                // selectedItem(local);
            });

        }
    }else if(mutation.type == 'childList'){
        if(mutation.target.id == 'totalNum' && mutation.addedNodes[0].length >0){
            //结算数量变动
            console.log('结算数量变动');
            // Run(selectedItem);
            Run(function(local){
                num_changed = true;
                // selectedItem(local);
            });

        }
    }
},observer_config);

function index(local) {
    console.log(local);
    var task = localXss.currentTask;

    updateHostStatus(1409000);//购物车

    var login = $(".empty-btn-wrapper .btn-jd-gray:visible");
    if (login.length > 0) {
        clue("3秒，去登陆");
        setTimeout(go_login, 3000);
        return false;
    }


    clue('核对商品');
    var products = $(".shp-cart-list li");

    if($("#emptyCart:visible").length > 0){
        clue('购物车没有商品，任务重置');
        //重置任务
        setTimeout(function(){
            tabCommon.sm(taskVars, '.taskReset',{taskName:'search'})
        },3*1000)
        
        // clue("购物车没有商品，自动处理返回首页", 'error');
        // checkActiveTab(function(){
        //     lazy(function(){
        //         $("#m_common_header_shortcut_m_index a:contains('首页')")[0].click();
        //     });
        // })
        

        return false;
    }

    ////核对商品数量
    //var number = 0;
    //var itemNumber = $('#num'+task.item_id).val();
    //if(itemNumber){
    //    var nums = $(".quantity-wrapper input[name='num']").not('#num'+task.item_id);
    //    nums.each(function (j) {
    //        number += parseInt($(this).val());
    //    });
    //}else{
    //    var nums = products.not('#product'+task.item_id).find(".shp-cart-item-core .num-cont-show");
    //    var nums = products.not('#product'+task.item_id).find(".items").not('#product'+task.item_id).find(".shp-cart-item-core .num-cont-show");
    //    //var nums = products.not('#product'+"1648995097").find(".items").not('#product'+"1648995097").find(".shp-cart-item-core .num-cont-show");
    //    nums.each(function (j) {
    //        number += parseInt(this.innerText);
    //    });
    //    itemNumber = parseInt($('#product'+task.item_id).find(".shp-cart-item-core .num-cont-show").text());
    //}


    checkActiveTab(function(){

        group_orders.forEach(function(order){
            if(check_products_exist){
                if($("#listContent").length >0){
                    selectedItem_new(order);
                }else{
                   selectedItem(order); 
                }
                
            }            
        })

        setTimeout(function(){
            if(check_products_exist){
                 if($("#listContent").length >0){
                     submit_new();
                 }else{
                     submit();
                 }
               
            }
        },3*1000);
        
    })
    
        

}

//结算
function submit(not_exist_item_id) {

    if(!check_products_exist && not_exist_item_id){
        clue('有主商品不存在，3S后任务重做');
        setTimeout(function(){
            var condition = {item_id:not_exist_item_id};
            tabCommon.sm(taskVars, '.taskRedo',{taskName:'search',condition:condition})
        },3*1000);
        return false;
    }

    var barItemNum = parseInt($("#checkedNum").text().match(/\d+/g));
    if(total_nums == barItemNum || (total_nums >99 && $("#checkedNum").text().indexOf('99+') != -1)){

        //检测是否有满减满折
        var order_ids = [];
        group_orders.forEach(function(order){
            checkDiscount(order,function(order_id){
                if(order_id){
                   order_ids.push(order.id); 
                }
                 
            });
        })

        if(drop_full_products.length >1){
            clue('满减满折踢单不异常');
            //满减满折踢单不异常
            reportGroupOrderException('活动商品满减，踢单不异常',group_orders[0].id,order_ids);
            return false;
        }else{
            setTimeout(function(){
                $("#submit")[0].click();
            },3000)
        }
        // setTimeout(function(){
        //     $("#submit")[0].click();
        // },3000)
    }else{
        clue('商品总数量不符,重新勾选');

        group_orders.forEach(function(order){

            $(".cart-checkbox").each(function(i){
               console.log($(this).attr('class'));
               if($(this).attr('id') == 'checkIcon' + order.item_id && $(this).attr('class').indexOf('checked') == -1){
                    clue('勾选主商品' + order.item_id);
                    this.click();
               }else if($(this).attr('class').indexOf('checked') != -1){
                    clue('取消已购选的非主商品');
                    this.click();
               }
            })

        })

        setTimeout("location.reload();",3*1000);
        
    }
}

function submit_new(not_exist_item_id) {

    if(!check_products_exist && not_exist_item_id){
        clue('有主商品不存在，3S后任务重做');
        setTimeout(function(){
            var condition = {item_id:not_exist_item_id};
            tabCommon.sm(taskVars, '.taskRedo',{taskName:'search',condition:condition})
        },3*1000);
        return false;
    }

    var barItemNum = parseInt($("#totalNum").attr('totalNum'));
    if(total_nums == barItemNum){

        //检测是否有满减满折
        var order_ids = [];
        group_orders.forEach(function(order){
            checkDiscount_new(order,function(order_id){
                if(order_id){
                   order_ids.push(order.id); 
                }
                 
            });
        })

        if(drop_full_products.length >1){
            clue('满减满折踢单不异常');
            //满减满折踢单不异常
            reportGroupOrderException('活动商品满减，踢单不异常',group_orders[0].id,order_ids);
            return false;
        }else{
            setTimeout(function(){
                // $("#submit")[0].click();
                $("#totalConfirmDiv a:contains('去结算')")[0].click();
            },3000)
        }
        // setTimeout(function(){
        //     $("#submit")[0].click();
        // },3000)
    }else{
        clue('商品总数量不符,重新勾选');

        group_orders.forEach(function(order){

            $(".section .goods").each(function(i){
               console.log($(this).attr('class'));
               if($(this).attr('attr-sku') == order.item_id && $(this).attr('class').indexOf('selected') == -1){
                    clue('勾选主商品' + order.item_id);
                    this.click();
               }else if($(this).attr('class').indexOf('selected') != -1){
                    clue('取消已购选的非主商品');
                    $(this).find('i')[0].click();
               }
            })

        })

        setTimeout("location.reload();",3*1000);
        
    }
}

//检测满减满折
function checkDiscount(order,callback){
    var obj = $("#product" + order.item_id).parents('ul').find(".promotion-space");
    if(obj.length >0 && obj.find("span:contains('已减')").length >0 && obj.find("span:contains('已购满')").length >0){
        //有满减商品 记录
        clue('主商品存在满减:' + order.item_id);
        drop_full_products.push(order.item_id);
        callback && callback(order.id);
    }   
}
function checkDiscount_new(order,callback){
    var obj = $(".goods[attr-sku=" + order.item_id + ']').parents('.section').find('.head');
    if(obj.length >0 && obj.find("p:contains('已减')").length >0 && obj.find("p:contains('已购满')").length >0){
        //有满减商品 记录
        clue('主商品存在满减:' + order.item_id);
        drop_full_products.push(order.item_id);
        callback && callback(order.id);
    }   
}


//去登陆
function go_login() {
    $(".empty-btn-wrapper .btn-jd-gray:visible")[0].click();
}

/**
 * 选中主商品
 */
function selectedItem(order){

    //$("#product1681659460")
    //$("#product1681659460 .cart-checkbox.checked[id='checkIcon1681659460']")
    //$("#checkIcon1681659460.checked")
    //$(".shp-cart-list li .cart-checkbox.checked[id*='checkIcon']")


    var checkedItem = $(".shp-cart-list li .cart-checkbox.checked[id*='checkIcon']");
    var barItemNum = parseInt($("#checkedNum").text().match(/\d+/g));
    var mainItemId = order.item_id;
    //var mainItemId = "1681659460";

    var mainItemChecked = $("#product"+mainItemId+" .cart-checkbox.checked[id='checkIcon"+mainItemId+"']");
    var mainItemCheckbox = $("#product"+mainItemId+" .cart-checkbox[id='checkIcon"+mainItemId+"']");
    var subItemCheckbox = checkedItem.not(mainItemCheckbox);

    //商品数量显示两种情况
    var mainItemNumber = $('#num'+mainItemId).val();
    if(!mainItemNumber){
        mainItemNumber = parseInt($('#product'+mainItemId).find(".shp-cart-item-core .num-cont-show").text());
    }

    if(mainItemCheckbox.length == 0){
        clue(mainItemId + "主商品不存在", "error");
        check_products_exist = false;
        submit(mainItemId);
        return false;
    }

    if(checkedItem.length > 0){
        //有已经勾选的商品,
        if(mainItemChecked.length > 0){
            //主商品已勾选
            // clue("主商品已勾选");
            //|| ($("#checkedNum").text().indexOf('99+') != -1 && mainItemNumber >99)
            if(order.amount == mainItemNumber){
                //勾选数量和主商品购物车数量相同
                //提交结算
                clue("主商品已勾选" + mainItemId, 'success');

                // total_nums += parseInt(order.amount);

                checkGift(mainItemId,function(){
                        // submit();  
                })
                // console.log('结算');
                // if(mainItemNumber == parseInt(localXss.currentTask.amount)){
                //     clue("主商品数量正确,去结算", "success");
                //     //验证赠品无货情况
                //     checkGift(mainItemId,function(){
                //         // submit();  
                //     })
                    
                }else{
                    clue("主商品错误("+mainItemNumber+")", "error");
                    
                    updateHostStatus(1409203);

                    clue('编辑商品数量');

                    //定位商品
                    var product_dom = $(".shp-cart-list").find('#product'+order.item_id);

                    if(product_dom.find('.cart-noshopping').length >0){
                        //商品无货
                        var remarks = '商品无货';
                        checkProductExceptionReloadNums('whuo',function(){
                            clue(remarks,'error');
                            reportProductStockout(remarks);
                        })
                    }
                    
                    //若有编辑框就打开
                    if(product_dom.find(".cart-new-btn").length >0){
                        product_dom.find(".cart-new-btn")[0].click();//点击编辑
                         
                    }
                    //兼容编辑框
                    else if(product_dom.find(".btn-msg-in").length >0){
                        product_dom.find(".btn-msg-in")[0].click();//点击编辑
                         
                    }
                    //数量写值(兼容2类模板) 
                    product_dom.find('#num'+order.item_id).val(order.amount);

                    // total_nums += parseInt(order.amount);
                
                    // //第一种情况 直接编辑
                    // if(product_dom.find('#num'+mainItemId).length>0){
                    //     product_dom.find('#num'+order.item_id).val(order.amount);
                        
                    // }else{
                    //     //第二种情况
                    //     if(product_dom.find(".cart-new-btn").length >0){
                    //         product_dom.find(".cart-new-btn")[0].click();//点击编辑
                    //         product_dom.find('#num'+order.item_id).val(order.amount);//写值 
                    //     }
                    // }
                    //触发变动
                    setTimeout(function(){
                        change_event(product_dom.find('#num'+order.item_id)[0]);
                        clue('商品数量修改成功');
                        console.log('修改商品数量成功');
                    },3000)

                    //20s超时刷新
                    setTimeout(function(){
                        if(!num_changed){
                            window.location.reload();
                        }
                    },20*1000)

                // }
            }
        }else{
            //主商品未勾选,勾选主商品
            clue('勾选主商品'+mainItemId);
            mainItemCheckbox[0].click();
        }

    }else{
        //没有勾选的商品.全选肯定没勾上
        //勾主商品
        //$("#toggle-checkboxes_down")[0].click();
        clue('勾选主商品'+mainItemId);
        mainItemCheckbox[0].click();
    }

    total_nums += parseInt(order.amount);
}

function selectedItem_new(order){


    var checkedItem = $(".section .item i");
    // var barItemNum = parseInt($("#checkedNum").text().match(/\d+/g));
    var mainItemId = order.item_id;
    //var mainItemId = "1681659460";

    var mainItemChecked = $(".selected[attr-sku='" + mainItemId +"'] i[icontip='" + mainItemId + "']");
    var mainItemCheckbox = $(".goods[attr-sku='" + mainItemId +"'] i[icontip='" + mainItemId + "']");
    var subItemCheckbox = checkedItem.not(mainItemCheckbox);

    //商品数量显示两种情况
    var mainItemNumber = $(".goods[attr-sku=" + mainItemId + ']').find('.num').val();
    // if(!mainItemNumber){
    //     mainItemNumber = parseInt($('#product'+mainItemId).find(".shp-cart-item-core .num-cont-show").text());
    // }

    if(mainItemCheckbox.length == 0){
        clue(mainItemId + "主商品不存在", "error");
        check_products_exist = false;
        submit(mainItemId);
        return false;
    }

    if(checkedItem.length > 0){
        //有已经勾选的商品,
        if(mainItemChecked.length > 0){
            //主商品已勾选
            // clue("主商品已勾选");
            //|| ($("#checkedNum").text().indexOf('99+') != -1 && mainItemNumber >99)
            if(order.amount == mainItemNumber){
                //勾选数量和主商品购物车数量相同
                //提交结算
                clue("主商品已勾选" + mainItemId, 'success');

                // total_nums += parseInt(order.amount);

                // checkGift(mainItemId,function(){
                //         // submit();  
                // })
                // console.log('结算');
                // if(mainItemNumber == parseInt(localXss.currentTask.amount)){
                //     clue("主商品数量正确,去结算", "success");
                //     //验证赠品无货情况
                //     checkGift(mainItemId,function(){
                //         // submit();  
                //     })
                    
                }else{
                    clue("主商品错误("+mainItemNumber+")", "error");
                    
                    updateHostStatus(1409203);

                    clue('编辑商品数量');

                    //定位商品
                    var product_dom = $(".goods[attr-sku=" + order.item_id + ']');

                    
                    // }
                    //数量写值(兼容2类模板) 
                    product_dom.find('.num').val(order.amount);

                   
                    //触发变动
                    setTimeout(function(){
                        change_event_for_blur(product_dom.find('.num')[0]);
                        clue('商品数量修改成功');
                        console.log('修改商品数量成功');
                    },3000)

                    //20s超时刷新
                    setTimeout(function(){
                        if(!num_changed){
                            window.location.reload();
                        }
                    },20*1000)

                // }
            }
        }else{
            //主商品未勾选,勾选主商品
            clue('勾选主商品'+mainItemId);
            mainItemCheckbox[0].click();
        }

    }else{
        //没有勾选的商品.全选肯定没勾上
        //勾主商品
        //$("#toggle-checkboxes_down")[0].click();
        clue('勾选主商品'+mainItemId);
        mainItemCheckbox[0].click();
    }

    total_nums += parseInt(order.amount);
}

function select_item(order){

}

function change_event_for_blur(o) {
    var changeEvent = document.createEvent("MouseEvents");
    changeEvent.initEvent("blur", true, true);
    o.dispatchEvent(changeEvent);
}

//验证赠品是否无货
function checkGift(mainItemId,callback){
    //var mainItemId = '2822662294';
    var gift_list = $("#product" + mainItemId).parents('.shp-cart-list');
    if(gift_list.length > 0){
        var remarks = '赠品无货:';var flag = false;
        // gift_list.each(function(){ })
            if(gift_list.find(".shp-cart-gift-list a:contains('无货')").length > 0){
                remarks  =  remarks + gift_list.find(".shp-cart-gift-list a:contains('无货')").text();
                flag = true;
            }
       
        //是否有无货情况
        if(flag){
            clue(remarks,'error');
            // addTaskOrderRemark(remarks ,function(){});
            set_group_order_exception(mainItemId,remarks);//标记异常
                
        }else{
            callback && callback();
        }
        
    }else{
        callback && callback();
    }
}

function set_group_order_exception(item_id,remarks){

     group_orders.forEach(function(order){

        if(order.item_id == item_id){
            //异常包下的单
            reportGroupOrderException(remarks,order.id);
        }
   })
}


//验证当前提交结算Tab为活动的
function checkActiveTab(callback){
    tabCommon.sm(taskVars, '.checkActiveTab')
    addListenerMessage(function(request){
        console.log(request);
        if(request.act == 'check_active_tab_result'){
            if(request.is_active == true && request.need_reload == true){
                clue('关闭其他标签页面，3S后刷新');
                setTimeout(function(){window.location.reload();},3000);
                //callback && callback();
            }else if(request.is_active == false){
                close_this_tab();
            }else{
                callback && callback();
            }
        }
    })
}

var deleteWare = function(h, c, g, b, f, e, d) {
        var a = "/cart/remove.json?wareId=" + h + "&num=" + c + "&sid=" + $("#sid").val();
        a = appendSuit(a, g, b, f, e);
        $.get(a, {}, function(l) {

        })
};
var appendSuit = function(e, f, b, d, c, a) {
    if (!isBlank(e)) {
        if (!isBlank(f)) {
            e += "&sType=" + f
        }
        if (!isBlank(b)) {
            e += "&suitSkuId=" + b
        }
        if (!isBlank(d)) {
            e += "&suitSkuNum=" + d
        }
        if (!isBlank(c)) {
            e += "&ybId=" + c
        }
        if (!isBlank(a)) {
            e += "&ybNum=" + a
        }
    }
    return e
};
var isBlank = function(a) {
    if (a == undefined || a == null || a.length == 0) {
        return true
    } else {
        return false
    }
};

            //匿名函数结束
        })
    }

}})();