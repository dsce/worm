(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/order.js

var pay_fee = 0;
var shipping_fee = 0;
var identity_id_inputted = false;
var identity_btn_clicked = false;

var group_orders = localXss.currentTask.orders;


tabCommon.mutation(observerId,function (mutation) {
    console.log(mutation.type);
     console.log(mutation.target);
     console.log(mutation);

    if(mutation.type === 'attributes'){
        if(mutation.target.id == 'inputPwdWindow'&&mutation.attributeName == 'style'&&mutation.oldValue!=null&&mutation.oldValue == "display: none;"){
            //使用京豆
            r(function(local){

                if($("#securityPayPassword:visible").length > 0){
                    $("#securityPayPassword:visible").val(localXss.currentTask.pay_password);
                    $("#virtualPaySubmit").removeClass('pass-no');

                    lazy(function(){
                        clue('确定使用京豆');
                        $("#virtualPaySubmit")[0].click();
                    },5);

                }
            });

        }else if(mutation.target.id == 'makeLocation' && mutation.target.innerText.indexOf('请先选择地址') != -1){
            // $(".step1 .add-address a")[0].click();
        }else if(mutation.target.id == 'invoices'){
            // console.log('发票页面');
            // if(mutation.oldValue && mutation.oldValue.indexOf('none') != -1){
            //     clue('发票页面');
            //     $("#fpType li:contains('个人')")[0].click();
            //     $("#fpContent li:contains('不开发票')")[0].click();
            //     setTimeout(function(){
            //         clue('确认发票修改');
            //         $("#btnBottomConfirmBar")[0].click();
            //     },3*1000)
            // }else{
            //     clue('发票修改完毕');
            //     submitBegin_new();
            // }
            
        }else if(mutation.target.id == 'addAddress'){
            // clue('收货地址列表');
            // setTimeout(function(){
            //     $("#addAddress")[0].click();
            // },3*1000)
            
        }else if(mutation.target.id == 'pageAddAddress'){
            // clue('添加收货地址页面');
            // add_address_new();
        }

    }else {
        if(mutation.target.id == 'wrapBody'){
            if($("#noStockTplModel:visible").length >0){
                if($("#noStockTplModel .btns a:contains('移除无货商品')").length >0){
                    set_group_order_exception('商品无货：' + $("#noStockTplModel .goods").text(),function(){
                        clue('移除无货商品');
                        $("#noStockTplModel .btns a:contains('移除无货商品')")[0].click();
                        setTimeout(function(){
                            location.reload();
                        },3*1000)
                    })
                    
                }else{
                    clue($("#noStockTplModel:visible").text());
                }
            }
        }
        if(mutation.target.id == 'message-prompt'){
            
            clue($("#message-prompt").text());
            var msg = $("#message-prompt").text();
            if(msg.indexOf('赠品') != -1 && msg.indexOf('无货') != -1){
                clue('3S后返回购物车');
                setTimeout(function(){
                    location.href = 'https://p.m.jd.com/cart/cart.action';
                    // $("#m_common_header_shortcut .shortcut-cart")[0].click();
                },3000);
            }else if(msg.indexOf('无货') != -1){
                clue('3S后返回购物车');
                setTimeout(function(){
                    // $("#m_common_header_shortcut .shortcut-cart")[0].click();
                    location.href = 'https://p.m.jd.com/cart/cart.action';
                },3000);
                // addTaskOrderRemark(msg,function(){
                //     reportProductStockout(msg);//标记异常
                // });
            }else if(msg.indexOf('无法购买商品') != -1 && (msg.indexOf('地区') != -1 || msg.indexOf('省份') != -1) ){
                // addTaskOrderRemark(msg, function(){
                    //使用新方法 订单异常 + 备注
                    // sendMessageToBackground('order_exception',msg);
                    set_group_order_exception(msg);
                // });
                // addTaskOrderRemark(msg,function(){
                //     reportProductStockout(msg);//标记异常
                // });

            }else if(msg.indexOf('同一账户只能抢购一件') != -1){
                // addTaskOrderRemark(msg, function(){
                    //使用新方法 订单异常 + 备注
                    tabCommon.sm(taskVars, '.order_exception',msg);
                // });
            }else if(msg.indexOf('您当前的“发票内容”选择有误，请重新选择') != -1){
                setTaskWorks({re_select_invoice:true},function(){
                    clue('重新选择发票信息');
                    setTimeout("location.reload();",3*1000);
                })

            }else if(msg.indexOf('抢购失败') != -1){
                set_group_order_exception(msg);
            }
            
        }

        if(!identity_id_inputted &&  mutation.target.className && mutation.target.className.indexOf("mjdToastContainer") != -1 && mutation.addedNodes.length > 0 && mutation.addedNodes[0].innerText.indexOf("保存收货人的身份证信息")!=-1){
                //需要填身份证信息+保存身份证信息
                Run(function(local){
                    clue("输入身份信息");
                    identity_id_inputted = true;
                    $("#identityId").val(localXss.currentTask.identity_card);
                    $("#identity-btn").addClass("active");
                    $("#identity-btn")[0].click();
                });
            }else if(!identity_btn_clicked && mutation.target.id == "identity-num" && mutation.addedNodes.length>0){
                //身份证信息已增加,提交订单
                Run(function(){
                    clue("允许通过海关", "success");
                    identity_btn_clicked = true;
                    submiOrder();
                });
            }
    }


});


//匹配异常商品
function set_group_order_exception(submit_message,callback){

     group_orders.forEach(function(order){

        var target_item = order.product_name.replace(/\s+/g,'');
        console.log(target_item);
        if(submit_message.replace(/\s+/g,'').indexOf(target_item) != -1){
            //异常包下的单
            reportGroupOrderException(submit_message,order.id);
        }else{
            callback && callback();
        }
   })
}


Task(index);

function index(local){
    var task = localXss.currentTask;
    var referrer = document.referrer;

    updateHostStep(_host_step.submit_order);//step6 提交订单
    updateHostStatus(1410000);//提交订单


    var payMoney = $("#payMoney");
    if(payMoney.length==0){
        payMoney = $(".JS-pay-total");
    }
    //2016-11-29 19:28
    if(payMoney.length==0){
        payMoney = $("#pay-amount");
    }

    if(payMoney.length == 0){
        payMoney = $("#pageTotalPrice");
    }

    //判断费用信息是否存在
    if(payMoney.length == 0){

        var buttonKnow = $("input.allow[value='知道了']");
        if(buttonKnow.length > 0){
            clicking(buttonKnow);
        }

        clue("提交订单信息为空,回到购物车", "error");


        setTimeout(function(){
            //remove_domain_cookie_by_name

            tabCommon.sm(taskVars, '.removeDomainCookieByName', {domain: 'jd.com',names: ['USER_FLAG_CHECK', 'sid', 'user-key']}, function(){
                clue("5秒后返回购物车");
                setTimeout(function(){
                    // clicking($("#m_common_header_shortcut_p_cart a"));
                    location.href = 'https://p.m.jd.com/cart/cart.action';
                }, 5000);
            });
            //tabCommon.sm(taskVars, '.startHost');

        }, 5000);
        return false;
    }

    if(taskOrderExist('group_xss')){return false;}
    
    if($(".mod_alert:visible").length >0){
        if($(".mod_alert:visible").find('p').text().indexOf('您还没有可用配送地址，是否新建一个配送地址') != -1){
            clicking($(".mod_alert:visible").find('.btns a:contains("新建")'));
            setTimeout(function(){
                add_address_new(function(){
                    // $("#tabInvoices a:contains('明细')")[0].click();
                    lazy(invoice_action);
                })
            },3*1000);

            return false;
        }else if($(".mod_alert:visible").find('p').text().indexOf('因行政区域名称变更，您选择的地址现已不存在') != -1){
            clicking($(".mod_alert:visible").find('.btns a:contains("修改地址")'));
            setTimeout(function(){
                $("#addressDefault").click();
                $("#addressList").show();
                setTimeout(function(){
                    if($("#addressList:visible").length >0){
                        $("#addressList #addAddress")[0].click();
                        setTimeout(function(){
                            add_address_new(function(){
                                // $("#tabInvoices a:contains('明细')")[0].click();
                                lazy(invoice_action);
                            })
                        },3*1000);
                    }
                },3*1000)
            },3*1000);
        }
    }

    getTaskWorks(function(tWorks){
        if(tWorks.re_select_invoice){
            lazy(invoice_action);
        }
    

    if(referrer.indexOf('http://p.m.jd.com/norder/payShipment.action') != -1){
        //改完 支付方式 修改发票
        clue("3秒后，修改发票信息");
        lazy(invoice_action);

    }else if(referrer.indexOf('p.m.jd.com/norder/invoice.action') != -1){//修改完发票，提交订单
        Run(submitBegin);

    }else{

        //核对是否有手机号
        var sPhoneText = $(".step1 .mt_new .s1-phone");
        if(sPhoneText.length > 0){
            if(!checkAddress(local)){
                // clue("手机号不正确,需编辑确认");
                $(".step1 .step1-in a")[0].click();

            }else{
                //改支付方式
                //clue("修改支付方式");
                clue("直接修改发票");
                //lazy(payment_action);
                lazy(invoice_action);

            }
        }else if($(".step1 .add-address a").length >0){

            $(".step1 .add-address a")[0].click();

        }else if($("#addressDefault").length >0){

            if(!checkAddress_new(local)){
                $("#addressDefault").click();
                setTimeout(function(){
                    if($("#addressList:visible").length >0){
                        $("#addressList #addAddress")[0].click();
                        setTimeout(function(){
                            add_address_new(function(){
                                // $("#tabInvoices a:contains('明细')")[0].click();
                                lazy(invoice_action);
                            })
                        },3*1000);
                    }
                },3*1000)
                
            }else{
                clue('直接修改发票');
                // $("#tabInvoices a:contains('明细')")[0].click();
                var address = jdMunicipality(local.task.consignee);
                setLocal({order_address:address},function(){
                    // saveAddressToRemote(function(){})
                        lazy(invoice_action);
                    
                })
                
            }
        }else{
            //重新开始
            tabCommon.sm(taskVars, '.order_operate','client_host_status_reset')
        }

    }

    })
    
}

//验证收货地址
function checkAddress(local){
    var address = jdMunicipality(local.task.consignee);
    var name = $("#address-name").text();
    var mobile = $("#address-mobile").text().match(/\d+/g);
    var full_address = $("#address-where").text().replace(/\s+/g,'');

    var its_name = name == address.name;
    var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

    var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

    var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
    if(its_name&&its_full_address&&its_mobile){
        clue('收货地址一致，继续');
        return true;
    }else{
        if(its_name&&!its_full_address&&its_mobile){
            var task_address = address.province+(address.city?address.city:'')+(address.area?address.area:'') + (address.street?address.street:'');
            if(address.area && full_address.replace(/省|市|\s|#/g,'').indexOf(task_address.replace(/省|市|\s|#/g,'')) != -1){
                clue('收货地址一致，继续~');
                return true;
            }else{
                clue('收货地址不一致,修改~');
                return false;
            }
        }else{
            clue('收货地址不一致,修改');
            return false;
        }
       
    }
}

function checkAddress_new(local){
    var address = jdMunicipality(local.task.consignee);

    if($("#addressDefault ul li").eq(0).text()){
        var name = $("#addressDefault ul li").eq(0).text().split(' ')[0];
        var mobile = $("#addressDefault ul li").eq(0).text().split(' ')[1].match(/\d+/g);
    }

    if($("#addressDefault").length >0){
        var full_address = $("#addressDefault ul li").eq(1).text().replace(/\s+/g,'');
    }
    // var name = $("#address-name").text();
    // var mobile = $("#address-mobile").text().match(/\d+/g);
    // var full_address = $("#address-where").text().replace(/\s+/g,'');

    var its_name = name == address.name;
    var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

    var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

    var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
    if(its_name&&its_full_address&&its_mobile){
        clue('收货地址一致，继续');
        return true;
    }else{
        if(its_name&&!its_full_address&&its_mobile){
            var task_address = address.province+(address.city?address.city:'')+(address.area?address.area:'') + (address.street?address.street:'');
            if(address.area && full_address.replace(/省|市|\s|#/g,'').indexOf(task_address.replace(/省|市|\s|#/g,'')) != -1){
                clue('收货地址一致，继续~');
                return true;
            }else{
                clue('收货地址不一致,修改~');
                return false;
            }
        }else{
            clue('收货地址不一致,修改');
            return false;
        }
       
    }
}

function add_address_new(callback){
    useLocal(function(local){
        var address = jdMunicipality(local.task.consignee);
        $("#pageAddAddress #name").val(address.name);
        $("#pageAddAddress #mobile").val(address.mobile);

        if(local.task.worldwide == '1'){
            console.log(local.task.identity_card);
            $("#pageAddAddress #idCard").val(local.task.identity_card);
        }
        // $("#pageAddAddress #adinfo").val(address.short_address);
        //selAddr
        $("#pageAddAddress #selAddr")[0].click();

        // setTimeout(function(){},3*1000)

        sel_address_new(1,function(){
            saveAddressToRemote(function(){
                $("#pageAddAddress #adinfo").val(address.short_address);
                setTimeout(function(){
                    $("#submitAddress")[0].click();
                    callback && callback()
                },3*1000)
                
            })
            
        });


    })
}

function sel_address_new(level,callback){

    if(level >4){
        callback && callback();
        return false;
    }

    useLocal(function(local){
        var address = jdMunicipality(local.task.consignee);
        var content;
        if(level == 1){
            content = address.province;
        }else if(level ==2){
            content = address.city;
        }else if(level == 3){
            content = address.area;
        }else if(level ==4){
            content = address.street;
        }

        if($(".mod_address_slide_list_2 li[level=" + level +"]").length >0){
            if(content && $(".mod_address_slide_list_2 li:contains('" + content +"')").length >0){
                clue('选择：' + content);
                $(".mod_address_slide_list_2 li:contains('" + content +"')")[0].click();
            }else{
                var index = random(0,parseInt($(".mod_address_slide_list_2 li").length-1));
                
                if(level == 3){
                    address.area = $(".mod_address_slide_list_2 li").eq(index).text();
                    content = address.area;
                }else if(level == 4){
                    address.street = $(".mod_address_slide_list_2 li").eq(index).text();
                    content = address.street;
                }

                setTimeout(function(){
                    clue('选择：' + content);
                    var task = local.task;
                    task.consignee = address;
                    setLocal({task:task},function(){
                        $(".mod_address_slide_list_2 li").eq(index).click();
                    })
                    
                },2*1000)
                
            }
            
        }
        setTimeout(function(){
            setLocal({order_address:address},function(){
                 sel_address_new(++level,callback)
            })
           
        },3*1000);

    })
}

//修改收货人信息
function addressLink(){
    $("#addressLink")[0].click();
}

function payment_action(){
    $(".step2 a").first()[0].click();
}

function invoice_action(){
    getTaskWorks(function(tWorks){
    
        if($("#orderForm a:contains('不开发票')").length>0){
            submitBegin();
        }
        else if($("#orderForm a:contains(纸质发票)").length>0){
            $("#orderForm a:contains(纸质发票)")[0].click();
        }else if($("#tabInvoices a:contains('不开发票')").length >0){
            submitBegin_new();
        }else if($("#tabInvoices a:contains('明细')").length >0 && !tWorks.selected_fp){
            $("#tabInvoices a:contains('明细')")[0].click();
            setTimeout(function(){
                invoice_action_new(submitBegin_new);
            },3*1000);
        }else{
            if($("#btnKuangPay").length >0 && $("#btnKuangPay a:contains('京东支付')").length >0){
                submitBegin_new();
            }else{
                submitBegin();
            }
            
        }

    })
}

function invoice_action_new(callback){

    if($("#invoices:visible").length >0){

        clue('发票页面');
        $("#fpType li:contains('个人')")[0].click();
        if($("#fpContent li:contains('不开发票')").length >0){
            $("#fpContent li:contains('不开发票')")[0].click();
        }else{
            clue('找不到不开发票');
            setTaskWorks({selected_fp:true},function(){
                $("#fpContent li").eq(0)[0].click()
            })
            
        }
        
        setTimeout(function(){
            var fp_content = $("#fpContent li[class='selected']").text();
            clue('确认发票修改,选择的发票内容：' + fp_content);
            $("#btnBottomConfirmBar")[0].click();
            callback && callback();
            // setTimeout("location.reload();",2*1000)
        },3*1000)

    }else{
        clue('重新选择发票');
        $("#invoices").show();
        setTimeout(function(){
            invoice_action_new(callback);
        },3*1000);
    }

    // clue('发票修改完毕');
    // submitBegin_new();
        
}

function submitBegin(){

    //检查赠品无货情况
    var stockDiv = $("#stockDiv");
    if(stockDiv.length > 0){
        var message = stockDiv[0].innerText.toString();
        updateHostStatus(1410208);
        //reportProductStockout("无货："+message);
        //添加任务备注
        var remark = "赠品无货："+ $.trim(message);
        //任务备注 + 异常标记 add 2016-06-03
        // addTaskOrderRemark(remark,function(){});
            reportProductStockout(remark);//标记异常
        
        clue(remark, 'error');
        clue(message);
        return false;
    }

    //京东手机端不使用京豆
    var use_bean = $("#useJdbean .switched");
    if(use_bean.length > 0){
        use_bean[0].click();
    }

    //提交订单核对商品数量
    var num = 0;
    if($(".step3-more:visible").length > 0){
        num = $(".step3-more .s-item .sitem-r")[0].innerText.match(/\d+/)[0];
    }else{
        var num_p = $(".step3 .sitem-m .sitem-m-txt").next('p');
        if(num_p.length > 0){
            num = num_p[0].innerText.match(/\d+/)[0];
        }else{
            var sam_sum = $(".sitem-btm-detail .sam-num");
            if(sam_sum.length > 0){
                num = sam_sum[0].innerText.match(/\d+/)[0];
            }
        }
    }




    //订单金额



    var payMoney = $("#pay-amount");

    if(payMoney.length >0){
        pay_fee = payMoney.find("span:contains('总价')").text().match(/\d+(=?.\d{0,2})/g)[0];
    }else if($("#payMoney").length >0){
        pay_fee = $("#payMoney").text().replace(/\s+/g,'');
        shipping_fee = $("#yunfeeList .s-item div:contains('运费')").next('div.sitem-r').text().match(/\d+(=?.\d{0,2})/g)[0];
    }

    //pay_fee = payMoney.text().replace(/\s+/g,'');
    
    //shipping_fee = $("#yunfeeList .s-item div:contains('运费')").next('div.sitem-r').text().match(/\S+/g)[0];
    //shipping_fee = payMoney.find("span:contains('运费')").text();
    clue('应付款金额：'+pay_fee);

    var total_nums = 0;

    group_orders.forEach(function(order){
        total_nums += parseInt(order.amount);
    })

    useLocal(function(local){
        var task = localXss.currentTask;
        if(total_nums != num){
            updateHostStatus(1409203);//商品数量不对
            clue('商品数量：'+num+' 件，检查购物车商品', 'error');
            $("#m_common_header_shortcut_p_cart a")[0].click();
        }else{
            clue('商品数量：'+num+' 件', 'success');
            lazy(submiOrder);
        }
    });

}

function submitBegin_new(){

    //提交订单核对商品数量
    var num = 0;
    $(".sku_num").each(function(){
        console.log($(this).text().match(/\d+/)[0]);
        num += parseInt($(this).text().match(/\d+/)[0]);
    })

    //订单金额

    pay_fee = $("#pageTotalPrice").attr('price');

    if($(".buy_chart_item:contains('运费')").length >0){
        shipping_fee = $(".buy_chart_item:contains('运费')").find('.buy_chart_item_price').text().match(/\d+(=?.\d{0,2})/g)[0];
    }else if($("#feeDetail").length >0){
        shipping_fee = $("#feeDetail .buy_chart_item:contains('运费')").find('.buy_chart_item_price').text().match(/\d+(=?.\d{0,2})/g)[0];
    }


    clue('应付款金额：'+pay_fee);

    var total_nums = 0;

    group_orders.forEach(function(order){
        total_nums += parseInt(order.amount);
    })

    useLocal(function(local){
        var task = localXss.currentTask;
        if(total_nums != num){
            updateHostStatus(1409203);//商品数量不对
            clue('商品数量：'+num+' 件，检查购物车商品', 'error');

            // $("#m_common_header_shortcut_p_cart a")[0].click();
        }else{
            clue('商品数量：'+num+' 件', 'success');
            lazy(submiOrder_new);
        }
    });

}

function submiOrder_new(){

    if($("#tabInvoices:visible").length >0){
        clue($.trim($("#tabInvoices .content span").text()));
    }
    
    clue('提交订单');

    var yunfee = shipping_fee;
    if(yunfee && yunfee > 50){
        clue('订单运费大于50， 不能提交', 'error');
        updateHostStatus(1410009);
        return false;
    }

    var items = {
        temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
        temp_shipping_fee: shipping_fee==0?shipping_fee:shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
    }
    setLocal(items, function(){
        if($("#btnPayOnLine a:contains('在线支付')").length >0){
            clue('在线支付');
            $("#btnPayOnLine a:contains('在线支付')")[0].click();
        }else if($("#btnKuangPay a:contains('京东支付')").length >0){
            clue('京东支付');
            $("#btnKuangPay a:contains('京东支付')")[0].click();
        }
        
    });

}

//提交订单
function submiOrder(){

    if($(".receipt-info").length >0){
        clue($.trim($(".receipt-info").text()));
    }
    
    clue('提交订单');

    //var yunfee = $("#yunfeeList .s-item:contains('运费')").find('.sitem-r').text().match(/\d+/);
    var shipping = $("#pay-amount span:contains('运费')");
    if(shipping.length > 0){
        var yunfee = shipping.text().match(/\d+(=?.\d{0,2})/g);
        shipping_fee = shipping.text();//运费
        if(yunfee && yunfee[0] > 50){
            clue('订单运费大于50， 不能提交', 'error');
            updateHostStatus(1410009);
            return false;
        }
    }else{
        var yunfee = shipping_fee;
        if(yunfee && yunfee[0] > 50){
            clue('订单运费大于50， 不能提交', 'error');
            updateHostStatus(1410009);
            return false;
        }
    }

    //无货
    if($(".confirm-w:visible").length >0){
        var message = $(".confirm-w:visible").find(".confirm-txt").text();
        if(message.indexOf('无货') != -1){

            if(message.indexOf('选购商品全部无货') != -1){
                group_orders.forEach(function(order){
                    reportGroupOrderException(message,order.id);
                })
            }else{
                clue(message,'error');
                set_group_order_exception(message);
            }
            // reportProductStockout(message);
            // reportGroupOrderException(message,group_orders[0].id);
        }
    }


    var items = {
        temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
        temp_shipping_fee: shipping_fee==0?shipping_fee:shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
    }
    setLocal(items, function(){
        if($("#orderForm a:contains(提交订单)").length >0){
            clue('提交订单');
            $("#orderForm a:contains(提交订单)")[0].click();
        }else{
            clue('在线支付');
            $("#yunfeeList a:contains('在线支付')")[0].click();
        }
        
    });

}


            //匿名函数结束
        })
    }

}})();