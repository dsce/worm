(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

            getSharedTaskWorks(function(shareTask){

                if(shareTask.jd_order_id || localXss.currentTask.business_oid){
                    //订单已存在
                    clue("准备浏览器");
                    setTimeout(function(){
                        clearChangeUseragent(to_login_message);
                    }, 3000);

                    return false;
                }else{
                    if($(".bottom-bar-pannel img[func='cart']").length >0){
                        $(".bottom-bar-pannel img[func='cart']")[0].click()
                    }else{
                        location.href = 'https://p.m.jd.com/cart/cart.action';
                    }
                }
            })

           
            //匿名函数结束
        })
    }

}})();