(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//M端添加收货地址

var address;

if($("#body:contains('新建收货地址')").length >0 ){

	//增加收获地址
	clue('添加收货地址');
	setTimeout(function(){
		// Task(index);
        index();
	},3*1000);

}else{

}




function index(local){

	updateHostStatus(1303000);

    address = localXss.currentTask.consignee;

    console.log(address);

    autoResetWatchDogTimer(180*1000);

    // var submit_address_url = 'https://trade.jd.com/shopping/dynamic/consignee/saveConsignee.action';

    var get_area_api = 'https://p.m.jd.com/norder/selectArea.action';

    var get_town_api = 'https://p.m.jd.com/norder/selectTown.action';


    var province_name = address.province;
    var city_name = address.city;
    var area_name = address.area;
    var street_name = address.street;

    var province_options = null;
    var city_options = null;
    var county_options = null;
    var town_options = null;


    async.waterfall([
        function (callback){
            useLocal(function(local){
                
                callback(null);
            });
        },
        function (callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){

                //ajaxAddressDataJson(get_province_url,{},function(provinces){
                    var provinces = jd_address_provinces;
                    province_options = provinces;
                    var province_id = getAddressJsonKey(province_name,provinces);
                    clue('省份 '+provinces[province_id]);
                    callback(null,province_id);
                //});

            //},6000);
        },
        function (province_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_city_url,{provinceId: province_id},function(citys){
                    var citys = jd_address_citys[province_id];
                    city_options = citys;
                    var city_id = getAddressJsonKey(city_name,citys);
                    clue('城市 '+citys[city_id]);
                    callback(null, province_id, city_id);
                //});
            //},6000);
        },
        function (province_id, city_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_county_url,{cityId: city_id},function(countys){
            ajaxNewAddressDataJson(get_area_api,{idCity: city_id},function(countys){
                // var countys = jd_address_countys[city_id];
                    console.log('当前地区' + area_name);
                    county_options = countys;
                    var county_id = getKey(area_name,countys);
                    clue('乡镇 '+countys[county_id].name);
                    callback(null, province_id, city_id, county_id);
            })

            


                //});
            //},6000);
        },
        function (province_id, city_id, county_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_town_url,{countyId: county_id},function(towns){
            ajaxNewAddressDataJson(get_town_api,{idArea: county_options[county_id].id},function(towns){
                    console.log('jd街道');
                    console.log(towns);
                    town_options = towns;
                    var town_id = getKey(street_name,towns);
                    //var town_id = null;
                    towns !== undefined && town_id && towns[town_id] !== undefined ? clue('街道 '+towns[town_id].name) : null;
                    callback(null, province_id, city_id, county_id, town_id);
            })
                    
                //});
            //},4000);
        },
        function(province_id, city_id, county_id, town_id, callback){
            autoResetWatchDogTimer();
            clue("地址生成，等待保存");
            setTimeout(function(){
                address.short_address = address.short_address.length>50?address.short_address.substr(0,50):address.short_address;
                if(!address.short_address){
                    clue('短地址为空,保存失败','error');
                    return false;
                }
                
                var form_data = {
                    'name'          : address.name,
                    'provinceId'    : province_id,
                    'cityId'        : city_id,
                    'countyId'      : county_options[county_id].id,
                    'townId'        : town_id && town_options ? town_options[town_id].id : '',
                    'address'       : address.short_address.replace(/\s|#|°|%/g,''),
                    'mobile'        : address.mobile,
                    'phone'         : '',
                    'email'         : '',
                    'addressName'  : address.name + ' ' + address.province ,
                    'provinceName'  : province_options[province_id] ? province_options[province_id] :'',
                    'cityName'  : city_options[city_id] ? city_options[city_id] :'',
                    'countyName'  : county_options[county_id] ? county_options[county_id].name :'',
                    'townName'  : town_options && town_id && town_options[town_id] ? town_options[town_id].name :'',

                };

                //保存收货地址信息
                var order_address = {
                    name: form_data.name,
                    province: province_name,
                    city: city_options[city_id] ? city_options[city_id] :'',
                    area: county_options[county_id] ? county_options[county_id].name :'',
                    street: town_options && town_id && town_options[town_id] ? town_options[town_id].name :'',
                    short_address: form_data.address,
                    mobile: form_data.mobile
                };
                setLocal({order_address: jdMunicipality(order_address)},function(){

                    //地址随机完毕
                    $("input[name='address.idProvince']").val(province_id);
                    $("input[name='address.idCity']").val(city_id);
                    $("input[name='address.idArea']").val(county_options[county_id].id);
                    $("input[name='address.idTown']").val(town_id && town_options ? town_options[town_id].id : '');

                    $("#editAddressForm #provinceNameIgnoreId").val(province_options[province_id] ? province_options[province_id] :'');
                    $("#editAddressForm #cityNameIgnoreId").val(city_options[city_id] ? city_options[city_id] :'');
                    $("#editAddressForm #areaNameIgnoreId").val(county_options[county_id] ? county_options[county_id].name :'');
                    $("#editAddressForm #townNameIngoreId").val(town_options && town_id && town_options[town_id] ? town_options[town_id].name :'');

                    $("#uersNameId").val(address.name);
                    $("#mobilePhoneId").val(address.mobile);
                    $("#address_where").val(address.short_address);

                    //设置默认
                    if($(".switch").length >0){
                        $(".switch")[0].click();
                    }else{
                        $(".set-default .myswitch")[0].click();
                    }
                    

                    callback && callback();

                    // $.ajax( {
                    //     type : "POST",
                    //     dataType : "text",
                    //     url : submit_address_url,
                    //     data :json2string(form_data),
                    //     cache : false,
                    //     success : function(dataResult) {
                    //         callback(null, order_address);
                    //     },
                    //     error : function(XMLHttpResponse) {
                    //         callback(null, order_address);
                    //     }
                    // });

                });

            },6000);
        }
    ],function(err, results){
        //clue("地址添加完成，保存后刷新");
        useLocal(function(local){
            var task = localXss.currentTask;
            task.consignee = local.order_address;
            var details = {task: task};

            if(local.address_update === true){
                details.address_update = false;
            }
            setLocal(details, function(){
                lazy(function(){
                    // location.reload();
                    
                    $("#editAddressForm").submit();

                });
            });
        });


    });

}

function ajaxAddressDataJson(url, data, callback) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: url,
        data: data,
        cache: false,
        success: function (dataResult) {
            callback(dataResult);
        },
        error: function (XMLHttpResponse) {
            console.log(XMLHttpResponse);
            autoAddress();
        }
    });

}

function ajaxNewAddressDataJson(url, data, callback) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: url,
        data: data,
        cache: false,
        success: function (dataResult) {
            // var reg = /\((.+)\)/;
            callback(dataResult.addressList);
        },
        error: function (XMLHttpResponse) {
            console.log(XMLHttpResponse);
            autoAddress();
        }
    });

}

function getAddressJsonKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key];
        //console.log(v);
        if(value != '' && v.indexOf(value) != -1){
            //console.log("return key",key);
            return key;
        }
    }

    var key = arr_keys[random(0,length-1)];
    console.log( "random", arr_keys,length,key);
    return key;
}



//获取ID
function getKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key].name;
        //console.log(v);
        if(value != '' && v == value){
            //console.log("return key",key);
            return key;
        }
    }
    var key = arr_keys[random(0,length-1)];
    console.log( "random", arr_keys,length,key);
    return key;
}



function json2string(json){
    var str = '';
    for(var i in json){
        str += 'consigneeParam.' + i + '=' + json[i] + '&';
    }
    str = str.substring(0, str.length-1);
    return str;
}




    
function checkAddress(callback){

    console.log('是否已检测过' + consigneeChecked);
    if(consigneeChecked == true){
        callback();return false;
    }

    var get_address_api = 'https://d.jd.com/area/get?callback=jQuery' + random(100000,9999999) + '&_=' + new Date().getTime();

    var province_id = getAddressJsonKey(address.province, jd_address_provinces);
    console.log('province_id', province_id, address.province);
    if(address.province && province_id){
        console.log("省正常");
        var city_id = getAddressJsonKey(address.city, jd_address_citys[province_id]);
        console.log('city_id', city_id, address.city);
        address.city = jd_address_citys[province_id][city_id];
        if(city_id){
            console.log("市正常");
            var area_id = getAddressJsonKey(address.area, jd_address_countys[city_id]);
            console.log('area_id', area_id, address.area);

                //拿到京东区
                ajaxNewAddressDataJson(get_address_api,{fid: city_id},function(countys){
                    
                    console.log(countys);
                    
                    //区是否存在
                    var area_key = getKey(address.area,countys);
                    address.area_key = countys[area_key].id;
                    address.area = countys[area_key].name;
                    console.log('记录当前区' + address.area);


                    //拿到JD接口当前正常区下街道
                    ajaxNewAddressDataJson(get_address_api,{fid: address.area_key},function(towns){
                        console.log('街道地址');
                        console.log(towns);
                        console.log(count(towns));
                        
                        if(count(towns) > 0){
                            var town_key = getKey(address.street,towns);
                            address.street = towns[town_key].name;
                            console.log(address.street);

                        }else{
                            address.street = '';
                            console.log('区下没有街道');
                       
                            
                        }
                        //设置标示
                        useLocal(function(local){
                            var task = localXss.currentTask;
                            task.consigneeChecked = true;
                            //task.consignee.area = address.area;
                            //task.consignee.street = address.street;
                            task.consignee = jdMunicipality(address);
                            console.log(task);
                            setLocal({task:task},function(){
                                callback();
                            })
                        });
                        
                    })

                    
                        
                })
                
        }
    }
}

//判断对象长度
function count(o){
    var t = typeof o;
    if(t == 'string'){
            return o.length;
    }else if(t == 'object'){
            var n = 0;
            for(var i in o){
                    n++;
            }
            return n;
    }
    return false;
}; 




            //匿名函数结束
        })
    }

}})();