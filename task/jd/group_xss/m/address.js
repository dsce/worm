(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/address.js

Task(index);

function index(local){
  console.log(local)
  var referrer = document.referrer;

  if(referrer.indexOf('p.m.jd.com/norder/order.action') >= 0){//订单页面来    http://p.m.jd.com/norder/order.action
    //先 删除 收货地址
    //try_btn_del();

    //纠正页面改变后的手机号问题,直接使用默认地址即可
    $("#addressForm .item-addr .m:contains('默认')")[0].click();

  }else if(referrer.indexOf('p.m.jd.com/norder/address.action') >= 0){//收货地址页面来 http://p.m.jd.com/norder/address.action
    //继续尝试删除
    try_btn_del();
  }else if(referrer.indexOf('p.m.jd.com/norder/editAddress.action') >= 0){//添加收货地址页面来 http://p.m.jd.com/norder/editAddress.action
    var btn_chk = $(".addr-info .btn-chk").not('.on');
    if(btn_chk.length >= 1){

        var tbl_cell = btn_chk.eq(0).parent(".tbl-cell");
        clue('3秒后，选择收货人信息');
        lazy(info_btn_chk(tbl_cell));

    }else{
      clue('3秒后，添加收货地址');
      lazy(addr_add);
    }
  }
}

//删除 其他收货人地址
function try_btn_del(){
  console.log('删除');
  var btn_dels = $(".addr-info .btn-del");
  if (btn_dels.length > 0) {
    clue('3秒后，删除收货人地址');
    lazy(function(){

      var del = btn_dels.eq(0);
      del.next()[0].click();

      //尝试删除收货地址
      //try_btn_del();
    });

  }else{
    //不能再删，进行添加
    clue('不能再删，进行添加');
    clue('3秒后，添加收货地址');
    lazy(addr_add);
  }
}

//选择收货信息
function info_btn_chk(btn_chk){
  btn_chk[0].click();
}

//添加收货地址
function addr_add(){
  $("#addressForm .addr-add a.btn-addr")[0].click();
}


            //匿名函数结束
        })
    }

}})();