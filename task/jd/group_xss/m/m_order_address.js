(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

// jd  修改收货地址
var address = null;
var local_order_address = null;
var promotion_url = null;
var deleted = false;
var address_update = null;
var consigneeChecked = null;

var indexed = false;

var sid = null;
Task(index);

function index(local,callback){


    sid = urlGet().sid;

    address = jdMunicipality(local.task.consignee);

    // address = (localXss.currentTask.consignee);

    //过滤收货人中间的多个空格问题
    address.name = address.name.replace(/\s+/g, '');

    consigneeChecked = localXss.currentTask.consigneeChecked;

    address_update = local.address_update;//地址需要更新，为true，删除，重新增加

    local_order_address = local.order_address;

    updateHostStatus(1302000);

    if($("#addressForm .item-addr").length >0){

        checkAddress(function(){
                tryDeleteAllAddress(callback)
            });
        // var address_default = $("#addressForm .item-addr:contains('默认')");
        // if(address_default.length > 0){
        //     checkAddress(function(){
        //         tryDeleteAllAddress(callback)
        //     });
        // }else{
        //     clue('没有默认，设第一个默认');
            
        // }
    }else{
        clue('无地址数据');
        $(".pay-btn a")[0].click();
    }
        

   

}

//五个地址内直接扫描目标地址
function scanAddressForFive(callback){
   // $("#addressList .sm").each(function(){
        var address_div = $("#addressForm .item-addr:contains('默认')");
        if(address_div.length > 0){
            
            var address_id = address_div.attr('id').match(/\d+/g)[0];
            var name = address_div.find(".mt_new span").text();
            var full_address = address_div.find('p').text().replace(/\s+/g,'').replace('默认','');
           
            var mobile = address_div.find(".mt_new strong").text().match(/\d+/g);
            var its_name = name == address.name;
            var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

            var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

            var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
            if(its_name&&its_full_address&&its_mobile&&!address_update){
                    //地址一样，不需要修改
                    clue("默认地址 一样");
                    saveAddressToRemote(function(){
                        // callback && callback();
                        address_div.find('.ia-m')[0].click();
                    })
                   

                }else{

                    clue("默认地址不一致，删除");
                        lazy(function(){
                            delAddress(address_id,function(){
                                updateHostStatus(1303000);
                                clue("添加收货地址");
                                deleted = true;
                                autoAddress();
                            });
                        });
                    

                }
            }else{
                
                lazy(function(){
                    if($("#consignee-list li").length == 5){
                         var address_div = $("#consignee-list li .consignee-item").last();
                         var address_id = address_div.attr('consigneeid').match(/\d+/g)[0];
                         delAddress(address_id,function(){
                            updateHostStatus(1303000);
                            clue("添加收货地址");
                            deleted = true;
                            autoAddress();
                        });
                     }else{
                        autoAddress();
                     }
                   
                });
            }
        

   
}

//删除所有的收货地址，，每次只删除第一个
function tryDeleteAllAddress(callback){
    if(deleted) {return false;}
    var address_list = $("#addressForm .item-addr");
    if(address_list.length >5){
            var address_div = address_list.last();
            var address_id = address_div.attr('id').match(/\d+/g)[0];
            if(address_div.find("p:contains('默认')").length > 0){ 
                var name = address_div.find(".mt_new span").text();
                var full_address = address_div.find('p').text().replace(/\s+/g,'').replace('默认','');

                var mobile = address_div.find(".mt_new strong").text().match(/\d+/g);
                var its_name = name == address.name;
                var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

                var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

                var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;

                    if(its_name&&its_full_address&&its_mobile&&!address_update){
                        //地址一样，不需要修改
                        clue("默认地址 一样");
                        saveAddressToRemote(function(){
                            // callback && callback();
                            address_div.find('.ia-m')[0].click();
                        });
                        // setIpLoc(function(){
                        //     saveAddressToRemote(function(){
                        //         updateHostStep(_host_step.open_promotion_link);//step3 打开推广链接
                        //         lazy(function(){
                        //             removeLocalPromotionUrl(function(){
                        //                 location.href = promotion_url;
                        //             });
                        //         })
                        //     })
                        // });

                    }else{

                        clue("默认地址不一致，删除");
                            lazy(function(){
                                delAddress(address_id);
                            });
                       

                    }

                }else{
                    lazy(function(){
                        delAddress(address_id);
                    });
                }


       
    }else{
        scanAddressForFive(callback);
    }
    
    
}


function autoAddress(){

    $(".pay-btn a")[0].click();

    

}

function ajaxAddressDataJson(url, data, callback) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: url,
        data: data,
        cache: false,
        success: function (dataResult) {
            callback(dataResult);
        },
        error: function (XMLHttpResponse) {
            console.log(XMLHttpResponse);
            autoAddress();
        }
    });

}

function ajaxNewAddressDataJson(url, data, callback) {
    $.ajax({
        type: "GET",
        dataType: "text",
        url: url,
        data: data,
        cache: false,
        success: function (dataResult) {
            var reg = /\((.+)\)/;
            callback(JSON.parse(reg.exec(dataResult)[1]));
        },
        error: function (XMLHttpResponse) {
            console.log(XMLHttpResponse);
            autoAddress();
        }
    });

}

function getAddressJsonKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key];
        //console.log(v);
        if(value != '' && v.indexOf(value) != -1){
            //console.log("return key",key);
            return key;
        }
    }

    var key = arr_keys[random(0,length-1)];
    console.log( "random", arr_keys,length,key);
    return key;
}

// //判断是否包含
// function checkValue(value,object){
//     var length = 0;
//     var arr_keys = new Array;
//     for(var key in object){
//         length ++;
//         arr_keys.push(key);
//         var v = object[key];
//         //console.log(v);
//         if(value != '' && v == value){
//             //console.log("return key",key);
//             return key;
//         }
//     }
//     return false;
// }

//获取ID
function getKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key].name;
        //console.log(v);
        if(value != '' && v == value){
            //console.log("return key",key);
            return key;
        }
    }
    var key = arr_keys[random(0,length-1)];
    console.log( "random", arr_keys,length,key);
    return key;
}


// //获取随机key
// function getRandomKey(object){
//     var length = 0;
//     var arr_keys = new Array;
//     for(var key in object){
//         length ++;
//         arr_keys.push(key);
        
//     }
//     var key = arr_keys[random(0,length-1)];
//     console.log( "random", arr_keys,length,key);
//     return key;
// }

function json2string(json){
    var str = '';
    for(var i in json){
        str += 'consigneeParam.' + i + '=' + json[i] + '&';
    }
    str = str.substring(0, str.length-1);
    return str;
}


// function delAddress(addressId,callback){
//     var del_address_url = "https://easybuy.jd.com/address/deleteAddress.action";
//     $.ajax({
//         type : "POST",
//         dataType : "text",
//         url : del_address_url,
//         data : "addressId=" + addressId,
//         cache : false,
//         success : function(dataResult) {
//             clue("多余地址删除，刷新");
//             if(callback){
//                 callback();
//             }else{
//                 lazy(function(){
//                     location.reload();
//                 });
//             }
            
//         },
//         error : function(XMLHttpResponse) {
//             clue("多余地址删除，刷新");
//             lazy(function(){
//                 location.reload();
//             });
//         }
//     });
// }

function delAddress(addressId,callback){
    var del_address_url = "https://p.m.jd.com/norder/delAddress.action";
    $.ajax({
        type : "GET",
        dataType : "text",
        url : del_address_url,
        data : "addressId=" + addressId + "&addressFrom=del&sid=" +sid,
        cache : false,
        success : function(dataResult) {
            clue("多余地址删除，刷新");
            if(callback){
                callback();
            }else{
                lazy(function(){
                    location.reload();
                });
            }
            
        },
        error : function(XMLHttpResponse) {
            clue("多余地址删除，刷新");
            lazy(function(){
                location.reload();
            });
        }
    });

    //出现https请求http 
    setTimeout(function(){
        if(callback){
            callback();
        }else{
           location.reload();
        }
    },10*1000)
}

    
function checkAddress(callback){

    getTaskWorks(function(tWorks){

    console.log('是否已检测过' + tWorks.consigneeChecked);
    if(tWorks.consigneeChecked){
        callback();return false;
    }

    var get_address_api = 'https://d.jd.com/area/get?callback=jQuery' + random(100000,9999999) + '&_=' + new Date().getTime();

    var province_id = getAddressJsonKey(address.province, jd_address_provinces);
    console.log('province_id', province_id, address.province);
    if(address.province && province_id){
        console.log("省正常");
        var city_id = getAddressJsonKey(address.city, jd_address_citys[province_id]);
        console.log('city_id', city_id, address.city);
        address.city = jd_address_citys[province_id][city_id];
        if(city_id){
            console.log("市正常");
            var area_id = getAddressJsonKey(address.area, jd_address_countys[city_id]);
            console.log('area_id', area_id, address.area);

                //拿到京东区
                ajaxNewAddressDataJson(get_address_api,{fid: city_id},function(countys){
                    
                    console.log(countys);
                    
                    //区是否存在
                    var area_key = getKey(address.area,countys);
                    address.area_key = countys[area_key].id;
                    address.area = countys[area_key].name;
                    console.log('记录当前区' + address.area);


                    //拿到JD接口当前正常区下街道
                    ajaxNewAddressDataJson(get_address_api,{fid: address.area_key},function(towns){
                        console.log('街道地址');
                        console.log(towns);
                        console.log(count(towns));
                        
                        if(count(towns) > 0){
                            var town_key = getKey(address.street,towns);
                            address.street = towns[town_key].name;
                            console.log(address.street);

                        }else{
                            address.street = '';
                            console.log('区下没有街道');
                       
                            
                        }
                        //设置标示

                        setTaskWorks({consigneeChecked:true},function(){
                            useLocal(function(local){
                                var task = localXss.currentTask;
                                // task.consigneeChecked = true;
                                //task.consignee.area = address.area;
                                //task.consignee.street = address.street;
                                task.consignee = jdMunicipality(address);
                                console.log(task);
                                setLocal({task:task},function(){
                                    
                                })
                                setLocal({order_address:task.consignee},function(){
                                    callback();
                                })
                            });
                        })
                        
                        
                    })

                    
                        
                })
                
        }
    }

    })
}

//判断对象长度
function count(o){
    var t = typeof o;
    if(t == 'string'){
            return o.length;
    }else if(t == 'object'){
            var n = 0;
            for(var i in o){
                    n++;
            }
            return n;
    }
    return false;
};

            //匿名函数结束
        })
    }

}})();