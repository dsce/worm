(function () {
    var observerId = tabCommon.observer(document.querySelector('#editAddressForm'))
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/addressedit.js

var town_selected = false;
var area_selected = false;
var city_selected = false;

var area_sel = '';

var setTime_new_abtn_type = false;

Task(index);

function index(local){

  var consignee = localXss.currentTask.consignee;


  // $("#address_name").val(consignee.name);
  // $("#address_mobile").val(consignee.mobile);
  // $("#address_where").val(consignee.short_address);

}

//保存收货人信息
function new_abtn_type(){

  $("#editAddressForm .new-abtn-type:visible")[0].click();
  $("#editAddressForm .new-abtn-type").hide();
  
}

function address_submit(local){
  var consignee = localXss.currentTask.consignee;

    // $("#address_name").val(consignee.name);
  // $("#address_mobile").val(consignee.mobile);
  // $("#address_where").val(consignee.short_address);
  writing($("#address_name"),consignee.name,function(){
    // lazy(function(){
      writing($("#address_mobile"),consignee.mobile,function(){
        // lazy(function(){
          writing($("#address_where"),consignee.short_address,function(){
            lazy(function(){
              clue("5秒后，下一步");
              new_abtn_type();
            })
          })
        // })
      })
    // })
  });


}

//选择省份
function set_province(local){

  var v = localXss.currentTask.consignee.province;
  var select = $("#address_province");
  var options = select.find('option');
    
  var option = options.filter(function() {
    var t = $(this).text();
    return t == v
  });

  if (option.length > 0) {
    option.prop('selected', true);
    change_event(select[0]);

  }else{//没有匹配到

    clue('没有匹配省份');

  }

}
//选择城市
function set_city(local){

  var v = localXss.currentTask.consignee.city;
  var select = $("#address_city");
  var options = select.find('option');
    
  var option = options.filter(function() {
    var t = $(this).text();
    return t == v
  });

  if (option.length > 0) {
    option.prop('selected', true);
    change_event(select[0]);

  }else{//没有匹配到城市
    clue('没有匹配城市，随机');
  
    var index = Math.floor(Math.random()*(options.length)+1);//不选第一个
    options.eq(index).prop('selected', true)
    change_event(select[0]);

  }

}

//选择县区
function set_area(local){

  var v = localXss.currentTask.consignee.area;
  var select = $("#address_area");
  var options = select.find('option');
    
  var option = options.filter(function() {
    var t = $(this).text();
    return t == v
  });

  if (option.length > 0) {
    option.prop('selected', true);
    area_sel = option.text();
    change_event(select[0]);
    //save();
  }else{//没有匹配到城市
    clue('县区，随机');
  
    var index = Math.floor(Math.random()*(options.length-1)+1);//不选第一个
    area_sel = options.eq(index).text();
    options.eq(index).prop('selected', true);
    change_event(select[0]);

  }
}

//选择乡镇
function set_town(){
  
  //随机乡镇
  var options = $("#address_town option");
  option = options.eq(parseInt(Math.random()*(options.length-1))+ 1);

  clue('乡镇，随机');

  option.prop('selected', true);
  change_event(option[0]);

}



//触发 change事件
function change_event(o){
  var changeEvent = document.createEvent("MouseEvents");
  changeEvent.initEvent("change", true, true);
  console.log(o);
  o.dispatchEvent(changeEvent);
}

function municipality(consignee){
  var province = consignee.province;

  if(province=='北京'||province=='上海'||province=='天津'||province=='重庆'){
    consignee.city = consignee.area;
  }

  return consignee;
}





//页面监控

tabCommon.mutation(observerId,function (mutation) {
      console.log(mutation.type);
      console.log(mutation.target);
      console.log(mutation);
      if (mutation.type === 'childList') {
        if (mutation.target.id === 'address_province' && mutation.addedNodes.length > 0 && mutation.addedNodes[0].id === 'option_add_84') {
          // 选择省，延迟5秒
          //   如果 地址 == 北京  不执行
          setTimeout(function(){
            Run(function(local){
              if(localXss.currentTask.consignee.province !== '北京'){
                set_province(local);
              }
              
            });
          },2000);

        }

        if (mutation.target.id === 'city_label' && mutation.addedNodes.length > 0) {
          // 选择市，延迟5秒
          // 如果 province == 北京    直接选择城市
          // 如果不等北京  并且 不等于朝阳区 执行选择城市

          setTimeout(function(){
            Run(function(local){
              localXss.currentTask.consignee = municipality(localXss.currentTask.consignee);
              if(localXss.currentTask.consignee.province === '北京'){
                if(localXss.currentTask.consignee.city !== '朝阳区'&&city_selected===false){
                  set_city(local);
                  city_selected = true;
                }
              }else{
                if(mutation.addedNodes[0].textContent !== '朝阳区' && city_selected === false){
                  set_city(local);
                  city_selected = true;
                }
              }
            });
          },2000);

        }

        if (mutation.target.id === 'area_label' && mutation.addedNodes.length > 0) {

          //setTimeout(function(){
            Run(function(local){
              setTimeout(function(){


              localXss.currentTask.consignee = municipality(localXss.currentTask.consignee);
              var province = localXss.currentTask.consignee.province;
              var city = localXss.currentTask.consignee.city;
              var area = localXss.currentTask.consignee.area;
              if(province === '北京' && city==='朝阳区'){
                if(area!=='三环以内'&&city_selected===true&&area_selected===false){
                  set_area(local);
                  area_selected = true;

                  setTimeout(function(){
                    if(town_display === false){
                      address_submit(local);//提交
                    }
                  },5000);

                }
              }else{
                if (province==$("#address_province option:selected").text()&&city==$("#address_city option:selected").text()) {
                  if(mutation.addedNodes[0].textContent!=area&&city_selected===true&&area_selected===false){
                    set_area(local);
                    area_selected = true

                    setTimeout(function(){
                      if(town_display === false){
                        address_submit(local);//提交
                      }
                    },5000);
                  }
                };
              }


              },5000);



              // if(province === '北京' && city === '朝阳区') {
              //     //执行选择区
              //     set_area(local);
              //     //延迟 5秒
              //     if (area_selected && town_display === false) {
              //       // 提交
              //       address_submit();
              //     }
              //       area_selected = true
              // }
              // else if (mutation.addedNodes[0].textContent !== '三环以内' && city === $('#address_city option:selected').text()) {

              //   //执行选择区
              //   set_area(local);
              //   //延迟 5秒
              //   if (area_selected && town_display === false) {
              //     // 提交
              //     address_submit();
              //   }
              //   area_selected = true
              // }
            });
          //},1000);



        }

        if (mutation.target.id === 'town_label' && mutation.addedNodes.length > 0) {
          Run(function(local){

          setTimeout(function(){
            localXss.currentTask.consignee = municipality(localXss.currentTask.consignee);
            var province = localXss.currentTask.consignee.province;
            var city = localXss.currentTask.consignee.city;
            var area = localXss.currentTask.consignee.area;

            if(town_display&&area_selected===true&&town_selected===false){

              if (province==$("#address_province option:selected").text()&&city==$("#address_city option:selected").text()&&area_sel==$("#address_area option:selected").text()) {
                set_town();
                town_selected = true;

                address_submit(local);//提交
              }
            }

          },5000);



          // 
          // if (area_selected && town_selected === false) {
          //   // 随机选择
          //   set_town();
          //   town_selected = true

          //   // 提交
          //   

          // }
          });

        }
      }
      else if (mutation.type === 'attributes') {
        if (mutation.target.id === 'townaddress') {
           town_display = $("#address_town:visible").length>0 ? true : false;
        }
      }

    })

            //匿名函数结束
        })
    }

}})();