(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/login.js
var verify_code_visible = false;
var verify_code_error = false;
var verify_cid = 0;
var write_full = false;

tabCommon.mutation(observerId,function (mutation) {
    //console.log(mutation.type);
    //console.log(mutation.target);
    //console.log(mutation);
    if(mutation.type == 'childList'){
        console.log(mutation.target);
        console.log(mutation);
        if(mutation.target.className == "err-msg" && mutation.addedNodes.length > 0){
            if($(".err-msg:contains('验证码输入错误')").length > 0){
                Run(function(){
                    //验证码错误，报错
                    verify_code_error = true;
                    chrome.extension.sendMessage({act: 'https_tabs_verify_fail', cid: verify_cid});
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                });

            }else if($(".err-msg:contains('账户名和密码不匹配')").length > 0){
                //账户名和密码不匹配  账号重置
                Run(function(){
                    var msg = $(".err-msg").text();
                    console.log(msg);
                    tabCommon.sm(taskVars, '.disable_account', '[worm-wap]'+msg);
                });

            }else{
                //登陆错误问题
                var msg = $(".err-msg").text();

                Run(function(){
                    console.log(msg);
                    if(msg.indexOf('账户名不存在') != -1 && msg.indexOf('账户名或密码不正确')!=-1){
                        tabCommon.sm(taskVars, '.disable_account', '[worm-wap]'+msg);
                    }

                });
            }
        }else if(mutation.target.className == 'pop-msg' && mutation.addedNodes.length > 0){
            var pop_msg = $("#pop-choose .pop-msg:visible").text();
            if(pop_msg.indexOf('您的账号存在安全风险') != -1){
                Run(function(){
                    //您的账号存在安全风险，请进行短信登录验证
                    tabCommon.sm(taskVars, '.disable_account', '[worm-wap]'+pop_msg);
                });
            }
        }
    }else if(mutation.type == 'attributes'){
        if(mutation.target.id == 'pop-confirm'){
            var pop_msg = $("#pop-confirm .pop-msg:visible").text();
            if(pop_msg.indexOf('您的账号存在安全风险') != -1){
                Run(function(){
                    //您的账号存在安全风险，请进行短信登录验证
                    tabCommon.sm(taskVars, '.disable_account', '[worm-wap]'+pop_msg);
                });
            }
        }
    }
});

Task(index);

function index(local) {
    var task = localXss.currentTask;

    //地址中包含home.jd.com
    //https://plogin.m.jd.com/user/login.action?appid=100&returnurl=http://home.m.jd.com/myJd/home.action?sid=4fcd62d17b6d3e94cc5912e69663da44
    if(location.href.indexOf('home.m.jd.com') > -1){
        location.href = decodeURIComponent(location.href).replace('http://home.m.jd.com/myJd/home.action', 'http://m.jd.com/');
    }


    updateHostStatus(1402200);//登陆-手机

    addListenerMessage(function (request) {
        console.log(request);
        if (request.act == 'business_account_ready') {
            useLocal(function(local){
                var task = localXss.currentTask;
                inputLogin(localXss.currentTask);
            });
        }
    });
    tabCommon.sm(taskVars, '.getBusinessAccount');



    //有验证码，放大一下
    if ($('#captcha-img:visible').length > 0) {
        //200*76
        $(".txt-captcha").width('50%');
        $("#captcha-img:visible").width(200).height(76);
        $("#captcha-img img:visible").width(200).height(76);


        //打码结果
        addListenerMessage(function (request) {
            console.log(request);
            if (request.act == 'https_tabs_verify_code_result') {
                verify_cid = request.cid;
                //add 20160325
                console.log('核对已填写的账号密码');

                var username = $('.txt-username').val();
                var password = $(".txt-password").val();

                //console.log(username + password);
                if(username != task.username){
                    $(".txt-username").val(task.username);
                }
                if(password != task.password){
                    $(".txt-password").val(task.password);
                }
                //console.log(username + password);
                //add end

                writing($(".txt-captcha"), request.text);
            }else if(request.act == 'tab_get_cookies_response'){
                //if(request.cookies.length > 0){
                //    var imgsrc = location.origin + $("#captcha-img img").attr('src') + "&v="+Math.LN2;
                //    var plogin_cookie = "lsid="+request.cookies[0].value;
                //    chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc, cookie: plogin_cookie});
                //}else{
                //    clue("cookie 不存在，不能打码");
                //}
            }
        });
    }

    //验证码输入够4位直接登录
    $('.txt-captcha').on('keyup', function () {
        if ($('.txt-captcha').val().length >= 4) {
            $(".btn-login").removeClass('btn-disabled');
            clue("4位 自动登录");
            lazy(function () {
                useLocal(function(local){
                    if(local.isRunning){
                        clicking($(".btn-login"));
                    }else{
                        setLocal({isRunning: true}, function(){
                            clicking($(".btn-login"));
                        });
                    }
                });

            }, 2);

        }
    }).focus()

}

function autoVerifyCode() {
    //准备开始打码
    clue('自动打码');
    updateHostStatus(1402200);

    //拿cookie
    //chrome.extension.sendMessage({act: 'tabs_get_cookies', details: {domain: 'plogin.m.jd.com', name: 'lsid'}});
    //chrome.extension.sendMessage({act: 'tabs_get_cookies', details: {url: location.href, name: 'lsid'}});

    var base64 = getBlobImg($("#captcha-img img")[0]);
    //console.log(blob);
    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});

    //var imgsrc = location.origin + $("#captcha-img img").attr('src') + "&v="+Math.LN2;
    //console.log('get img');
    //var xhr = new XMLHttpRequest();
    //xhr.open('GET', imgsrc, true);
    //xhr.responseType = 'blob';
    //xhr.onload = function(e) {
    //    if (this.readyState==4){
    //        if (this.status == 200) {
    //            console.log(this);
    //            console.log(e);
    //
    //            var imgblob = this.response;
    //            console.log(imgblob);
    //            $(".qq").append('<img id="qqimg" src="" />');
    //            $("#qqimg")[0].src = URL.createObjectURL(imgblob);
    //            chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_blob', imgblob: imgblob});
    //        }else{
    //            console.warn('get img result error',this.response);
    //            location.reload();
    //        }
    //    }else{
    //        console.warn('get img error',this);
    //        location.reload();
    //    }
    //};
    //xhr.send();

}

function inputLogin(task){
    writing($(".txt-username"), task.username, function () {
        writing($(".txt-password"), task.password, function () {


            if($('#captcha-img:visible').length > 0){
                autoVerifyCode();
                updateHostStatus(1402201);
            }else{
                $(".btn-login").removeClass('btn-disabled');
                lazy(function(){
                    clicking($(".btn-login"));
                }, 5);
            }

        })
    })
}

            //匿名函数结束
        })
    }

}})();