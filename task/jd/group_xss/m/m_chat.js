(function () {
    var observerId = tabCommon.observer($("#scrollDiv")[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东聊天 - M端 - 店铺客服版 - 有验证码
var checkedKufu,checkedMe = false;
var msgData;
var openTime = new Date().getTime();
var sendedNums = 0;

//insertScript(function(){onbeforeunload=function(){}});


tabCommon.mutation(observerId,function(mutation){
    console.log(mutation);

    if(mutation.type === 'childList'){
    	
    	if(mutation.addedNodes.length>0){

    		if(mutation.addedNodes[0].childNodes.length > 0){
    			
    			if(mutation.addedNodes[0].childNodes[0].className){
    				console.log(mutation.addedNodes[0].childNodes[0].className);

    				// useLocal(function(local){
		    		// 	if(localXss.currentTask.is_mobile == '1'){
		    		// 		if(mutation.addedNodes[0].childNodes[0].className.indexOf('i-notice1') !=-1){
		    		// 				console.log('监控到提示');
		    		// 				Run(checkNotice);
		    		// 			}
		    		// 	}else{
		    		// 		if(mutation.addedNodes[0].childNodes[0].className.indexOf('i-success') !=-1){
		    		// 				console.log('监控到提示');
		    		// 				Run(checkNotice);
		    		// 			}
		    		// 	}
		    		// })

    				if(mutation.addedNodes[0].childNodes[0].className.indexOf('list_risk') !=-1){
		    			 console.log('监控到出现验证码');
			    		 //index();
			    		 Run(index);
					}else if(mutation.addedNodes[0].childNodes[0].className.indexOf('i-notice1') !=-1 || mutation.addedNodes[0].childNodes[0].className.indexOf('i-success') !=-1){
						 console.log('监控到提示');
						 // Run(checkNotice);
						 useLocal(function(local){
						 	if(localXss.currentTask.is_mobile != '1' && mutation.addedNodes[0].innerHTML.indexOf('恭喜您，输入正确') != -1 ){
						 		console.log('PC端验证码输入正确');
						 		return false;
						 	}else{
						 		Run(checkNotice);
						 	}
						 })
						  
		    		}else if(mutation.addedNodes[0].childNodes[0].className.indexOf('service') !=-1){
		    			console.log('监控到客服的消息');
		    			//checkedKufuMsg();

		    		}else if(mutation.addedNodes[0].childNodes[0].className.indexOf('customer') !=-1){
		    			if(mutation.addedNodes[0].attributes){
		    				console.log(mutation.addedNodes[0].attributes.time.value);
		    				var msgTime = mutation.addedNodes[0].attributes.time.value;
		    				var defaultMeMsg = mutation.addedNodes[0].innerHTML.indexOf('list-txt');//系统默认发的信息
		    				console.log('defaultMeMsg',defaultMeMsg);
		    				if(msgTime>openTime && defaultMeMsg == -1){
		    					console.log('监控到自己的消息');
		    					Run(checkMeMsg);
		    				}else{
		    					console.log('监控到历史消息');
		    				}
		    			}
		    			
		    			// checkMeMsg();
		    		}


    			}
    			
    		}
    		
    	}
    }

},observer_config);

var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}

//监听消息
addListenerMessage(function (request) {
    console.log(request);
    if (request.act == 'https_tabs_verify_code_result') {
        if(request.error == 1){
        	checkCodeLimits(img_src);
        	
        }else{
        	var last_prompt_msg = $("#scrollDiv .list_risk").last();
        	writing(last_prompt_msg.find('.list-tmb').next().find('input'),request.text,function(){
        		if(last_prompt_msg.find('.list-tmb').next().find('a').attr('class').indexOf('btn_blue_no') == -1){
        			//提交验证码
        			useLocal(function(local){
        				if(localXss.currentTask.is_mobile == '1'){
        					var evt = document.createEvent("MouseEvents");
							evt.initEvent("touchstart", true, true);
							last_prompt_msg.find('.list-tmb').next().find('a')[0].dispatchEvent(evt);
        				}else{
        					clicking(last_prompt_msg.find('.list-tmb').next().find('a'));
        				}
        			})
        		}
        	});

        	//sendChatMsg(request.text,doSend);
        	
        }
        
    }
});

//Task(index);

//监控验证码
function index(local){

	msgData = localXss.currentTask.chatMsg;


	var last_prompt_msg = $("#scrollDiv .list_risk").last();
	// var last_notice_msg = $("#scrollDiv .i-notice1").last();
	var last_notice_msg = $("#scrollDiv .chat-tips").last();

	if(last_prompt_msg.length > 0){
		//需要打码
		var img_src = last_prompt_msg.find('.list-tmb').find('img').attr('src');
		console.log(img_src);
		if(img_src){
			if(img_src.indexOf(':') == -1){
				img_src = location.protocol + img_src;
			}

			checkCodeLimits(img_src);
			
		}else{
			clue('服务繁忙，关闭对话');
			setTimeout(function(){
				closeChatWin();
				//close_this_tab();
			},2000);
		}
		

	}else if(last_notice_msg.length > 0){
		//切换验证notice
		checkNotice(local);
	}else{
		//会话加载失败
		clue('会话结束，3S后页面刷新');
		setTimeout(function(){
			//close_this_tab();
			window.location.reload();
		},3000)
	}

	
}

function checkCodeLimits(img_src){

	useLocal(function(local){
		var task = localXss.currentTask;
		var verify_code_nums = task.verify_code_nums?task.verify_code_nums:0;
		if(parseInt(verify_code_nums)<3){
			task.verify_code_nums = verify_code_nums + 1;
			setLocal({task:task},function(){
				autoVerifyCode(img_src);//再次打码
			});

		}else{

			clue('打码超过3次， 回主商品页');
			//close_this_tab();
			setTimeout(function(){
				closeChatWin();
				//close_this_tab_by_active();
				//close_this_tab();
				return false;
			},3000)
		}
	})
	
}

//监控到notice
function checkNotice(local){

	msgData = localXss.currentTask.chatMsg;

	// var last_notice_msg = $("#scrollDiv .i-notice1").last().find('.cont').text();
	var last_notice_msg = $("#scrollDiv .chat-tips").last().find('.cont').text();
	//chat-tips

	if(last_notice_msg.indexOf('恭喜') != -1){
		//可以开始聊天
		sendChatMsg('',doSend);

	}else if(last_notice_msg.indexOf('客服很高兴为您服务') != -1 && ($("#scrollDiv .i-notice1").length == 1 || $("#scrollDiv .i-success").length == 1)){
		sendChatMsg('',doSend);

	}else if(last_notice_msg.indexOf('验证码输入错误') != -1){
		clue('验证码错误,重新打码');
		var img_src = $("#scrollDiv .list_risk").last().find('.list-tmb').find('img').attr('src');
		checkCodeLimits(img_src);

	}else{
		console.log('其他notice：',last_notice_msg);
		//closeChatWin();
		// clue('店铺客服不在线或会话超时，3S后页面关闭');
		// setTimeout(function(){
		// 	close_this_tab();
		// },3000)
	}
}

//发消息
function sendChatMsg(msg,callback){

	var msg = msg ? msg : getMsg();
	if(!msg){
		console.log('消息为空，不再发送');
		closeChatWin();
		return false;
	}
	clue('输入内容: ' + msg);
	if($('#text_in').length > 0){
		useLocal(function(local){
			if(localXss.currentTask.is_mobile == '1'){
				writing($("#text_in"),msg,callback);
			}else{
				$("#text_in").text(msg);
				callback && callback();
			}
		})
		
		
	}else{
		clue('找不到发消息区域','error');
	}
	
}
//点击发送
function doSend(){
	if($("#sendMsg").length > 0){
		console.log('点击发送');
		setTimeout(function(){
			
			useLocal(function(local){
				if(localXss.currentTask.is_mobile == '1'){
					var evt = document.createEvent("MouseEvents");
					evt.initEvent("touchstart", true, true);
					$("#sendMsg")[0].dispatchEvent(evt);
				}else{
					clicking($("#sendMsg"));
				}
			})
			
		},2000);
		
	}else{
		clue('找不到发送按钮','error');
	}
}


//获取接口消息
function getMsg(){
	
    	//修复因有历史记录影响
    	if(!msgData){
    		closeChatWin();
    	}
    	if(msgData.length == 1){
    		return msgData[0];
    	}
		if(sendedNums <= msgData.length){
			return msgData[sendedNums];
		}else{
			//之前已经聊过
			clue('之前聊过了，不聊了')
			closeChatWin();
		}
 
    	
	// }
 
	
}

function autoVerifyCode(imgsrc) {
    //准备开始打码
    clue('自动打码');
    //updateHostStatus(1402200);
    chrome.extension.sendMessage({act: 'https_tabs_verify_code_for_chat', imgsrc: imgsrc});
}

//满意度评价
function comment(){
	if($("#degreeButton").length > 0){
		$("#degreeButton")[0].click();
		$('#degree100')[0].click();
		if($('#submitDegreeButton').attr('class').indexOf('im-btn-submit') != -1){
			clue('提交评价');
			setTimeout(function(){
				$('#submitDegreeButton')[0].click();
			},2000)
			
		}
	}else{
		clue('找不到评价按钮','error');
	}
}
//关闭窗口
function closeChatWin(){
	clue('准备结束会话,回到主商品');
	//return false;
	setTimeout(function(){
		useLocal(function(local){
			if(localXss.currentTask.is_mobile == '1'){
				if($("#goBack").length >0){
					clicking($("#goBack"));
				}else{
					history.go(-1);
				}
				// window.location.href = 'http://item.m.jd.com/product/' + localXss.currentTask.item_id +'.html';
			}else{
				close_this_tab();
			}
			 
		})
		
	},3000)
	
}

//监控到客服的消息
function checkedKufuMsg(){

	if(sendedNums == 0){
		sendedNums = sendedNums + 1;
		console.log('还没发过，发一条');
		checkSended();
	}
	
}
//监控到自己发信息成功
function checkMeMsg(){
	// if(checkedMe){return false;}
	// checkedMe = true;

	sendedNums = sendedNums + 1;
	clue('已发送信息数: ' + sendedNums);
	
	setTimeout(function(){

		checkSended();
		
	},10000);
}

function checkSended(){
	useLocal(function(local){
		msgData = localXss.currentTask.chatMsg;

		if(sendedNums < msgData.length){
			clue('还有信息，继续发送');
			sendChatMsg('',doSend);
		}else{
			closeChatWin();
		}
	})
	
}

            //匿名函数结束
        })
    }

}})();