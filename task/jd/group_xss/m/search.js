(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/search.js
var opened_rand_product = false;
var rand_open_product_number = _t.product_rand_open_count;

windowsTopNotSelf();
resetWatchDogTimer();
Task(index);

function index(local) {

    console.log(local);
    var task = localXss.currentTask;
    var item_id = task.item_id;

    if(task.is_mobile != 1){
        clue('不是手机单，手动', 'error');
        return false;
    }

    if (taskOrderExist('group_xss')) {
        return false;
    }

    // if(task.worldwide == '1' && location.protocol == 'https:'){

    //     location.protocol = 'http:';
    //     return false;
    // }

    //是否有活动链接
    if(localXss.currentTask.active_url){
        window.location.href = localXss.currentTask.active_url;
        return false;
    }

    updateHostStatus(1405000);//货比三家

    //货比三家 商品列表 进行货比
    open_rand_product(item_id);
}

//直接去商品详情
function open_product(item_id) {
    //直接打开商品详情

    updateHostStep(_host_step.view_product);//step4 浏览主商品
    //setTimeout(function(){
    //    location.href = 'http://item.m.jd.com/product/' + item_id + '.html';
    //},3000);
    console.log('open_product');
    useLocal(function(local){
        var itemId = localXss.currentTask.item_id;

        //var listBody = $("#list .list_body:visible");
        //var listLi = $("#list .list_body li:visible");
        //var listLi = $("#list .list_body li:visible").find("a[href*='item.m.jd.com']").add($("#list .list_body li:visible").find("a[href*='m.jd.hk']")).parents('li');

        var listBody = $("#seach_list ul[id*=searchlist]:visible");
        var listLi = $("#seach_list ul[id*=searchlist] li[page]:visible").find("a[href*='item.m.jd.com']").add($("#seach_list ul[id*=searchlist] li[page]:visible").find("a[href*='m.jd.hk']")).parents('li');

        if(listLi.length > 0){

            var itemLi = listLi.find("a[href*='"+itemId+"']:visible");
            if(itemLi.length > 0){
                var href = itemLi.attr("href").replace('http:','');
                console.log(href);
                itemLi.attr('href',href);
                var itemLi = listLi.find("a[href*='"+itemId+"']:visible");
                //主商品存在,直接点
                itemLi[0].click();
            }else{
                //主商品不存在,模拟进入
                var itemTemp = listLi.eq(0);
                var itemTempId = itemTemp.find("a").attr('href').match(/\d+/g)[0];
                var itemTempHtml = itemTemp[0].outerHTML;
                var mainItem = $(itemTempHtml);
                mainItem.find("a").attr("id", "xss-product");
                var href = mainItem.find("a").attr("href").replace('http:','');
                if(href.indexOf('jd.hk') != -1){
                    var href = href.replace(/[mite]*m.jd.hk/,'m.jd.com');
                }
                mainItem.find("a").attr("href",href);

                var eventParamTemp = mainItem.find("a").attr("report-eventparam");
                var eventParamTempSubIndex = eventParamTemp.indexOf("__");
                var eventParamMain = eventParamTemp.substr(0, eventParamTempSubIndex+4) +"0"+ eventParamTemp.substr(eventParamTempSubIndex+5);
                mainItem.find("a").attr("report-eventparam", eventParamMain);
                var mainItemHtml = mainItem[0].outerHTML;
                mainItemHtml = mainItemHtml.replace(new RegExp(itemTempId, 'g'), itemId);

                listBody.prepend(mainItemHtml);

                clue("已创建模拟商品");

                $("#xss-product")[0].click();
            }
        }else{

            var urlget = urlGet();
            console.log(urlget);
            var search_key = decodeURI(urlget.keyword);
            var noRightProduct = $(".no-right-product:visible");
            if($(".notice").length > 0 || noRightProduct.length>0){
                //var ns = $(".notice")[0].innerText;
                var ns = $(".notice").text();

                if((ns.indexOf('没有找到') != -1 && ns.indexOf('抱歉') != -1) || noRightProduct.length>0){
                    var task = localXss.currentTask;
                    //当前搜索次数
                    var now_search_num = task.search_nums?parseInt(task.search_nums):0;
                    if(now_search_num >=3){
                        //标记异常
                        clue('当前搜索次数：' + now_search_num);
                        clue('无搜索结果,人工通知! 先人工通知,确认正常可调整为自动','error');
                        tabCommon.sm(taskVars, '.search_keyword_exception');
                    }else{
                        clue('无搜索结果,3s后重新搜索');
                        //当前次数+1
                        task.search_nums = (now_search_num + 1);
                        setLocal({task:task},function(){
                            setTimeout(function(){
                                writing($("#layout_newkeyword"), task.keyword, function () {
                                    clicking($("#layout_search_submit"));
                                })
                            },3000)
                        });
                        
                    }
                }
            }else{
                 clue('不能确认商品列表', 'error');
            }
            
        }
    });
}

//打开随机商品
function open_rand_product(item_id) {
    if (opened_rand_product) {
        return false;
    }
    opened_rand_product = true;

    var product_list = $("#searchlist29 li.list-li-changeh");
    var product_list = $("#list .list_body li:visible");
    var product_list = $("#seach_list ul[id*=searchlist] li[page]:visible").find("a[href*='item.m.jd.com']").add($("#seach_list ul[id*=searchlist] li[page]:visible").find("a[href*='m.jd.hk']")).parents('li');


    if ((product_list.length == 0) || (product_list.length> 0 && product_list.length < 3)) {
        open_product(item_id);
        return false;
    }

    var product_list_ids = new Array();
    product_list.each(function (i) {

        console.log($(this).find('a').attr('href'));
        var pro = $(this).find('a:first-child').attr('href').match(/\d+/);console.log(pro);
        if(pro && pro.length>0){
            var pro_id = pro[0];
            if (pro_id != item_id) {//排除主商品
                product_list_ids.push(pro_id);
            }
        }

    });

    var randNumArr = new Array();

    var pro_len = product_list_ids.length;
    if (pro_len > 0 && rand_open_product_number > 0) {
        for (var j = 0; j < rand_open_product_number; j++) {
            var randNum = parseInt(Math.random() * pro_len);
            if (!in_array(randNum, randNumArr)) {
                randNumArr.push(randNum);
            }
        }

        var around_items = new Array();
        for (var r = 0; r < randNumArr.length; r++) {
            var itemBoxId = product_list_ids[randNumArr[r]];
            var randProduct = product_list.find("a[href*='/product/"+itemBoxId+".html']");
            randProduct.attr('target', '_blank');
            console.log(randProduct.attr('href'));
            var href = randProduct.attr('href').replace('http:','');
            console.log(href);
            randProduct.attr('href',href);
            console.log(randProduct.attr('href'));
            clicking(randProduct);
            around_items.push(itemBoxId);
        }

        chrome.storage.local.set({around_items: around_items}, function () {
            lazy(function () {
                open_product(item_id);
            });
        });
    }

}

            //匿名函数结束
        })
    }

}})();