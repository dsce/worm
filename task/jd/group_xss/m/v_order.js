(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/order.js

var pay_fee = 0;
var shipping_fee = 0;
var identity_id_inputted = false;
var identity_btn_clicked = false;

index();

function index(local){
    var task = localXss.currentTask;
    var referrer = document.referrer;

    updateHostStep(_host_step.submit_order);//step6 提交订单
    updateHostStatus(1410000);//提交订单
    if(taskOrderExist('group_xss')){return false;}
    //收货人信息

    if(referrer.indexOf('http://p.m.jd.com/norder/payShipment.action') != -1){
        //改完 支付方式 修改发票
        clue("3秒后，修改发票信息");
        lazy(invoice_action);

    }else if(referrer.indexOf('v.m.jd.com/presaleOrder/invoice.action') != -1){//修改完发票，提交订单
        Run(submitBegin);

    }else{

        //核对是否有手机号
        var sPhoneText = $(".bal-user-number");
        if(sPhoneText.length > 0){
            if(!checkAddress(local)){
                clicking($(".ba-location"))

            }else{
                //改支付方式
                //clue("修改支付方式");
                clue("直接修改发票");
                //lazy(payment_action);
                lazy(invoice_action);

            }
        }else if($(".ba-location").length >0){
            clicking($(".ba-location"));
           
        }else{
            //重新开始
            tabCommon.sm(taskVars, '.order_operate','client_host_status_reset')
        }

    }
    
}

//验证收货地址
function checkAddress(local){
    var address = jdMunicipality(localXss.currentTask.consignee);
    var name = $(".bal-user-name").text();
    var mobile = $(".bal-user-number").text().match(/\d+/g);
    var full_address = $(".bal-detail").text().replace(/\s+/g,'');

    var its_name = name == address.name;
    var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

    var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

    var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
    if(its_name&&its_full_address&&its_mobile){
        clue('收货地址一致，继续');
        return true;
    }else{
        clue('收货地址不一致,修改');
        clicking($(".ba-location"));
        return false;
    }
}

function invoice_action(){
    if($(".bae-bottom span:contains('不开发票')").length>0){
        submitBegin();
    }
    else if($(".bae-bottom:contains(纸质发票)").length>0){
        $(".bae-bottom:contains(纸质发票)")[0].click();
    }else{
        submitBegin();
    }
}

function submitBegin(){
    //订单金额

    if($(".total-fixed .num").length >0){
        var pay_fee = $(".total-fixed .num strong").text().match(/\d+(=?.\d{0,2})/g)[0];
    }
    clue('应付款金额：'+pay_fee);

    submiOrder();
}
//提交订单
function submiOrder(){
    clue('提交订单');

    var items = {
        temp_pay_fee: $(".ba-pay strong").eq(0).text().match(/\d+(=?.\d{0,2})/g)[0],
        temp_shipping_fee: $(".ba-pay strong").eq(1).text().match(/\d+(=?.\d{0,2})/g)[0]
    }
    setLocal(items, function(){

        if($(".total-fixed a:contains('提交订单')").length >0){
            $(".total-fixed a:contains('提交订单')")[0].click()
        }else{
            clue('未找到提交订单按钮','error');
        }
        
        
    });

}


            //匿名函数结束
        })
    }

}})();