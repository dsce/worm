(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//jd/m/submit.js

windowsTopNotSelf();
Task(index);

function index(local) {
    tabCommon.sm(taskVars, '.saveUserCookiesToRemote', {domain: 'jd.com'});//save cookies


    var order_tip = $(".order-tip p");
    var order_dd = $('.o-step dt:contains("订单编号")').next('dd');
    if (order_tip.length > 0) {
        var order_id = order_tip.text().match(/\d+/)[0];
        begin_pay(order_id);
    } else if(order_dd.length > 0){
        var order_id = order_dd.text().match(/\d+/)[0];
        begin_pay(order_id);

    } else {

        var back_button = $("#back_button");//返回修改
        var mc = $(".mc")
        if (back_button.length > 0 && mc.length > 0) {
            updateHostStatus(1410005);//
            clue('提交订单失败', 'error');

            var message = $(".mc")[0].innerText;
            if(message.indexOf('商品无货') != -1){
                reportProductStockout(message);
            }

            //地区无法购买
            if(message.indexOf('省份无法购买') != -1){
                updateHostStatus(1406005);
                // addTaskOrderRemark("当前省份无法购买商品(WAP)"+localXss.currentTask.task_order_oid, function(){});

                    tabCommon.sm(taskVars, '.order_exception', message);
                
            }
            //啊哦，同一Ip地址只能抢购一件哟！别灰心，请返回购物车删除商品威士雅蛋白粉 金罐乳清蛋白质粉 600g 1罐装后提交订单吧！
            if(message.indexOf('同一Ip地址只能抢购一件') != -1){
                updateHostStatus(1406006);
                tabCommon.sm(taskVars, '.order_exception', '同一Ip地址只能抢购一件(WAP)['+localXss.currentTask.item_id+']');
            }

            //啊哦，同一账户只能抢购一件哟！别灰心，请返回购物车删除商品漫渺自慰器 成人情趣性用品 男士高端飞机杯倒模非充气娃娃 电动版后提交订单吧！
            if(message.indexOf('同一账户只能抢购一件') != -1){
                updateHostStatus(1406006);
                tabCommon.sm(taskVars, '.order_exception', '同一Ip地址只能抢购一件(WAP)['+localXss.currentTask.item_id+']');
            }

            //啊哦，没有抢到商品，同账户或同IP只能抢购一件哟！别灰心，请返回购物车删除抢购商品后再提交订单吧！
            if(message.indexOf('同账户或同IP只能抢购一件') != -1){
                updateHostStatus(1406007);
                tabCommon.sm(taskVars, '.order_exception', '同账户或同IP只能抢购一件(WAP)['+localXss.currentTask.item_id+']');
            }

            //由于京东地址库升级，请重新编辑当前收货地址中”所在地区”选项，即可顺利下单！
            if(message.indexOf('京东地址库升级') != -1){
                updateHostStatus(1406010);
                if(local.address_update == undefined || local.update_address == true){
                    setLocal({address_update: true}, function(){
                        location.href = 'https://easybuy.jd.com/address/getEasyBuyList.action';
                    });
                }else{
                    updateHostStatus(1406011);
                    clue("新添加的地址过不去，地址库需要更新", 'error');
                }
            }

        } else {
            clue('订单号为 空');
        }


    }
}


function begin_pay(order_id){
    updateHostStatus(1411000);//手机下单完成
    clue("订单号： " + order_id);
    setLocal({order_id: order_id}, function () {
        clue("准备浏览器");
        setTimeout(function(){
            clearChangeUseragent(to_login_message);
        }, 3000);//切换ua
    });
}

            //匿名函数结束
        })
    }

}})();