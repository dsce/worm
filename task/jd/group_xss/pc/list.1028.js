(function () {
    var observerId = tabCommon.observer($('#container')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 订单列表 jd/list.js

var orderToPay = 0;
var runIndexed = false;
var amount = 0;
chrome.storage.local.get(null, function (local) {
    var tem = $('body a[href="http://item.jd.com/' + localXss.currentTask.item_id + '.html"]');
    var tem = $('body .p-'+ localXss.currentTask.item_id +'');
    console.log('tem', tem);
})
Task(index);

setTimeout(function(){
    if(!runIndexed){
        Run(index);
    }
}, 20000);

var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}
tabCommon.mutation(observerId,function(mutation){
//tabCommon.mutation(observerId,function(mutation){

    if(mutation.type == 'childList'){
        console.log(mutation.type);
        console.log(mutation.target);
        console.log(mutation);

        //记录未付款数量
        if(mutation.previousSibling != undefined && mutation.previousSibling.id == 'ordertoPay' && mutation.target.tagName == 'LI' && mutation.addedNodes.length > 0){
            var n = $("#order02 .extra-l em").text();
            if(n > 0){
                orderToPay = parseInt(n);
            }
        }else if(mutation.target.className == 'operate'){
            if(orderToPay == 1){
                Run(index);
                orderToPay--;
            }else{
                orderToPay--;
            }
        }
    }
}, observer_config);

function index(local) {

    if(runIndexed){
        return false;
    }
    runIndexed = true;

    var task = localXss.currentTask;
    var order_id = local.order_id;
    amount = task.amount;
    var order_should_pay = local.order_should_pay;
    if (task.is_mobile == 1) {//手机单
        if(order_id !== undefined){
            clue('手机订单： ' + order_id);
            toPayBegin(order_id);
        }else{
            if(order_should_pay !== undefined){
                //只核对第一个订单，必须是手机单，金额相等，存在主商品，未付款
                //兼容列表改版
                var order_tbody = $(".tb-void tbody:first");
                if(order_tbody.length == 0){
                    order_tbody = $(".td-void tbody:first");
                }

                if(order_tbody.length > 0){
                    var cellphone = order_tbody.find(".cellphone-icon");
                    //var should_pay = cellphone.parent()[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
                    var should_pay = order_tbody.find(".amount")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
                    var pay_status = order_tbody.find("a:contains('付 款')");
                    var pay_status2 = order_tbody.find("a:contains('付款')");
                    var product = order_tbody.find('a[href="http://item.jd.com/'+task.item_id+'.html"]');
                    //去掉确认手机单条件 cellphone.length > 0 &&
                    if( order_should_pay==should_pay && (pay_status.length > 0 || pay_status2.length > 0) && product.length > 0) {
                        order_id = order_tbody.find('a[name="orderIdLinks"]')[0].innerText;
                        clue('手机订单： ' + order_id);
                        toPayBegin(order_id);
                        //setLocal({order_id: order_id},function(){
                        //    toPayBegin(order_id);
                        //});
                    }else{
                        clue('没有找到正常手机订单', 'error');
                        clue('金额'+order_should_pay+', 请手动核对订单进行操作', 'error');
                    }
                }else{
                    clue('没有找到正常手机订单', 'error');
                    clue('金额'+order_should_pay+', 请手动核对订单进行操作', 'error');
                }


            }else{
                toPayBegin(order_id);
            }
        }
    } else {

        if(order_id == undefined){

            //未付款订单下的主商品
            var order_tbody = $("a.btn:contains('付 款'):visible").parents('tr').find("a[href='http://item.jd.com/"+task.item_id+".html']").parents('tbody').first();
            var order_tbody = $(".operate a:contains('付款'):visible").parents('tr').find("a[href='http://item.jd.com/"+task.item_id+".html']").parents('tbody').first();
            if(order_tbody.length > 0){

                order_id = order_tbody.find('a[name="orderIdLinks"]')[0].innerText;
                clue('未付款订单： ' + order_id);
                toPayBegin(order_id);
                //setLocal({order_id: order_id},function(){
                //    toPayBegin(order_id);
                //});
            }

        }else{
            if($("#idUrl"+order_id).length > 0){
                clue('未付款订单：' + order_id);
                toPayBegin(order_id);
            }
        }


    }


}

function toPayBegin(order_id){

    //var order_id = '10416062022';
    //var order_id = '10415707583';
    var number = 0;
    $("#track"+order_id+" .goods-number").each(function(){
        number += parseInt(this.innerText.match(/\d+/));
    });

    if(amount == 0 || amount != number){
        clue('进入订单进一步核对');
        orderUrl(order_id);
        return false;
    }else{
        clue("商品数量一致", 'success');
    }

    orderUrl(order_id);
    lazy(close_this_tab);

    //var pay_button = $("#pay-button-" + order_id + " a");
    //pay_button = pay_button.length > 0 ? pay_button : $("#operate"+order_id+" .btn-pay");
    //
    //if (pay_button.length > 0) {
    //    updateHostStatus(1413000);
    //    lazy(function(){
    //        //pay_button[0].click();
    //        orderUrl(order_id);
    //        lazy(close_this_tab);
    //    });
    //} else {
    //    useLocal(function(local){
    //        var statu_payment = $("#track"+order_id).find(":contains('等待付款')");
    //        if(local.host_step == _host_step.save_order_info || statu_payment.length == 0){
    //            clue("查看订单");
    //            orderUrl(order_id);
    //        }else{
    //            clue('没有找到订单付款按钮');
    //        }
    //    });
    //
    //}
}

function orderUrl(order_id){

    var order_url = $("#idUrl"+order_id);
    if(order_url.length > 0){
        $("#idUrl"+order_id)[0].click();
        lazy(close_this_tab);
    }else{
        clue('没有找到订单','error');
    }
}


            //匿名函数结束
        })
    }

}})();