(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东商品详情页面

//计时
var count_timer = 0;
//计时器
var time_interval = null;


//底部停留计时
var bottom_count_timer = 0;
//底部停留计时器
var bottom_time_interval = null;

var jd_rand_main_wait_seconds = random(20, 50);//10-30多长时间放入购物车
var jd_rand_main_bottom_wait_seconds = 15;//页面底部等待时间

var jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, _t.main_product_wait_rand_time_end);
var jd_aux_product_wait_time = random(_t.aux_product_wait_rand_time_start, _t.aux_product_wait_rand_time_end);

var indexed = false;
var favoritedShop = false;

var p_price = '';

//Task(index);

            tabCommon.mutation(observerId,function (mutation) {
                //console.log(mutation.type);
                // console.log(mutation.target);
                // console.log(mutation);
                if(mutation.type === 'childList'){
                    //console.log(mutation.target);
                    //console.log(mutation);
                    if(mutation.target.id === 'store-prompt' && mutation.addedNodes.length > 0){
                        $('#InitCartUrl').hide();
                        $('#InitCartUrl').after('加入购物车');
                        Run(readyIndex);
                    }else if(mutation.target.id == 'choose-btn-easybuy' && mutation.addedNodes.length > 0){
                        $('#choose-btn-easybuy').hide();
                    }else if(mutation.target.id == 'm-itemover-content' && mutation.addedNodes.length > 0){
                        //下柜后显示的类似商品 监控，不会与store-prompt共存

                        Run(readyIndex);
                    }else if(mutation.target.tagName == 'SPAN' && mutation.target.textContent == '已关注店铺'){
                        Run(function(local){
                            followProduct(localXss.currentTask.follow_product);
                        });
                    }
                }else if(mutation.type == 'attributes'){
                    if(mutation.target.id == "product-intro" && mutation.attributeName == "class" && mutation.target.className.indexOf('product-intro-itemover') != -1){
                        //下柜
                        Run(readyIndex);
                    }
                }else if(mutation.type == 'characterData'){
                    //console.log(mutation.target);
                    //console.log(mutation);

                    //关注店铺
                    if(mutation.oldValue != undefined && mutation.oldValue == '关注店铺'){
                        //关注店铺成功，关注商品
                        Run(function(local){
                            followProduct(localXss.currentTask.follow_product);
                        });

                    }
                }
            })
            




addListenerMessage(function(request){
    console.log(request);
    var act = request.act;


    if(act=='next_keyword') {//下一页
        var ref = document.referrer;
        var key = $("#search-2014 .form #key");

        if(key.length > 0){
            clue("当前页面搜索");
            clue("关闭上一个搜索页面");
            sendMessageToTabByUrl(ref, "search_stop", null, function(){
                useLocal(readySearch);
            });


        }else{
            clue("当前页面不能完成搜索,上一个搜索结果页,再打开一个");
            sendMessageToTabByUrl(ref,"next_product",null, function(){
                setTimeout(function(){
                    close_this_tab();
                }, random(20, 40)*1000);
            });
        }
    }else if(act=='tab_over'){
        close_this_tab();

    }
});

Task(function(local){

    //var si = 0;
    //var s = setInterval(function(){
    //    console.log(si,$("#itemInfo .m-itemover-title:contains('商品已下柜'):visible").length);
    //    if($("#itemInfo .m-itemover-title:contains('商品已下柜'):visible").length > 0){
    //        console.error(si);
    //    }
    //    si++;
    //    if(si>1000){
    //        clearInterval(s);
    //    }
    //},10);

    //30s后检查下柜
    //setTimeout(function(){
    //    var task = localXss.currentTask;
    //    var item_over = $("#itemInfo .m-itemover-title:contains('商品已下柜'):visible");
    //    if(item_over.length > 0){
    //        clue("主商品[" + task.item_id + "]下柜", 'error');
    //
    //        notifyMessage("主商品[" + task.item_id + "]下柜，30s后标记下柜");
    //
    //        updateHostStatus(1406002);
    //        //30s后标记商品下柜
    //        setTimeout(function(){
    //            reportProductStockout("商品[" + task.item_id + "]下柜");
    //        }, 30000)
    //
    //    }
    //}, 30000);

    //页面嵌入框架
    if(top != self){
        console.error('页面嵌入框架，错误');
    }else{
        console.log('页面没有嵌入框架，正常');
    }


    if($(".m-itemover-title:contains('已下柜')").length > 0 || $(".itemover-tip:contains('已下柜')").length > 0){
        lazy(function(){
            readyIndex(local);
        }, 5);
    }

    //京东电子书
    if(location.href.indexOf('//e.jd.com') != -1){
        readyIndex(local);
    }

    //30s后判断任务是否开始
    setTimeout(function(){
       console.log(indexed);
       if(!indexed){//未开始则刷新
            console.log('未开始则刷新');
            location.reload();
       }
    },40000);

    

})


function readyIndex(local){
    console.log('readyIndex');
    //var itemId = location.pathname.match(/\d+/g);
    //var lastItemId = local.rand_search_items.pop();
    var task = local.task;

    if (window.location.href.indexOf(task.item_id) != -1) {

        var province = task.consignee.province;
        var city = task.consignee.city;
        var area = task.consignee.area ? task.consignee.area : '';
        var street = task.consignee.street ? task.consignee.street : '';

        if(province == '北京' || province == '上海' || province == '天津' || province == '重庆'){
            var task_address = province + area + street;
        }else{
            var task_address = province + city + area;
        }

        if($("#store-selector .text div").length >0){
            var page_address = $("#store-selector .text div").text().replace(/\s+/g,'').replace(/市/g,'');
        }else{
            var page_address = $("#stock-address .head span").first().text().replace(/\s+/g,'').replace(/市/g,'')
        }
        if(page_address.indexOf(task_address.replace(/市/g,'')) == -1){
            setAddress(0,function(){
                index(local);
            });
        }else{
            index(local);
        }

    }else{
        index(local);
    }
}

//随即打开聊天
function openChat(local){
    if($("#popbox .sprite-im").length > 0){
        //if(random(0,1)){
        if(1){
            var task = localXss.currentTask;
            //请求消息
            tabCommon.sm(taskVars, '.get_chat_msg');
            //监听消息
            addListenerMessage(function (request) {
                console.log(request);
                if(request.act == 'get_chat_msg_result'){
                    var msgData = request.msg;
                    console.log('msgData',msgData);
                    if(request.msg){
                        task.chatMsg = request.msg;
                        task.checkChatted = true;
                        setLocal({task:task},function(){
                            clue('进入聊天');
                            setTimeout(function(){
                               clicking($("#popbox .sprite-im")); 
                            },3000)
                        })  
                    }
                }
            });

            // clue('进入聊天');
            // clicking($("#popbox .sprite-im"));
        }
    }
    
}

            function formatAddress(consignee) {
                var province = consignee.province;
                var city = consignee.city;
                var area = consignee.area ? consignee.area : null;
                var street = consignee.street ? consignee.street :null;
                if (province == '北京' || province == '上海' || province == '天津' || province == '重庆') {
                    //北京 上海
                    return [province,area,street];

                }else{
                    return  [province,city,area];
                }
            }

            function setAddress(level,callback){
                useLocal(function(local){
                    var task = local.task;
                    var province = task.consignee.province;
                    var city = task.consignee.city;
                    var area = task.consignee.area ? task.consignee.area : null;


                    var address = formatAddress(local.task.consignee);

                    if($(".stock-address-list:visible").length == 0 && $('#stock-address').length >0){
                        // if($('#stock-address').length >0){ }
                        clicking($('#stock-address'));

                    }

                    if($("#JD-stock-wrap:visible").length == 0 && $("#store-selector").length >0){
                        clicking($("#store-selector"));
                    }

                    var get_level = level ? level : 0;


                    setTimeout(function(){

                        if($('#store-selector').length >0){
                            var t = $("#store-selector .tab li:visible").length;
                        }else{
                            var t = $("#stock-address .address-tab .tab li:visible").length;
                        }

                        if(get_level <t){
                            doSetAddress(get_level,address,callback);
                        }else{
                            callback && callback();
                        }


                    },3*1000)

                })

            }

            function doSetAddress(level,address,callback){
                if($("#stock-address .address-tab .tab:visible").length >0){

                    $("#stock-address .address-tab .tab li:eq(" + level +")").click();
                    setTimeout(function(){
                        if($("#stock-address .tab-con div[data-level=" + level +"]:visible").length >0){

                            if(!address[level]){
                                clicking($("#stock-address .tab-con div[data-level=" + level +"]:visible li:eq(0) a"));
                            }else{
                                clicking($("#stock-address .tab-con div[data-level=" + level +"]:visible li a:contains('" + address[level] +"')"));
                            }

                            level++;
                            setAddress(level,callback);
                        }
                    },3*1000)

                }else if($("#store-selector .tab:visible").length >0){

                    $("#store-selector .tab li:eq(" + level +") a")[0].click();
                    setTimeout(function(){
                        if($("#JD-stock div[data-area=" + level +"]:visible").length >0){

                            if(!address[level]){
                                clicking($("#JD-stock div[data-area=" + level +"]:visible li:eq(0) a"));
                            }else{
                                clicking($("#JD-stock div[data-area=" + level +"]:visible li a:contains('" + address[level] +"')"));
                            }

                            level++;
                            setAddress(level,callback);
                        }
                    },3*1000)

                }
            }


function index(local) {

    console.log('index');
    if(indexed){ return false;}
    indexed = true;
    var task = localXss.currentTask;

    if (taskOrderExist('group_xss')) {
        return false;
    }

    //切换主商品显示
    tabCommon.sm(taskVars, '.tab_update_selected_by_url', {url: 'http://item.jd.com/'+task.item_id+'.html'});


    if (window.location.href.indexOf(task.item_id) != -1) {

        //是主商品,确认是否有来源
        // if(document.referrer == ''){
        //     clue("没有发现搜索来源,3秒后重新搜索", "error");
        //     //0603 https重定向到http没有搜索来源,
        //     setTimeout(function(){
        //         searchPc(task.keyword);
        //     }, 3000);
        //     return false;
        // }
        // clue("来源:"+document.referrer);

        //打开主商品
        updateHostStatus(1406000);

        if(localXss.currentTask.chat == '1'){
            //打开聊天
            if(localXss.currentTask.checkChatted !== true){
                openChat(local);
            }else{
                console.log('已经聊过了');
            }
        }
        
        

        if($(".summary .p-price").length){
          var jdPrice = $(".summary .p-price")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
        }else if($("#jd-price").length > 0){
          var jdPrice = $("#jd-price")[0].innerText.match(/\d+(=?.\d{0,2})/g)[0];
        }else{
            console.log('未获取到商品价格');
        }

        if(jdPrice){
            p_price = jdPrice;
            //主商品计算页面停留时间
            var priceTime = getItemWaitTimeEnd(jdPrice);
            jd_main_product_wait_time = random(_t.main_product_wait_rand_time_start, priceTime);
           // console.log(localXss.currentTask.product_price - jdPrice);
            if(jdPrice - localXss.currentTask.product_price  >= 400){
                clue('商品价格差价大于400','error');
                return false;
            }
        }

        


        //地区
        var area_text = $('#store-selector .text').text();

        var task_address = localXss.currentTask.consignee;
        var remarks = "[" + task_address.province + task_address.city + localXss.currentTask.item_id + "]" ;


        //主产品下柜
        var item_over = $("#itemInfo .m-itemover-title:visible:contains('下柜')");
        var itemOver = $(".itemover-tip:contains('已下柜')");
        if (item_over.length > 0 || itemOver.length>0) {
            checkProductExceptionReloadNums('xgui',function(){
                var jd_remarks = $("#itemInfo .m-itemover-title:visible").text() ? $("#itemInfo .m-itemover-title:visible").text() : $(".itemover-tip").text();//兼容两种下柜信息
                clue(jd_remarks);
                clue("主商品[" + task.item_id + "]下柜", 'error');
                updateHostStatus(1406002);
                reportProductStockout(remarks + '下柜');
                return false;
            });
            
        }

        var jd_remarks = $("#store-prompt:visible")[0].innerText;
        var store_prompt = $("#store-prompt:visible:contains('无货')");
        if (store_prompt.length > 0) {
           checkProductExceptionReloadNums('whuo',function(){
                clue(jd_remarks);
                clue("主商品[" + task.item_id + "]无货", 'error');
                updateHostStatus(1406001);
                reportProductStockout(remarks + '无货');
                return false; 
            });
        }

        var store_prompt = $("#store-prompt:visible:contains('有货，仅剩')");
        if(store_prompt.length > 0){
            var remain = store_prompt[0].innerText.match(/\d+/);
            if(remain.length >0){
                if(remain[0] < task.amount){
                    clue("主商品[" + task.item_id + "]仅剩"+remain[0]+"件，库存不足", 'error');
                    updateHostStatus(1406004);
                    reportProductStockout("商品[" + task.item_id + "]仅剩"+remain[0]+"件，库存不足");
                    return false;
                }
            }
            
        }

        //该地区不支持配送
        var region_unsupport_delivery = $("#store-prompt:visible:contains('不支持配送')");
        if(region_unsupport_delivery.length > 0){
            var region = $("#store-selector .text").text();
            var message = region+'该地区暂不支持配送';
            updateHostStatus(1406005);
            // addTaskOrderRemark(message, function(){ });
                //使用新方法 订单异常 + 备注
                tabCommon.sm(taskVars, '.order_exception',message);

           
            clue(region + jd_remarks,function(){});
            return false;
        }

        //该地区暂不支持销售
        var region_unsupport_delivery = $("#store-prompt:visible:contains('不支持销售')");
        if(region_unsupport_delivery.length > 0){
            var region = $("#store-selector .text").text();
            var message = region+'该地区暂不支持销售';
            updateHostStatus(1406005);
            // addTaskOrderRemark(message, function(){ });
                //使用新方法 订单异常 + 备注
                tabCommon.sm(taskVars, '.order_exception',message);

           
            clue(region + jd_remarks,function(){});
            return false;
        }

        //判断是否京东发货
        if($("#summary-service").length > 0){
            if($("#summary-service").text().indexOf('京东 发货') != -1){
                //出现京东配送  标记异常发工单
                var jd_remarks = $("#summary-service").text();
                clue(jd_remarks);
                updateHostStatus(1406012);
                reportProductStockout('京东发货:' + jd_remarks);
                return false;
            }
        }
        
        //该地区暂停销售
        var no_sell = $("#store-prompt:visible:contains('该地区暂停销售')");
        if(no_sell.length > 0){
            var region = $("#store-selector .text").text();
            var message = region+'该地区暂停销售';
            // addTaskOrderRemark(message, function(){ });
                //使用新方法 订单异常 + 备注
                tabCommon.sm(taskVars, '.order_exception',message);

           
            clue(region + jd_remarks,function(){});
            return false;
        }


        //汇报当前状态
        updateHostStatus(1407000, jd_main_product_wait_time+20);
        clue('等待' + jd_main_product_wait_time + '秒后，放入购物车');

        

        //到最底部停留后，到最顶部
        scroll_bottom_wait_right(function () {
            goto_top();
        });


        //随机推荐商品 ,带有水星链接的都算是吧,
        $("li[data-clk*='mercury.jd.com/log.gif']").length;
        var mercuryClkLi = $("li[data-clk*='mercury.jd.com/log.gif']");
        var mercuryLinks = mercuryClkLi.find("a[href*='item.jd.com']");
        if(mercuryLinks.length>0){
            clue("随机打开一个推荐商品");
            var link = mercuryLinks.eq(random(0, mercuryLinks.length-1));
            var mercuryItemId = link.attr('href').match(/\d+/)[0];

            if($.isArray(local.rand_search_items)){
                setLocal({rand_search_items: local.rand_search_items.push(mercuryItemId)});//rand_search_items 不是一个数组
            }
            clicking(link);
        }



        // //检查购物车
        // var cart_cleared = localXss.currentTask.cart_cleared;
        // if(cart_cleared === undefined){

        //     var cart_btn = $("#settleup-2014 a:contains('我的购物车')");
        //     if(cart_btn.length > 0){
        //         setLocalTask({cart_cleared: false}, function(){
        //             setTimeout(function(){
        //                 clicking(cart_btn);
        //             },3*1000)
                    
        //         });
        //     }else{
        //         clue('购物车入口改变，不能进行清空操作', 'error');
        //     }

        // }



        //显示时间
        // show_wait_time(function(){
        autoResetWatchDogTimer(jd_main_product_wait_time * 1000);
        setTimeout(function () {
            clue("关闭上一个搜索页面");
            sendMessageToTabByUrl(document.referrer, "search_stop", null, function(){
                right_product_buy(task);
            });
        }, jd_main_product_wait_time * 1000)
        //正常购买商品


        // });

    } else {

        //updateHostStatus(1405001, jd_aux_product_wait_time+20);//商品详情页 的货比三家
        var itemId = location.pathname.match(/\d+/g)[0];
        clue('货比三家 '+itemId);
        //clue(local.rand_search_items.toString());
        scrollBottom(function(){
            //当前的商品编号
            useLocal(function(local){
                if (local.rand_search_items != undefined) {
                    //clue(local.rand_search_items.toString());
                    //是货比三家商品
                    if (in_array(itemId, local.rand_search_items)) {
                        var lastItemId = local.rand_search_items.pop();
                        //if(itemId==lastItemId && localXss.currentTask.searchGroupItems.length > 0){

                        close_this_tab();
                        //if(itemId==lastItemId){
                        //    clue('下一页 ');
                        //    var ref = document.referrer;
                        //    sendMessageToTabByUrl(ref, "next_page", null, function(){
                        //        //readySearch(local);
                        //        //close_this_tab();
                        //    });
                        //
                        //}else{
                        //    close_this_tab()
                        //}
                    }else{
                        close_this_tab();//部分关不掉问题
                    }
                }else{
                    close_this_tab();
                }
            });
        });
    }
}

//检查有其他的服务费用
function checkOtherService(callback){
    //是否有送装服务
    if($("#choose-luodipei:visible").length > 0){
        var fee = [];
        //获取所有选项的服务费
        $("#choose-luodipei .item").each(function(i){
            var item = $(this).find("a").text();
            if(item.indexOf('￥') != -1){
                fee.push(item.split('￥')[1]);
            }else{
                console.log('选项中没有金额',item);
            }
        })

        if(fee.length>0){
            //获取最少费用
            var min_item = Math.min.apply(null,fee);

            $("#choose-luodipei .item").each(function(i){
                // console.log($(this).find('a').text());
                var item = $(this).find('a').text();
                if(item.indexOf('￥') != -1){
                    if(item.split('￥')[1] == min_item){
                        console.log('click');
                        clicking($(this).find('a'));
                        callback && callback();
                    } 
                }
            })
        }else{
            clue('有送装服务项，但未找到费用','error');
            // callback && callback();
            return false;
        }
        
    }else{
        callback && callback();
    }
}


//正常的商品
function right_product_buy(data) {

    //设置购买数量
    $('#buy-num').val(data.amount)

    var event = new KeyboardEvent('keyup');
    event.key = data.amount;
    document.querySelector('#buy-num').dispatchEvent(event);

    updateHostStatus(1408000);

    //关注店铺
    favoriteShop(data.favorite_shop, function(){
        //关注商品
        followProduct(data.follow_product, function(){
            //加入购物车
            addCart();
        })
    })

}


//到最底端，然后继续
function scrollBottom(callback) {
    goto_bottom();
    clue('浏览'+jd_aux_product_wait_time + '秒后，继续');
    setTimeout(function () {
        //关闭页面
        callback && callback();
    }, jd_aux_product_wait_time * 1000);
}


//正常商品，到最底端，等待，然后到最顶端
function scroll_bottom_wait_right(func) {


    bottom_time_interval = setInterval(function () {

        //到最底部
        goto_bottom();

        bottom_count_timer++;

        //达到等待时间
        if (bottom_count_timer >= jd_rand_main_bottom_wait_seconds) {
            func();
            clearInterval(bottom_time_interval);
        }

    }, 1000);

}


//到顶端
function goto_top() {
    window.scrollTo(0, 0);
}

//到底端
function goto_bottom() {
    window.scrollTo(0, document.body.scrollHeight);
}

/**
 * 关注店铺
 * @param favorite_shop
 * @param callback
 */
function favoriteShop(favorite_shop, callback){

    favoritedShop = true;

    if(favorite_shop == '1'){
        clue('关注店铺');
        var shop = $("#extInfo .btn-shop-follower");
        var shop_new = $(".contact .follow");
        if(shop.length > 0){
            if(shop.not('.followed').length > 0){
                shop[0].click();
            }else{
                clue('已经关注过该店铺');
                lazy(callback, 3);
            }
        }else if(shop_new.length > 0){
            if(shop_new.not('.followed').length > 0){
                shop_new[0].click();
            }else{
                clue('已经关注过该店铺');
                lazy(callback, 3);
            }
        }else{
            
            if($("#summary-service a[href*='mall.jd.com']").length >0){
                $("#summary-service a[href*='mall.jd.com']")[0].click();
            }

            lazy(callback, 3);
            
            // clue('没有找到关注店铺按钮','error');
            // updateHostStatus(1406013);
            // clue('请求URL关注店铺');
            // favoriteShopByUrl(callback);
            //callback && callback();
        }
    }else{
        callback && callback();
    }

}

/**
 * 关注商品
 * @param follow_product
 * @param callback
 */
function followProduct(follow_product){
    if(favoritedShop) {

        if (follow_product == '1') {
            clue('关注商品');
            var follow_button = $("#choose-btn-coll");
            var follow_button_new = $(".preview-info .follow");

            if (follow_button.length > 0) {
                if (follow_button.not('.followed').length > 0) {
                    follow_button[0].click();
                } else {
                    clue('已经关注过该商品');
                    lazy(addCart, 3);
                }
            }else if (follow_button_new.length > 0) {
                if (follow_button_new.not('.followed').length > 0) {
                    follow_button_new.find(".sprite-follow-sku")[0].click()
                } else {
                    clue('已经关注过该商品');
                    lazy(addCart, 3);
                }

            } else {
                clue('没有找到关注商品按钮','error');
                updateHostStatus(1406014);
                //addCart();
                // clue('请求URL关注商品');
                // followProductByUrl(addCart);
            }
        } else {
            addCart();
        }

    }

}

//访问URL关注店铺
function favoriteShopByUrl(callback){
    // if(!$(".score-infor a").first().attr('href')){
    //     //callback && callback();return false;
        
    // }
    useLocal(function(local){
        var data = {
            callback:'jQuery'+random(1000000,9999999),
            venderId:
                $(".score-infor a").first().attr('href') ? $(".score-infor a").first().attr('href').match('[0-9]+')[0] : localXss.currentTask.shop_identity,
            sysName:'item.jd.com',
            _:new Date().getTime()
        }
        $.ajax({
            type:'GET',
            url:"//follow-soa.jd.com/rpc/vender/followByVid",
            data:data,
            dataType:"text",
            success:function(msg){
                var reg = /\((.+)\)/;
                if(reg.exec(msg).length > 1){
                    var res = JSON.parse(reg.exec(msg)[1]);
                    if(res.msg == "cookie value is null or empty, can't fetch user's pin"){
                        console.log('未登录，3S后步骤重置');
                        // updateHostStep(_host_step.check_address_login);//设置步骤
                        clue('未登录，3秒后，步骤重置~');
                        setTimeout(resetHostByStep,3000);
                    }else{
                        console.log('关注店铺成功');
                        console.log(res);
                        callback && callback();
                    }
                }else{
                    console.log('请求url关注店铺失败');
                    callback && callback();
                }
                
            }
        
        })
    })
    
}

//访问URL关注商品
function followProductByUrl(callback){
    useLocal(function(local){
        var data = {
            productId: localXss.currentTask.item_id,
            t:Math.random()
        }
        $.ajax({
            type:'GET',
            url:"//t.jd.com/product/followProduct.action",
            data:data,
            success:function(msg){
                console.log('关注商品成功');
                //console.log(msg);
                callback && callback();
            }
        })
    })   
}

/**
 * 商品加入购物车
 */
function addCart(){

    p_price ? clue('商品价格：' + p_price):clue('无法获取到商品价格');

    //是否随机使用一键购
    var easyBuy = $("#btn-easybuy-submit");
    //if(easyBuy.length>0){
    //    if(isGoldenRatio()){
    //        clue("使用一键购");
    //        //执行一键购
    //        easyBuy[0].click();
    //        return false;
    //    }
    //}

    checkOtherService(function(){
        var InitCartUrl = $('#InitCartUrl');
        var qg_btn = $("#btn-reservation");//立即抢购
        var ys_btn = $("#btn-reservation");//预售
        if (InitCartUrl.length >= 1) {
            InitCartUrl[0].click();
        }else if(qg_btn.length >0){
            qg_btn[0].click();
        }else if(ys_btn.length >0){
            ys_btn[0].click();
        } else {
            clue('没有找到加入购物车按钮', 'error');
        }
    })


    
}

            //匿名函数结束
        })
    }

}})();