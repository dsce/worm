(function () {
    var observerId = tabCommon.observer($(".login-form")[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东登陆  --- 弹出登录窗口用到
var submit_username = null;
var submit_password = null;
var user_disabled = false;

var observer_config = {
    //attributes: true,
    childList: true,
    //characterData: true,
    //attributeOldValue: true,
    //characterDataOldValue: true,
    subtree: true
}
// tabCommon.mutation(observerId,function(mutation){
  tabCommon.mutation(observerId,function(mutation){
    console.log(mutation.type);
    console.log(mutation.target);
    console.log(mutation);

    if(mutation.type == 'childList'){

        if(mutation.target.className == 'msg-wrap' && mutation.addedNodes.length > 0){
            Run(function(local){
                var msg_error = $(".msg-wrap .msg-error").text();
                if(msg_error.indexOf("请刷新页面后重新提交") != -1){
                    //存在多余cookie _t
                    clue("等待删除多余cookie, 稍后刷新");
                    tabCommon.sm(taskVars, '.tabs_remove_cookies', {details: {url: document.location.href, name: "_t"}});
                    lazy(function(){
                        location.reload(true);
                    });
                }else if(msg_error.indexOf('账户名不存在，请重新输入') != -1 || msg_error.indexOf('你的账号因安全原因被暂时封锁') != -1){
                    //提示账号密码错误，
                    if(localXss.currentTask.username == submit_username && localXss.currentTask.password == submit_password){
                        //账号重置
                        if(!user_disabled){
                            user_disabled = true;
                            console.log('账号重置');
                            tabCommon.sm(taskVars, '.disable_account', msg_error);
                        }

                    }
                }else if(msg_error.indexOf("账户名与密码不匹配，请重新输入") != -1){
                    var username_password_unmatch_count = localXss.currentTask.username_password_unmatch_count != undefined ? localXss.currentTask.username_password_unmatch_count : 1;

                    if(username_password_unmatch_count > 3){
                        if(!user_disabled){
                            user_disabled = true;
                            console.log('账号重置');
                            tabCommon.sm(taskVars, '.disable_account', msg_error);
                        }
                    }else{
                        localXss.currentTask.username_password_unmatch_count = username_password_unmatch_count + 1;
                        setLocal({task: localXss.currentTask}, function(){
                            lazy(function(){
                                location.reload(true);
                            });
                        });
                    }


                }
            });

        }else if(mutation.target.className == "qrcode-img" && mutation.addedNodes.length > 0){
            //切换TAB选账户登录兼容出现默认扫码登录
            $(".login-form .login-tab a:contains('账户登录')")[0].click();
        }
        // if(mutation.target.className == "qrcode-img" && mutation.addedNodes.length > 0){
        //     //切换TAB选账户登录兼容出现默认扫码登录
        //     $(".login-form .login-tab a:contains('账户登录')")[0].click();
        // }else{

        // }

    }
},observer_config);


Task(index);

// addListenerMessage(function (request) {
//     console.log(request);
//     if (request.act == 'business_account_ready') {
//         useLocal(function(local){
//             inputUsername(localXss.currentTask);
//         });
//     }
// });

addListenerMessage(function (request) {
    console.log(request);
    if (request.act == 'business_account_ready') {
        useLocal(inputUsername);
    }else if(request.act == "https_tabs_verify_code_result"){
        writing($("#authcode"), request.text, null);
    }
});


function index(local) {
    var task = localXss.currentTask;

    tabCommon.sm(taskVars, '.getBusinessAccount');

    // $('#loginname').val(task.username)
    // $('#nloginpwd').val(task.password)
    // $('input[name=chkRememberMe]').prop('checked', true)

    // lazy(submit);

    $('#authcode').on('keyup', function () {
        if ($('#authcode').val().length >= 4) {

            accountLoginLog('pc', function(){
                $('#loginsubmit')[0].click()
                // $('#loginsubmit')[0].click();
            })
            
        }
    }).focus()

    $('#JD_Verification1').on('click', function () {
        $('#authcode').focus()
    })
}


function inputUsername(local) {

    //$('#loginname').val(task.username);
    writing($("#loginname"), localXss.currentTask.username, function () {
        inputPassword(local);
    });
}

function inputPassword(local) {
    writing($("#nloginpwd"), localXss.currentTask.password, function () {
        submit();
    });
}

function submit() {

    submit_username = $("#loginname").val();
    submit_password = $("#nloginpwd").val();


    if($('#authcode:visible').length > 0){
        updateHostStatus(1301001);

        console.log("有验证码");
        clue("有验证码,等待打码");
        
        //打码前,开启监听
        tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginListener', null, function(){
            var imgSrc = $("#JD_Verification1").attr("src2") + '&yys='+new Date().getTime();
            getCrossDomainAuthCodeBase64(imgSrc, function(base64){
                //console.log(base64);
                tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginRemoveListener', null, function(){
                    //关闭监听,准备打码
                    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                });

            });
        });



    }
    // if ($('#authcode:visible').length > 0) {
    //     updateHostStatus(1301001);
    // }
    //登录按钮
    var $loginsubmit = $('#loginsubmit');

    var $loginsubmitframe = $('#loginsubmitframe');

    var $btnSubmit = null;

    if ($loginsubmit.length > 0) {
        $btnSubmit = $loginsubmit;
    } else {
        $btnSubmit = $loginsubmitframe;
    }


    // $btnSubmit[0].click();
    accountLoginLog('pc', function(){
        $btnSubmit[0].click();
       
    })

}

            //匿名函数结束
        })
    }

}})();