(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 激活账户页面
Task(index);

function index(local){
    var task = localXss.currentTask;

    var sendMobileCode = $("#sendMobileCode:visible");
    var sendMail = $("#sendMail:visible");
    var sendHistoryMobile = $("#sendHistoryMobileCode:visible");
    var lackVerifyMode = $("#content .mod-main .safe-icon-box h3:contains('缺少可用的验证方式')");
    if(sendMobileCode.length > 0 || $("#code").length >0 ){
        //账号需要验证
        updateHostStatus(1000002);
        var message = $('.tip-box').text();
        message = message ? message : '账户存在风险，验证手机';
        //自动重置账号操作
        setTimeout(function(){
            tabCommon.sm(taskVars, '.disable_account', message);
        }, 3000);


    }else if(sendMail.length > 0){

        updateHostStatus(1000002);

        var message = $("#content .mb10").text();
        message = message ? message : '账户存在风险，验证邮箱';
        //自动重置账号操作
        setTimeout(function(){
            tabCommon.sm(taskVars, '.disable_account', message);
        }, 3000);
    }else if(sendHistoryMobile.length > 0){
        //账号风险，验证历史收货人手机
        updateHostStatus(1000002);

        //var message = $("#content .mb10").text();
        message = '账号存在被盗风险，验证历史收货人手机';
        //自动重置账号操作
        setTimeout(function(){
            tabCommon.sm(taskVars, '.disable_account', message);
        }, 3000);
    }else if(lackVerifyMode.length>0){
        //缺少可用的验证方式
        var message = $("#content .mod-main .safe-icon-box h3").text();
        message = message ? message : '缺少可用的验证方式！';
        setTimeout(function(){
            tabCommon.sm(taskVars, '.disable_account', message);
        }, 3000);
    }
}

            //匿名函数结束
        })
    }

}})();