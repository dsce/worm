(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

            localXss.currentTask.cart_cleared=true;

//结算

var item_stock_ed = false;
var selfItemNum = 0;
var group_orders = localXss.currentTask.orders;

var check_products_exist = true;

var checkedNums = 0;

var drop_full_products = [];

var observer_config = {
    //attributes: true,
    childList: true,
    characterData: true,
    //attributeOldValue: true,
    //characterDataOldValue: true,
    subtree: true
}

//tabCommon.mutation(observerId,function(mutation){
tabCommon.mutation(observerId,function(mutation){
    //console.log(mutation.type);
    // console.log(mutation);
    if(mutation.type === 'childList'){
        // console.log(mutation.target);
        // console.log(mutation);
        if(mutation.target.className == 'ui-ceilinglamp-1' && mutation.addedNodes.length > 0){
            //结算
            Run(function(local){
                if (taskOrderExist('group_xss')) {
                    return false;
                }
                // checkGiftStock(local);
            });
        }else if(mutation.target.className == "number" && mutation.addedNodes.length > 0){
            console.log('number');
            // Run(checkGiftStock);

        }else if(mutation.target.className == "item-list" && mutation.addedNodes.length>0&&mutation.removedNodes.length>0){
            //删除赠品后刷新页面
            Run(function(local){
                //checkGiftStock();
                //location.reload();//影响弹出登陆窗口去掉
            });

        }else if(mutation.target.className.indexOf("quantity-txt")!=-1 && mutation.addedNodes.length>0){
            //商品显示无货
            r(function(local){
                var quantity = $(".quantity-txt[_stock*='stock_']:contains('无货'):visible");
                if(!item_stock_ed && quantity.length > 0){
                    $(".quantity-txt[_stock*='stock_']:contains('无货'):visible").each(function(i){
                            var item_id = $(this).attr('_stock').split('_')[1];
                            var mythis = $(this);
                            var task_address = localXss.currentTask.consignee;
                            var remarks = "[" + task_address.province + task_address.city + item_id + "]无货" ;
                            group_orders.forEach(function(order){
                                if(order.item_id == item_id){
                                    var jd_remarks = 'JD状态：' + mythis.text();
                                    clue(jd_remarks);
                                    updateHostStatus(1406001);
                                    console.log('出现无货');
                                    item_stock_ed = true;
                                    clue("主商品[" + item_id + "]无货", 'error');
                                    //异常包下的单
                                    reportGroupOrderException(remarks,order.id);

                                    return false;
                                }
                            })
                    })
                   
                    
                    
                   
                    
                    
                    // reportProductStockout(remarks);
                    
                    return false;
                }
            });

        }else if(mutation.target.className == 'ui-dialog-content' && mutation.addedNodes.length >0 ){
            Run(function(local){
                var dialog_content = $(".ui-dialog-content");
                if(dialog_content.find("h3:contains('删除商品')").length > 0){
                    var select_remove = dialog_content.find('.select-remove');
                    if(select_remove.length > 0){
                        select_remove[0].click();
                    }else{
                        clue('没有删除按钮');
                    }

                }
            });

        }else if(mutation.target.id == 'favorite-products' && mutation.addedNodes.length >0){
                Run(checkLogin);
        }
    }

},observer_config);

addListenerMessage(function(request){
    console.log(request);
    var act = request.act;
    if(request.act == 'check_active_tab_result'){
        if(request.is_active == true && request.need_reload == true){
            clue('关闭其他标签页面');
        }else if(request.is_active == false){
            close_this_tab();
        }
    }
});

windowsTopNotSelf();
 Task(index);
// useLocal(index);
function index(local) {
    //if(document.referrer.indexOf('cart.jd.com/cart') == -1 ){
    //    location.href = 'http://cart.jd.com/cart';
    //}

    // if (taskOrderExist('group_xss')) {
    //     return false;
    // }

    if(localXss.currentTask.is_mobile == 1){
        clue("手机单，不能进行pc下", 'error');
        return false;
    }

    tabCommon.sm(taskVars,'.check_active_tab');
    
    getSharedTaskWorks(function(sTask){

        var business_oid = local.task.business_oid;

        if(business_oid || sTask.first_submit_order_id){
            clue('已经提交过订单，去订单列表');
            tabPc.myOrderListClick();

            setTimeout(close_this_tab,3*1000)
            return false;
        }else{

            var cartEmpty = $("#container div.message:contains('购物车空空的')")
            // if(cartEmpty.length > 0 && localXss.currentTask.cart_cleared === false){
            //     updateHostStatus(1304000);
            //     clue('购物车已经清空');
            //     setLocalTask({cart_cleared: true}, function(){
            //         console.log('host_step',local.host_step);
            //         if(local.host_step < 4){
            //             close_this_tab();
            //             return false;
            //         }else{
            //             window.location.reload();
            //         }
                    
            //     });
            // }
            setLocalTask({cart_cleared: true}, function(){

                updateHostStatus(1409000);
                if(cartEmpty.length > 0 && localXss.currentTask.cart_cleared === true){
                    clue('购物车为空','error');
                    updateHostStep(_host_step.open_promotion_link);//设置步骤
                    // clue('3秒后，步骤重置~');
                    // setTimeout(resetHostByStep,3000);
                    clue('3S后任务重置');
                    setTimeout(function(){
                        tabCommon.sm(taskVars, '.taskReset',{taskName:'search'})
                    },3*1000)
                    
                    return false;

                }else if($("#container a:contains('登录'):visible").length >0){

                    $("#container a:contains('登录'):visible")[0].click();

                }else{
                   checkGiftStock(); 
                }
            })
        
        }

     })
    //submit();
}

function checkLogin(){
    var login = $("#container a:contains('登录'):visible");
    if(login.length > 0){
        login[0].click();
    }else{
        var goShoping = $("#container a:contains('去购物'):visible");
        if(goShoping.length > 0){
            goShoping[0].click();
        }
    }
   
}


function checkGiftStock(local){

    checkedNums++;

    if(localXss.currentTask.cart_cleared !== true){
        //清空购物车操作

        var checkboxes = $("#toggle-checkboxes_down");
        var removeBatch = $("#cart-floatbar a.remove-batch")
        if(checkboxes.length > 0 && removeBatch.length > 0){
            if(!checkboxes[0].checked){
                checkboxes[0].click();
            }
            setTimeout(function(){
                clicking(removeBatch);
            }, 2000);
        }else{

            clue('无法完成清空购物车操作', 'error');
        }

        return false;
    }

    //勾上主商品
    //var selfItemId = localXss.currentTask.item_id;
    //if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length == 0){
    //    var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']");
    //    selfItemCheckbox[0].click();
    //}

    group_orders.forEach(function(order){
        if(check_products_exist){
            selectedItem(order);
        }
        
    })


    setTimeout(function(){
        if(check_products_exist){
            checkGroupNums();
        }
        
    },5*1000);

    

    return false;


    var stockout = $('.gift-items .gift-item:visible:contains("无货")').first();
    if (stockout.length > 0) {
        var item_id = stockout.find("a").first().prop("href").match(/\d+/g)[0];

        addTaskOrderRemark("赠品无货:删除无货赠品"+item_id, function(){
            lazy(function(){
                clicking(stockout.find("a.remove-selfgift"));
            },5);
        });

        //updateHostStatus(1409101);//赠品无货
        //return false;
    }else{
        var itxt = $(".cart-item-list .itxt");
        var number = 0;
        itxt.each(function(){
            number += parseInt(this.value);
        });
        if(number==localXss.currentTask.amount){
            submit();
        }else{
            clue('购物车商品数量错误', 'error');
            updateHostStatus(1409102);//商品数量错误
        }

    }

}

function submit() {

    // if ($('.gift-item:contains("无货")').length > 0) {
    //     updateHostStatus(1409101);//赠品无货
    //     return false;
    // }

    setTimeout(function(){
        location.reload();
    },60*1000)


    var submit_btn = $("#cart-floatbar a.submit-btn");
    if (submit_btn.length > 0 && !item_stock_ed) {
        clicking(submit_btn);
    }
}

/**
 * 选中主商品
 */
function selectedItem(item){

    var selfItemId = item.item_id;
    var amount = parseInt(item.amount);

    var checkItem = $("#cart-list .item-list .item-form input[name='checkItem']");
    // var barItemNum = parseInt($(".cart-filter-bar .switch-cart-item .number").text());

    // var selfItemNum = 0;

    // var selfItemId = localXss.currentTask.item_id;

    var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']")
    if(selfItemCheckbox.length == 0){

        clue(selfItemId + "主商品不存在", "error");
        //add 20160328
        updateHostStep(_host_step.open_promotion_link);//设置步骤

        check_products_exist = false;

        checkGroupNums(selfItemId);

        return false;
        // clue('3秒后，步骤重置~');
        // setTimeout(resetHostByStep,3000);

        // clue('3S后任务重置');
        // setTimeout(function(){
        //     tabCommon.sm(taskVars, '.taskReset',{taskName:'search'})
        // },3*1000)
        // //add end
        // return false;
    }

    if(checkItem.length > 0){
        //有已经勾选的商品,
        if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length > 0){
            //主商品已勾选
            console.log(selfItemId +"主商品已勾选");

            modify_nums(selfItemId,amount);

            // selfItemNum += parseInt($("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val());
            // var selfItemNum = $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val();
            
        }else{
            clue('勾选主商品'+selfItemId);
            // selfItemCheckbox[0].click();

            selectItem(selfItemId,selfItemCheckbox,amount);
            

            // var mainItem = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']");

            // if(mainItem.parents('.cart-item-list').find('.item-list .item-form').length == 1){
            //     //该店铺下只有一个主商品 需要点击店铺
            //     clicking(mainItem.parents(".cart-item-list").find('.shop input[name="checkShop"]'));
            // }else{
            //     clicking(selfItemCheckbox);
            // }

            // setTimeout(function(){return false},5*1000)
            
        }

    }else{
        //没有勾选的商品.全选肯定没勾上
        //勾主商品
        //$("#toggle-checkboxes_down")[0].click();
        clue('勾选主商品'+selfItemId);
        // selfItemCheckbox[0].click();
        

        selectItem(selfItemId,selfItemCheckbox,amount)
        
        // setTimeout(function(){return false},5*1000)
    }

    

    // selfItemNum += amount;

    
}

function selectItem(selfItemId,selfItemCheckbox,amount){
    var mainItem = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']");

    if(mainItem.parents('.cart-item-list').find('.item-list .item-form').length == 1){
        //该店铺下只有一个主商品 需要点击店铺
        clicking(mainItem.parents(".cart-item-list").find('.shop input[name="checkShop"]'));
    }else{
        clicking(selfItemCheckbox);
    }

    modify_nums(selfItemId,amount);
}

function modify_nums(selfItemId,amount){
    var item_amounts = parseInt($("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val());
    if(item_amounts != amount){
        clue(selfItemId +'主商品数量不符：' + item_amounts + '!=' + amount + '重新编辑');
        $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val(amount);
        change_event($("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']")[0]);
    }
    // checkDiscount(selfItemId,function(){})   
        selfItemNum += parseInt($("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val());
    
}

//检测满减满折
function checkDiscount(selfItemId,callback){
    var obj = $(".quantity-form input.itxt[id*='"+selfItemId+"']").parents(".cart-item-list");
    if(obj.find(".item-header:contains('活动商品已购满')").length >0 && obj.find(".item-header:contains('已减')").length >0){
        //有满减商品 记录
        clue('主商品存在满减:' + selfItemId);
        drop_full_products.push(selfItemId);
    }

    callback && callback();
}

function checkGroupNums(not_exist_item_id){

    if(!check_products_exist && not_exist_item_id){
        clue('有主商品不存在，3S后任务重做');
        setTimeout(function(){
            var condition = {item_id:not_exist_item_id};
            tabCommon.sm(taskVars, '.taskRedo',{taskName:'search',condition:condition})
        },3*1000);
        return false;

    }else{

        var barItemNum = parseInt($(".comm-right .amount-sum em").first().text());

        if(barItemNum == selfItemNum){

                var groupAmount = 0;

                group_orders.forEach(function(order){
                    groupAmount += parseInt(order.amount);
                    checkDiscount(order.item_id);
                })
                    //勾选数量和主商品购物车数量相同
                    //提交结算
                    clue("主商品都已勾选", 'success');
                    console.log('结算');
                    if(selfItemNum == parseInt(groupAmount)){
                        clue("主商品数量正确,去结算", "success");
                        // submit();
                        if(drop_full_products.length >1){
                            var order_ids = [];
                            group_orders.forEach(function(order){
                                drop_full_products.forEach(function(d_order_item_id){
                                    if(d_order_item_id == order.item_id){
                                        order_ids.push(order.id);
                                    }
                                })
                            })
                            clue('满减满折踢单不异常');
                            //满减满折踢单不异常
                            reportGroupOrderException('活动商品满减，踢单不异常',group_orders[0].id,order_ids);
                            return false;
                        }else{
                            submit();
                        }
                        
                    }else{
                        clue("主商品错误("+selfItemNum+")", "error");
                        updateHostStatus(1409102);//商品数量错误

                        clue('开始编辑商品数量');
                        group_orders.forEach(function(order){
                            var selfItemId = order.item_id;
                            modify_nums(selfItemId,order.amount);
                        })
                        
                        clue('商品数量修改成功');

                        // setTimeout("location.reload();",3*1000);

                    }

                }else{
                    // selfItemNum = 0;
                    //已勾选商品数量和主商品购物车数量不一致,说明勾选有其他商品,全选,或全部选
                    clue("已选择商品数量和购物车主商品数量不一致,重新勾选主商品");
                    if($("#toggle-checkboxes_down:checked").length==0){
                        // clue('3s后刷新');
                        // setTimeout("location.reload();",3*1000)
                        if(barItemNum > selfItemNum){
                            selfItemNum = 0;
                            clue('全选');
                            $("#toggle-checkboxes_down")[0].click();
                            checkGiftStock();
                            // checkedNums = 0;
                            // setTimeout("location.reload();",3*1000)
                        }else{
                            selfItemNum = 0;
                            checkGiftStock();
                        }
                        
                        
                    }else {
                        clue("全不选");
                        selfItemNum = 0;
                        $("#toggle-checkboxes_down")[0].click();
                        checkGiftStock();
                    }

                    
                    
                    
                }

        }
}

            //匿名函数结束
        })
    }

}})();