(function () {
    var observerId = tabCommon.observer($('#main')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 订单列表 jd/list.js

var orderToPay = 0;
var runIndexed = false;
var amount = 0;
var worldwideOrderId = 0;

var request_pay_state_nums = 0;//获取付款状态请求次数

var first_reload_page = true;

var group_orders = localXss.currentTask.orders;

// Task(readyIndex);

readyIndex();

setTimeout(function(){
    if(!runIndexed){
        Run(index);
    }
}, 20000);

var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}
tabCommon.mutation(observerId,function(mutation){

    if(mutation.type == 'childList'){
        // console.log(mutation.type);
        // console.log(mutation.target);
        // console.log(mutation);

    }
}, observer_config);

addListenerMessage(function(request){
    if(request.act == "created_360hk_cookie_thor"){
        orderUrl(worldwideOrderId);
    }
});

function readyIndex(){
    getSharedTaskWorks(function(shareTask){
        getTaskWorks(function(task){
            if(shareTask.orderinfo == undefined && !task.opened_rand_business_oid){
                if(random(0,1)){
                    randOpenOrderDetail(function(){
                        index();
                    });
                }else{
                   index(); 
                }
                
            }else{
                index();
            }
        })
        
    })
    
}

//检测评价
function randOpenOrderDetail(callback){
    //检测已完成订单
    var finished_orders = $(".order-tb").find("tbody[id*=tb-]").find(".status:contains('已完成')");
    if(finished_orders.length > 0){
        var open_target_index = random(0,parseInt(finished_orders.length - 1));
        var business_oid = finished_orders.eq(open_target_index).parents("tbody").attr("id").match(/\d+/)[0];
        if($("#idUrl"+business_oid).length >0){
                
            setTaskWorks({opened_rand_business_oid:business_oid},function(){
                $("#idUrl"+business_oid)[0].click();
                setTimeout(close_this_tab,3*1000);
                // callback && callback();
            })
            
            
        }else{
            callback && callback();
        }
        // finished_orders.each(function(i){})
            // var business_oid = $(this).parents("tbody").attr("id").match(/\d+/)[0];
            // var create_url = 'http://club.jd.com/myJdcomments/orderEvaluate.action?ruleid=' + business_oid;
            //tabCommon.sm(taskVars, '.createTabByUrl', {url: url});

            // if(finished_orders.length - 1 == i){
            //     clue('检测已完成订单评价完成');
            //     getTaskWorks(function(local){
            //         var task = localXss.currentTask;
            //         task.checked_comment = true;
            //         setTaskWorks({task:task},function(){
            //             callback && callback();
            //         })
            //     })
                
            // }
        

    }else{
        callback && callback();
    }
   
}

function index() {

    if(runIndexed){
        return false;
    }
    runIndexed = true;
    //updateHostStatus(1413001);

    if(localXss.currentTask.business_oid){
        clue('订单已存在');
        tabCommon.report(taskVars, 'xss');
        return false;
    }

    //如果是app付款，直接进入订单
    if(localXss.usage == '1'){
        clue("APP付款订单");
        orderUrl(localXss.currentTask.business_oid);
        return false;
    }

    


    var goods_item = null;
    var target_order = null;

    group_orders.forEach(function(order){
        if($('#main .p-'+ order.item_id +'').length >0){
            goods_item = $('#main .p-'+ order.item_id +'').first();
            target_order = order;
        }
    })

    // var goods_item = $('#main .p-'+ group_orders[0].item_id +'').first();
    if(goods_item && goods_item.length > 0){

        var goods_number = goods_item.next(".goods-number").text().match(/\d+/)[0];

        if(goods_number == target_order.amount){
            var order_body = goods_item.parents("tbody");
            var order_id = order_body.find('a[name="orderIdLinks"]')[0].innerText

            if(localXss.currentTask.worldwide == '1'){
                sendDataToBackground('add_360hk_cookie_thor', {}, function(){
                    clue("检查全球购登录cookie");
                    worldwideOrderId = order_id;
                })
            }else{
                orderUrl(order_id);
            }

        }else{
            clue("商品数量错误", 'error');
        }

    // var goods_item = $('#main .p-'+ localXss.currentTask.item_id +'').first();
    // if(goods_item.length > 0){
    //     var goods_number = goods_item.next(".goods-number").text().match(/\d+/)[0];

    //     if(goods_number == localXss.currentTask.amount){
    //         var order_body = goods_item.parents("tbody");
    //         var order_id = order_body.find('a[name="orderIdLinks"]')[0].innerText

    //         if(localXss.currentTask.worldwide == '1'){
    //             sendDataToBackground('add_360hk_cookie_thor', {}, function(){
    //                 clue("检查全球购登录cookie");
    //                 worldwideOrderId = order_id;
    //             })
    //         }else{
    //             orderUrl(order_id);
    //         }

    //     }else{
    //         clue("商品数量错误", 'error');
    //     }
    }else{
        clue('未找到对应订单，3s后刷新');

        if(!localXss.currentTask.business_oid){

            
            getSharedTaskWorks(function(shareTask){
                if(shareTask.first_submit_order_id){
                    tabCommon.sm(taskVars, '.save_no_found_order',shareTask.first_submit_order_id,function(){
                    });
                    // if(!task.jd_order_id && !task.orderinfo){
                        //提交了订单号，未确认过订单
                        //未保存过付款数据
                        getTaskWorks(function(task){
                            if(!shareTask.saved_card_pay){
                                // var message = '找不到订单，预计订单号(手动标记异常)：' + shareTask.first_submit_order_id;
                                var message = '未付款找不到订单，订单号：' + shareTask.first_submit_order_id;
                                clue(message);
                                reportGroupOrderException(message,group_orders[0].id);
                                // sendMessageToBackground('order_exception',message);
                                return false;
                            }else{
                                lazy(function(){
                                    location.reload(true)
                                }, 3);
                            }
                        })
                    // }else{
                    //     lazy(function(){
                    //         location.reload(true)
                    //     }, 3);
                    // }
                }else{
                    // lazy(function(){
                    //     location.reload(true)
                    // }, 3);

                    clue('未提交过订单'); 

                    if(localXss.currentTask.is_mobile == 1){
                        clue('M端订单未找到,自动踢单');
                        if(!shareTask.saved_card_pay){
                            var message = '未付款找不到订单';
                            clue(message);
                            reportGroupOrderException(message,group_orders[0].id);
                            return false;
                        }
                    }
                        
                    getTaskWorks(function(tw){
                        var redirect_nums = tw.redirect_nums || 0;
                        if(redirect_nums >3){
                            clue('出现刷新频繁次数大于3次，10S重新开始吧');
                            //重新开始
                            setTimeout(function(){
                                tabCommon.sm(taskVars, '.startHost');
                            },10*1000);
                            
                        }else{
                            clue('当前出现刷新频繁跳转次数=' + redirect_nums +' ,10S后重置当前任务');
                            setTimeout(function(){
                                tabCommon.sm(taskVars,'.currentTaskReset');
                            },10*1000)
                        }
                    })
                        
                    
                }
            })
            
        }else{
           getSharedTaskWorks(function(task){

                   if(!task.saved_card_pay){

                        var message = '未付款找不到订单,订单号：' +localXss.currentTask.business_oid;
                        // var message = '找不到订单，预计订单号(手动标记异常)：' + localXss.currentTask.business_oid;
                        clue(message);
                        // tabCommon.sm(taskVars, '.order_exception',message);
                        reportGroupOrderException(message,group_orders[0].id);
                        return false;

                    } 

            })
        }
    }
}

function toPayBegin(order_id){

    //var order_id = '10416062022';
    //var order_id = '10415707583';
    var number = 0;
    $("#track"+order_id+" .goods-number").each(function(){
        number += parseInt(this.innerText.match(/\d+/));
    });

    if(amount == 0 || amount != number){
        clue('进入订单进一步核对');
        orderUrl(order_id);
        return false;
    }else{
        clue("商品数量一致", 'success');
    }

    orderUrl(order_id);
    lazy(close_this_tab);

    //var pay_button = $("#pay-button-" + order_id + " a");
    //pay_button = pay_button.length > 0 ? pay_button : $("#operate"+order_id+" .btn-pay");
    //
    //if (pay_button.length > 0) {
    //    updateHostStatus(1413000);
    //    lazy(function(){
    //        //pay_button[0].click();
    //        orderUrl(order_id);
    //        lazy(close_this_tab);
    //    });
    //} else {
    //    getTaskWorks(function(local){
    //        var statu_payment = $("#track"+order_id).find(":contains('等待付款')");
    //        if(local.host_step == _host_step.save_order_info || statu_payment.length == 0){
    //            clue("查看订单");
    //            orderUrl(order_id);
    //        }else{
    //            clue('没有找到订单付款按钮');
    //        }
    //    });
    //
    //}
}

function orderUrl(order_id){

    // getTaskWorks(function(local){   });

        //全球购手机单替换进入链接
        var order_url = $("#idUrl"+order_id);
        if(localXss.currentTask.worldwide == '1' && localXss.currentTask.is_mobile == '1'){

            var hk_url = order_url.attr('href');
            // if(hk_url.indexOf('order.jd360.hk') != -1){
            //     var com_url = hk_url.replace('order.jd360.hk', 'gjz.order.jd.com');
            //     order_url[0].href = com_url;
            // }
        }


        if(order_url.length > 0){

            //进订单详情前获取下单时间
            var task = localXss.currentTask;
            var order_submit_time = $("#datasubmit-" + order_id).val();
            setTaskWorks({order_submit_time:order_submit_time},function(){ })

            //是否已经入过订单详情
            getSharedTaskWorks(function(shareTask){
                if(shareTask.orderinfo !== undefined){
                //已进入过订单详情，可以在订单列表判断最终状态
                    var entry_order_info = localXss.currentTask.entry_order_info === undefined ? 1 : localXss.currentTask.entry_order_info;
                    entry_order_info =1;//不管几次都进入订单详情
                    if(entry_order_info > 3){
                        clue('已经多次尝试进入订单详情');
                        clue('订单列表核对订单付款状态');
                        //核对订单付款状态
                        var tb_order = $('#tb-'+ order_id);
                        var order_status = tb_order.find('.order-status').text();
                        //等待厂商处理
                        if(order_status.indexOf('正在出库') != -1 || order_status.indexOf('付款成功') != -1 || order_status.indexOf('等待厂商处理') != -1){
                            clue(order_id + order_status, 'success');

                            clue('正在保存订单数据....');
                            //订单完成，准备保存订单数据
                            updateHostStatus(1603000);//开始保存数据
                            tabCommon.sm(taskVars, '.save_host_task_order_detail');
                        }else{
                            clue(order_id + order_status);

                            // clue(_t.order_detail_refresh_hz + "秒后，刷新");
                            // setTimeout(function () {
                            //     location.reload(true);
                            // }, _t.order_detail_refresh_hz * 1000);
                            reload_page();
                        }

                    }else{
                        entry_order_info ++;
                        clue('再次尝试进入订单详情');
                        setTaskWorks({entry_order_info: entry_order_info}, function(){

                            var tb_order = $('#tb-'+ order_id);
                            var order_status = tb_order.find('.order-status').text();
                            //等待厂商处理
                            if(order_status.indexOf('正在出库') != -1 || order_status.indexOf('付款成功') != -1 || order_status.indexOf('等待厂商处理') != -1){
                                clue(order_id + order_status, 'success');

                                clue('正在保存订单数据....');
                                //订单完成，准备保存订单数据
                                updateHostStatus(1603000);//开始保存数据
                                tabCommon.sm(taskVars, '.save_host_task_order_detail');
                            }
                            

                            //自营单点付款
                            if(order_url.attr('href').indexOf('dps.ws.jd.com/normal/item.action') != -1){
                                //点击付款按钮
                                if(order_url.parents("tbody").find(".btn-pay:contains('付款')").length >0){
                                    order_url.parents("tbody").find(".btn-pay:contains('付款')")[0].click();
                                }    
                            }else{
                                $("#idUrl"+order_id)[0].click();
                            }

                            lazy(close_this_tab);
                        });

                    }


                }else{
                    $("#idUrl"+order_id)[0].click();
                    lazy(close_this_tab);
                }
            })
            

        }else{
            clue('没有找到订单 ['+order_id+']','error');
            // tabCommon.sm(taskVars, '.save_no_found_order',order_id);
            if(!localXss.currentTask.business_oid){

            getSharedTaskWorks(function(shareTask){
                if(shareTask.first_submit_order_id){
                    tabCommon.sm(taskVars, '.save_no_found_order',shareTask.first_submit_order_id,function(){
                    });
                    // if(!task.jd_order_id && !task.orderinfo){
                        //提交了订单号，未确认过订单
                        //未保存过付款数据
                        getTaskWorks(function(task){
                            if(!shareTask.saved_card_pay){
                                var message = '未付款找不到订单,订单号：' + shareTask.first_submit_order_id;
                                // var message = '找不到订单，预计订单号(手动标记异常)：' + shareTask.first_submit_order_id;
                                clue(message);
                                
                                reportGroupOrderException(message,group_orders[0].id);
                                // sendMessageToBackground('order_exception',message);

                                return false;
                            }else{
                                lazy(function(){
                                    location.reload(true)
                                }, 3);
                            }
                        })
                  
                }else{
                    clue('未提交过订单，10S后重新开始');
                    setTimeout(function(){
                        //重新开始
                        tabCommon.sm(taskVars, '.startHost');
                    },10*1000);
                }
            })
            
        }else{
            getSharedTaskWorks(function(task){

                   if(!task.saved_card_pay){

                   
                        var message = '未付款找不到订单,订单号:' + localXss.currentTask.business_oid;
                                // var message = '找不到订单，预计订单号(手动标记异常)：' + shareTask.first_submit_order_id;
                        clue(message);

                        reportGroupOrderException(message,group_orders[0].id);

                        return false;
                    } 

            })
        }
    }
 


}

function reload_page() {

    if(first_reload_page){
        console.log('第一次请求付款状态延迟10S');
        setTimeout(function(){
            tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
            first_reload_page = false;
        },10*1000);

    }else{
        tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
    }
    
}

//刷新当前页面
function do_reload_page(){
    clue(_t.order_detail_refresh_hz + "秒后，刷新");
    setTimeout(function () {

        window.location.reload(true);

    }, _t.order_detail_refresh_hz * 1000);

}

//监听付款状态结果
addListenerMessage(function(request){
    if(request.act == 'get_pay_state_result'){
        console.log(request);
        getSharedTaskWorks(function(task){
            if(task.jd_order_id == request.data.business_oid){
                if(request.data.pay_state == 3){//付款成功
                    clue('付款成功了');
                    do_reload_page();
                }else if(request.data.pay_state == 4){//代理失败
                    //标记付款失败
                    clue('付款失败了');
                    tabCommon.sm(taskVars, '.change_proxy');//更换代理

                }else if(request.data.pay_state == 5){//付款失败
                    //标记付款失败
                    clue('付款失败了');
                    updateHostStatus(1502003);
                    // do_reload_page();
                    setTaskWorks({entry_order_info:0},function(){
                        do_reload_page();
                    })

                }else{
                    //还没获取到付款状态，继续获取
                    if(request_pay_state_nums > 20){
                        do_reload_page();
                    }else{
                        request_pay_state_nums++;
                        console.log('正在获取付款状态，请等待...');
                        setTimeout(function(){
                            tabCommon.sm(taskVars, '.getOrderPayState');//获取付款状态
                        },3000);   
                    }
                }
            }else{
                clue('获取付款状态的订单号不一致');
                do_reload_page();
            }
        })
        
        
    }else if(request.act == 'change_proxy_result'){
        //更换代理成功
        updateHostStatus(1502003);//标记付款失败
        setTaskWorks({entry_order_info:0},function(){
            do_reload_page();
        })
        
    }else if(request.act == 'save_host_task_order_detail_result'){
        //提交订单数据成功
        setTimeout(function(){
            if($("#nav .fore-3 .dd a:contains('我的级别')").length >0){
                $("#nav .fore-3 .dd a:contains('我的级别')")[0].click();
            }else{
                location.href = 'https://usergrade.jd.com/user/grade';
            }
        },3*1000)
    }
})

            //匿名函数结束
        })
    }

}})();