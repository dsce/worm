(function () {
    var observerId = tabCommon.observer(document.querySelector('#mainframe'))
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 提交订单

var new_consignee = false;//新增收货地址
var payment_selected = false;//选择在线支付
var invoice_selected = false;//不开发发票
var jdbean_use = false;//使用京豆
var order_submit_checked_number = false;//提交订单检查商品数量
var order_submit_checked_product = false;//提交订单检查商品
var pay_password_error = false;//用豆，支付密码错误

var pay_fee = 0;
var shipping_fee = 0;
var bean_fee = 0;

var order_check_code_img = true;
var verify_cid = 0;

var group_orders = localXss.currentTask.orders;

var checkJdPrice = true;

Task(index);

//页面监控

tabCommon.mutation(observerId,function (mutation) {
        //console.log(mutation.type);
        //console.log(mutation.target);
        //console.log(mutation);


        if (mutation.type === 'childList') {
            //if (mutation.target.id == "jdBeans" && mutation.addedNodes.length > 0) {
            if (mutation.target.id == "jdBeans-new" && mutation.addedNodes.length > 0) {
                //填写使用京豆数量
                // Run(set_jdbeans);
            } else if (mutation.target.id == 'checkCodeDiv' && mutation.addedNodes.length > 0) {
                Run(function(local){
                    if($("#orderCheckCodeImg:visible").length == 0){
                        return false;
                    }
                    //有验证码
                    order_check_code_img = false;

                    // var imgsrc = $("#orderCheckCodeImg")[0].src;
                    $("#checkcodeTxt").focus();
                    updateHostStatus(1410007);

                    $('#checkcodeTxt').on('keyup', function () {
                        if ($('#checkcodeTxt').val().length >= 4) {
                            clue("4位 自动登录");
                            lazy(function () {
                                useLocal(function(local){
                                    if(local.isRunning){
                                        order_check_code_img = true;
                                    }else{
                                        setLocal({isRunning: true}, function(){
                                            order_check_code_img = true;
                                        });
                                    }
                                });

                            }, 2);

                        }
                    }).focus();

                    //打码结果
                    addListenerMessage(function (request) {
                        console.log(request);
                        if (request.act == 'https_tabs_verify_code_result') {
                            verify_cid = request.cid;
                            $("#checkcodeTxt").val(request.text);
                            order_check_code_img = true;
                        }else if(request.act == 'tab_get_cookies_response'){
                            //if(request.cookies.length > 0){
                            //    var imgsrc = location.origin + $("#captcha-img img").attr('src') + "&v="+Math.LN2;
                            //    var plogin_cookie = "lsid="+request.cookies[0].value;
                            //    chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc, cookie: plogin_cookie});
                            //}else{
                            //    clue("cookie 不存在，不能打码");
                            //}
                        }else if(request.act == 'https_tabs_verify_code_result_errno'){
                            var imgsrc = $("#orderCheckCodeImg")[0].src;
                            var act = 'https_tabs_verify_code_from_uuyun';
                            getErrorNo(request,imgsrc,act);
                        }
                    });
                    //发送打码
                    tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginListener', null, function(){
    

                         var rid = Math.random().toString() + "_" + Math.random().toString();
                         var encryptClientInfo = $("#encryptClientInfo").val();
                         var checkCodeUrl = "//captcha.jd.com/verify/image?acid=" + rid + "&srcid=trackWeb&is=" + encryptClientInfo;
                         console.log(checkCodeUrl);
                         $("#checkcodeRid").val(rid);

                            getCrossDomainAuthCodeBase64(checkCodeUrl, function(base64){
                                //console.log(base64);
                                tabCommon.sm(taskVars, '.JdLoginAuthCodeCrossOriginRemoveListener', null, function(){
                                    //关闭监听,准备打码
                                    chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                                });

                            });
                    });


                    // var base64 = getBlobImg($("#orderCheckCodeImg")[0]);
                    //console.log(blob);
                    // chrome.extension.sendMessage({act: 'https_tabs_verify_code_by_base', base: base64});
                    // chrome.extension.sendMessage({act: 'https_tabs_verify_code', imgsrc: imgsrc});
                    // chrome.extension.sendMessage({act: 'https_tabs_verify_code_from_uuyun', imgsrc: imgsrc});
                    
                });



            } else if (mutation.target.id == 'submit_message' && mutation.addedNodes.length > 0) {
                //提交订单的错误提示
                var submit_message_text = $("#submit_message")[0].innerText;
                var jd_remarks = submit_message_text;

                if(submit_message_text.indexOf('支付密码不正确') != -1){
                    //支付密码错误//不使用京豆
                    clue('支付密码错误，不使用京豆');
                    pay_password_error = true;
                    var cancel_btn = $("#jdBeans a.a-link:visible:contains('取消使用')");
                    if(cancel_btn.length > 0){
                        Run(set_jdbeans);
                    }else{
                        $("#orderBeanItem a:contains('使用京豆')")[0].click();
                    }


                }else if(submit_message_text.indexOf("收货人信息不对") != -1 || submit_message_text.indexOf("区镇选择有误") != -1 || submit_message_text.indexOf("地址升级") != -1
                || submit_message_text.indexOf("地址已经升级") != -1){

                    //您的收货人地址所在区镇选择有误！请核对后再提交
                    //提交订单收货人地址有误，或者地址库待更新，address_update true,进行删除地址，重新添加 ,
                    //你当前的收货人信息不对,2016-3-11
                    Run(function(local){
                        $(".ui-switchable-panel-selected a:contains('编辑')")[0].click();
                        //updateHostStatus(1406010);
                        //if(local.address_update == undefined || local.address_update == true){
                        //    setLocal({address_update: true}, function(){
                        //        location.href = 'https://easybuy.jd.com/address/getEasyBuyList.action';
                        //    });
                        //}else{
                        //    updateHostStatus(1406011);
                        //    clue("新添加的地址过不去，地址库需要更新", 'error');
                        //}

                    });

                }else if(submit_message_text.indexOf('商品无货') != -1){
                    checkProductExceptionReloadNums('whuo',function(){
                        console.log('商品无货');
                        updateHostStatus(1406003);//无货
                        clue(submit_message_text, 'error');
                        // reportProductStockout(submit_message_text);//自动标记异常
                        set_group_order_exception(submit_message_text);
                        
                    })



                }else if(submit_message_text.indexOf('同一账户只能抢购一件') != -1){
                    //"啊哦，同一账户只能抢购一件哟！别灰心，请返回购物车删除商品AMOVO魔吻黑巧克力礼盒装 情人节礼物送男友送女友生日创意礼品后提交订单吧！"
                    //进入我的订单列表
                    clue(submit_message_text);
                    $("a:contains('我的订单')")[0].click();

                }else if(submit_message_text.indexOf('您当前的“发票内容”选择有误') != -1){

                    getTaskWorks(function(tw){
                        var reselect_invoice_nums = tw.reselect_invoice_nums || 0;
                        if(reselect_invoice_nums >3){
                            clue('发票选择有误超过3次，自动踢单');
                            reportGroupOrderException('发票选择有误，踢单不异常',group_orders[0].id);
                        }else{
                            reselect_invoice_nums++;
                             setTaskWorks({reselect_invoice:true,reselect_invoice_nums:reselect_invoice_nums},function(){
                                clue('发票内容选择有误,重新选择');
                                setTimeout(function(){
                                    $('#part-inv .invoice-edit:contains("修改")')[0].click();
                                },3*1000)
                                
                            })
                        }
                    })

                   

                }else if(submit_message_text.indexOf('无法购买商品') != -1 || submit_message_text.indexOf('限购') != -1){
                    clue(submit_message_text, 'error');
                    //您当前选择的地区无法购买商品秋顿【套装】中老年女装妈妈装夏装2017夏装新款女士短袖印花T恤+松紧七分裤S520 2号色 2XL(建议110-125斤)
                    set_group_order_exception(submit_message_text);

                }else if(submit_message_text.indexOf('对不起，系统繁忙，请稍候再试') != -1){
                    clue(submit_message_text, 'error');
                    var message = '踢单不异常，异常提示：' + submit_message_text;
                    var order_ids = [];
                    group_orders.forEach(function(order){
                        order_ids.push(order.id);
                    })
                    reportGroupOrderException(message,group_orders[0].id,order_ids);
                    return false;
                }else{


                    clue(submit_message_text, 'error');
                }
                 //赠品无货任务标记   
                }else if(mutation.addedNodes.length > 0 && mutation.target.id == 'out-skus'){
                         useLocal(function(local){
                            var remark = '赠品无货:'+ $("#submit_message")[0].innerText;
                            //添加任务备注
                            // addTaskOrderRemark(remark,function(){});
                            clue($("#submit_message")[0].innerText);
                         });

                         clue('3秒后，继续提交订单');
                         setTimeout(function(){
                            $(".ui-dialog .btn-1")[0].click();
                         },3000);
                }  
        }

        if (mutation.type === 'attributes') {
            if (mutation.target.id == 'paypasswordPanel' && mutation.attributeName === 'style') {
                //填写支付密码
                if($("#txt_paypassword:visible").length > 0){
                    Run(set_paypassword);
                }
            }
        }


});

function set_group_order_exception(submit_message){
    $(".goods-items .p-name a").each(function(i){
        var target_item = $(this).text().replace(/\s+/g,'');
        console.log(target_item);
        if(submit_message.replace(/\s+/g,'').indexOf(target_item) != -1){
            var sub_order_item_id = $(this).attr('href').match(/\d+/)[0];
            console.log(sub_order_item_id);

            group_orders.forEach(function(order){
                if(order.item_id == sub_order_item_id){
                    //异常包下的单
                    reportGroupOrderException(submit_message,order.id);

                    return false;
                }
            })

        }
    })
}

//运行
function index(local) {

    //限制app付款下单
    if(local.usage == '1'){
        notify('APP付款，不能下单')
        setLocal({isRunning: false},function(){
            window.location.reload(true);
        })
        return false;
    }

    //只有pc单可以下单
    if(localXss.currentTask.is_mobile == 1){
        clue("手机单，不能进行pc下", 'error');
        return false;
    }

    if (taskOrderExist('group_xss')) {
        return false;
    }

    $("#payment-list li:contains('货到付款')").remove();

    updateHostStep(_host_step.submit_order);//step6 提交订单
    updateHostStatus(1410000);

    //进入提交订单，先检查一遍商品数量和金额，
    check_address(local,function(){
        checkService(function(){
            order_check();
            setTimeout(online_payment, 5000);//选择在线支付
        });
        // order_check(local);
        
    })
    

    
}

//选择在线支付
function online_payment() {
    if (payment_selected) {
        return false;
    }
    //$("#payment-list .payment-item[payid='4']").parent()[0].click();
    if($("#payment-list li div:contains('在线支付')").length >0){

        $("#payment-list li div:contains('在线支付')")[0].click()
        edit_invoice();//修改发票信息

        //使用京豆
        r(use_jdbean_btn);

    }else{
        clue('提交订单时找不到在线支付');
        updateHostStatus(1410010);
        return false;
    }

}

//修改发票信息
function edit_invoice() {
    if (invoice_selected) {
        return false;
    }
    ;
    $('#part-inv .invoice-edit:contains("修改")')[0].click();
}

//使用京豆
function use_jdbean_btn(local) {
    var task = localXss.currentTask;
    task.business_use_jd_beans = 0;
    if (task.business_use_jd_beans == 1) {
        //可以使用京豆
        setTimeout(function () {
            $("#orderBeanItem a:contains('使用京豆')")[0].click();
        }, 2000);

    } else {
        order_submit(local);
    }
}

//设置京豆
function set_jdbeans(local) {

    var task = localXss.currentTask;
    task.business_use_jd_beans = 0;
    if (task.business_use_jd_beans == 1 && jdbean_use == false && pay_password_error == false) {

        if ($('#usedBeans').attr('fmax') > 0) {

            //最大京豆
            var maxJd = parseInt($('#usedBeans').attr('fmax'));

            //转换失败情况
            if (isNaN(maxJd)) {
                maxJd = 0;
            }

            if (maxJd > 60) {
                maxJd = Math.ceil(maxJd / 2);
            }

            //先判断京豆输入框是否存在, 不存在说明已经输入京豆 ，存在则开始输入
            if ($('#usedBeans:visible').length > 0) {
                maxJd = maxJd > 60 ? 60 : maxJd;
                $('#usedBeans').val(maxJd);
                setTimeout(function () {
                    $('#orderBeanItem input[type="button"]').trigger("click");
                    jdbean_use = true;

                }, 3000);
            }

            //检查支付密码框是否存在， 存在直接输入支付密码
            if ($('#txt_paypassword:visible').length > 0) {
                set_paypassword(local);
            }

        } else {
            order_submit(local);
        }

    }else{
        if(pay_password_error == true){
            //不使用京豆 处理，
            var cancel_btn = $("#jdBeans a.a-link:visible:contains('取消使用')");
            if(cancel_btn.length > 0){
                cancel_btn[0].click();
                order_submit(local);
            }
        }
    }

}

//设置支付密码
function set_paypassword(local) {
    var task = localXss.currentTask;
    task.business_use_jd_beans = 0;
    if (task.business_use_jd_beans == 1) {
        addListenerMessage(function (request) {
            console.log(request);
            if (request.act == 'business_account_ready') {
                useLocal(function(local){
                    $('#txt_paypassword').val(localXss.currentTask.pay_password);
                    order_submit(local);
                });
            }
        });
        tabCommon.sm(taskVars, '.getBusinessAccount');
        //$('#txt_paypassword').val(localXss.currentTask.pay_password);
        //order_submit(local);
    }
}

function checkService(callback){
    group_orders.forEach(function(order){

        var target_item = $("#shopping-lists a[href*='" + order.item_id +"']");
        var service_items = target_item.parents(".shopping-list").find(".service-items:visible");
        var goods_item = target_item.parents(".goods-list").find(".goods-msg .p-price");
        if(goods_item.length >0 && checkJdPrice){
            if(goods_item.find('.p-presell strong').length >0){
                var jd_price = parseFloat(goods_item.find('.p-presell strong').text().match(/\d+(=?.\d{0,2})/g)[0]);
            }else{
                var jd_price = parseFloat(goods_item.find('.jd-price').text().match(/\d+(=?.\d{0,2})/g)[0]);
            }
            
            var p_nums = parseInt(goods_item.find(".p-num").text().match(/\d+/)[0]);

            if(p_nums != order.amount){
                clue('商品数量不一致' + p_nums + '!=' + order.amount + ',去购物车吧');
                cartReture();
            }else if((jd_price - order.product_price)*order.amount  >= 400){
                clue(order.item_id + '商品价格差价大于400','error');
                checkJdPrice = false;
            }
        }
        if(service_items.length >0){
            //有其他服务
            if(service_items.find(".service-item:contains('店铺服务费')").length >0){
                var service_fee = service_items.find(".service-item:contains('店铺服务费')").find(".service-price").text().match(/\d+(=?.\d{0,2})/g)[0];
                if(service_fee >0){
                    //服务费大于0,修改送装服务
                    clue(order.item_id + ': 店铺服务费' + service_fee);
                    if(service_items.find(".service-item:contains('送装服务')").length >0){
                        service_items.find(".service-item .service-edit")[0].click();

                        setTimeout(function(){
                            if(service_items.find(".service-item .service-list:visible").length >0){
                                clue('重新选择送装服务');
                                service_items.find(".service-item .service-list ul li span").last()[0].click();
                                var new_service_fee = service_items.find(".service-item:contains('店铺服务费')").find(".service-price").text().match(/\d+(=?.\d{0,2})/g)[0];
                                clue('当前店铺服务费：' + new_service_fee);
                            } 
                            
                        },3*1000)
                    }

                }
            }
        }

     })

    setTimeout(function(){
        if(checkJdPrice){
            callback && callback();
        }
    },3*1000);
}

//核对订单
function order_check(local) {

    var amount = 0;

    group_orders.forEach(function(order){

        amount += parseInt(order.amount);

    //检查主商品是否存在
    if ($("#shopping-lists a[href*='" + order.item_id +"']").length > 0) {
        order_submit_checked_product = true;
        //检测服务费
        // checkService(order);

    } else {
        clue('主商品[' + order.item_id + ']不存在', 'error');
        order_submit_checked_product = false;
        // updateHostStep(_host_step.open_promotion_link);//设置步骤
        // clue('3秒后，步骤重置');
        // setTimeout(resetHostByStep,3000);
        clue('3S后任务重置');
        setTimeout(function(){
            // tabCommon.sm(taskVars, '.taskReset',{taskName:'search'})
        },3*1000)
        return false;
    }

    })

    if($('.order-summary .list span em').length >0){
        var number = $('.order-summary .list span em').text().match(/\d+/);
    }else if($(".goods-items div[goods-id='" + localXss.currentTask.item_id +"']").length >0){
        var number = $(".goods-items div[goods-id='" + localXss.currentTask.item_id +"']").find('.p-num').text().match(/\d+/)[0];
    }

    

    if (number != amount) {
        clue('商品数量不一致', 'error');
        cartReture();
        order_submit_checked_number = false;

    } else {
        clue('商品数量正确', 'success');
        order_submit_checked_number = true;
    }

    //金额
    if($(".order-summary .statistic").length >0){
        pay_fee = $(".order-summary .statistic .price").first().text();
    }else if($(".order-summary .presale-total .presale-list").length >0){
        pay_fee = $(".order-summary .presale-total .presale-list").first().text();
    }else{
        pay_fee = $("#sumPayPriceId").text();//应付款金额
    }
    
    if($("#presaleFreightId").length >0){
        shipping_fee = $("#presaleFreightId").text();
    }else{
        shipping_fee = $("#freightPriceId").text();//运费
    }
    
    bean_fee = $("#usedJdBeanId").text().replace('-', '');//京豆优惠金额

    clue("<p>优惠：" + bean_fee + "</p><p>运费：" + shipping_fee + "</p><p>应付总额：" + pay_fee + "</p>");

    clue('发票信息：' + $("#part-inv").text());
    clue('支付方式: ' + $("#payment-list .item-selected").text());


}

//提交订单
function order_submit(local) {

    //多包裹，
    // if($("#shopping-lists .shopping-list").length > 1){
    //     clue("多包裹，核对主商品，注意多余商品",'error');
    //     updateHostStatus(1410006);//多包裹，核对主商品，注意多余商品
    //     return false;
    // }

    // if($("#shopping-lists .shopping-list .goods-items .goods-item:visible").length > 1){
    //     clue("多商品存在，核对主商品，注意多余商品",'error');
    //     updateHostStatus(1410102);//多包裹，核对主商品，注意多余商品
    //     return false;
    // }

    // //检查主商品是否存在
    // if ($("#shopping-lists").html().indexOf(localXss.currentTask.item_id) == -1) {
    //     updateHostStatus(1410101);//主商品不存在
    //     return false;
    // }

     if($('.order-summary .list span em').length >0){
        var number = $('.order-summary .list span em').text().match(/\d+/);
    }else if($(".goods-items div[goods-id='" + localXss.currentTask.item_id +"']").length >0){
        var number = $(".goods-items div[goods-id='" + localXss.currentTask.item_id +"']").find('.p-num').text().match(/\d+/)[0];
    }

    // if (number != localXss.currentTask.amount) {
    //     updateHostStatus(1410103);//商品数量错误
    //     cartReture();
    //     return false;
    // }

    if (taskOrderExist('group_xss')) {
        return false;
    }

    order_check(local);
    if (order_submit_checked_number && order_submit_checked_product) {
        if($(".order-summary .presale-total .presale-list").length >0){
            var items = {
                temp_pay_fee:$(".order-summary .presale-total .presale-list").first().text().split('+')[1].match(/\d+(=?.\d{0,2})/g)[0]
            };
        
        }else{

            var items = {
                temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
                temp_shipping_fee: shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
            }

        }
        setLocal(items ,function(){
            console.log('提交订单');
            clue('检测是否使用了优惠券');
            checkCoupon();
            
            lazy(function () {
                order_submit_click();
            }, 12);
        });

    }
}


//检查优惠券
function checkCoupon(){
    $(".order-virtual").show()

    if($("#bestCouponCheck:checked").length >0){
        clue('取消推荐按钮');
        click_event($("#bestCouponCheck:checked")[0])
    }

    setTimeout(function(){
        if($(".coupon-enable .coupon-item .item-selected").length >0){
            clue('取消使用优惠券')
            $(".coupon-enable .coupon-item .item-selected .item-selected-cancel")[0].click()
        }
        
    },3*1000)
    
    
}

//返回购物车
function  cartReture(){
    lazy(function(){
        $("#cartRetureUrl")[0].click();
    },3);
}

//提交订单
function order_submit_click(){

    //运费大于50，不提交
    var yunfee = $("#freightPriceId").text().match(/\d+/);
    if(yunfee && yunfee[0] > 50){
        clue('订单运费大于50， 不能提交', 'error');
        updateHostStatus(1410009);
        return false;
    }

    if(!order_check_code_img){
        console.log('等待验证码');
        setTimeout(order_submit_click, 3000);
        return false;
    }
    console.log('order submit');
    clue('提交订单喽~');

    if($("#payment-list li .item-selected:contains('货到付款')").length >0){
        clue('支付方式不正确, 支付方式：' + $("#payment-list li .item-selected").text() +' ,5s后刷新');
        setTimeout("location.reload();",5*1000);
    }else{
        clue('支付方式:' + $("#payment-list li .item-selected").text());
        clue('发票信息：' + $("#part-inv").text());
        $("#order-submit")[0].click();
    }
    // $("#order-submit")[0].click();
    setTimeout(function(){
        if(document.location.protocol == 'https:'){
            location.reload(true);
        }else{
            reloadHttps();
        }
    }, 13000);
}

            //匿名函数结束
        })
    }

}})();