(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 提交订单 新增收货地址

var county_selected = false;
var saveConsigneeTitleDiv_btn_clicked = false;
var areaTabLoaded = false;
var edited = false;
var selected = false;

Task(index);

//页面监控

tabCommon.mutation(observerId,function (mutation) {
        console.log(mutation.type);
        console.log(mutation.target);
         console.log(mutation);

        if (mutation.type === 'childList') {
            //console.log(mutation.type);
            //console.log(mutation.target);
            //console.log(mutation);

            if (mutation.target.className == 'ui-area-tab' && mutation.addedNodes.length > 0) {

                if(!areaTabLoaded){
                    areaTabLoaded = true;
                    setTimeout(function(){selectAddress();},2000);
                    console.log("可以编辑");
                    edited = true;
                }

            } else if (mutation.target.className == 'ui-area-content' && mutation.addedNodes.length > 0 && mutation.removedNodes.length == 0) {
                console.log('这一次');
                console.log(mutation.addedNodes[0].className);

            } else if (mutation.target.id == 'span_province' && mutation.addedNodes.length > 0) {
                //设置省份
                Run(set_province);
            } else if (mutation.target.id == 'span_city' && mutation.addedNodes.length > 0) {
                //设置城市
                Run(set_city);
            } else if (mutation.target.id == 'span_county' && mutation.addedNodes.length > 0 && mutation.addedNodes[0].length > 1) {
                //设置 县区
                Run(set_county);
            } else if (mutation.target.id == 'span_town' && mutation.addedNodes.length > 0 && mutation.addedNodes[0].length > 1) {
                //设置 街道
                Run(set_town);
            } else if (mutation.target.id == 'span_town' && mutation.addedNodes.length == 0 && mutation.removedNodes.length > 0) {
                //删除街道
                if ($("#span_town:visible").length == 0) {
                    //console.log('===============================================================================================');
                }

            }

        } else if (mutation.type === 'attributes') {
            if (mutation.target.id == 'consignee_address' && mutation.attributeName == "tabindex" && mutation.oldValue == "6") {

                if ($("#consignee_town[tabindex='5']:visible").length == 0 && county_selected) {
                    //已经选择 县区，，没有 街道，准备保存
                    Run(saveConsigneeTitleDiv_btn);
                    // Run(input_cosignee_name)
                }
            }else if(mutation.target.className=='ui-area-text'&&mutation.attributeName=='data-id'&&mutation.oldValue!=''){
                if(edited){
                    console.log('完成更新');
                    setTimeout(function(){
                        //save();
                    }, 2000);
                }
            }

            //else if(mutation.target.className='.ui-switchable-panel .ui-switchable-panel-selected'&&mutation.attributeName=='style'&&mutation.oldValue=='display: none;'){
            //    console.log("第一次编辑");
            //}
        }
});


//运行
function index(local) {
    var consignee = localXss.currentTask.consignee;

    //收货人，详细地址，手机号
    // $("#consignee_name").val(consignee.name);
    // $("#consignee_address").val(consignee.short_address);
    // $("#consignee_mobile").val(consignee.mobile);

    //input_cosignee_name();

    //检查是否有空的地区
    var consignee_province_id = $("#consignee_province").val();
    var consignee_city_id = $("#consignee_city").val();
    var consignee_county_id = $("#consignee_county").val();
    var consignee_town_id = $("#consignee_town").val();

    //if(consignee_county_id == ""){
    //  //城市有问题
    //  set_county(local);
    //
    //}

    //if(consignee_town_id == ""){
    //  set_town();
    //}else{
    //  save();
    //}

    //兼容需要编辑一下的个别地址
    setTimeout(function(){
        save();
    }, 5000);
}

function selectAddress() {
    if(selected){
        return false;
    }
    selected = true;
    $("#jd_area .ui-area-text").attr('data-id').match(/\d+/g);
    if($("#jd_area .ui-area-text").length > 0){
        var areaIds = $("#jd_area .ui-area-text").attr('data-id').match(/\d+/g);

        var aArea = $("#jd_area .ui-switchable-panel-selected li a");
        aArea[random(0, aArea.length-1)].click();
        //var countyId = areaIds[2];
        //var townId = areaIds[3];
        //if(countyId > 0){
        //
        //    if(townId){
        //
        //    }else{
        //
        //    }
        //
        //}else{
        //    //随机选区
        //    var aCounty = $("#jd_area .ui-area-content .ui-switchable-panel[data-index='2']").find(".ui-area-content-list a[data-id]")
        //    aCounty[random(0, aCounty.length-1)].click();
        //}
    }
}


//设置省份
function set_province(local) {
    var consignee = localXss.currentTask.consignee;
    var province = $('#consignee_province option').filter(function () {
        var text = $(this).text().replace('*', '')
        return text == consignee.province;
    })
    if (province.length > 0) {
        province.prop('selected', true)
        change_event($("#consignee_province")[0]);
    } else {
        clue("不能匹配的省份 " + consignee.province);
    }
}

//设置城市
function set_city(local) {
    var consignee = municipality(localXss.currentTask.consignee);
    var select = $("#consignee_city");
    var options = $("#consignee_city option");

    var city = options.filter(function () {
        var text = $(this).text().replace('*', '')
        return text == consignee.city;
    })
    if (city.length > 0) {
        city.prop('selected', true)
        change_event($("#consignee_city")[0]);
    } else {
        clue('不能匹配的城市 ' + consignee.city + " 随机");

        var index = Math.floor(Math.random() * (options.length - 1) + 1)
        options.eq(index).prop('selected', true);
        change_event(select[0]);
    }

}

//设置县区
function set_county(local) {
    var consignee = municipality(localXss.currentTask.consignee);
    var select = $("#consignee_county");
    var options = $("#consignee_county option");

    var option = options.filter(function () {
        var text = $(this).text().replace('*', '')
        return text == consignee.area;
    })

    if (option.length > 0) {
        option.prop('selected', true)
        change_event(select[0]);
    } else {
        //clue('无法匹配县区');
        //clue('县区，随机');

        var index = Math.floor(Math.random() * (options.length - 1) + 1)
        options.eq(index).prop('selected', true);
        change_event(select[0]);
    }

    county_selected = true;

    // if(is_municipality(consignee.province)){
    //   setTimeout(saveConsigneeTitleDiv_btn,2000);
    // }

}

//设置街道
function set_town(local) {
    var select = $("#consignee_town");
    var options = $("#consignee_town option");

    var index = Math.floor(Math.random() * (options.length - 1) + 1)
    options.eq(index).prop('selected', true);
    change_event(select[0]);

    setTimeout(save, 2000);
}

function municipality(consignee) {
    var province = consignee.province;

    if (province == '北京' || province == '上海' || province == '天津' || province == '重庆') {
        consignee.city = consignee.area;
    }

    return consignee;
}

//保存收货人信息
function saveConsigneeTitleDiv_btn() {

    if (saveConsigneeTitleDiv_btn_clicked) {
        return false;
    }
    saveConsigneeTitleDiv_btn_clicked = true;

    chrome.storage.local.get(null, function (local) {


        writing($("#consignee_name"), localXss.currentTask.consignee.name, function () {
            lazy(function () {
                writing($("#consignee_address"), localXss.currentTask.consignee.short_address, function () {
                    lazy(function () {
                        writing($("#consignee_mobile"), localXss.currentTask.consignee.mobile, function () {
                            lazy(function () {
                                $("#saveConsigneeTitleDiv:visible")[0].click();

                                $("#saveConsigneeTitleDiv:visible").hide();
                            });
                        });
                    });
                });
            });
        });
    })

}


function save() {
    $("#saveConsigneeTitleDiv:visible")[0].click();
}

            //匿名函数结束
        })
    }

}})();