(function () {
    var observerId = tabCommon.observer($(".pay-load")[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//全球购选银行

var indexed = false;
var selected = false;

tabCommon.mutation(observerId,function (mutation) {
		// console.log(mutation.type);
		// console.log(mutation.target);
		// console.log(mutation);
		if(mutation.type === 'attributes' && !indexed){
			if(mutation.target.className == 'paybox-newcard animate-enter animate-enter-active'){
			   indexed = true;
               Run(function(){
                     if($(".pn-new:contains('网银支付')").length >0){
                        $(".pn-new:contains('网银支付')")[0].click();

                        //获取银行信息
                        chrome.runtime.sendMessage({act:'get_payment_info'});
                        
                    }
               })  
               
			}else if(mutation.target.className == 'pay-newUser animate-enter animate-enter-active'){
				indexed = true;
                Run(function(){
                    if($(".pn-c-text:contains('网银支付')").length >0){
                        $(".pn-c-text:contains('网银支付')")[0].click();
                    }

                    //获取银行信息
                    chrome.runtime.sendMessage({act:'get_payment_info'});
                    
                })
                
			}else if(mutation.target.id == 'modal_newCardWangyin' && !selected){
				// selected = true;
				// if($("#modal_newCardWangyin").length >0){
				// 	$("#modal_newCardWangyin .ui-tab-items a:contains('网银支付')")[0].click();
				// 	setTimeout(function(){
				// 		select_bank(bank);
						
				// 	},3*1000)
					
				// }else{
				// 	clue('找不到付款区域');
				// }
				
			}
		}

});


var bank,bankUsername;

//监听消息
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
        console.log(request);
        //clue('监听到返回消息');
        //updateHostStatus(1414002);
        if (request.act == 'get_payment_info_result') {
            if(request.error == 1){
                console.log("没有设置银行用户信息！");
                updateHostStatus(1414001);
                notifyMessage("没有设置银行用户信息！");  
            }else{
                var bank_info = request.data;
                if(bank_info.bank_code && bank_info.card_no){
                    clue('银行类别:' + bank_info.bank_code + ', 卡号:' + bank_info.card_no);
                    bank = bank_info.bank_code.toLowerCase();
                    chrome.storage.local.set({'bank_info':bank_info},function(){
                        //选择银行付款
                        chrome.storage.local.get(null,function(local){
                            //console.log(local)
                            var task = localXss.currentTask;
                            task.bank_user = bank_info.bank_user;
                            task.card_no = bank_info.card_no;
                            setLocal({task:task},function(){
                                select_bank_to_pay(local,bank);
                            })
                            
                        }) 
                    })
                    
                }else{
                    notifyMessage("银行用户信息设置不全！");  
                }
            }
            
        }
    }
);

//检测实名制
userDeclare(function(){
    Task(index);
});




function index(local){
    updateHostStatus(1414000);

    $('#paySubmit').hide();//隐藏 支付按钮

    // //优先使用接口银行信息
    // if(localXss.currentTask.bank_code && localXss.currentTask.bank_user){
    //     bank = localXss.currentTask.bank_code.toLowerCase();
    // }else{
    //     bank = local.pay.bank;
    // }


    //识别来源页面，提交订单页面，跳转订单列表页面
    var referrer = document.referrer;
    if(referrer.indexOf('trade.jd.com/shopping/order') != '-1'){
        document.location.href = 'https://order.jd.com/center/list.action';
        return false;
    }


    //只执行已确认的订单号
    // var orderId = $("#orderId").val();
    var orderId = $(".o-tips-item:contains('订单号')").text().match(/\d+/)[0];
    clue("订单号: "+orderId);
    if(local.order_id === undefined && local.order_id!=orderId){
        document.location.href = 'https://order.jd.com/center/list.action?s=1';
        return false;
    }

    // //获取银行信息
    // chrome.runtime.sendMessage({act:'get_payment_info'});

   

    
}

//选择银行付款
function select_bank_to_pay(local,bank){
     var task = localXss.currentTask;
    // var pay = local.pay;

        if(bank == 'icbc'){
            tabCommon.sm(taskVars, '.icbcChangeUseragentStart');//工商银行替换UserAgent
        }else if(bank == 'ccb'){
            tabCommon.sm(taskVars, '.intercept_ccb_form');//建行截获form数据
        }else if(bank == 'boc' && local.pay.remote_payment_boc !== true){
            tabCommon.sm(taskVars, '.intercept_boc_form');//中行截获form数据
        }else if(bank == 'comm'){
            bank = 'bcom';//交行转换代码
        }


        if($("#modal_newCardWangyin:visible").length >0){
            $("#modal_newCardWangyin .ui-tab-items a:contains('网银支付')")[0].click();
            setTimeout(function(){
                select_bank(bank);
                
            },3*1000)
            
        }else{
            clue('找不到付款区域');
        }
       
}

function select_bank(bank){
	var li_bank = $(".payment-list:visible span[id=bank-" + bank +"]");

    if(li_bank.length >0){

        clue(li_bank.text());
        //clicking(li_bank);
        li_bank[0].click();//点击银行

        
        //保存订单号,,不在这个地方保存订单号,统一在订单详情确认订单后保存订单号
        var orderId = $(".o-tips-item:contains('订单号')").text().match(/\d+/)[0];
        useLocal(function(local){
            if(local.order_id == orderId){
                clue('订单 ['+orderId+'] 立即支付');
                var need_pay_price = $(".o-price:contains('应付金额') strong").text();
                clue('应付金额：' + need_pay_price);
                
                if($(".ui-form-line .ui-form-group input[value='跳转网银并支付']").length >0){
                	//提交
					$(".ui-form-line .ui-form-group input[value='跳转网银并支付']")[0].click();
				}
            }else{
                clue("订单号错了,回订单列表吧", 'error');
            }
        });

         

            setTimeout(function(){
                pay_finish();
            },5000);
       

    }else{
            //选择银行失败
            clue('选择银行失败,再次选择');
            select_bank(bank);
            // updateHostStatus(1502004);
            // close_this_tab();
        
    }
}

//点击支付完成
function pay_finish(){

    if(location.href.indexOf('pcashier.jd.hk') != -1){
        clue('5s后，跳转订单详情');
        setTimeout(function(){
            location.href = '//order.jd.com/center/list.action';
        },5000);
    }
    
    
}


//填写实名制信息
function userDeclare(callback){
    if($("#globalUserName:visible").length > 0){
        useLocal(function(local){
            writing($("#globalUserName"),localXss.currentTask.identity_name,function(){
                writing($("#globalUserNo"),localXss.currentTask.identity_card,
                    function(){
                        if($("#userDeclareModal .um-button a:contains('确认实名')").length >0){
                            $("#userDeclareModal .um-button a:contains('确认实名')")[0].click();
                            
                        }else if($(".ui-button-XL:contains('确认并完成支付')").length >0){
                            $(".ui-button-XL:contains('确认并完成支付')")[0].click();
                        }

                        setTimeout(function(){
                                checkUserDeclareErr(changeIdCard);
                            },3000);  
                    }
                )
            })
        })
    }else{
        callback && callback();
    }
}

//实名验证失败
function checkUserDeclareErr(callback){
    var reason = '';
    if($("#globalUserNameError:visible").length >0){
        reason = $("#globalUserNameError:visible").text();
        callback && callback(reason);
    }else if($("#globalUserNoError:visible").length >0){
        reason = $("#globalUserNoError:visible").text();
        callback && callback(reason);
    }else if($("#globalRealNameErrorInfo:visible").length >0){
        reason = $("#globalRealNameErrorInfo:visible").text();
        callback && callback(reason);
    }else{
        Task(index);
    }  
}

//重置实名信息
function changeIdCard(reason){
    if(reason){
        clue(reason);
        clue('重置实名信息');
        tabCommon.sm(taskVars, '.change_id_card',reason);
    }
}

//监听消息
addListenerMessage(function (request) {
  console.log(request);
  if (request.act == 'id_card_changed') {
    var identity = request.identity;
    var identity_name = identity.name;
    var identity_card = identity.identity_card;

    setLocalTask({identity_name:identity_name,identity_card:identity_card},function(){
        window.location.reload();
    })
  }
});


            //匿名函数结束
        })
    }

}})();