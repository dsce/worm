(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 个人信息
var new_nickName = null;
var new_sex = null;

Task(index);

function index(local) {

    // getRandNickName(localXss.currentTask,function(){
    //     compareUsername(localXss.currentTask.username);
    // });

    updateHostStep(_host_step.get_account_info);//step2

    updateUserInfo(localXss.currentTask, function(){
        compareUsername(localXss.currentTask.username);
    });

}

function compareUsername(username){
    clue('对比账号.用户名是否一样');
    var user = $("#user-info b:contains('用户名')").text().substr(4);//截掉 '用户名：'

    if(user != username){
        var html = '<a href="https://passport.jd.com/uc/login?ltype=logout" class="link-logout xss-link-logout">退出</a>';
        $('body').append(html);
        $('.xss-link-logout')[0].click();
    }else{
        clue(user+"="+username);
        // $("#menu a:contains('收货地址')")[0].click();
        checkCart(function(){
            setTimeout(function(){
                $("#menu a:contains('我的级别')")[0].click();
            },3*1000)
            
        })
        
    }
}

function checkCart(callback){
    useLocal(function(local){
        //打开购物车页面
        var cart_cleared = localXss.currentTask.cart_cleared;
        if(cart_cleared === undefined){

            var cart_btn = $("#nav a:contains('我的购物车')");
            if(cart_btn.length > 0){
                setLocalTask({cart_cleared: false}, function(){
                    setTimeout(function(){
                        clicking(cart_btn);
                        callback && callback();
                    },3*1000)
                    
                });
            }else{
                clue('购物车入口改变，不能进行清空操作', 'error');
            }

        }else{
            callback && callback();
        }
    })
    
}
/**
 * 京东原版更新用户信息
 */
function updateUserInfo(task, callback) {

    if($("#container a:contains('提交')").length == 0){
        window.location.reload();
    }

    if(task.birthday_updated_at && task.nickname_updated_at){
        clue("已修改"+task.birthday_updated_at);
        callback && callback();
        return false;
    }

    //随机生日
    var date,a, b,birthday;
    a=new Date('1970-1-31');
    b=new Date('2000-12-31');
    date = new Date(),
        date.setTime(random(a.getTime(), b.getTime()));
    birthday = date;
    var yearBirthday = birthday.getFullYear();
    var monthBirthday = birthday.getMonth();
    var dayBirthday = birthday.getDay();

    var nickName=new_nickName ? new_nickName :$("#nickName").val();
    var sex= new_sex ? new_sex : $("input[name=sex]:checked").val();
    //var nickName=$("#nickName").val();
    var realName=$("#realName").val();
    var realName = checkRealName(task.consignee.name.replace(/\s+/g, ''));//收货人姓名
    //var sex=$("input[name=sex]:checked").val();
    var birthday=$("#birthdayYear").val() + "-" + $("#birthdayMonth").val() + "-" + $("#birthdayDay").val();
    var birthday=yearBirthday + "-" + monthBirthday + "-" + dayBirthday;
    var province=$("#province").val();
    var city=$("#city").val();
    var county=$("#county").val();
    var address=$("#address").val();
    var code=$("#code").val();
    var rkey=$("#rkey").val();
    var newAliasName =$.trim($("#aliasName").val());
    var oldAliasName = $("#hiddenAliasName").val();

    var hobby="";
    $(".hobul").children().each(function(){
        if($(this).attr("class")=="selected"){
            hobby+=$(this).val()+",";
        }
    });

    var datas = "1=1";
    datas += "&userVo.nickName=" + encodeURI(encodeURI(nickName));
    datas += "&userVo.realName=" + encodeURI(encodeURI(realName));
    datas += "&userVo.sex=" + sex;
    datas += "&userVo.birthday=" + birthday;
    datas += "&userVo.hobby=" + hobby;
    datas += "&userVo.code=" + code;
    ; datas += "&userVo.rkey=" + rkey;
    if(newAliasName != $.trim(oldAliasName)){
        datas += "&newAliasName=" + encodeURI(encodeURI(newAliasName));
    }
    jQuery.ajax({
        type : "post",
        url : "/user/userinfo/updateUserInfo.action",
        data : datas,
        timeout: 10000,
        success : function(html) {
            if (html=="2") {
                //数据更新成功,保存,或结束
                console.log("success");

                tabCommon.sm(taskVars, '.updateNickname', function(){

                    tabCommon.sm(taskVars, '.updateBirthday', function () {
                        callback && callback();
                    });
                });
                // if(new_nickName){
                //     chrome.extension.sendMessage({act: 'update_nikename'}, function(){
                //         chrome.extension.sendMessage({act: 'update_birthday'}, function(){
                //             callback && callback();
                //         });
                //     });
                // }else{
                //     chrome.extension.sendMessage({act: 'update_birthday'}, function(){
                //         callback && callback();
                //     });
                // }
                

            }else if(html != "1"){
                console.log(html);
                var rand_nums = task.randNickNameNums ? parseInt(task.randNickNameNums) : 0;
                if(rand_nums < 3){
                    clue("个人信息填写有误，3S后再次保存");
                    task.randNickNameNums = rand_nums + 1;
                    clue('保存次数: ' + task.randNickNameNums);
                    setLocal({task:task},function(){
                        setTimeout(function(){
                            getRandNickName(task,callback);//随机昵称
                        },3000);
                    });
                    
                }else{
                    clue('保存信息失败超过3次，2s后跳过')
                    setTimeout(function(){
                        callback && callback();
                    },2000)
                    
                }
                
            }else {
                console.log(html);
                clue('保存失败');
                callback && callback();
            }
        }
    });
}



//获取贴吧随机昵称
function getRandNickName(task,callback){
    var userNum = random(10000000,999999999);
    var rand_nums = task.randNickNameNums ? parseInt(task.randNickNameNums) : 0;
    console.log(userNum);
    $.ajax({
        type:"GET",
        dataType:"html",
        url:"http://tieba.baidu.com/i/" + userNum +"/fans?qq-pf-to=pcqq.c2c",
        success:function(msg){
            console.log(msg);
            var pat1 = /\<p\sclass=\"aside_user_name\"\>([^\<]+)\<\/p\>/;
            var pat2 = /\<p\sclass=\"aside_user_info\"\>\<span\>([^\<]+)\<\/span\>/;
            console.log(pat1.exec(msg));
            console.log(pat2.exec(msg));
            if(pat1.exec(msg)){
                new_nickName = pat1.exec(msg)[1].replace(/\n/,'');
                //new_nickName = '刷这是什么';
                clue('随机到昵称：'+ new_nickName);
                if(new_nickName.indexOf('刷') != -1 || new_nickName.indexOf('淘') != -1 || new_nickName.indexOf('单') != -1){
                    if(rand_nums < 3){
                        clue('含有过滤字符,重新随机昵称');
                        task.randNickNameNums = rand_nums + 1;
                        clue('保存次数: ' + task.randNickNameNums);
                        setLocal({task:task},function(){
                            setTimeout(function(){
                                getRandNickName(task,callback);//随机昵称
                            },3000);
                        });
                    }
                    
                }
                if(pat2.exec(msg)){
                    new_sex = pat2.exec(msg)[1]=='男'?0:1; 
                }
                console.log(new_nickName,new_sex);   
                updateUserInfo(task, callback);
                
            }else{
                
                
                if(rand_nums < 3){
                    console.log('未获取到昵称,3s后 go on');
                    task.randNickNameNums = rand_nums + 1;
                    clue('保存次数: ' + task.randNickNameNums);
                    setLocal({task:task},function(){
                        setTimeout(function(){
                            getRandNickName(task,callback);//随机昵称
                        },3000);
                    });
                    
                }else{
                    clue('超过3次，2s后跳过')
                    setTimeout(function(){
                        callback && callback();
                    },2000)
                    
                }

            }
        }   
    })
}

//检查真实姓名
function checkRealName(name){

    if(!name){
        return '真实姓名';
    }
    var reg3 = /([^a-zA-Z\u4E00-\u9FFF])+/;
    //console.log(reg.exec(name));
    console.log(reg3.exec(name));
    if(reg3.exec(name)){
        var new_name = name.replace(reg3.exec(name)[0],'');
    }else{
        return name;
    }
    console.log(new_name.length);
    if(new_name.length  == 0){
        var new_name = '真实姓名';
    }else if(new_name.length == 1){
        var new_name = new_name + new_name;
    }
    console.log(new_name);
    return new_name;
}

            //匿名函数结束
        })
    }

}})();