(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

// jd  修改收货地址
var address = null;
var local_order_address = null;
var promotion_url = null;
var deleted = false;
var address_update = null;
var consigneeChecked = null;


Task(index);

function index(local){

    if(!localXss.currentTask.task_order_id){
        clue('任务数据不存在，5S后重新开始','error');
        setTimeout(function(){
            tabCommon.sm(taskVars, '.startHost');
        },5000);
        return false;
    }

    if(location.protocol == 'http:' && local.env != 2){//正式环境跳转https:
        location.protocol = 'https:';
    }

    updateHostStep(_host_step.update_address);//step2 修改收货地址
    tabCommon.sm(taskVars, '.saveUserCookiesToRemote', {domain: 'jd.com'});//save cookies

    if(localXss.currentTask.is_mobile != 1){

    }

    address = jdMunicipality(localXss.currentTask.consignee);

    //过滤收货人中间的多个空格问题
    address.name = address.name.replace(/\s+/g, '');

    consigneeChecked = localXss.currentTask.consigneeChecked;

    address_update = local.address_update;//地址需要更新，为true，删除，重新增加

    local_order_address = local.order_address;
    promotion_url = localXss.currentTask.promotion_url;

    //地址需要强制更新，更新已完成，直接回到购物车
    if(local.address_update === false){
        if(localXss.currentTask.is_mobile == "1"){
            promotion_url = "http://p.m.jd.com/cart/cart.action";
        }else{
            promotion_url = "http://cart.jd.com/";
        }

    }

    updateHostStatus(1302000);

    setTimeout(function(){
        var address_default = $("#addressList .sm").find("span:contains('默认地址')");
        var set_default_btn = $("#addressList a:contains(设为默认)");
        if(address_default.length > 0 || set_default_btn.length == 0){
            checkAddress(tryDeleteAllAddress);
        }else{

            var default_btn = $("#addressList a:contains(设为默认)").first();
            if(default_btn.length > 0){
                clue('没有默认，设第一个默认');
                //检测地址
                // checkAddress(function(){
                //     var old_address_area = default_btn.parents(".smc").find("span:contains('所在地')").next('.fl')[0].innerText;
                //     console.log('老地址',old_address_area);
                //     var checked_address_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'');
                //     console.log('新地址',checked_address_area);
                //     if(old_address_area == checked_address_area){
                //         console.log('地址正确,设为默认');
                //         default_btn[0].click();
                //         lazy(function(){
                //             location.reload();
                //         },5);
                //     }else{
                //         console.log('新老地址不匹配');
                //         tryDeleteAllAddress();
                //     } 
                // });
                default_btn[0].click();
                //lazy(tryDeleteAllAddress);
                lazy(function(){
                   location.reload();
                },5);
            }else{
                checkAddress(tryDeleteAllAddress);
                // tryDeleteAllAddress();
            }

        }

    },3000);

    //打开购物车页面
    var cart_cleared = localXss.currentTask.cart_cleared;
    if(cart_cleared === undefined){

        var cart_btn = $("#nav a:contains('我的购物车')");
        if(cart_btn.length > 0){
            setLocalTask({cart_cleared: false}, function(){
                clicking(cart_btn);
            });
        }else{
            clue('购物车入口改变，不能进行清空操作', 'error');
        }

    }

}

//五个地址内直接扫描目标地址
function scanAddressForFive(){
   // $("#addressList .sm").each(function(){
        var address_div = $("#addressList").find(".sm:contains('默认地址')");
        if(address_div.length > 0){
            var address_id = address_div.prop('id').match(/\d+/g)[0];
            var name = address_div.find("span:contains('收货人')").next('.fl')[0].innerText;
            var where_area = address_div.find("span:contains('所在地')").next('.fl')[0].innerText;
            var where = address_div.find("span:contains('地址')").next('.fl')[0].innerText;
            var mobile = address_div.find("span:contains('手机')").next('.fl')[0].innerText.match(/\d+/g);
            var its_name = name == address.name;
            var its_where = where==address.short_address;
            var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'');

            var its_where_area = where_area.replace(/省|市/g,'') == address_where_area.replace(/省|市/g,'');
            var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
            if(its_name&&its_where&&its_where_area&&its_mobile&&!address_update){
                    //地址一样，不需要修改
                    clue(where_area.replace(/省|市/g,'') + '==' + address_where_area.replace(/省|市/g,''));

                    clue("默认地址 一样");

                    setIpLoc(function(){
                        saveAddressToRemote(function(){
                            updateHostStep(_host_step.open_promotion_link);//step3 打开推广链接
                            lazy(function(){
                                removeLocalPromotionUrl(function(){
                                    location.href = promotion_url;
                                });
                            })
                        })
                    });

                }else{
                    if(its_name&&its_where_area&&its_mobile&&!its_where&&local_order_address!==undefined&&!address_update){
                        clue("<p>已添加地址，且只有短地址不一致。</p><p> 更新短地址，且认为地址是不需要修改的。</p>");

                        local_order_address.short_address = where;
                        setLocal({order_address: local_order_address},function(){
                            saveAddressToRemote(function(){
                                updateHostStep(_host_step.open_promotion_link);//step3 打开推广链接
                                lazy(function(){
                                    removeLocalPromotionUrl(function(){
                                        location.href = promotion_url;
                                    });
                                })
                            })
                        });

                    }else{
                        clue("默认地址不一致，删除");
                        lazy(function(){
                            delAddress(address_id,function(){
                                updateHostStatus(1303000);
                                clue("添加收货地址");
                                deleted = true;
                                autoAddress();
                            });
                        });
                    }

                }
            }else{
                
                lazy(function(){
                    if($("#addressList .sm").length == 5){
                         var address_div = $("#addressList .sm").last();
                         var address_id = address_div.prop('id').match(/\d+/g)[0];
                         delAddress(address_id,function(){
                            updateHostStatus(1303000);
                            clue("添加收货地址");
                            deleted = true;
                            autoAddress();
                        });
                     }else{
                        autoAddress();
                     }
                   
                });
            }
        

   
}

//删除所有的收货地址，，每次只删除第一个
function tryDeleteAllAddress(){
    if(deleted) {return false;}
    var address_list = $("#addressList .sm");
    if(address_list.length >5){
        var address_div = address_list.last();
        var del_btn = address_div.find(".del-btn:visible");
        if(del_btn.length > 0){
            var address_id = address_div.prop('id').match(/\d+/g)[0];
            if(address_div.find("span:contains('默认地址')").length > 0){
                var name = address_div.find("span:contains('收货人')").next('.fl')[0].innerText;
                var where_area = address_div.find("span:contains('所在地')").next('.fl')[0].innerText;
                var where = address_div.find("span:contains('地址')").next('.fl')[0].innerText;
                var mobile = address_div.find("span:contains('手机')").next('.fl')[0].innerText.match(/\d+/g);
                var its_name = name == address.name;
                var its_where = where==address.short_address;
                var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'');
                //var address_where_area = (address.street?address.street:'');
                //var its_where_area = where_area.indexOf(address.province)!=-1&&where_area.indexOf(address.city?address.city:'')!=-1&&where_area.indexOf(address.area?address.area:'')!=-1;
                var its_where_area = where_area.replace(/省|市/g,'') == address_where_area.replace(/省|市/g,'');
                var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;

                //var itsAddressRight = checkAddress();

                if(its_name&&its_where&&its_where_area&&its_mobile&&!address_update){
                    //地址一样，不需要修改
                    clue(where_area.replace(/省|市/g,'') + '==' + address_where_area.replace(/省|市/g,''));
                    clue("默认地址 一样");
                    setIpLoc(function(){
                        saveAddressToRemote(function(){
                            updateHostStep(_host_step.open_promotion_link);//step3 打开推广链接
                            lazy(function(){
                                removeLocalPromotionUrl(function(){
                                    location.href = promotion_url;
                                });
                            })
                        })
                    });

                }else{
                    if(its_name&&its_where_area&&its_mobile&&!its_where&&local_order_address!==undefined&&!address_update){
                        clue("<p>已添加地址，且只有短地址不一致。</p><p> 更新短地址，且认为地址是不需要修改的。</p>");

                        local_order_address.short_address = where;
                        setLocal({order_address: local_order_address},function(){
                            saveAddressToRemote(function(){
                                updateHostStep(_host_step.open_promotion_link);//step3 打开推广链接
                                lazy(function(){
                                    removeLocalPromotionUrl(function(){
                                        location.href = promotion_url;
                                    });
                                })
                            })
                        });

                    }else{
                        clue("默认地址不一致，删除");
                        lazy(function(){
                            delAddress(address_id);
                        });
                    }

                }

            }else{
                lazy(function(){
                    delAddress(address_id);
                });
            }


        }else{
            updateHostStatus(1303000);

            clue("添加收货地址");
            deleted = true;
            autoAddress();
        }
    }else{
        scanAddressForFive();
    }
    
    
}


function autoAddress(){

    useLocal(function(local){
        address = localXss.currentTask.consignee;
    });

    console.log(address);

    autoResetWatchDogTimer(180*1000);
    var get_province_url = "https://easybuy.jd.com//address/getProvinces.action";
    var get_city_url = "https://easybuy.jd.com//address/getCitys.action";
    var get_county_url = "https://easybuy.jd.com//address/getCountys.action";
    var get_town_url = "https://easybuy.jd.com//address/getTowns.action";
    var submit_address_url = "https://easybuy.jd.com/address/addAddress.action";



    var province_name = address.province;
    var city_name = address.city;
    var area_name = address.area;
    var street_name = address.street;

    var province_options = null;
    var city_options = null;
    var county_options = null;
    var town_options = null;


    async.waterfall([
        function (callback){
            useLocal(function(local){
                //var delivery_regions = localXss.currentTask.delivery_regions;
                //var delivery_regions_len = delivery_regions.length;
                // if(delivery_regions_len > 0){
                //     var region_name = null;
                //     while(delivery_regions_len--){
                //         var regions = delivery_regions[delivery_regions_len];
                //         regions.name = regions.name.replace(/省|市|区/g,'');
                //         if(regions.name == area_name){
                //             region_name = regions.name;
                //             break;
                //         }
                //     }
                //     if(region_name === null){
                //         region_name = delivery_regions[random(0,delivery_regions.length-1)].name;
                //     }

                //     area_name = region_name;

                // }
                callback(null);
            });
        },
        function (callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){

                //ajaxAddressDataJson(get_province_url,{},function(provinces){
                    var provinces = jd_address_provinces;
                    province_options = provinces;
                    var province_id = getAddressJsonKey(province_name,provinces);
                    clue('省份 '+provinces[province_id]);
                    callback(null,province_id);
                //});

            //},6000);
        },
        function (province_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_city_url,{provinceId: province_id},function(citys){
                    var citys = jd_address_citys[province_id];
                    city_options = citys;
                    var city_id = getAddressJsonKey(city_name,citys);
                    clue('城市 '+citys[city_id]);
                    callback(null, province_id, city_id);
                //});
            //},6000);
        },
        function (province_id, city_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_county_url,{cityId: city_id},function(countys){
            ajaxAddressDataJson(get_county_url,{cityId: city_id},function(countys){
                // var countys = jd_address_countys[city_id];
                    console.log('当前地区' + area_name);
                    county_options = countys;
                    var county_id = getAddressJsonKey(area_name,countys);
                    clue('乡镇 '+countys[county_id]);
                    callback(null, province_id, city_id, county_id);
            })

            


                //});
            //},6000);
        },
        function (province_id, city_id, county_id, callback){
            //autoResetWatchDogTimer();
            //setTimeout(function(){
            //    ajaxAddressDataJson(get_town_url,{countyId: county_id},function(towns){
            ajaxAddressDataJson(get_town_url,{countyId: county_id},function(towns){
                    console.log('jd街道');
                    console.log(towns);
                    town_options = towns;
                    var town_id = getAddressJsonKey(street_name,towns);
                    //var town_id = null;
                    towns !== undefined && towns[town_id] !== undefined ? clue('街道 '+towns[town_id]) : null;
                    callback(null, province_id, city_id, county_id, town_id);
            })
                    
                //});
            //},4000);
        },
        function(province_id, city_id, county_id, town_id, callback){
            autoResetWatchDogTimer();
            clue("地址生成，等待保存后刷新");
            setTimeout(function(){
                address.short_address = address.short_address.length>50?address.short_address.substr(0,50):address.short_address;
                if(!address.short_address){
                    clue('短地址为空,保存失败','error');
                    return false;
                }
                var fullAddress = province_name;
                fullAddress += city_options[city_id]!==undefined?city_options[city_id]:'';
                fullAddress += county_options[county_id]!==undefined?county_options[county_id]:'';
                fullAddress += town_options!==undefined&&town_options[town_id]!==undefined?town_options[town_id]:'';
                fullAddress += address.short_address;
                var form_data = {
                    'consigneeName' : address.name,
                    'provinceId'    : province_id,
                    'cityId'        : city_id,
                    'countyId'      : county_id,
                    'townId'        : town_id,
                    'consigneeAddress': address.short_address.replace(/\s|#|°|%/g,''),
                    'mobile'        : address.mobile,
                    'fullAddress'   : fullAddress,
                    'phone'         : '',
                    'email'         : '',
                    'addressAlias'  : province_name + '家里',
                    'easyBuy'       : undefined
                };

                //保存收货地址信息
                var order_address = {
                    name: form_data.consigneeName,
                    province: province_name,
                    city: city_options[city_id]!==undefined?city_options[city_id]:'',
                    area: county_options[county_id]!==undefined?county_options[county_id]:'',
                    street: town_options!==undefined&&town_options[town_id]!==undefined?town_options[town_id]:'',
                    short_address: form_data.consigneeAddress,
                    mobile: form_data.mobile
                };
                setLocal({order_address: jdMunicipality(order_address)},function(){
                    $.ajax( {
                        type : "POST",
                        dataType : "text",
                        url : submit_address_url,
                        data :json2string(form_data),
                        cache : false,
                        success : function(dataResult) {
                            callback(null, order_address);
                        },
                        error : function(XMLHttpResponse) {
                            callback(null, order_address);
                        }
                    });
                });

            },6000);
        }
    ],function(err, results){
        //clue("地址添加完成，保存后刷新");
        useLocal(function(local){
            var task = localXss.currentTask;
            task.consignee = local.order_address;
            var details = {task: task};

            if(local.address_update === true){
                details.address_update = false;
            }
            setLocal(details, function(){
                lazy(function(){
                    location.reload();
                });
            });
        });


    });

}

function ajaxAddressDataJson(url, data, callback) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: url,
        data: data,
        cache: false,
        success: function (dataResult) {
            callback(dataResult);
        },
        error: function (XMLHttpResponse) {
            console.log(XMLHttpResponse);
            autoAddress();
        }
    });

}

function getAddressJsonKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key];
        //console.log(v);
        if(value != '' && v.indexOf(value) != -1){
            //console.log("return key",key);
            return key;
        }
    }

    var key = arr_keys[random(0,length-1)];
    console.log( "random", arr_keys,length,key);
    return key;
}

// //判断是否包含
// function checkValue(value,object){
//     var length = 0;
//     var arr_keys = new Array;
//     for(var key in object){
//         length ++;
//         arr_keys.push(key);
//         var v = object[key];
//         //console.log(v);
//         if(value != '' && v == value){
//             //console.log("return key",key);
//             return key;
//         }
//     }
//     return false;
// }

//获取ID
function getKey(value,object){
    var length = 0;
    var arr_keys = new Array;
    for(var key in object){
        length ++;
        arr_keys.push(key);
        var v = object[key];
        //console.log(v);
        if(value != '' && v == value){
            //console.log("return key",key);
            return key;
        }
    }
    return false;
}


// //获取随机key
// function getRandomKey(object){
//     var length = 0;
//     var arr_keys = new Array;
//     for(var key in object){
//         length ++;
//         arr_keys.push(key);
        
//     }
//     var key = arr_keys[random(0,length-1)];
//     console.log( "random", arr_keys,length,key);
//     return key;
// }

function json2string(json){
    var str = '';
    for(var i in json){
        str += 'addressInfoParam.' + i + '=' + json[i] + '&';
    }
    str = str.substring(0, str.length-1);
    return str;
}


function delAddress(addressId,callback){
    var del_address_url = "https://easybuy.jd.com/address/deleteAddress.action";
    $.ajax({
        type : "POST",
        dataType : "text",
        url : del_address_url,
        data : "addressId=" + addressId,
        cache : false,
        success : function(dataResult) {
            clue("多余地址删除，刷新");
            if(callback){
                callback();
            }else{
                lazy(function(){
                    location.reload();
                });
            }
            
        },
        error : function(XMLHttpResponse) {
            clue("多余地址删除，刷新");
            lazy(function(){
                location.reload();
            });
        }
    });
}

    
function checkAddress(callback){
    //address = jdMunicipality(localXss.currentTask.consignee);
    //JD接口
    console.log('是否已检测过' + consigneeChecked);
    if(consigneeChecked == true){
        callback();return;
    }

    var get_county_url = "https://easybuy.jd.com/address/getCountys.action";
    var get_town_url = "https://easybuy.jd.com/address/getTowns.action";

    var province_id = getAddressJsonKey(address.province, jd_address_provinces);
    console.log('province_id', province_id, address.province);
    if(address.province && province_id){
        console.log("省正常");
        var city_id = getAddressJsonKey(address.city, jd_address_citys[province_id]);
        console.log('city_id', city_id, address.city);
        address.city = jd_address_citys[province_id][city_id];
        if(city_id){
            console.log("市正常");
            var area_id = getAddressJsonKey(address.area, jd_address_countys[city_id]);
            console.log('area_id', area_id, address.area);

                //拿到京东区
                ajaxAddressDataJson(get_county_url,{cityId: city_id},function(countys){
                    
                    console.log(countys);
                    
                    //区是否存在
                    !address.area ? clue('区为空,获取随机区'):clue('检测当前区:' + address.area);
                    var area_key = getAddressJsonKey(address.area,countys);
                    address.area_key = area_key;
                    address.area = countys[area_key];
                    console.log('记录当前区' + address.area);
                    clue('检测完毕，记录当前区：' + address.area);


                    //拿到JD接口当前正常区下街道
                    ajaxAddressDataJson(get_town_url,{countyId: address.area_key},function(towns){
                        console.log('街道地址');
                        console.log(towns);
                        console.log(count(towns));
                        
                        if(count(towns) > 0){
                            var town_key = getAddressJsonKey(address.street,towns);
                            address.street = towns[town_key];
                            console.log(address.street);

                        }else{
                            address.street = '';
                            console.log('区下没有街道');
                       
                            
                        }
                        //设置标示
                        useLocal(function(local){
                            var task = localXss.currentTask;
                            task.consigneeChecked = true;
                            //task.consignee.area = address.area;
                            //task.consignee.street = address.street;
                            task.consignee = jdMunicipality(address);
                            console.log(task);
                            setLocal({task:task},function(){
                                callback();
                            })
                        });
                        
                    })

                    
                        
                })
                
        }
    }
}

//判断对象长度
function count(o){
    var t = typeof o;
    if(t == 'string'){
            return o.length;
    }else if(t == 'object'){
            var n = 0;
            for(var i in o){
                    n++;
            }
            return n;
    }
    return false;
}; 


//设置
function setIpLoc(callback){

    var province_id = getAddressJsonKey(address.province, jd_address_provinces);
    var city_id = getAddressJsonKey(address.city, jd_address_citys[province_id]);
    var area_id = getAddressJsonKey(address.area, jd_address_countys[city_id]);
    //var street_id = getAddressJsonKey(address.street, jd_address_towns);

    var iploc_djd_value = province_id + '-' + city_id + '-' + area_id + '-' + '0';
    var areaid_value = province_id;
    var iplocation_value = escape(address.province);

    var iploc_djd = {"domain":".jd.com","hostOnly":"false","httpOnly":"false","path":"/","secure":"false","session":"false"};
    iploc_djd.value = iploc_djd_value;
    iploc_djd.name = 'ipLoc-djd';
    var areaid = {"domain":".jd.com","hostOnly":"false","httpOnly":"false","path":"/","secure":"false","session":"false"};
    areaid.name = 'areaId';
    areaid.value = areaid_value;
    var iplocation = {"domain":".jd.com","hostOnly":"false","httpOnly":"false","path":"/","secure":"false","session":"false"};
    iplocation.name = 'ipLocation';
    iplocation.value = iplocation_value;

    var cookies = [iploc_djd, areaid, iplocation];
    console.log(cookies);


    tabCommon.sm(taskVars, '.set_cookies_to_windows', cookies, function(){
        callback && callback();
    });

    //areaId
    //ipLoc-djd
    //ipLocation

}

function removeLocalPromotionUrl(callback){

    useLocal(function(local){

        localXss.currentTask.promotion_url = 'https://www.jd.com';
        localXss.currentTask.unpromotion = true;
        if(localXss.currentTask.is_mobile == 1){
            if(localXss.currentTask.worldwide == 1){
                localXss.currentTask.promotion_url = 'https://m.jd.hk';
            }else{
                localXss.currentTask.promotion_url = 'https://m.jd.com';
            }
        }else{
            if(localXss.currentTask.worldwide == 1){
                localXss.currentTask.promotion_url = 'https://www.jd.hk';
            }else{
                localXss.currentTask.promotion_url = 'https://www.jd.com';
            }
        }

        setLocal({task: localXss.currentTask}, function(){
            callback();
        });
    });

}

            //匿名函数结束
        })
    }

}})();