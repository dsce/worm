(function () {
    var observerId = tabCommon.observer($("#scrollDiv")[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东聊天
var checkedKufu,checkedMe = false;
var msgData;
var sendedNums = 0;

insertScript(function(){onbeforeunload=function(){}});

// tabCommon.sm(taskVars, '.get_chat_msg');//请求消息
// //监听消息
// addListenerMessage(function (request) {
//     console.log(request);
//     if(request.act == 'get_chat_msg_result'){
//     	msgData = request.msg;
//     	console.log('msgData',msgData);
//     }
// });

tabCommon.mutation(observerId,function(mutation){
    console.log(mutation);

    if(mutation.type === 'childList'){
    	console.log(mutation.addedNodes[0].className);
    	if(mutation.addedNodes[0].className && mutation.addedNodes.length>0){
    		if(mutation.addedNodes[0].className.indexOf('im-msg-prompt') !=-1){
	    		console.log('监控到出现验证码');
	    		 //index();
	    		 Run(index);
			}else if(mutation.addedNodes[0].className.indexOf('im-msg-notice') !=-1){
				console.log('监控到提示');
				 Run(checkNotice);
				 // checkNotice();
    		}else if(mutation.addedNodes[0].className.indexOf('im-others') !=-1){
    			console.log('监控到客服的消息')
    			// Run(checkedKufuMsg);
    			//checkedKufuMsg();

    		}else if(mutation.addedNodes[0].className.indexOf('im-me') !=-1){

    			console.log('监控到自己的消息');
    			 Run(checkMeMsg);
    			// checkMeMsg();
    		}
    	}
    }

},observer_config);

var observer_config = {
    attributes: true,
    childList: true,
    characterData: true,
    attributeOldValue: true,
    characterDataOldValue: true,
    subtree: true
}

//Task(index);

//监控验证码
function index(local){

	msgData = localXss.currentTask.chatMsg;

	// if(!msgData){
	// 	clue('没有聊天信息,不聊了，2S后关闭窗口');
	// 	setTimeout(function(){
	// 		close_this_tab();
	// 	},2000);
	// 	return false;
 //    }

	var last_prompt_msg = $("#scrollDiv .im-msg-prompt").last();
	var last_notice_msg = $("#scrollDiv .im-msg-notice").last();

	if(last_prompt_msg.length > 0){
		//需要打码
		var img_src = last_prompt_msg.find('#risk_sys_msg img').attr('src');
		console.log(img_src);
		if(img_src){
			checkCodeLimits(img_src);
		}else{
			clue('服务繁忙，关闭对话');
			setTimeout(function(){
				close_this_tab();
			},2000);
		}
		

	}else if(last_notice_msg.length > 0){
		//切换验证notice
		checkNotice(local);
	}else{
		//会话加载失败
		clue('会话结束，3S后页面关闭');
		setTimeout(function(){
			close_this_tab();
		},3000)
	}

	//监听消息
	addListenerMessage(function (request) {
        console.log(request);
        if (request.act == 'https_tabs_verify_code_result') {
            verify_cid = request.cid;
            if(request.error == 1){
            	checkCodeLimits(img_src);
            	
            }else{
            	sendChatMsg(request.text,doSend);
            }
            
        }
    });
}

function checkCodeLimits(img_src){

	useLocal(function(local){
		var task = localXss.currentTask;
		var verify_code_nums = task.verify_code_nums?task.verify_code_nums:1;
		if(parseInt(verify_code_nums)<3){
			task.verify_code_nums = verify_code_nums + 1;
			setLocal({task:task},function(){
				autoVerifyCode(img_src);//再次打码
			});

		}else{

			clue('打码超过3次， 关闭页面');
			//close_this_tab();
			setTimeout(function(){
				//close_this_tab_by_active();
				close_this_tab();
				return false;
			},3000)
		}
	})
	
}

//监控到notice
function checkNotice(local){

	msgData = localXss.currentTask.chatMsg;

	var last_notice_msg = $("#scrollDiv .im-msg-notice").last().find('p').text();

	if(last_notice_msg.indexOf('已加入会话') != -1){
		//可以开始聊天
		sendChatMsg('',doSend);

	}else if(last_notice_msg.indexOf('评价成功') != -1){
		clue('评价成功，3S后关闭')
		closeChatWin();

	}else{
		console.log(last_notice_msg);
		closeChatWin();
		// clue('店铺客服不在线或会话超时，3S后页面关闭');
		// setTimeout(function(){
		// 	close_this_tab();
		// },3000)
	}
}

//发消息
function sendChatMsg(msg,callback){

	var msg = msg ? msg : getMsg();
	clue('输入内容: ' + msg);
	if($('#text_in').length > 0){
		$('#text_in').text(msg);
		callback && callback();
	}else{
		clue('找不到发消息区域','error');
	}
	
}
//点击发送
function doSend(){
	if($("#sendMsg").length > 0){
		setTimeout(function(){
			$("#sendMsg")[0].click();
		},2000);
		
	}else{
		clue('找不到发送按钮','error');
	}
}


//获取接口消息
function getMsg(){
	//msgData =  ['你好','包邮吗','质量咋样'];
	
	// if(!msgData){
 //    		clue('没有聊天信息,不聊了，2S后关闭窗口');
 //    		setTimeout(function(){
 //    			close_this_tab();
 //    		},2000);
 //    		return false;
 //    }else{
    	//修复因有历史记录影响
    	if(!msgData){
    		closeChatWin();
    	}
		if(sendedNums <= msgData.length){
			return msgData[sendedNums].content;
		}else{
			//之前已经聊过
			closeChatWin();
		}
 
    	
	// }
 
	
}

function autoVerifyCode(imgsrc) {
    //准备开始打码
    clue('自动打码');
    //updateHostStatus(1402200);
    chrome.extension.sendMessage({act: 'https_tabs_verify_code_for_chat', imgsrc: imgsrc});
}

//满意度评价
function comment(){
	if($("#degreeButton").length > 0){
		$("#degreeButton")[0].click();
		$('#degree100')[0].click();
		if($('#submitDegreeButton').attr('class').indexOf('im-btn-submit') != -1){
			clue('提交评价');
			setTimeout(function(){
				$('#submitDegreeButton')[0].click();
			},2000)
			
		}
	}else{
		clue('找不到评价按钮','error');
	}
}
//关闭窗口
function closeChatWin(){
	clue('准备关闭会话');
	//return false;
	if($('#j_chatClose').length > 0){
		setTimeout(function(){
			$('#j_chatClose')[0].click();
			if($('.j_recommend:visible').length > 0){
				clue('需要评价');
				comment();
			}
		},3000);
		
	}else{
		clue('找不到关闭按钮','error');
	}
}

//监控到客服的消息
function checkedKufuMsg(){

	//checkSended();
	// if(checkedKufu){return false;}
	// checkedKufu = true;
	// var me_msg = $("#scrollDiv .im-me").length;
	// if(sendedNums == msgData.length){
	// 	//clue('消息发完 10S后关闭会话框');
	// 	setTimeout(function(){
	// 		closeChatWin();
	// 	},10000);
	// }else{
	// 	//clue('准备发送聊天信息');
	// 	setTimeout(function(){
	// 		sendChatMsg('',doSend);
	// 	},3000)
	// }
}
//监控到自己发信息成功
function checkMeMsg(){
	// if(checkedMe){return false;}
	// checkedMe = true;
	sendedNums = sendedNums + 1;
	clue('已发送信息数: ' + sendedNums);
	
	setTimeout(function(){
		var next_msg = $('.im-me').last().next().attr('class');
		if(next_msg){
			if(next_msg.indexOf('im-others') != -1){
				console.log('客服已经回复');
				checkSended();
			}
		}else{
			
			console.log('客服一直没回复');
			checkSended();		
		}
	},10000);
}

function checkSended(){
	useLocal(function(local){
		msgData = localXss.currentTask.chatMsg;

		if(sendedNums < msgData.length){
			clue('还有信息，继续发送');
			sendChatMsg('',doSend);
		}else{
			closeChatWin();
		}
	})
	
}

            //匿名函数结束
        })
    }

}})();