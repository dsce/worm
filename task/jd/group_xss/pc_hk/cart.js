(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

            localXss.currentTask.cart_cleared=true;

//结算

var item_stock_ed = false;
var observer_config = {
    //attributes: true,
    childList: true,
    characterData: true,
    //attributeOldValue: true,
    //characterDataOldValue: true,
    subtree: true
}

var group_orders = localXss.currentTask.orders;

var item_nums = 0;

var check_products_exist = true;


tabCommon.mutation(observerId,function(mutation){

    if(mutation.type === 'childList'){
        console.log(mutation.target);
        console.log(mutation);
        if(mutation.target.className == 'ui-ceilinglamp-1' && mutation.addedNodes.length > 0){
            //结算
            console.log('ui-ceilinglamp-1');
            console.log(mutation);
            Run(function(local){
                if (taskOrderExist('group_xss')) {
                    return false;
                }
                checkGiftStock(local);
            });
        }else if(mutation.target.className == "number" && mutation.addedNodes.length > 0){
            console.log('number');
            Run(checkGiftStock);
            
        }else if(mutation.target.className == "item-list" && mutation.addedNodes.length>0&&mutation.removedNodes.length>0){
            //删除赠品后刷新页面
            Run(function(local){
                //checkGiftStock();
                location.reload();
            });

        }else if(mutation.target.className.indexOf("quantity-txt")!=-1 && mutation.addedNodes.length>0){
            //商品显示无货
            r(function(local){
                var quantity = $(".quantity-txt[_stock='stock_"+localXss.currentTask.item_id+"']:contains('无货'):visible");
                if(!item_stock_ed && quantity.length > 0){
                    console.log('无货');
                    item_stock_ed = true;
                    clue("主商品[" + localXss.currentTask.item_id + "]无货", 'error');
                    updateHostStatus(1406001);
                    reportProductStockout("商品[" + localXss.currentTask.item_id + "]无货");
                    return false;
                }
            });

        }else if(mutation.target.className == 'ui-dialog-content' && mutation.addedNodes.length >0 ){
            Run(function(local){
                var dialog_content = $(".ui-dialog-content");
                if(dialog_content.find("h3:contains('删除商品')").length > 0){
                    var select_remove = dialog_content.find('.select-remove');
                    if(select_remove.length > 0){
                        select_remove[0].click();
                    }else{
                        clue('没有删除按钮');
                    }

                }
            });

        }else if(mutation.target.id == 'ttbar-login' && mutation.addedNodes.length >0){
                //checkLogin();
                Run(checkLogin);
        }

    }

},observer_config);

windowsTopNotSelf();
Task(index);
// useLocal(index);
function index(local) {

    //if(document.referrer.indexOf('cart.jd.com/cart') == -1 ){
    //    location.href = 'http://cart.jd.com/cart';
    //}
    if($("body").length < 1){
        window.location.reload();
    }

    updateHostStatus(1409000);
    if (taskOrderExist('group_xss')) {
        return false;
    }

    var cartEmpty = $("#container div.message:contains('购物车空空的')")
    // if(cartEmpty.length > 0 && localXss.currentTask.cart_cleared === false){
    //     clue('购物车已经清空');
    //     setLocalTask({cart_cleared: true}, function(){
    //         close_this_tab();
    //     });
    // }

    setLocalTask({cart_cleared: true}, function(){
        if(cartEmpty.length > 0 && localXss.currentTask.cart_cleared === true){
            clue('购物车为空','error');
            updateHostStep(_host_step.open_promotion_link);//设置步骤
            // clue('3秒后，步骤重置~');
            // setTimeout(resetHostByStep,3000);

            clue('3S后重置到搜索任务');

            setTimeout(function(){
                tabCommon.sm(taskVars, '.taskReset',{taskName:'search'})
            },3*1000)

            


        }
    })

    

    //submit();
}

function checkLogin(){
    var login = $("#container a:contains('登录'):visible");
    if(login.length > 0){
        login[0].click();
    }else{
        var goShoping = $("#container a:contains('去购物'):visible");
        if(goShoping.length > 0){
            goShoping[0].click();
        }
    }
}


function checkGiftStock(local){

    if(localXss.currentTask.cart_cleared !== true){
        //清空购物车操作

        var checkboxes = $("#toggle-checkboxes_down");
        var removeBatch = $("#cart-floatbar a.remove-batch")
        if(checkboxes.length > 0 && removeBatch.length > 0){
            if(!checkboxes[0].checked){
                checkboxes[0].click();
            }
            setTimeout(function(){
                clicking(removeBatch);
            }, 2000);
        }else{

            clue('无法完成清空购物车操作');
        }

        return false;
    }

    group_orders.forEach(function(order){
        //勾选主商品
        if(check_products_exist){
            selectedItem(order);
        }
        
    })

    setTimeout(function(){
        if(check_products_exist){
            checkItemNums();
        }
    },3*1000);
    
    return false;

    var stockout = $('.gift-items .gift-item:visible:contains("无货")').first();
    if (stockout.length > 0) {
        var item_id = stockout.find("a").first().prop("href").match(/\d+/g)[0];

        addTaskOrderRemark("删除无货赠品"+item_id, function(){
            lazy(function(){
                clicking(stockout.find("a.remove-selfgift"));
            },5);
        });

        //updateHostStatus(1409101);//赠品无货
        //return false;
    }else{
        var itxt = $(".cart-item-list .itxt");
        var number = 0;
        itxt.each(function(){
            number += parseInt(this.value);
        });
        if(number==localXss.currentTask.amount){
            submit();
        }else{
            clue('购物车商品数量错误', 'error');
            updateHostStatus(1409102);//商品数量错误
        }

    }

}

/**
 * 选中主商品
 */
function selectedItem(order){

    var checkItem = $("#cart-list .item-list .item-form input[name='checkItem']");
    
    // var selfItemId = localXss.currentTask.item_id;
    var selfItemId = order.item_id;

    var selfItemCheckbox = $("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']")
    if(selfItemCheckbox.length == 0){
        clue(selfItemId + "主商品不存在", "error");
        check_products_exist = false;
        checkItemNums(selfItemId);
        return false;
    }

    if(checkItem.length > 0){
        //有已经勾选的商品,
        if($("#cart-list .cart-checkbox input[name='checkItem'][value*='"+selfItemId+"']:checked").length > 0){
            //主商品已勾选
            clue(selfItemId + "主商品已勾选");
            var selfItemNum = $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val();
                //勾选数量和主商品购物车数量相同
                //提交结算
                // clue("主商品已勾选", 'success');
                // console.log('结算');
                if(selfItemNum == parseInt(order.amount)){
                    clue(selfItemId + "主商品数量正确");
                    // submit();
                }else{
                    clue("主商品错误("+selfItemNum+")", "error");
                    updateHostStatus(1409102);//商品数量错误

                    clue('开始编辑商品数量');
                    //定位商品修改数量
                    $("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']").val(order.amount);
                    //触发变动
                    change_event($("#cart-list .quantity-form input.itxt[id*='"+selfItemId+"']")[0]);
                    clue('商品数量修改成功');

                }

           
        }else{
            //主商品未勾选,勾选主商品
            clue('勾选主商品'+selfItemId);
            selfItemCheckbox[0].click();

        }

    }else{
        //没有勾选的商品.全选肯定没勾上
        //勾主商品
        //$("#toggle-checkboxes_down")[0].click();
        clue('勾选主商品'+selfItemId);
        selfItemCheckbox[0].click();
    }

    item_nums += parseInt(order.amount);
}

function checkItemNums(not_exist_item_id){

    if(!check_products_exist && not_exist_item_id){
        clue('有主商品不存在，3S后任务重做');
        setTimeout(function(){
            var condition = {item_id:not_exist_item_id};
            tabCommon.sm(taskVars, '.taskRedo',{taskName:'search',condition:condition})
        },3*1000);
        return false;
    }

    var barItemNum = parseInt($(".cart-filter-bar .switch-cart-item .number").text());

    if(barItemNum == item_nums){
        clue("主商品数量正确,去结算", "success");
        submit();
    }else{
        clue("已选择商品数量和购物车主商品数量不一致,重新勾选主商品");
        if($("#toggle-checkboxes_down:checked").length==0){
            // clue('3s后刷新');
            // setTimeout("location.reload();",3*1000)
            if(barItemNum > item_nums){
                item_nums = 0;
                clue('全选');
                $("#toggle-checkboxes_down")[0].click();
                // checkedNums = 0;
                // setTimeout("location.reload();",3*1000)
            }else{
                item_nums = 0;
                checkGiftStock();
            }
            
            
        }else {
            clue("全不选");
            item_nums = 0;
            $("#toggle-checkboxes_down")[0].click();
        }
    }
}

function submit() {

    if ($('.gift-item:contains("无货")').length > 0) {
        updateHostStatus(1409101);//赠品无货
        return false;
    }


    var submit_btn = $("#cart-floatbar a.submit-btn");
    if (submit_btn.length > 0 && !item_stock_ed) {
        clicking(submit_btn);
    }
}

            //匿名函数结束
        })
    }

}})();