(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

var id_name = null;
var id_card = null;

tabCommon.mutation(observerId,function (mutation) {
    if(mutation.type === 'childList'){
    }
    else if(mutation.type==='attributes'){
    }
});



Task(index);

function index(local){
  var task = localXss.currentTask;
  id_name = task.identity_name;
  id_card = task.identity_card;
  if(id_name && id_card){
    idCardErrorCheck();
  }else{
   clue('身份证:'+id_card+';姓名:'+id_name, 'error')
  }
}

function idCardErrorCheck(){
  var ui_tip = $('.ui-tip');
  if(ui_tip.length > 0){
    if(ui_tip.text().indexOf('身份证号码错误') != -1){
      clue(ui_tip.text());
      changeIdCard(ui_tip.text())
    }else if(ui_tip.text().indexOf('身份证和姓名不相符') != -1){
      clue(ui_tip.text());
      changeIdCard(ui_tip.text())
    }else{
      clue(ui_tip.text(), 'error')
    }
  }else{
    authInfo()
  }
}

function authInfo(){
  var user_name = $('#userName');
  if(user_name.length > 0){
    writing(user_name,id_name,function(){
      lazy(function(){
        var identity = $('#identity');
        if(identity.length > 0){
          writing(identity,id_card,function(){
            lazy(function(){
              submit()
            },3)
          })
        }else{
          clue('找不到填写身份证号框')
        }
      },3)
    })
  }else{
    clue('找不到填写姓名框')
  }
}

function changeIdCard(reason){
  tabCommon.sm(taskVars, '.change_id_card',reason);
}

function submit(){
  var auth_button = $('#auth_button');
  if(auth_button.length > 0){
    clue('提交');
    auth_button[0].click()
  }else{
    clue('找不到确定按钮')
  }
}

addListenerMessage(function (request) {
  console.log(request);
  if (request.act == 'id_card_changed') {
    var identity = request.identity;
    id_card = identity.identity_card ? identity.identity_card : id_card;
    id_name = identity.identity_name ? identity.identity_name : id_name;
    lazy(authInfo,3)
  }
});


            //匿名函数结束
        })
    }

}})();