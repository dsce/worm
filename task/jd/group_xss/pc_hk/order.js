(function () {
    var observerId = tabCommon.observer(document.querySelector('#mainframe'))
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东 提交订单

var new_consignee = false;//新增收货地址
var payment_selected = false;//选择在线支付
var invoice_selected = false;//不开发发票
var jdbean_use = false;//使用京豆
var order_submit_checked_number = false;//提交订单检查商品数量
var order_submit_checked_product = false;//提交订单检查商品
var pay_password_error = false;//用豆，支付密码错误

var pay_fee = 0;
var shipping_fee = 0;
var bean_fee = 0;

var group_orders = localXss.currentTask.orders;

var id_name = null;
var id_card = null;

Task(index);

//页面监控

tabCommon.mutation(observerId,function (mutation) {
        //console.log(mutation.type);
        //console.log(mutation.target);
        //console.log(mutation);
        if (mutation.type === 'childList') {
            if (mutation.target.id == 'checkCodeDiv' && mutation.addedNodes.length > 0) {
                //有验证码
                updateHostStatus(1410007);
            }
            else if (mutation.target.id == 'submit_message' && mutation.addedNodes.length > 0) {
                var submit_message_text = $("#submit_message")[0].innerText;
                if(submit_message_text.indexOf('省份无法购买商品') != -1){
                    clue(submit_message_text, 'error');
                    clue('该地区无法购买商品', 'error');
                    clue('任务自动异常');
                    Run(function(local){
                        updateHostStatus(1406005);
                        // addTaskOrderRemark('当前省份无法购买商品(PC)'+localXss.currentTask.task_order_oid, function(){  });});
                        //使用新方法 订单异常 + 备注
                        // sendMessageToBackground('order_exception','当前省份无法购买商品(PC)'+localXss.currentTask.task_order_oid);
                        set_group_order_exception(msg);
                      
                    });

                }else if(submit_message_text.indexOf('被最高人民法院限制高消费') != -1){
                    clue(submit_message_text);
                    clue('更换身份信息');
                    changeIdCard(submit_message_text);

                }else if(submit_message_text.indexOf('您输入的姓名与身份证号不匹配') != -1){

                    clue(submit_message_text);
                    clue('修改收货人姓名');
                    changeIdCard(submit_message_text);
                    // setTaskWorks({change_card:true},function(){
                    //     $("#consignee-addr ul li[selected='selected'] .op-btns a:contains('编辑')")[0].click();
                    // })

                    lazy(function(){
                        if($(".ui-dialog").length >0){
                            setTimeout(function(){
                                window.location.reload();
                            },10*1000)
                            
                        }else{
                             setTaskWorks({change_card:false},function(){
                                $("#order-submit")[0].click();
                            })
                        }
                       
                    },20)
                    
                }else if(submit_message_text.indexOf('对不起，系统繁忙，请稍候再试') != -1){

                    clue(submit_message_text, 'error');
                    var message = '踢单不异常，异常提示：' + submit_message_text;
                    var order_ids = [];
                    group_orders.forEach(function(order){
                        order_ids.push(order.id);
                    })
                    reportGroupOrderException(message,group_orders[0].id,order_ids);
                    return false;
                }else{
                    clue(submit_message_text, 'error');
                }
            }
            else if(mutation.target.id == 'mainframe'){
                if(mutation.removedNodes.length > 0){
                    console.log(mutation.type);
                    console.log(mutation.target);
                    console.log(mutation);
                    if(mutation.removedNodes[0].className == 'ui-dialog'){
                        window.location.reload(true)
                    }
                }
            }
            else {

            }
        }
        else if (mutation.type === 'attributes') {
            if(mutation.target.className.indexOf('online-payment') != -1){
                console.log(mutation.type);
                console.log(mutation.target);
                console.log(mutation);
            }
            else if(mutation.target.className == 'ui-dialog'){
                console.log(mutation.type);
                console.log(mutation.target);
                console.log(mutation);
            }
        }
});

function set_group_order_exception(submit_message){
    $(".goods-item .p-name a").each(function(i){
        var target_item = $(this).text().replace(/\s+/g,'');
        console.log(target_item);
        if(submit_message.replace(/\s+/g,'').indexOf(target_item) != -1){
            var sub_order_item_id = $(this).attr('href').match(/\d+/)[0];
            console.log(sub_order_item_id);

            group_orders.forEach(function(order){
                if(order.item_id == sub_order_item_id){
                    //异常包下的单
                    reportGroupOrderException(submit_message,order.id);

                    return false;
                }
            })

        }
    })
}

function changeIdCard(reason){
    tabCommon.sm(taskVars, '.change_id_card',reason);
    setTaskWorks({change_card:true},function(){
        $("#consignee-addr ul li[selected='selected'] .op-btns a:contains('编辑')")[0].click();
    })
}

//运行
function index(local) {
    updateHostStep(_host_step.submit_order);//step6 提交订单
    updateHostStatus(1410000);
    tabCommon.sm(taskVars, '.saveUserCookiesToRemote', {domain: 'jd.hk'});//save cookies

    if (taskOrderExist('group_xss')) {
        return false;
    }

    //增加检测地址
    check_address(local,function(){
        //进入提交订单，先检查一遍商品数量和金额，
        order_check(local);
        setTimeout(online_payment, 5000);//选择在线支付
    })
    
}

//选择在线支付
function online_payment() {
    var payment_online = $("#payment-list .payment-item[payid='4']");
    if(payment_online.hasClass('item-selected')){
        //全球购商品暂不支持开发票
        //不使用使用京豆
        //提交订单
        r(order_submit)
    }
    else{
        payment_online.parent()[0].click()
    }
}

//核对订单
function order_check(local) {

    //检查主商品是否存在
    var amount = 0;

    group_orders.forEach(function(order){

        amount += parseInt(order.amount);

    //检查主商品是否存在
    if ($("#shopping-lists a[href*='" + order.item_id +"']").length > 0) {
        order_submit_checked_product = true;
    } else {
        clue('主商品[' + order.item_id + ']不存在', 'error');
        order_submit_checked_product = false;
        // updateHostStep(_host_step.open_promotion_link);//设置步骤
        // clue('3秒后，步骤重置');
        // setTimeout(resetHostByStep,3000);
        clue('3S后任务重置');
        setTimeout(function(){
            // tabCommon.sm('.taskReset',{taskName:'search'})
        },3*1000)
        return false;
    }

    })
    // if ($(".goods-list").html().indexOf(localXss.currentTask.item_id) > 0) {
    //     order_submit_checked_product = true;
    // } else {
    //     clue('主商品[' + localXss.currentTask.item_id + ']不存在', 'error');
    //     order_submit_checked_product = false;
    // }

    var number = $('.order-summary .list span em').text().match(/\d+/);
    if (number != amount) {
        clue('商品数量不一致', 'error');
        cartReture();
        order_submit_checked_number = false;

    } else {
        clue('商品数量正确', 'success');
        order_submit_checked_number = true;
    }

    //金额
    pay_fee = $("#sumPayPriceId").text();//应付款金额
    shipping_fee = $("#freightPriceId").text();//运费
    bean_fee = $("#usedJdBeanId").text().replace('-', '');//京豆优惠金额

    clue("<p>优惠：" + bean_fee + "</p><p>运费：" + shipping_fee + "</p><p>应付总额：" + pay_fee + "</p>");


}

//提交订单
function order_submit(local) {

    //多包裹，
    // if($("#shopping-lists .shopping-list").length > 1){
    //     clue("多包裹，核对主商品，注意多余商品",'error');
    //     updateHostStatus(1410006);//多包裹，核对主商品，注意多余商品
    //     return false;
    // }

    // if($("#shopping-lists .shopping-list .goods-items .goods-item:visible").length > 1){
    //     clue("多商品存在，核对主商品，注意多余商品",'error');
    //     updateHostStatus(1410102);//多包裹，核对主商品，注意多余商品
    //     return false;
    // }

    //检查主商品是否存在
    // if ($("#shopping-lists").html().indexOf(localXss.currentTask.item_id) == -1) {
    //     updateHostStatus(1410101);//主商品不存在
    //     return false;
    // }

    // var number = $('.order-summary .list span em').text().match(/\d+/);
    // if (number != group_amounts) {
    //     updateHostStatus(1410103);//商品数量错误
    //     cartReture();
    //     return false;
    // }

    if (taskOrderExist('group_xss')) {
        return false;
    }

    order_check(local);
    if (order_submit_checked_number && order_submit_checked_product) {

        //运费大于50，不提交
        var yunfee = $("#freightPriceId").text().match(/\d+/);
        if(yunfee && yunfee[0] > 50){
            clue('订单运费大于50，不能提交');
            return false;
        }

        var items = {
            temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
            temp_shipping_fee: shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
        };
        setLocal(items ,function(){
            console.log('提交订单');
            lazy(function () {
                console.log('order submit');
                // changeIdCard('限制高消费');
                setTaskWorks({change_card:false},function(){
                    $("#order-submit")[0].click();
                })
                // $("#order-submit")[0].click();
            }, 10);
        });

    }
}

//返回购物车
function  cartReture(){
    var cart_return_url = $('#cartRetureUrl');
    if(cart_return_url.attr('href').indexOf('cart.jd.hk') != -1){
        lazy(function(){
            cart_return_url[0].click();
        },3)
    }
    else{
        cart_return_url.attr('href',cart_return_url.attr('href').replace('cart.jd.com','cart.jd360.hk'));
        lazy(function(){
            cartReture()
        },3)
    }
}

            //匿名函数结束
        })
    }

}})();