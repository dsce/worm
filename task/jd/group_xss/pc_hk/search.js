(function () {

    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东搜索页面
var item_id;

var task = null;
var compare_count = 2;

windowsTopNotSelf();
Task(index);

function index(local) {
    if (taskOrderExist('group_xss')) {
        return false;
    }

    task = localXss.currentTask;

    //是否有活动链接
    if(localXss.currentTask.active_url){
        window.location.href = localXss.currentTask.active_url;
        return false;
    }

    if(!localXss.currentTask.worldwide){
        window.location.href = 'https://www.jd.com';
        return false;
    }
    
    updateHostStatus(1405000);

    var urlget = urlGet();
    var search_key = decodeURIComponent(urlget.keyword);
    var keyword = task.keyword.replace(/^\s+|\s+$/g,"");

    if(search_key != keyword){
        location.href = "http://search.jd.hk/Search?keyword="+ keyword +"&enc=utf-8"
    }else{
        if($('#plist').length > 0 && $(".nf-content").length == 0){
            // direct_goto_product(task.item_id);
            open_product_J_goodsList(task.item_id);
        }
        else if($(".nf-content").length > 0){
            var ns = $(".nf-content")[0].innerText;
            if(ns.indexOf('没有找到') != -1 && ns.indexOf('抱歉') != -1){
                if(search_key == keyword){
                    //当前搜索次数
                    var now_search_num = task.search_nums?parseInt(task.search_nums):0;
                    if(now_search_num >=3){
                        //标记异常
                        clue('当前搜索次数：' + now_search_num);
                        clue('无搜索结果,人工通知','error');
                        tabCommon.sm(taskVars, '.search_keyword_exception');
                    }else{
                        clue('无搜索结果,3s后重新搜索');
                        //当前次数+1
                        task.search_nums = (now_search_num + 1);
                        setLocal({task:task},function(){
                            setTimeout(function(){
                                location.href = "http://search.jd.hk/Search?keyword="+keyword+"&enc=utf-8&wq="+keyword+"";
                            },3000)
                        });
                        
                    }
                }
            }
        }
    }
}

/**
 * 货比商品
 * @param product_list
 * @param type
 */
function compareProduct(product_list){
    //随机商品sku数组
    var rand_skus = [];
    var j = 0;
    do{
        if(j >= compare_count){
            break;
        }
        var index = parseInt(Math.random() * product_list.length);
        var product = product_list.eq(index);
        var sku = product.attr('skuid') ? product.attr('skuid') : product.attr('data-sku');
        if(!in_array(sku,rand_skus)){
            product.find('.p-img a')[0].click();
            rand_skus.push(sku.replace('J_',''));
            j++;
        }
    }while(1);
    chrome.storage.local.set({rand_search_items: rand_skus}, function () {
        //打开货比之后 等待10秒关闭搜索窗口
        setTimeout(close_this_tab, 30000);

    });
}

//直接跳到商品页
function direct_goto_product(item_id) {

    updateHostStep(_host_step.view_product);//step4 浏览主商品

    if ($("li[sku='" + item_id + "']").length > 0) {
        clicking($("li[sku='" + item_id + "'] .p-img a"));
    }else {

        var index = (Math.random() * 10).toFixed();

        var s = '<li sku="$item_id" done="1" class="">\
<div class="lh-wrap">\
  <div class="p-img">\
    <a id="a_quick_link" target="_blank" href="http://item.jd.hk/$item_id.html" onclick="searchlog(1,$item_id,3,2)" title="">\
      <img width="220" height="220" data-img="1" src="http://img5.2345.com/duoteimg/qqbiaoqing/140822032010/12.jpg" class="err-product">\
    </a>\
    <div shop_id="31837" tpl="1"></div>\
  </div>\
  <div class="p-scroll">\
    <a href="javascript:;" class="p-scroll-btn p-scroll-prev">&lt;</a>\
    <div class="p-scroll-wrap">\
    </div>\
    <a href="javascript:;" class="p-scroll-btn p-scroll-next">&gt;</a>\
  </div>\
  <div class="p-name">\
    <a target="_blank" href="http://item.jd.hk/$item_id.html" onclick="searchlog(1,$item_id,3,1)" title="">' + item_id + ' </a>\
  </div>\
  <div class="p-price">\
    <em></em><strong class="J_$item_id" done="1"></strong>    <span id="p$item_id"></span>\
  </div>\
  <div class="extra">\
    <span class="star"><span class="star-white"><span class="star-yellow h5">&nbsp;</span></span></span>\
    <a id="comment-$item_id" target="_blank" href="http://item.jd.hk/$item_id.html#comments-list" onclick="searchlog(1,$item_id,3,3)"></a>\
    <span class="reputation" style="display:none;"></span>\
  </div>\
  <div class="stocklist">\
    <span class="st33"></span>\
  </div>\
    <div class="hide"><a stock="$item_id" presale="0"></a></div>\
  <div class="product-follow" style="display: none;"><a href="javascript:;" class="btn-coll" id="coll$item_id" onclick="searchlog(1,$item_id,3,5)"></a></div>\
</div>\
</li>';

        s = s.replace(/\$item_id/g, item_id);

        $('#plist').prepend(s);

        $('#a_quick_link')[0].click();

        var product_list = $("#plist li[sku!='" + task.item_id + "']");
        if (product_list.length < 3) {
            //搜索结果小于3
            return false;
        }
        setTimeout(function () {
            compareProduct(product_list)
        },2000)
    }
}

function open_product_J_goodsList(item_id) {

    updateHostStep(_host_step.view_product);//step4 浏览主商品

    if ($("li[data-sku='" + item_id + "']").length > 0) {
        clicking($("li[data-sku='" + item_id + "'] .p-img a"));
    }else {

        var html = '<li data-sku="$item_id" class="gl-item" id="jd_find_item" style="float: inherit;">\
      <div class="gl-i-wrap">\
        <div class="p-img">\
          <a target="_blank" title="" href="http://item.jd.hk/$item_id.html" onclick="searchlog(1,$item_id,2,2)">\
            <img width="220" height="220" data-img="1" data-lazy-img="done" class="err-product" src="http://img5.2345.com/duoteimg/qqbiaoqing/140822032010/12.jpg">\
          </a>\
          <div></div>\
        </div>\
        <div class="p-price"></div>\
        <div class="p-name"></div>\
        <div class="p-commit"></div>\
        <div class="p-operate"></div>\
      </div>\
    </li>\
  ';

        html = html.replace(/\$item_id/g, item_id);
        $("#plist ul").prepend(html);
        setTimeout(function () {

            if ($("#jd_find_item .p-img a").length > 0) {

                $("#jd_find_item .p-img a")[0].click();
                var product_list = $("#plist .gl-item .item[skuid!='J_" + task.item_id + "']");
                if (product_list.length < 3) {
                    //搜索结果小于3
                    return false;
                }
                setTimeout(function () {
                    compareProduct(product_list)
                },2000)
            }

        }, 2000);
    }
}



            //匿名函数结束
        })
    }

}})();