(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

//京东支付页面
var btnSubmitNum = 0;
var btnSubmit = $('#paySubmit');
var bank;

Task(index);

function index(local){
    updateHostStatus(1414000);

    checkReferer();
    checkOrderInfo(local);

    //获取银行信息
    chrome.runtime.sendMessage({act:'get_payment_info'});

    //监听消息
    addListenerMessage(function (request) {
        console.log(request);
        if (request.act == 'get_payment_info_result') {
            if(request.error == 1){
                console.log("没有设置银行用户信息！");
                updateHostStatus(1414001);
                notifyMessage("没有设置银行用户信息！");  
            }else{
                var bank_info = request.data;
                if(bank_info.bank_code && bank_info.card_no){
                    bank = bank_info.bank_code.toLowerCase();
                    setLocal({'bank_info':bank_info},function(){
                        //选择银行付款
                        checkLocalPaySet(local)
                        // select_bank_to_pay(local,bank);
                    })
                    
                }else{
                    notifyMessage("银行用户信息设置不全！");  
                }
            }
            
        }
    });

    //r(checkLocalPaySet);
}

//识别来源页面，提交订单页面，跳转订单列表页面
function checkReferer(){
    var referrer = document.referrer;
    if(referrer.indexOf('cashier.jd.hk/cashier/identity') != -1){
        document.location.href = 'https://order.jd.com/center/list.action';
        return false;
    }
}

function checkOrderInfo(local){
    if(local.order_id === undefined){
        document.location.href = 'https://order.jd.com/center/list.action';
        return false;
    }
}

function checkLocalPaySet(local){
    //var pay = local.pay;
    // if(pay !=''){
    //     if('bank' in pay&&pay.bank=='null'){
    //         clue('手动选择付款');
    //     }else{
    //         var bank = pay.bank;

            if(bank == 'icbc'){
                tabCommon.sm(taskVars, '.icbcChangeUseragentStart');//工商银行替换UserAgent
            }else if(bank == 'ccb'){
                tabCommon.sm(taskVars, '.intercept_ccb_form');//建行截获form数据
            }else if(bank == 'boc' && local.pay.remote_payment_boc !== true){
                tabCommon.sm(taskVars, '.intercept_boc_form');//中行截获form数据
            }

            r(selectBank)
    //     }
    // }else{
    //     console.log("没有设置银行用户信息！");
    //     updateHostStatus(1414001);
    //     notifyMessage("没有设置银行用户信息！");
    // }
}

function selectBank(local){
    //var bank = 'boc';
    //bank = local.pay.bank;
    var upper_bank = bank.toUpperCase();
    var show_bank = $('.ui-item .ui-showbank .img-card-p');
    if(show_bank.length <= 0){
        clue('页面上没有找到银行选择', 'error');
        return false
    }
    if(show_bank.find('.' + upper_bank).length > 0 && show_bank.find('.savingsCard-wy')){
        //选择的银行已经是 设定的银行&网银
        clue('去付款', 'success');
        submit()
    }else{
        //需要选择银行
        var bank_pay = $('.bankPay:visible');
        if(bank_pay.length > 0){
            var bank_pay_span = bank_pay.find('.'+upper_bank);
            if(bank_pay_span.length > 0){
                bank_pay_span[0].click()
            }else{
                clue('无设定银行的网银可选择', 'error')
            }
        }else{
            show_bank[0].click()
        }
    }
}

function submit(){
    var form_submit = $('.J-formSubmit:visible');
    if(form_submit.length > 0){
        clue(form_submit.text());
        form_submit[0].click();
    } else {
        clue('未找到去付款按钮')
    }
}

//点击已完成付款
function pay_finish(){
    var payFinishInterval = setInterval(function(){
        var pay_finish = $('#showDlwsyhfkLayer').find("span:contains('已完成付款')");
        if(pay_finish.length > 0){
            pay_finish[0].click();
            clearInterval(payFinishInterval)
        }
    },500)
}

tabCommon.mutation(observerId,function (mutation) {
        if (mutation.type === 'childList') {
            if(mutation.target.className == 'ui-showbank' && mutation.addedNodes.length > 0){
                console.log(mutation.type);
                console.log(mutation.target);
                console.log(mutation);
                r(selectBank)
            }
            else if(mutation.target.id == 'showDlwsyhfkLayer' && mutation.addedNodes.length > 0){
                console.log(mutation.type);
                console.log(mutation.target);
                console.log(mutation);
                pay_finish()
            }
        }
        else if (mutation.type === 'attributes') {
            if(mutation.target.className.indexOf('ui-bankpay') != -1){
                if(mutation.attributeName.indexOf('style') != -1 && mutation.oldValue){
                    if(mutation.oldValue.indexOf('display') != -1 && mutation.oldValue.indexOf('none') != -1){
                        console.log(mutation.type);
                        console.log(mutation.target);
                        console.log(mutation);
                        r(selectBank)
                    }
                }
            }
        }
});



            //匿名函数结束
        })
    }

}})();