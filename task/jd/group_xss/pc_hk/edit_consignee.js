(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

var id_card_error_check = false;
var id_card = '411323198905021563';

var changing = false;

tabCommon.mutation(observerId,function (mutation) {

    if(mutation.type === 'childList'){
      if(mutation.target.id == 'idCard_div_error'){
        if(mutation.addedNodes.length > 0){
          if(mutation.target.innerText.indexOf('请您填写身份证号码') != -1){
            console.log(mutation.target.innerText);
            console.log(mutation);
            setIdCard()
          }
        }
      }
    }
    else if(mutation.type==='attributes'){
      if(mutation.target.id == 'idCard_div_error'){
        if(mutation.target.innerText.indexOf('身份证号码格式不正确') != -1){
          if(id_card_error_check == true && changing == false){
            changing = true;
            console.log(mutation.target.innerText);
            console.log(mutation);
            clue(mutation.target.innerText);
            changeIdCard(mutation.target.innerText)
          }
        }
      }
    }
});

Task(function(local){
  getSharedTaskWorks(function(sTask){
    
    getTaskWorks(function(tworks){

      var task = sTask.account_info;
      id_card = task.identity_card;
      id_name = task.identity_name;
      if(tworks.change_card){
        setIdCard();
      }else{
        submit()
      }
    })
      
      // submit()
      
  })
  
});

function submit(){
  var save_consignee = $('#saveConsigneeTitleDiv');
  if(save_consignee.length > 0){
    save_consignee[0].click()
    changing = false;
  }else{
    clue('未找到提交收货人信息')
  }
}

function setIdCard(){
    if(!id_card || !id_name){
        changeIdCard('省份证信息或姓名不能为空');
        return false;
    }
  writing($("#consignee_name"),id_name,function(){
    //consignee_name
    writing($("#consignee_idCard"),id_card,function(){
        lazy(function(){
          id_card_error_check = true;
          submit()
        },3)

     })
  })
}

function changeIdCard(reason){
  tabCommon.sm(taskVars, '.change_id_card',reason);
}

function idCardChanged(){
  id_card_error_check = false;
  $("#consignee_idCard").val(null);
  setIdCard()
}

addListenerMessage(function (request) {
  console.log(request);
  if (request.act == 'id_card_changed') {
    var identity = request.identity;
    id_card = identity.identity_card ? identity.identity_card : id_card;
    id_name = identity.identity_name ? identity.identity_name : id_name;
    idCardChanged()
  }
});

            //匿名函数结束
        })
    }

}})();