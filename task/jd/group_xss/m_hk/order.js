(function () {
    var observerId = tabCommon.observer($('body')[0])
    tabCommon.start('group_xss', taskStartRun)

    function taskStartRun(localXss) {
    var taskVars=localXss.taskVars;
    run(localXss);
    function run(localXss) {
        lazy(function () {
            //匿名函数开始

var pay_fee = 0;
var shipping_fee = 0;
var identity_id_inputted = false;
var identity_btn_clicked = false;

var group_orders = localXss.currentTask.orders;

tabCommon.mutation(observerId,function (mutation) {
        // console.log(mutation.type, mutation.target, mutation);
        if(mutation.type === 'attributes'){
            if(mutation.target.id == 'inputPwdWindow'&&mutation.attributeName == 'style'&&mutation.oldValue!=null&&mutation.oldValue == "display: none;"){
                //使用京豆
            }
        }else if(mutation.type == "childList"){
            if(!identity_id_inputted && mutation.target.className.indexOf("mjdToastContainer") != -1 && mutation.addedNodes.length > 0 && mutation.addedNodes[0].innerText.indexOf("保存收货人的身份证信息")!=-1){
                //需要填身份证信息+保存身份证信息
                Run(function(local){
                    clue("输入身份信息");
                    identity_id_inputted = true;
                    $("#identityId").val(localXss.currentTask.identity_card);
                    $("#identity-btn").addClass("active");
                    $("#identity-btn")[0].click();
                });
            }else if(!identity_btn_clicked && mutation.target.id == "identity-num" && mutation.addedNodes.length>0){
                //身份证信息已增加,提交订单
                Run(function(){
                    clue("允许通过海关", "success");
                    identity_btn_clicked = true;
                    submiOrder();
                });
            }
        }else if(mutation.target.id == 'message-prompt'){
            // clue($("#message-prompt").text());
            // var msg = $("#message-prompt").text();
            // if(msg.indexOf('您输入的姓名与身份证号不匹配') != -1){
            //     clue('更换身份证信息');
            //     changeIdCard(msg);
            // }
        }
});

// function changeIdCard(reason){
//   sendMessageToBackground('change_id_card',reason);
// }

// addListenerMessage(function (request) {
//   console.log(request);
//   if (request.act == 'id_card_changed') {
//     var identity = request.identity;
//     id_card = identity.identity_card ? identity.identity_card : id_card;
//     id_name = identity.identity_name ? identity.identity_name : id_name;
//     lazy(authInfo,3)
//   }
// });

Task(index);

function index(local){
    var task = localXss.currentTask;
    var referrer = document.referrer;

    updateHostStep(_host_step.submit_order);//step6 提交订单
    updateHostStatus(1410000);//提交订单


    if(taskOrderExist('group_xss')){return false;}

    //核对是否有手机号
    var sPhoneText = $(".step1 .mt_new .s1-phone");
    if(sPhoneText.length > 0){
        if(!checkAddress(local)){
            // clue("手机号不正确,需编辑确认");
            $(".step1 .step1-in a")[0].click();

        }else{
            //收货人信息
            submitBegin()

        }
    }else if($(".step1 .add-address a").length >0){

        $(".step1 .add-address a")[0].click();
    }else{
        //重新开始
        // tabCommon.sm(taskVars, '.order_operate','client_host_status_reset')

        submitBegin()
    }
    
}

function submitBegin(){

    //检查赠品无货情况
    var stockDiv = $("#stockDiv");
    if(stockDiv.length > 0){
        var message = stockDiv[0].innerText.toString();
        updateHostStatus(1410208);
        reportProductStockout("无货："+message);

        clue("暂时无货："+message, 'error');
        return false;
    }

    //京东手机端不使用京豆
    var use_bean = $("#useJdbean .switched");
    if(use_bean.length > 0){
        use_bean[0].click();
    }

    //提交订单核对商品数量
    var num = 0;
    if($(".step3-more:visible").length > 0){
        num = $(".step3-more .s-item .sitem-r")[0].innerText.match(/\d+/)[0];
    }else{
        var num_p = $(".step3 .sitem-m .sitem-m-txt").next('p');
        if(num_p.length > 0){
            num = num_p[0].innerText.match(/\d+/)[0];
        }else{
            var sam_sum = $(".sitem-btm-detail .sam-num");
            if(sam_sum.length > 0){
                num = sam_sum[0].innerText.match(/\d+/)[0];
            }
        }
    }

    var group_amount = 0;

    group_orders.forEach(function(order){
        group_amount += parseInt(order.amount);
    })


    //订单金额
    pay_fee = $("#payMoney").text().replace(/\s+/g,'');
    shipping_fee = $("#yunfeeList .s-item div:contains('运费')").next('div.sitem-r').text().match(/\S+/g)[0];
    clue('应付款金额：'+pay_fee);

    useLocal(function(local){
        var task = localXss.currentTask;
        if(group_amount != num){
            updateHostStatus(1409203);//商品数量不对
            clue('商品数量：'+num+' 件，检查购物车商品', 'error');
            // $("#m_common_header_shortcut_p_cart a")[0].click();
            location.href = 'https://p.m.jd.com/cart/cart.action';
        }else{
            clue('商品数量：'+num+' 件', 'success');
            lazy(submiOrder);
        }
    });

}

//验证收货地址
function checkAddress(local){
    var address = jdMunicipality(local.task.consignee);
    if($(".s1-name").length >0){
        var name = $(".s1-name").text();
    }else{
        var name = $("#address-name").text();
    }
    
    if($(".s1-phone").length >0){
        var mobile = $(".s1-phone").text().match(/\d+/g);
    }else{
        var mobile = $("#address-mobile").text().match(/\d+/g);
    }
    
    if($(".step1-in-con").length >0){
        var full_address = $(".step1-in-con").text().replace(/\s+/g,'');
    }else{
        var full_address = $("#address-where").text().replace(/\s+/g,'');
    }
    

    var its_name = name.replace(/\s+/g,'') == address.name;
    var address_where_area = address.province+(address.city?address.city:'')+(address.area?address.area:'')+(address.street?address.street:'') + address.short_address;

    var its_full_address = full_address.replace(/省|市|\s|#|°/g,'') == address_where_area.replace(/省|市|\s|#|°/g,'');

    var its_mobile = address.mobile.indexOf(mobile[0])==0&&address.mobile.substr(-4).indexOf(mobile[1])==0;
    if(its_name&&its_full_address&&its_mobile){
        clue('收货地址一致，继续');
        return true;
    }else{
        clue('收货地址不一致,修改');
        return false;
    }
}
//提交订单
function submiOrder(){
    clue('提交订单');

    var yunfee = $("#yunfeeList .s-item:contains('运费')").find('.sitem-r').text().match(/\d+/);
    if(yunfee && yunfee[0] > 50){
        clue('订单运费大于50， 不能提交');
        return false;
    }

    var items = {
        temp_pay_fee: pay_fee.match(/\d+(=?.\d{0,2})/g)[0],
        temp_shipping_fee: shipping_fee.match(/\d+(=?.\d{0,2})/g)[0]
    }
    setLocal(items, function(){
        if($("#submiOrder").length > 0){
            $("#submiOrder")[0].click();
        }else{
            $("#orderForm a:contains(提交订单)")[0].click();
        }
    });

}


            //匿名函数结束
        })
    }

}})();