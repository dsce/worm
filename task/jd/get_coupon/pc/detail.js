(function () {

    tabCommon.start('getCoupon', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('开始查找优惠券');
        lazy(function () {
            if (tabCommon.findStrByHref(local.currentTask.product_id)) {
                $('#summary-quan').find('a')[0].click()
                tabCommon.waitElementReady(function () {
                    return $('#summary-quan').find('a');
                }).then(function () {
                    setTaskWorks({shopName:$.trim($("div.name").find('a').text())},function () {
                        $('#summary-quan').find('a')[0].click();
                    })
                }, function () {
                    clue('找不到优惠券');
                    tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'找不到优惠券'})
                })
            }else{
                clue('不是目标商品页面 忽略');
                tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'不是目标商品页'})
                // label('不是目标商品页面',3,function () {
                //     location.href = 'https://item.jd.com/'+local.currentTask.product_id+'.html';
                // });
            }
        })
    }

}})();