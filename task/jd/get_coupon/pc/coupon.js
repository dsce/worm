(function () {

    tabCommon.start('getCoupon', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('开始领取优惠券');
        lazy(function () {
            $.trim($("div.name").text())
            getTaskWorks(function (taskWorks) {
                if(taskWorks.shopName){
                    var _flag = false;
                    for (var i=0;i<$('.coupon-info').length;i++){
                        var _this = $('.coupon-info').eq(i);
                        if(_flag==false && $('.coupon-info').attr('title').indexOf(taskWorks.shopName)>-1){
                            _flag = true;
                            if(_this.parent().find("a:contains('立即领取')").length){
                                _this.parent().find("a:contains('立即领取')")[0].click();
                            }else{
                                tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'没有可以领取的优惠券了'})
                                return false;
                            }
                            tabCommon.waitElementReady(function () {
                                return $('b:contains("领取成功")');
                            }).then(function () {
                                tabCommon.sm(taskVars, '.report.clank.success')
                            },function () {
                                clue('没有收到领取成功的信息 检测是不是出现了验证码');
                                lazy(function () {
                                    if($('.authcode-content').length){
                                        clue('出现了验证码');
                                        tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'遇到了验证码'})
                                        /**
                                        tabCommon.dama(function () {
                                            return tabCommon.changeURLPar($('#code_1').attr('src'),'_t',new Date().getTime());
                                        },function () {
                                            return $('#answer_1');
                                        },function () {
                                            $('.btn-get-withcode')[0].click();
                                            tabCommon.waitElementReady(function () {
                                                return $('b:contains("领取成功")');
                                            }).then(function () {
                                                tabCommon.sm(taskVars, '.report.clank.success')
                                            },function () {
                                                clue('领取失败 刷新重试')
                                                top.location.reload();
                                            })
                                        })
                                         **/
                                    }else{
                                        tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'没有检测到领取成功的标识'})
                                    }
                                })
                            })
                        }
                    }
                    if(_flag == false){
                        clue('没有当前店铺专属的优惠券')
                        tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'没有当前店铺专属的优惠券'})
                    }
                }else{
                    clue('找不到店铺名称 任务失败')
                    tabCommon.sm(taskVars, '.report.clank.error',{status:4,finished_result:'找不到店铺名称'})
                }
            })
        })
    }

}})();