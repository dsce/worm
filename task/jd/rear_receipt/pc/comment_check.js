(function () {

    tabCommon.start('rearReceipt', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('点开我的订单');
        lazy(function () {
            checkCommentted();
            //检测JD订单评价状态
            function checkCommentted() {
                var comm_product_length = 0;
                comm_product_length = $(".comt-plists .comt-plist").length;
                if (comm_product_length > 0) {
                    // var btns = $(".comt-plists comt-plist .op-btns a");
                    $(".comt-plists .comt-plist .op-btns a").each(function (i) {
                        if ($(this).text() != '点击评价') {
                            //记录评价数据
                            var data = {
                                guid: $(this).attr('guid'),
                                business_oid: $(this).parents(".pro-info").attr('oid'),
                                sku_code: $(this).attr('alt')
                            }

                            tabCommon.sm(taskVars, 'saveCommentedState',data,function () {
                                if (comm_product_length - 1 == i) {
                                    clue('检测评价结束');
                                    tabCommon.sm(taskVars, '.closeThisTab');
                                }
                            });
                        } else {
                            if (comm_product_length - 1 == i) {
                                clue('检测评价结束');
                                tabCommon.sm(taskVars, '.closeThisTab');
                            }
                        }
                    })
                } else {
                    clue('暂无商品评价');
                    tabCommon.sm(taskVars, '.closeThisTab');
                }
            }
        })
    }

}})();