(function () {

    tabCommon.start('rearReceipt', taskStartRun)

    function taskStartRun(local) {
    var taskVars=local.taskVars;
    run(local);
    function run(local) {
        label('点开我的订单');
        lazy(function () {
            tabPc.myOrderListClick()
            setTimeout(function () {
                tabCommon.sm(taskVars, '.closeThisTab');
            },10e3)
        })
    }

}})();