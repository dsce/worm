(function () {

    var bodyObserver = tabCommon.observer(document.body);
    tabCommon.start('rearReceipt', taskStartRun)

    function taskStartRun(localTask) {
    var taskVars=localTask.taskVars;
    run(localTask);
    function run(localTask) {
        label('收货 寻找目标订单');
        //其他任务
        //寻找取消的订单保存
        cancel_order_discover();

        //检查历史订单评价guid 暂时不需要
        getTaskWorks(function (taskWorks) {
            if(!taskWorks.checked_comment){
                // getHistoryOrderCommentGuids();
            }
        })

        //主任务 检查订单状态
        var task = localTask.currentTask;
        getTaskWorks(function (taskWorks) {
            clue('任务已重试次数:'+task.client_try_times||0);
            task.business_oid = taskWorks.business_oid;
            tabPc.rear.findOrder(task.business_oid,task.item_id,function () {
                clue('检测需要完成的动作');
                checkOrderReceived();
            },taskVars);
        })

//检查收获状态
        function checkOrderReceived(){
            console.log("订单收货");
            startMutation();
            var $btn_received = $('#track' + task.business_oid).find('a').filter(':contains("确认收货")');
            if($btn_received.length > 0){
                console.log("确认收货");
                clue("确认收货");
                label('准备点击收货按钮');
                var $onclick_str =  $btn_received.attr('onclick');
                var class_str =  $btn_received.attr('class');
                console.log("判断是否为弹出的收货确认");
                if($onclick_str!=undefined && $onclick_str.indexOf('showConfirmGoods') != -1){
                    $btn_received[0].click();
                    console.log("页面有收货弹出框，页面变化");
                }else if(class_str.indexOf('order-confirm') != -1){
                    $btn_received[0].click();
                    console.log("721新版,页面有收货弹出框，页面变化");
                }else{
                    console.log("无确认收货，往下进行评价操作");
                    // lazy(function () {
                    //     checkOrderCommented();
                    // })
                }
            }else{
                var order_status = $('#track' + task.business_oid).find('span:contains("出库")');
                if(order_status.length > 0){
                    clue("正在出库");
                    console.log("未发现确认收货，订单正在出库");
                    tabCommon.sm(taskVars, 'reportFail',{message:"订单正在出库",delay:3600*24});
                }else{
                    var _orderStatus = $('#track' + task.business_oid).find('span.order-status').text().trim();
                    if(_orderStatus == '已完成'){
                        clue("已收货");
                        tabCommon.sm(taskVars, 'reportSuccess');
                    }else if(_orderStatus == '等待收货'){
                        clue("等待签收");
                        tabCommon.sm(taskVars, 'reportFail',{message:"订单正在等待签收",delay:3600*24});
                    }else if(_orderStatus == '请上门自提'){
                        clue("错误: 请上门自提");
                        tabCommon.sm(taskVars, 'reportFail',{message:"请上门自提"});
                    }else{
                        clue('未知状态 延期');
                        clue('收货:'+_orderStatus);
                        tabCommon.sm(taskVars, 'reportFail',{message:_orderStatus,delay:3600*24});
                    }
                    // console.log("未发现确认收货，往下进行评价操作");
                    //
                    // wangwang();
                    // checkOrderCommented();
                }
            }
        }

        function confirmReceived(){
            var thickconfirm = $(".thickbox").find('a:contains("确认")');
            var dialog_confirm = $(".ui-dialog").find('a:contains("确认")');
            if(thickconfirm.length > 0 || dialog_confirm.length > 0){
                //thickconfirm[0].click();
                confirmDeliver();
            }
        }

        function confirmDeliver(){
            var url = "http://odo.jd.com/oc/toolbar_confirmDeliver?action=confirmDeliver&orderid="+ task.business_oid;
            var url = "//orderop.jd.com/oc/toolbar_confirmDeliver?action=confirmDeliver&orderid="+ task.business_oid;

            console.log(url);
            ajaxSend();
            function ajaxSend() {
                $.ajax({
                    type:"GET",
                    url:url,
                    data:"",
                    dataType:"json",
                    timeout: 6e3,
                    success: function(e) {
                        console.log(e);
                        setTimeout(function(){
                            tabCommon.sm(taskVars, 'reportSuccess');
                        },3000);
                    },
                    error: function (e) {
                        console.log('error');
                        console.log(e);
                        var message = e.responseText ? e.responseText : null;
                        //responseText: "{html:"抱歉! 订单的状态不能执行该业务"}";
                        clue(message);
                        if(message){
                            if(message.indexOf('订单的状态不能执行该业务') != -1){
                                setTimeout(function(){
                                    tabCommon.sm(taskVars, 'reportFail',{message:message});
                                },3000);
                            }else if(message.indexOf('抱歉！根据订单状态不允许操作') != -1){
                                setTimeout(function(){
                                    tabCommon.sm(taskVars, 'reportFail',{message:message});
                                },3000);
                            }else if(message.indexOf('确认收货成功') != -1){
                                tabCommon.sm(taskVars, 'reportSuccess');
                            }else{
                                setTimeout(function(){
                                    ajaxSend();
                                },3000);
                            }
                        }else{
                            setTimeout(function(){
                                ajaxSend();
                            },3000);
                        }
                    }
                })
            }
        }

        function startMutation() {
            tabCommon.mutation(bodyObserver,function (mutation) {
                //console.log(mutation.type);
                //console.log(mutation);
                if(mutation.target.className === "thickbox"){
                    console.log(mutation.type);
                    console.log(mutation);
                }
                if(mutation.type === 'childList'){
                    console.log(mutation.type);
                    console.log(mutation.target.className);
                    console.log(mutation);
                    //if(mutation.target.className === "thickwrap"){
                    if(mutation.target.className === "thickwrap" || mutation.target.className === "ui-dialog-content"){
                        console.log(mutation.type);
                        console.log(mutation);
                        console.log("出现弹窗");


                        var thicktitle = $(".thickwrap").find('.thicktitle:contains("确认收货")');
                        var dialog_title = $(".ui-dialog").find('.ui-dialog-title:contains("确认收货")');
                        if(thicktitle.length > 0 || dialog_title.length > 0){
                            console.log("出现确认收货");
                            var addNodes = mutation.addedNodes;
                            if(addNodes.length == 1 ){
                                var node = addNodes[0];
                                console.log('node');
                                console.log(node);
                                //if(node.textContent == "确认收货"){
                                if(node.textContent.indexOf("请确认是否已收到货") != -1 || node.textContent == "确认收货"){
                                    console.log("确认收货 start");
                                    setTimeout(function () {
                                        wangwang();
                                        confirmReceived();
                                    }, 3000);
                                }
                            }
                        }
                        var thickbox = $(".thickbox").find('a:contains("立即评价")');
                        var dialog_comment = $(".ui-dialog").find('a:contains("立即评价")');
                        if(thickbox.length > 0 || dialog_comment.length > 0){
                            console.log("出现立即评价,去评价页");
                            setTimeout(function(){
                                wangwang();
                                //goComment($(".thickbox").find('a:contains("立即评价")'));
                                //window.location.reload(true);
                            },3000);
                        }
                    }
                }
            });
        }

        /**
         * 获取取消订单信息进行保存
         * @param callback
         */
        function cancel_order_discover(){

            var source_name = "千手多开";
            var source_name = chrome.runtime.getManifest().name;

            var businessOrders = [];
            $("#main .order-tb tbody[id*='tb-']").each(function(){
                var tb = $(this);
                var shopName = tb.find(".order-shop .shop-txt").text();
                if(shopName != '京东' && shopName != '流量充值'){
                    var order = {};
                    // var st = tb.find(".order-status")[0].innerText;
                    var st = tb.find(".status span")[0].innerText;
                    //订单状态异常需要保存
                    order.business_order_status = st;
                    order.business_oid = tb.find("a[id*='idUrl']").text();
                    var _temp = tb.find(".amount").text().match(/\d+(=?.\d{0,2})/g);
                    order.business_total_fee = _temp?_temp[0]:'0.00';
                    order.business_account_id = localTask.accountId;
                    console.log(order);
                    businessOrders.push(order);
                }
                //api.saveCancelledOrderToRemote(order);

            });

            //businessOrders;
            // api.saveBusinessOrderToRemote(businessOrders);
            tabCommon.sm(taskVars, 'reportBusinessOrderStatus',businessOrders);

        }

    }

}})();