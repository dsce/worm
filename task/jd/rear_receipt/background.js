M.rearReceipt = {

    reportSuccess: function (request, sender) {
        getTaskWorks(function (task) {
            var order_id = task.order_id;
            var message = request.message ? request.message : "收货成功";
            var delay = request.delay ? request.delay : 0;
            var cmd = 'receipt_success';
            M.global.report.rear.success({
                cmd: cmd,
                order_id: task.order_id,
                delay: delay,
                message: message
            }, sender)
        })
    },

    reportFail: function (request, sender) {
        getTaskWorks(function (task) {
            var order_id = task.order_id;
            var message = request.message ? request.message : "";
            var delay = request.delay ? request.delay : 0;
            var cmd = 'receipt_error';
            M.global.report.rear.error({
                cmd: cmd,
                order_id: task.order_id,
                delay: delay,
                message: message
            }, sender)
        })
    },

    reportBusinessOrderStatus:function (businessOrders) {
        var api = new Api();
        console.warn('取消的订单',businessOrders);
        api.saveBusinessOrderToRemote(businessOrders)
    },

    saveCommentedState: function (request, sender) {
        var data = {};
        data.guid = request.guid;
        data.business_oid = request.business_oid;
        data.sku_code = request.sku_code;
        data.state = request.state;
        var _api = new Api();
        _api.saveCommentState(data);
    },

    accountFail: function (remark, sender) {
        //汇报帐号异常 任务完成
        notify('帐号异常 任务汇报')
        getTaskWorks(function (task) {
            M.global.report.rear.error({cmd: 'receipt_error', order_id: task.order_id,delay: 0,message: remark}, sender)
        })
    }

}